const {check, body, query} = require('express-validator');
const {DAY_DATE_REGEX, TIME_REGEX, CHAR_13_ID_REGEX} = require("./regex");

const stringNotNullOrEmpty = field => check(field)
    .exists()
    .withMessage(`${field} must be provided`)
    .notEmpty()
    .withMessage(`${field} must not be empty`);

const queryDayDate = fieldName => query(fieldName)
    .matches(DAY_DATE_REGEX);

const queryDayDateOptional = fieldName => query(fieldName)
    .optional()
    .matches(DAY_DATE_REGEX);

const bodyDayDate = fieldName => body(fieldName)
    .exists()
    .matches(DAY_DATE_REGEX);

const bodyTime = fieldName => body(fieldName)
    .exists()
    .matches(TIME_REGEX);

const bodyIntId = fieldName => body(fieldName)
    .exists()
    .isNumeric();

const bodyChar13Id = fieldName => body(fieldName)
    .exists()
    .matches(CHAR_13_ID_REGEX);

module.exports = {
    stringNotNullOrEmpty,
    queryDayDate,
    queryDayDateOptional,
    bodyDayDate,
    bodyTime,
    bodyIntId,
    bodyChar13Id,
};
