const {SchedulePeriodTypeEnum} = require("./enums");

const splitInto45MinPeriods = (timeFrom, timeTo) => {
    const today = new Date();
    const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    const periodTimeFromD = new Date(`${date} ${timeFrom}`);
    const periodTimeToD = new Date(`${date} ${timeFrom}`);
    periodTimeToD.setMinutes(periodTimeToD.getMinutes() + 45);

    const initTimeToD = new Date(`${date} ${timeTo}`);

    const splittedList = [];

    while (periodTimeToD <= initTimeToD) {
        splittedList.push({
            type: SchedulePeriodTypeEnum.FREE,
            beginTime: periodTimeFromD.toTimeString().substring(0, 5),
            endTime: periodTimeToD.toTimeString().substring(0, 5),
        });
        periodTimeFromD.setMinutes(periodTimeFromD.getMinutes() + 45);
        periodTimeToD.setMinutes(periodTimeToD.getMinutes() + 45);
    }

    return splittedList;
};

module.exports = {
    splitInto45MinPeriods,
};
