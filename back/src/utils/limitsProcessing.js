const {LIMIT_DEFAULT, PAGE_DEFAULT} = require("./constants").pagination;

const normalize_limit = (limit) => {
    return limit === undefined ? LIMIT_DEFAULT : limit;
};

const normalize_page = (page) => {
    return page === undefined || page < 1 ? PAGE_DEFAULT : page;
};

module.exports = {
    normalize_limit,
    normalize_page
};