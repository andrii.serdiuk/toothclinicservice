const express = require('express');
const {appointmentController} = require("../controllers");
const {createAppointmentValidation} = require("../middleware/validationMiddleware/appointment");
const {authMiddleware} = require("../middleware");
const {RoleEnum} = require("../utils");
const router = express.Router();

router.get('/procedures/:category', appointmentController.getAllProceduresForTheSpecifiedCategory);
router.get('/procedure/:procedureId', appointmentController.getProcedureData);
router.get('/equipmentAndDevicesForProcedure/:procedureId', appointmentController.getEquipmentAndDevicesForProcedure);
router.get('/performedProcedure/:performedProcedureId', appointmentController.getPerformedProcedure);
router.get('/:appointmentId', authMiddleware(RoleEnum.REGISTRANT, RoleEnum.DIRECTOR, RoleEnum.DOCTOR, RoleEnum.PATIENT), appointmentController.getOneById);
router.get('/', authMiddleware(RoleEnum.DIRECTOR, RoleEnum.REGISTRANT), appointmentController.getAppointments);


router.post('/', authMiddleware(RoleEnum.DOCTOR, RoleEnum.REGISTRANT, RoleEnum.DIRECTOR),  ...createAppointmentValidation, appointmentController.createAppointment);
router.post('/createPerformedProcedure', appointmentController.createPerformedProcedure);
router.post('/createFinishedAppointment', appointmentController.createFinishedAppointment);

router.put('/cancel/:appointmentId', appointmentController.cancelAppointment);
router.put('/info/:appointmentId', appointmentController.updateAppointmentInfo);
router.put('/:performedProcedureId', appointmentController.updatePerformedProcedure);

router.delete('/performedProcedure/delete/:performedProcedureId', appointmentController.deletePerformedProcedure);

module.exports = router;
