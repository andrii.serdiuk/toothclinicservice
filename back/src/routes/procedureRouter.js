const express = require('express');
const {authMiddleware} = require("../middleware");
const {RoleEnum} = require("../utils");
const {procedureController} = require("../controllers");

const router = express.Router();

router.get('/', authMiddleware(RoleEnum.DIRECTOR, RoleEnum.REGISTRANT, RoleEnum.PATIENT, RoleEnum.DOCTOR), procedureController.getProceduresByPage);
router.get('/:id', authMiddleware(RoleEnum.DIRECTOR, RoleEnum.REGISTRANT, RoleEnum.PATIENT, RoleEnum.DOCTOR), procedureController.getProcedure);
router.get('/:id/equipment', authMiddleware(RoleEnum.DIRECTOR, RoleEnum.REGISTRANT, RoleEnum.PATIENT, RoleEnum.DOCTOR), procedureController.getEquipmentForProcedure);

router.post('/', authMiddleware(RoleEnum.DIRECTOR,), procedureController.addProcedure);

router.put('/:id', authMiddleware(RoleEnum.DIRECTOR,), procedureController.updateProcedure);
router.delete('/:id', authMiddleware(RoleEnum.DIRECTOR,), procedureController.deleteProcedure);

module.exports = router;