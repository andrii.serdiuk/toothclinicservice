const express = require('express');
const router = express.Router();

const { deviceController } = require("../controllers");
const { authMiddleware } = require("../middleware");
const { RoleEnum } = require("../utils/");

router.get('/:id', authMiddleware(RoleEnum.DIRECTOR, RoleEnum.DOCTOR), deviceController.getDevice);
router.post('/', authMiddleware(RoleEnum.DIRECTOR), deviceController.addDeviceByEquipmentId);
router.put('/', authMiddleware(RoleEnum.DIRECTOR), deviceController.updateDevice);
router.delete('/:deviceId', authMiddleware(RoleEnum.DIRECTOR), deviceController.deleteDevice);
module.exports = router;