const express = require('express');
const router = express.Router();

const { equipmentController } = require("../controllers");
const { authMiddleware } = require("../middleware");
const { RoleEnum } = require("../utils/");
const { addEquipment } = require("../middleware/validationMiddleware");


router.get('/', authMiddleware(RoleEnum.DIRECTOR, RoleEnum.DOCTOR), equipmentController.getEquipmentPage);
router.get('/all', authMiddleware(RoleEnum.DIRECTOR, RoleEnum.DOCTOR), equipmentController.getAllEquipment);
router.post('/', authMiddleware(RoleEnum.DIRECTOR), ...addEquipment, equipmentController.addEquipment);
router.get('/:id/devices', authMiddleware(RoleEnum.DIRECTOR, RoleEnum.DOCTOR), equipmentController.getDevicePage);
router.get('/:id/allDevices', authMiddleware(RoleEnum.DIRECTOR, RoleEnum.DOCTOR), equipmentController.getAllEquipmentDevices);

router.put('/:id', authMiddleware(RoleEnum.DIRECTOR), equipmentController.updateEquipment);

router.delete('/:id', authMiddleware(RoleEnum.DIRECTOR), equipmentController.deleteEquipment);

module.exports = router;