const express = require('express');
const router = express.Router();

const { userController } = require('../controllers');
const {authMiddleware} = require("../middleware");
const { RoleEnum } = require('../utils');

router.post('/login', userController.login);
router.get('/my-patients', authMiddleware(RoleEnum.DOCTOR), userController.get_my_patients_by_page);
router.get('/my-appointments', authMiddleware(RoleEnum.DOCTOR, RoleEnum.PATIENT), userController.get_my_appointments_by_page);
router.get('/:accountId', authMiddleware(RoleEnum.DOCTOR, RoleEnum.PATIENT, RoleEnum.DIRECTOR, RoleEnum.REGISTRANT), userController.getAccountInformation);
module.exports = router;
