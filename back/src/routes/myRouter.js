const express = require('express');
const { myController, scheduleController} = require("../controllers");
const { authMiddleware } = require("../middleware");
const { RoleEnum } = require("../utils/");
const {createMyAppointmentValidation, scheduleValidation} = require("../middleware/validationMiddleware");
const router = express.Router();

router.get('/my-appointments', authMiddleware(RoleEnum.DOCTOR, RoleEnum.PATIENT), myController.myAppointments);
router.post('/my-appointments', authMiddleware(RoleEnum.DOCTOR, RoleEnum.PATIENT), ...createMyAppointmentValidation, myController.createMyAppointment);

router.get('/my-schedule', authMiddleware(RoleEnum.DOCTOR),  ...scheduleValidation, scheduleController.getMySchedule)

module.exports = router;
