const express = require('express');
const {authMiddleware} = require("../middleware");
const {RoleEnum} = require("../utils");
const {receptionistController} = require("../controllers");
const router = express.Router();

router.get('/', authMiddleware(RoleEnum.DIRECTOR), receptionistController.get_receptionists_by_page);
router.post('/', authMiddleware(RoleEnum.DIRECTOR), receptionistController.add_receptionist);

router.put('/:id', authMiddleware(RoleEnum.DIRECTOR), receptionistController.edit_receptionist);
router.delete('/:id', authMiddleware(RoleEnum.DIRECTOR), receptionistController.delete_receptionist);


module.exports = router;