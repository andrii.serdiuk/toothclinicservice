const express = require('express');
const router = express.Router();

const { doctorController } = require('../controllers');
const {authMiddleware} = require("../middleware");
const {RoleEnum} = require("../utils");
const {scheduleValidation} = require("../middleware/validationMiddleware");
const { addDoctor } = require("../middleware").validation;

//Get requests
router.get('/', authMiddleware(RoleEnum.DOCTOR, RoleEnum.PATIENT, RoleEnum.REGISTRANT, RoleEnum.DIRECTOR), doctorController.show_doctors);
router.get('/all', authMiddleware(RoleEnum.DOCTOR, RoleEnum.PATIENT, RoleEnum.REGISTRANT, RoleEnum.DIRECTOR), doctorController.get_all_doctors);
router.get('/account/:id', authMiddleware(RoleEnum.DOCTOR), doctorController.getDoctorByAccountId);
router.get('/test', authMiddleware(RoleEnum.DIRECTOR, RoleEnum.REGISTRANT), doctorController.test);
router.get('/:id/patients', authMiddleware(RoleEnum.DOCTOR, RoleEnum.REGISTRANT, RoleEnum.DIRECTOR), doctorController.get_doctor_patients);
router.get('/:doctorId/schedule', authMiddleware(RoleEnum.DOCTOR, RoleEnum.PATIENT, RoleEnum.REGISTRANT, RoleEnum.DIRECTOR), ...scheduleValidation, doctorController.getSchedule);
router.get('/:id', authMiddleware(RoleEnum.DOCTOR, RoleEnum.PATIENT, RoleEnum.REGISTRANT, RoleEnum.DIRECTOR), doctorController.show_doctor);


//Post requests
router.post('/', authMiddleware(RoleEnum.DIRECTOR), ...addDoctor, doctorController.add_doctor);
router.post('/createWorkingPeriod', authMiddleware(RoleEnum.DIRECTOR), doctorController.createWorkingPeriod);
router.post('/:id/account', authMiddleware(RoleEnum.DIRECTOR), doctorController.add_doctor_account);

//Put requests
router.put('/workingPeriod', authMiddleware(RoleEnum.DIRECTOR),doctorController.updateWorkingPeriod);
router.put('/:id/account', authMiddleware(RoleEnum.DIRECTOR), doctorController.update_doctor_account);
router.put('/:id', authMiddleware(RoleEnum.DIRECTOR), doctorController.update_doctor);

//Delete requests
router.delete('/workingPeriod/:workingHoursId/:scheduleId',
    authMiddleware(RoleEnum.DIRECTOR), doctorController.deleteWorkingPeriod);
router.delete('/:id/account', authMiddleware(RoleEnum.DIRECTOR), doctorController.delete_doctor_account);
router.delete('/:id', authMiddleware(RoleEnum.DIRECTOR), doctorController.delete_doctor);

module.exports = router;