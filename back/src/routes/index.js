const Router = require('express');
const router = new Router();

const testRouter = require('./testRouter');
const userRouter = require('./userRouter');
const doctorRouter = require('./doctorRouter');
const patientRouter = require('./patientRouter');
const receptionistRouter = require('./receptionistRouter');
const myRouter = require('./myRouter');
const reportRouter = require('./reportRouter');
const equipmentRouter = require('./equipmentRouter');
const appointmentRouter = require('./appointmentRouter');
const deviceRouter = require('./deviceRouter');
const procedureRouter = require('./procedureRouter');

router.use('/test', testRouter);
router.use('/user', userRouter);
router.use('/doctor', doctorRouter);
router.use('/patient', patientRouter);
router.use('/receptionist', receptionistRouter);
router.use('/report', reportRouter);
router.use('/appointment', appointmentRouter);
router.use('/equipment', equipmentRouter);
router.use('/device', deviceRouter);
router.use('/procedure', procedureRouter);
router.use('', myRouter);

module.exports = router;
