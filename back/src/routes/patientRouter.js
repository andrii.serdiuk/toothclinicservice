const express = require('express');
const router = express.Router();

const {patientController} = require('../controllers');
const {authMiddleware} = require("../middleware");
const {RoleEnum} = require("../utils");
const {addPatient} = require("../middleware").validation;

//Get requests
router.get('/all', authMiddleware(RoleEnum.DIRECTOR, RoleEnum.REGISTRANT, RoleEnum.DOCTOR, RoleEnum.PATIENT), patientController.show_patients);
router.get('/account/:id', authMiddleware(RoleEnum.DIRECTOR, RoleEnum.REGISTRANT, RoleEnum.DOCTOR, RoleEnum.PATIENT), patientController.getPatientByAccountId);
router.get('/', authMiddleware(RoleEnum.DIRECTOR, RoleEnum.REGISTRANT, RoleEnum.DOCTOR, RoleEnum.PATIENT), patientController.show_patients_by_page);
router.get('/:id', authMiddleware(RoleEnum.DIRECTOR, RoleEnum.REGISTRANT, RoleEnum.DOCTOR, RoleEnum.PATIENT), patientController.show_patient);

//Post requests
router.post('/', authMiddleware(RoleEnum.DIRECTOR), ...addPatient, patientController.add_patient);
router.post('/:id/account', authMiddleware(RoleEnum.DIRECTOR), patientController.add_patient_account);

//Put requests
router.put('/:id', authMiddleware(RoleEnum.DIRECTOR), patientController.update_patient);
router.put('/:id/account', authMiddleware(RoleEnum.DIRECTOR), patientController.update_patient_account);

//Delete requests
router.delete('/:id', authMiddleware(RoleEnum.DIRECTOR), patientController.delete_patient);
router.delete('/:id/account', authMiddleware(RoleEnum.DIRECTOR), patientController.delete_patient_account);

module.exports = router;