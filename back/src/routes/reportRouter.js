
const express = require('express');
const { reportController } = require("../controllers");
const { authMiddleware } = require("../middleware");
const { RoleEnum } = require("../utils/");
const {pageLimit} = require("../middleware/validationMiddleware");
const router = express.Router();

router.get('/procedures', authMiddleware(RoleEnum.DIRECTOR), ...pageLimit, reportController.getProceduresReport);
router.get('/equipment', authMiddleware(RoleEnum.DIRECTOR), ...pageLimit, reportController.getEquipmentReport);
router.get('/workaholics', authMiddleware(RoleEnum.DIRECTOR), ...pageLimit, reportController.getWorkaholics);

//Serdiuk Andrii
router.get('/importantProcedures', authMiddleware(RoleEnum.DIRECTOR), ...pageLimit, reportController.getProceduresThatAllPatientsThisDayUnderwent);
router.get('/doctorBenefit', authMiddleware(RoleEnum.DIRECTOR), ...pageLimit, reportController.getDoctorBenefitForCompany);
router.get('/doctorPatientsAmount', authMiddleware(RoleEnum.DIRECTOR), ...pageLimit, reportController.getUniquePatientsAmountForEveryDoctor);
// May be used somewhere else
router.get('/doctorSchedule', authMiddleware(RoleEnum.DIRECTOR, RoleEnum.DOCTOR, RoleEnum.PATIENT, RoleEnum.REGISTRANT), ...pageLimit, reportController.getDoctorScheduleNextWeek);

router.get('/averageProceduresAge', authMiddleware(RoleEnum.DIRECTOR), ...pageLimit, reportController.getAverageProceduresAge);
router.get('/equipmentPopularity', authMiddleware(RoleEnum.DIRECTOR), ...pageLimit, reportController.getEquipmentPopularity);
router.get('/proceduresWithAllDevices', authMiddleware(RoleEnum.DIRECTOR), ...pageLimit, reportController.getProceduresWithAllDevices);
router.get('/patientFinishedAppointment/:id', authMiddleware(RoleEnum.DIRECTOR), ...pageLimit, reportController.getPatientFinishedAppointments);


module.exports = router;
