const connection = require('./connection')
const { SELECT_ALL,
    SELECT_PATIENT_BY_ACCOUNT_ID,
    COUNT_PATIENTS,
    SELECT_PATIENTS_FROM_PAGE,
    COUNT_PATIENTS_BY_ID,
    CREATE_PATIENT,
    UPDATE_PATIENT,
    GET_PATIENT_ACCOUNT_ID,
    ADD_PATIENT_ACCOUNT_ID,
    DELETE_PATIENT, GET_PATIENT_BY_ACCOUNT_ID, SELECT_PATIENT_APPOINTMENTS_FROM_PAGE
} = require('./queries/patient');
const accountRepository = require("./accountRepository");
const {ADD_DOCTOR_ACCOUNT_ID} = require("./queries");

class PatientRepository {
    async findAll(page, limit) {
        const res = await connection.query(SELECT_ALL);
        return res.rows;
    }

    async getPatientById(id) {
        return (await connection.query(SELECT_PATIENT_BY_ACCOUNT_ID, [id])).rows[0];
    }

    async getPatientByAccountId(id) {
        return (await connection.query(GET_PATIENT_BY_ACCOUNT_ID, [id])).rows[0];
    }

    async getPatientNumber() {
        return await connection.query(COUNT_PATIENTS);
    }

    async getPatientsWithOffset(limit, offset) {
        return await connection.query(SELECT_PATIENTS_FROM_PAGE, [limit, offset]);
    }

    async getPatientAppointmentsWithOffset(id, limit, offset) {
        return await connection.query(SELECT_PATIENT_APPOINTMENTS_FROM_PAGE, [id, limit, offset]);
    }

    async getAllPatients() {
        return (await connection.query(SELECT_ALL)).rows;
    }

    async existsById(id) {
        const {rows} = await connection.query(COUNT_PATIENTS_BY_ID, [id]);
        return !!Number.parseInt(rows[0]?.count);
    }

    async updatePatientById(previousId, {
        patientId, accountIdFk, firstName, secondName, lastName, birthday, phoneNumber
    }) {
        return await connection.query(UPDATE_PATIENT, [previousId, patientId, firstName, lastName, secondName, birthday, phoneNumber, accountIdFk])
    }

    async addPatientWithAccount({
                                    patientId, email, password, firstName, secondName, lastName, birthday, phone
                                }) {
        try {
            await connection.query('BEGIN');
            const accountIdFk = (await accountRepository.addUser(email, password, 'patient')).rows[0].accountId;
            const res = await connection.query(CREATE_PATIENT, [patientId, firstName, lastName, secondName, birthday, phone, accountIdFk]);
            await connection.query('COMMIT');

            return res;
        } catch (e) {
            await connection.query('ROLLBACK');
            throw e;
        }
    }

    async updatePatientAccountByPatientId(id, login, password) {
        const {rows} = await connection.query(GET_PATIENT_ACCOUNT_ID, [id]);
        return await accountRepository.updateUser(rows[0].accountIdFk, login, password);

    }

    async deletePatientById(id) {
        try {
            await connection.query('BEGIN');
            await this.deleteAccountByPatientId(id);
            const res = await connection.query(DELETE_PATIENT, [id]);
            await connection.query('COMMIT');
            return res;
        } catch (e) {
            await connection.query('ROLLBACK');
            throw e;
        }
    }

    async deleteAccountByPatientId(id) {
        try {
            await connection.query('BEGIN');
            const accountIdFk = (await connection.query(GET_PATIENT_ACCOUNT_ID, [id])).rows[0].accountIdFk;
            const res = await accountRepository.removeUser(accountIdFk);
            await connection.query('COMMIT');
            return res
        } catch (e) {
            await connection.query('ROLLBACK');
            throw e;
        }
    }

    async addPatientAccount(patientId, login, password) {
        try {
            await connection.query('BEGIN');
            const accountId = (await accountRepository.addUser(login, password, 'patient')).rows[0].accountId;
            const res = await connection.query(ADD_PATIENT_ACCOUNT_ID, [patientId, accountId]);
            await connection.query('COMMIT');
            console.log(accountId);
            return res;
        } catch (e) {
            await connection.query('ROLLBACK');
            throw e;
        }
    }
}

module.exports = new PatientRepository();
