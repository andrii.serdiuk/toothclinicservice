const connection = require("./connection");
const queries = require("./queries/equipment");
const {UPDATE_EQUIPMENT} = require("./queries");

class EquipmentRepository {

    async countEquipment() {
        const { rows } = await connection.query(queries.COUNT_EQUIPMENT);
        return rows[0]?.count;
    }

    async getEquipment() {
        const {rows} = await connection.query(queries.GET_EQUIPMENT);
        return rows;
    }

    async getEquipmentPage(limit, page) {
        return await connection.query(queries.GET_EQUIPMENT_PAGE, [limit, page]);
    }

    async addEquipment(equipmentName) {
        return await connection.query(queries.CREATE_EQUIPMENT, [equipmentName]);
    }

    async getAllEquipmentDevices(equipmentId) {
        return await connection.query(queries.GET_EQUIPMENT_DEVICES, [equipmentId]);
    }

    async updateEquipment(equipmentId, equipmentName) {
        return await connection.query(UPDATE_EQUIPMENT, [equipmentId, equipmentName]);
    }

    async deleteEquipmentById(equipmentId) {
        return await connection.query(queries.DELETE_EQUIPMENT_BY_ID, [equipmentId]);
    }

    async deleteEquipmentByName(equipmentName) {
        return await connection.query(queries.DELETE_EQUIPMENT_BY_NAME, [equipmentName]);
    }
}

module.exports = new EquipmentRepository();
