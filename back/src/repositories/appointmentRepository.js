const connection = require("./connection");
const {
    SELECT_APPOINTMENT_BY_ID,
    SELECT_SCHEDULED_APPOINTMENTS_BY_DOCTOR_ID,
    SELECT_SCHEDULED_APPOINTMENTS_BY_DOCTOR_ID_PAGINATED,
    SELECT_SCHEDULED_APPOINTMENTS_SHORT_BY_DOCTOR_ID,
    COUNT_SCHEDULED_APPOINTMENTS_BY_PATIENT_ID_AND_DATE_TIME_BOUNDARIES,
    COUNT_SCHEDULED_APPOINTMENTS_BY_DOCTOR_ID_AND_DATE_TIME_BOUNDARIES,
    CREATE_SCHEDULED_APPOINTMENT,
    GET_PERFORMED_PROCEDURE_DURING_APPOINTMENT,
    GET_EQUIPMENT_AND_DEVICE_DURING_APPOINTMENT,
    GET_DOCTOR_ACCOUNT_ID_BY_APPOINTMENT_ID,
    DELETE_PERFORMED_PROCEDURE_BIND,
    GET_PROCEDURES,
    CREATE_PERFORMED_PROCEDURE,
    GET_EQUIPMENT_AND_DEVICES_FOR_PROCEDURE,
    GET_PROCEDURE_DATA,
    BIND_DEVICE_TO_PERFORMED_PROCEDURE,
    UPDATE_PERFORMED_PROCEDURE_DISCOUNT,
    UPDATE_DEVICE_APPOINTMENT,
    UPDATE_APPOINTMENT_INFO,
    CREATE_FINISHED_APPOINTMENT,
    SELECT_FINISHED_APPOINTMENT_ID_BY_SCHEDULED_APPOINTMENT,
    GET_PROCEDURES_FOR_SPECIFIED_CATEGORY, SELECT_ALL_APPOINTMENTS_FILTERED, SET_APPOINTMENT_STATUS_FINISHED,
    GET_PATIENT_ACCOUNT_ID_BY_APPOINTMENT_ID,
    SET_APPOINTMENT_STATUS_CANCELLED
} = require("./queries/appointment");
const {GET_PROCEDURES_USED_DURING_APPOINTMENT, GET_PERFORMED_PROCEDURE} = require("./queries");

class AppointmentRepository {
    async getAllByDoctorIdPaginated(doctorId, dateFrom, dateTo, offset, limit) {
        const {rows} = await connection.query(SELECT_SCHEDULED_APPOINTMENTS_BY_DOCTOR_ID_PAGINATED, [doctorId, dateFrom, dateTo, limit, offset]);
        return rows;
    }

    async getAllByDoctorId(doctorId, dateFrom, dateTo) {
        const {rows} = await connection.query(SELECT_SCHEDULED_APPOINTMENTS_BY_DOCTOR_ID, [doctorId, dateFrom, dateTo]);
        return rows;
    }

    async getAllByDoctorIdForSchedule(doctorId, dateFrom, dateTo) {
        const {rows} = await connection.query(SELECT_SCHEDULED_APPOINTMENTS_SHORT_BY_DOCTOR_ID, [doctorId, dateFrom, dateTo]);
        return rows;
    }

    async getAllAppointmentsByPatientId() {

    }

    async getAll(dateFrom, dateTo) {

    }

    async getOneById(appointmentId) {
        const {rows} = await connection.query(SELECT_APPOINTMENT_BY_ID, [appointmentId]);
        return rows[0] || null;
    }

    async existsByPatientIdDateAndTimeBoundaries(patientCardId, date, beginTime, endTime) {
        const {rows} = await connection.query(COUNT_SCHEDULED_APPOINTMENTS_BY_PATIENT_ID_AND_DATE_TIME_BOUNDARIES,
            [patientCardId, date, beginTime, endTime]);
        return !!Number.parseInt(rows[0]?.count);
    }

    async existsByDoctorIdDateAndTime(doctorId, date, beginTime, endTime) {
        const {rows} = await connection.query(COUNT_SCHEDULED_APPOINTMENTS_BY_DOCTOR_ID_AND_DATE_TIME_BOUNDARIES,
            [doctorId, date, beginTime, endTime]);
        return !!Number.parseInt(rows[0]?.count);
    }

    async create(doctorId, patientCardId, date, beginTime, endTime) {
        const { rows } = await connection.query(CREATE_SCHEDULED_APPOINTMENT,
            [doctorId, patientCardId.trim(), date, beginTime, endTime]);
        return rows[0]?.appointmentId;
    }

    async getProceduresDoneDuringAppointment(appointmentId) {
        const {rows} = await connection.query(GET_PROCEDURES_USED_DURING_APPOINTMENT, [appointmentId]);
        return rows;
    }

    async getPerformedProcedureDuringAppointment(performedProcedureId) {
        const {rows} = await connection.query(GET_PERFORMED_PROCEDURE, [performedProcedureId]);
        return rows[0];
    }

    async getEquipmentAndDeviceForPerformedProcedure(performedProcedureId) {
        const {rows} = await connection.query(GET_EQUIPMENT_AND_DEVICE_DURING_APPOINTMENT, [performedProcedureId]);
        return rows;
    }

    async getDoctorAccountIdByAppointmentId(appointmentId) {
        const {rows} = await connection.query(GET_DOCTOR_ACCOUNT_ID_BY_APPOINTMENT_ID, [appointmentId]);
        return rows[0];
    }

    async getPatientAccountIdByAppointmentId(appointmentId) {
        const {rows} = await connection.query(GET_PATIENT_ACCOUNT_ID_BY_APPOINTMENT_ID, [appointmentId]);
        return rows[0];
    }

    async deletePerformedProcedure(performedProcedureId) {
        return await connection.query(DELETE_PERFORMED_PROCEDURE_BIND, [performedProcedureId]);
    }

    async getProceduresForTheSpecifiedCategory(category) {
        return await connection.query(GET_PROCEDURES_FOR_SPECIFIED_CATEGORY, [category]);
    }

    async createPerformedProcedure(procedureId, appointmentId, devices, fullPrice, discount) {
        console.log(appointmentId);
        await connection.query("BEGIN");
        const finishedAppointmentId = (await connection.query(SELECT_FINISHED_APPOINTMENT_ID_BY_SCHEDULED_APPOINTMENT, [appointmentId]))
            .rows[0].finishedAppointmentId;
        const result = (await connection.query(CREATE_PERFORMED_PROCEDURE,
            [procedureId, finishedAppointmentId, fullPrice, discount / 100])).rows[0].performedProcedureId;
        for (const deviceId of devices) {
            await connection.query(BIND_DEVICE_TO_PERFORMED_PROCEDURE, [deviceId, result]);
        }
        await connection.query("COMMIT");
    }

    async getEquipmentAndDevicesForProcedure(procedureId) {
        return await connection.query(GET_EQUIPMENT_AND_DEVICES_FOR_PROCEDURE, [procedureId]);
    }

    async getProcedureData(procedureId) {
        return await connection.query(GET_PROCEDURE_DATA, [procedureId]);
    }

    async createFinishedAppointment(scheduledAppointmentId, data) {
        const {date, diagnosis, patientState, treatment, beginTime, endTime} = data;
        return await connection.query(CREATE_FINISHED_APPOINTMENT,
            [scheduledAppointmentId, diagnosis, date, beginTime, endTime, patientState, treatment]);
    }

    async updatePerformedProcedureDiscount(performedProcedureId, discount) {
        return await connection.query(UPDATE_PERFORMED_PROCEDURE_DISCOUNT, [discount / 100, performedProcedureId]);
    }

    async updatePerformedProcedure(performedProcedureId, data) {
        const {discount, equipmentDevices, toChange} = data;
        await connection.query("BEGIN");
        await this.updatePerformedProcedureDiscount(performedProcedureId, discount);
        for (const entry of toChange) {
            const equipmentId = entry[0];
            const deviceId = entry[1];
            for (const equipment of equipmentDevices) {
                //Exactly == !
                if (equipment.equipmentId == equipmentId) {
                    await this.updateDeviceAppointment(performedProcedureId, equipment.selectedInventoryNumber, deviceId);
                }
            }
        }
        await connection.query("COMMIT");
    }

    async updateDeviceAppointment(performedProcedureId, selectedInventoryNumber, newInventoryNumber) {
        return await connection.query(UPDATE_DEVICE_APPOINTMENT, [performedProcedureId, selectedInventoryNumber, newInventoryNumber]);
    }

    async updateAppointmentInfo(appointmentId, date, diagnosis, patientState, treatment, beginTime, endTime) {
        return await connection.query(UPDATE_APPOINTMENT_INFO, [appointmentId, date, diagnosis, patientState, treatment, beginTime, endTime]);
    }

    async getFilteredAppointments(limit, page, doctorId, patientId,
                                  dateFrom, dateTo, appointmentStatus) {
        const {rows: appointments} = await connection.query(SELECT_ALL_APPOINTMENTS_FILTERED, [
            doctorId, patientId, dateFrom, dateTo,
            appointmentStatus, limit, page,]);

        if (parseInt(appointments[0].generalCount) === 0) {
            return {count: 0, appointments: []};
        }
        return {
            count: appointments[0].generalCount,
            appointments: appointments.map(appointment => {
                delete appointment.generalCount;
                return appointment;
            })
        };
    }

    async setAppointmentFinished(appointmentId) {
        return await connection.query(SET_APPOINTMENT_STATUS_FINISHED, [appointmentId]);
    }

    async setAppointmentCancelled(appointmentId) {
        return await connection.query(SET_APPOINTMENT_STATUS_CANCELLED, [appointmentId]);
    }
}

module.exports = new AppointmentRepository();
