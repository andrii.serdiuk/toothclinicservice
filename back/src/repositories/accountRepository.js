const connection = require('./connection');
const { AUTHENTICATION, SAVE_ACCOUNT, DELETE_ACCOUNT, GET_ACCOUNT_ID_BY_DOCTOR_ID, UPDATE_ACCOUNT, GET_ACCOUNT} = require('./queries/account');

class AccountRepository {
    async authenticate(login, password){
        // await connection.query(SAVE_ACCOUNT, [login, password, 'doctor']);
        return await connection.query(AUTHENTICATION, [login, password]);
    }

    async addUser(login, password, role){
        return await connection.query(SAVE_ACCOUNT, [login, password, role]);
    }

    async removeUser(id){
        return await connection.query(DELETE_ACCOUNT, [id]);
    }

    async getAccountIdByDoctorId(doctorId) {
        const { rows } = await connection.query(GET_ACCOUNT_ID_BY_DOCTOR_ID, [doctorId]);
        return rows[0]?.doctorId;
    }

    async updateUser(id, login, password){
        return await connection.query(UPDATE_ACCOUNT, [id, login, password]);
    }

    async getAccount(id) {
        return await connection.query(GET_ACCOUNT, [id]);
    }
}

module.exports = new AccountRepository();