module.exports = `
    DO $$ BEGIN
        CREATE TYPE APPOINTMENT_STATUS AS ENUM ('finished', 'scheduled', 'cancelled', 'unattended');
    EXCEPTION
    WHEN duplicate_object THEN null;
    END $$;
`;