module.exports = `
    DO $$ BEGIN
        CREATE TYPE role AS ENUM ('director', 'registrant', 'doctor', 'patient');
    EXCEPTION
    WHEN duplicate_object THEN null;
    END $$;
`;