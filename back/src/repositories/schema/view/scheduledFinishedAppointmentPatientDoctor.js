module.exports = `
    CREATE OR REPLACE VIEW scheduled_finished_appointment_patient_doctor AS
    SELECT sa."appointmentId", sa."appointmentStatus", sa."appointmentDate" AS "scheduledAppointmentDate",
           sa."startTime" AS "scheduledAppointmentStartTime",
           p."patientCardId", p."firstName" AS "patientFirstName", p.surname AS "patientSurname",
           p."lastName" AS "patientLastName", p.birthday AS "patientBirthday",
           d."doctorId", d."firstName" AS "doctorFirstName", d."secondName" AS "doctorSecondName",
           d."lastName" AS "doctorLastName", d.birthday AS "doctorBirthday",
           fa."finishedAppointmentId", fa."appointmentDate" AS "finishedAppointmentDate",
           fa."appointmentStartTime" AS "finishedAppointmentStartTime", fa."appointmentEndTime" AS "finishedAppointmentEndTime",
           fa.diagnosis, fa."patientState", fa.treatment
    FROM scheduled_appointment sa
             INNER JOIN doctor d
                        ON sa."doctorId" = d."doctorId"
             INNER JOIN patient p
                        ON sa."patientCardId" = p."patientCardId"
             LEFT OUTER JOIN finished_appointment fa
                             ON sa."appointmentId" = fa."scheduledAppointmentId";
`;
