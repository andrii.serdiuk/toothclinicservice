module.exports = `
CREATE OR REPLACE VIEW equipment_report AS
    SELECT e."equipmentId", e."equipmentName", COUNT(*) AS "TimesUsed", SUM(pad."fullPrice") AS "TotalIncome"
    FROM equipment e
        INNER JOIN device d
            ON e."equipmentId" = d."equipmentIdFK"
        INNER JOIN procedure_appointment_device pad
            ON d."inventoryNumber" = pad."deviceId"
    GROUP BY e."equipmentId";
`;
