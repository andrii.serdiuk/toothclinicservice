module.exports = `CREATE OR REPLACE VIEW doctor_scheduled_appointment_patient AS
SELECT doctor."doctorId",
       sa."appointmentId",
       sa."appointmentDate",
       sa."startTime",
       sa."approximateEndTime",
       sa."appointmentStatus",
       p."patientCardId",
       p."firstName",
       p."lastName",
       p."surname",
       p."birthday",
       p."phoneNumber"
FROM doctor
         INNER JOIN scheduled_appointment sa on doctor."doctorId" = sa."doctorId"
         INNER JOIN patient p on sa."patientCardId" = p."patientCardId";`;