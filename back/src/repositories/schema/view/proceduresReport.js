module.exports = `
    CREATE OR REPLACE VIEW procedures_report AS
    SELECT p."procedureId", p."procedureName", COUNT(*) AS "TimesUsed", SUM(pad."fullPrice") "TotalIncome"
    FROM procedure p
         INNER JOIN procedure_appointment_device pad
             ON p."procedureId" = pad."procedureId"
    GROUP BY p."procedureId";
`;
