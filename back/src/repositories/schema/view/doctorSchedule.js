module.exports = `
CREATE OR REPLACE VIEW doctor_full_schedule AS
SELECT doctor."doctorId", "scheduleDate", "beginTime", "endTime"
FROM ((doctor INNER JOIN doctor_schedule ds on doctor."doctorId" = ds."doctorIdFk")
    INNER JOIN doctor_schedule_working_hours ON "scheduleId" = doctor_schedule_working_hours."scheduleIdFk")
         INNER JOIN doctor_working_hours ON "workingHoursIdFk" = doctor_working_hours."workingHoursId";`;