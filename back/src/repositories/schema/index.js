const CREATE_TABLE_ACCOUNT = require('./table/account');
const CREATE_TABLE_DOCTOR = require('./table/doctor');
const CREATE_TABLE_DOCTOR_SCHEDULE = require('./table/doctorSchedule');
const CREATE_TABLE_DOCTOR_WORKING_HOURS = require('./table/doctorWorkingHours.table');
const CREATE_TABLE_DOCTOR_SCHEDULE_WORKING_HOURS = require('./table/doctorScheduleWorkingHours');
const CREATE_TABLE_PATIENT = require('./table/patient');
const CREATE_TABLE_DEVICE = require('./table/device');
const CREATE_TABLE_EQUIPMENT = require('./table/equipment');
const CREATE_TABLE_PROCEDURE = require('./table/procedure');
const CREATE_TABLE_PROCEDURE_EQUIPMENT = require('./table/procedureEquipment');
const CREATE_TABLE_SCHEDULED_APPOINTMENT = require('./table/scheduledAppointment');
const CREATE_TABLE_DOCTOR_APPOINTMENT = require('./table/finishedAppointment');
//const CREATE_TABLE_PROCEDURE_APPOINTMENT_DEVICE = require('./table/procedureAppointmentDevice');

const CREATE_APPLICATION_STATUS_ENUM = require('./enum/applicationStatus');
const CREATE_ROLE_ENUM = require('./enum/role');

const CREATE_DOCTOR_FULL_SCHEDULE_VIEW = require('./view/doctorSchedule');
const CREATE_DOCTOR_SCHEDULED_APPOINTMENT_PATIENT_VIEW = require('./view/doctorScheduledAppointmentPatient');

module.exports = {
    CREATE_APPLICATION_STATUS_ENUM,
    CREATE_ROLE_ENUM,
    CREATE_TABLE_ACCOUNT,
    CREATE_TABLE_DOCTOR,
    CREATE_TABLE_DOCTOR_SCHEDULE,
    CREATE_TABLE_DOCTOR_WORKING_HOURS,
    CREATE_TABLE_DOCTOR_SCHEDULE_WORKING_HOURS,
    CREATE_TABLE_PATIENT,
    CREATE_TABLE_DEVICE,
    CREATE_TABLE_EQUIPMENT,
    CREATE_TABLE_PROCEDURE,
    CREATE_TABLE_PROCEDURE_EQUIPMENT,
    CREATE_TABLE_SCHEDULED_APPOINTMENT,
    CREATE_TABLE_DOCTOR_APPOINTMENT,
    /*CREATE_TABLE_PROCEDURE_APPOINTMENT_DEVICE,*/
    CREATE_DOCTOR_FULL_SCHEDULE_VIEW,
    CREATE_DOCTOR_SCHEDULED_APPOINTMENT_PATIENT_VIEW
};
