const {regex: {EMAIL_REGEX}} = require('../../../utils');

module.exports = `
    CREATE TABLE IF NOT EXISTS account
    (
        "accountId"       BIGSERIAL PRIMARY KEY,
        "accountEmail"    VARCHAR(50) NOT NULL UNIQUE
            CONSTRAINT emailValid
                CHECK ("accountEmail" ~ '${EMAIL_REGEX}'),
        "accountPassword" CHAR(60) NOT NULL,
        "accountRole"     role        NOT NULL
    );`;