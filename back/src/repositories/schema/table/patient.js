const {regex: {PHONE_NUMBER_REGEX}} = require('../../../utils');

module.exports = `
    CREATE TABLE IF NOT EXISTS patient
    (
        "patientCardId" CHAR(13) PRIMARY KEY,
        "firstName"     VARCHAR(30) NOT NULL,
        "lastName"      VARCHAR(30) NOT NULL,
        "surname"       VARCHAR(30) NOT NULL,
        "birthday"      DATE        NOT NULL
            CONSTRAINT birthdayValid
                CHECK ("birthday" < CURRENT_DATE),
        "phoneNumber"   CHAR(13)    NOT NULL
            CONSTRAINT phoneNumberValid
                CHECK ("phoneNumber" ~ '${PHONE_NUMBER_REGEX}'),
        "accountIdFK"   BIGINT,
        CONSTRAINT fk_account
            FOREIGN KEY ("accountIdFK")
                REFERENCES account ("accountId")
                ON UPDATE CASCADE
                ON DELETE SET NULL
    );
`;
