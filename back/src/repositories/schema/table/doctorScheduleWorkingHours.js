module.exports = `
    CREATE TABLE IF NOT EXISTS doctor_schedule_working_hours
    (
        "workingHoursIdFk" INTEGER REFERENCES doctor_working_hours ("workingHoursId")
            ON UPDATE CASCADE ON DELETE CASCADE,
        "scheduleIdFk"     INTEGER REFERENCES doctor_schedule ("scheduleId")
            ON UPDATE CASCADE ON DELETE CASCADE,
        PRIMARY KEY ("workingHoursIdFk", "scheduleIdFk")
    );
`;