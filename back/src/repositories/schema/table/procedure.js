module.exports = `
    CREATE TABLE IF NOT EXISTS procedure
    (
        "procedureId"     SERIAL PRIMARY KEY,
        "procedureName"   VARCHAR(50)        NOT NULL,
        "currentPrice"    MONEY              NOT NULL
            CONSTRAINT "positiveMoney" CHECK ("currentPrice"::numeric > 0),
        "minimalCategory" SMALLINT           NOT NULL
            CHECK ("minimalCategory" >= 0 AND "minimalCategory" < 5)
    );
`;
