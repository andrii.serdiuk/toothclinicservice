module.exports = `
    CREATE TABLE IF NOT EXISTS doctor_schedule
    (
        "scheduleId"   BIGSERIAL PRIMARY KEY,
        "doctorIdFk"   INTEGER REFERENCES doctor ("doctorId") NOT NULL,
        "scheduleDate" DATE                                   NOT NULL
            CONSTRAINT correctScheduleDate
                CHECK ("scheduleDate" >= CURRENT_DATE),
        UNIQUE ("doctorIdFk", "scheduleDate")
    );
`;