module.exports = `
    CREATE TABLE IF NOT EXISTS device
    (
        "inventoryNumber" BIGINT PRIMARY KEY
            CONSTRAINT positiveInventoryNumber CHECK ( "inventoryNumber" > 0 ),
        "receiptDate"     DATE    NOT NULL DEFAULT CURRENT_DATE
            CONSTRAINT receiptDateValid
                CHECK ("receiptDate" <= CURRENT_DATE),
        "price"           MONEY   NOT NULL
            CONSTRAINT positiveMoney
                CHECK ( "price"::numeric >= 0),
        "equipmentIdFK"   INTEGER NOT NULL,
        CONSTRAINT equipmentIdFK
            FOREIGN KEY ("equipmentIdFK")
                REFERENCES equipment ("equipmentId")
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
    );
`;
