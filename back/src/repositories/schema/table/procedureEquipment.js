module.exports = `
    CREATE TABLE IF NOT EXISTS procedures_equipment
    (
        "procedureIdFK" INTEGER NOT NULL,
        "equipmentIdFK" INTEGER NOT NULL,
        PRIMARY KEY ("procedureIdFK", "equipmentIdFK"),
        CONSTRAINT procedureIdFK
            FOREIGN KEY ("procedureIdFK")
                REFERENCES procedure ("procedureId")
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
        CONSTRAINT equipmentIdFK
            FOREIGN KEY ("equipmentIdFK")
                REFERENCES equipment ("equipmentId")
                ON DELETE NO ACTION
                ON UPDATE NO ACTION
    );
`;
