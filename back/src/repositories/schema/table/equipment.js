module.exports = `
    CREATE TABLE IF NOT EXISTS equipment
    (
        "equipmentId"   BIGSERIAL PRIMARY KEY,
        "equipmentName" VARCHAR(50) UNIQUE NOT NULL
    );
`;
