module.exports = `
    CREATE TABLE IF NOT EXISTS finished_appointment
    (
        "finishedAppointmentId"    BIGSERIAL PRIMARY KEY,
        "scheduledAppointmentId"   INTEGER      NOT NULL
            REFERENCES scheduled_appointment ("appointmentId")
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
        "diagnosis"                VARCHAR(150) NOT NULL,
        "appointmentDate" DATE NOT NULL,
        "appointmentStartTime" TIME NOT NULL,
        --now() returns timestamp with time zone and it's incomparable with just time, we should use CURRENT_DATE instead
        "appointmentEndTime"       TIME         NOT NULL,
        "patientState"             VARCHAR(200) NOT NULL,
        "treatment"                VARCHAR(150) NULL,
        CONSTRAINT start_time_lt_end_time CHECK ("appointmentStartTime" < "appointmentEndTime")
    );
`;