const { regex: { PHONE_NUMBER_REGEX }} = require('../../../utils');

module.exports = `
    CREATE TABLE IF NOT EXISTS doctor
    (
        "doctorId" INTEGER PRIMARY KEY
            CONSTRAINT positiveDoctorId CHECK ( "doctorId" > 0 ),
        "accountIdFk"    BIGINT      NULL,
        "firstName"      VARCHAR(50) NOT NULL,
        "secondName"     VARCHAR(50) NOT NULL,
        "lastName"       VARCHAR(50) NOT NULL,
        "birthday"       DATE        NOT NULL
            CONSTRAINT validBirthday CHECK ( "birthday" < CURRENT_DATE ),
        "experience"     SMALLINT    NOT NULL
            CONSTRAINT positiveExperience CHECK ( "experience" >= 0 ),
            --CONSTRAINT correctExperience CHECK ( YEAR - birthday - experience >= 18 )
        "phoneNumber"    CHAR(13)    NOT NULL
            CONSTRAINT validPhoneNumber CHECK ( "phoneNumber" ~ '${PHONE_NUMBER_REGEX}'),
        "doctorCategory" SMALLINT    NOT NULL
            CONSTRAINT doctorCategoryInBounds CHECK ( "doctorCategory" >= 0 AND "doctorCategory" <= 4),
        CONSTRAINT fk_account
            FOREIGN KEY ("accountIdFk")
                REFERENCES account ("accountId")
                ON DELETE SET NULL
                ON UPDATE CASCADE
    );
`;