module.exports = `
    CREATE TABLE IF NOT EXISTS scheduled_appointment
    (
        "appointmentId"      BIGSERIAL PRIMARY KEY,
        "doctorId"           INTEGER
            REFERENCES doctor ("doctorId")
                ON DELETE NO ACTION
                ON UPDATE NO ACTION             NOT NULL,
        "patientCardId"      CHAR(13) NOT NULL
            REFERENCES patient ("patientCardId")
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
        "appointmentDate"    DATE               NOT NULL
            CONSTRAINT correctAppointmentDate CHECK ("appointmentDate" >= CURRENT_DATE),
        "startTime"          TIME               NOT NULL,
        "approximateEndTime" TIME               NOT NULL,
        UNIQUE ("doctorId", "patientCardId", "appointmentDate", "startTime"),
            CONSTRAINT correctEndTime CHECK ("approximateEndTime" > "startTime"),
        "appointmentStatus"  APPOINTMENT_STATUS NOT NULL DEFAULT 'unattended'
    );
`;