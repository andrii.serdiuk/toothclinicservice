module.exports = `
    CREATE TABLE IF NOT EXISTS doctor_working_hours
    (
        "workingHoursId" BIGSERIAL PRIMARY KEY,
        "beginTime"      TIME NOT NULL,
        "endTime"        TIME NOT NULL,
        UNIQUE ("beginTime", "endTime"),
        CONSTRAINT begin_end_times_valid CHECK ( "beginTime" < "endTime" )
    );

`;