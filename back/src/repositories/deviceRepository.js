const connection = require('./connection');
const {CREATE_DEVICE_BIND_BY_EQUIPMENT_NAME, SELECT_DEVICES_PAGE_BY_EQUIPMENT_ID, COUNT_DEVICES_BY_EQUIPMENT_ID,
    SELECT_DEVICE_BY_ID, UPDATE_DEVICE, CREATE_DEVICE_BIND_BY_EQUIPMENT_ID, DELETE_DEVICE
} = require("./queries");
const e = require("express");

class DeviceRepository {

    async createDeviceBindByEquipmentName({inventoryNumber, receiptDate, price, equipmentName}) {
        return await connection.query(CREATE_DEVICE_BIND_BY_EQUIPMENT_NAME,
            [inventoryNumber, price, receiptDate, equipmentName]);
    }

    async createDeviceBindByEquipmentId(inventoryNumber, price, receiptDate, equipmentId) {
        return await connection.query(CREATE_DEVICE_BIND_BY_EQUIPMENT_ID,
            [inventoryNumber, price, receiptDate, equipmentId]);
    }

    async updateDevice(inventoryNumber, receiptDate, price, oldInventoryNumber) {
        return await connection.query(UPDATE_DEVICE, [inventoryNumber, price, receiptDate, oldInventoryNumber]);
    }

    async countDevicesByEquipmentId(equipmentId) {
        const {rows} = await connection.query(COUNT_DEVICES_BY_EQUIPMENT_ID, [equipmentId]);
        return rows[0]?.count;
    }

    async getDevicesPageByEquipmentId(equipmentId, limit, offset) {
        return await connection.query(SELECT_DEVICES_PAGE_BY_EQUIPMENT_ID, [equipmentId, limit, offset]);
    }

    async getOneById(inventoryNumber) {
        return await connection.query(SELECT_DEVICE_BY_ID, [inventoryNumber]);
    }

    async getAll(page, limit) {

    }

    async delete(deviceId) {
        return await connection.query(DELETE_DEVICE, [deviceId]);
    }
}

module.exports = new DeviceRepository();
