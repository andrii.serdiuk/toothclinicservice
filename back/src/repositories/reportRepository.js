const connection = require("./connection");
const {
    WORKAHOLICS, EVERY_PROCEDURE_COUNT_AND_INCOME_WITHIN_A_MONTH,
    EVERY_EQUIPMENT_COUNT_AND_INCOME_WITHIN_A_MONTH, WORKAHOLICS_WITH_PAGINATION, COUNT_WORKAHOLICS,
    COUNT_UNIQUE_PATIENTS_FOR_EVERY_DOCTOR,
    SELECT_EVERY_DOCTOR_BENEFIT_FOR_COMPANY,
    SELECT_PROCEDURES_THAT_ALL_PATIENTS_FROM_SPECIFIED_DAY_UNDERWENT_ON_THIS_DAY,
    GET_DOCTOR_WORKING_TIME_NEXT_WEEK
} = require("./queries/report");
const {
    SELECT_AVERAGE_PROCEDURES_AGE, SELECT_EQUIPMENT_POPULARITY, SELECT_PROCEDURES_WITH_ALL_DEVICES,
    COUNT_PATIENT_FINISHED_APPOINTMENTS, SELECT_PATIENT_FINISHED_APPOINTMENTS,
    COUNT_DOCTOR_WORKING_PERIODS_NEXT_WEEK
} = require("./queries");

class ReportRepository {
    async countWorkaholics(dateFrom, dateTo) {
        const {rows} = await connection.query(COUNT_WORKAHOLICS, [dateFrom, dateTo]);
        return rows[0]?.count;
    }

    async getWorkaholics(limit, offset, dateFrom, dateTo) {
        const {rows} = await connection.query(WORKAHOLICS_WITH_PAGINATION, [limit, offset, dateFrom, dateTo]);
        return rows;
    }

    async getProceduresReport(limit, offset) {
        const {rows} = await connection.query(EVERY_PROCEDURE_COUNT_AND_INCOME_WITHIN_A_MONTH, [limit, offset]);
        return rows;
    }

    async getEquipmentReport(limit, offset) {
        const {rows} = await connection.query(EVERY_EQUIPMENT_COUNT_AND_INCOME_WITHIN_A_MONTH, [limit, offset]);
        return rows;
    }

    async getBenefitOfEveryDoctor(limit, offset) {
        return await connection.query(SELECT_EVERY_DOCTOR_BENEFIT_FOR_COMPANY, [limit, offset]);
    }

    async getAverageProceduresAge(limit, offset) {
        return await connection.query(SELECT_AVERAGE_PROCEDURES_AGE, [limit, offset]);
    }

    async getEquipmentPopularity(limit, offset) {
        return await connection.query(SELECT_EQUIPMENT_POPULARITY, [limit, offset]);
    }

    async getProceduresWithAllDevices(limit, offset) {
        return await connection.query(SELECT_PROCEDURES_WITH_ALL_DEVICES, [limit, offset]);
    }

    async getPatientFinishedAppointments(limit, offset, id) {
        await connection.query('BEGIN');
        const count = (await connection.query(COUNT_PATIENT_FINISHED_APPOINTMENTS, [id])).rows[0].appointmentsNumber;
        const patientFinishedAppointments = (await connection.query(SELECT_PATIENT_FINISHED_APPOINTMENTS, [limit, offset, id])).rows;
        await connection.query('COMMIT');

        return {count: count, patientFinishedAppointments: patientFinishedAppointments};
    }

    async getUniquePatientsAmountForEveryDoctor(dateFrom, dateTo, limit, offset) {
        return await connection.query(COUNT_UNIQUE_PATIENTS_FOR_EVERY_DOCTOR, [dateFrom, dateTo, limit, offset]);
    }

    async getImportantProcedures(limit, offset) {
        return await connection.query(SELECT_PROCEDURES_THAT_ALL_PATIENTS_FROM_SPECIFIED_DAY_UNDERWENT_ON_THIS_DAY, [limit, offset]);
    }

    async getDoctorWorkingTime(id, itemsOnPage, offset) {
        return await connection.query(GET_DOCTOR_WORKING_TIME_NEXT_WEEK, [id, itemsOnPage, offset]);
    }

    async countDoctorWorkingPeriods(id) {
        return await connection.query(COUNT_DOCTOR_WORKING_PERIODS_NEXT_WEEK, [id]);
    }
}

module
    .exports = new ReportRepository();
