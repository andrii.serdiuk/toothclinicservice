const SELECT_ALL = `
    SELECT *
    FROM patient
    ORDER BY "surname", "firstName", "lastName"`;
const SELECT_PATIENT_BY_ACCOUNT_ID = `
    SELECT "patientCardId", "firstName", surname, "lastName", birthday, "phoneNumber", "accountEmail"
    FROM patient
             FULL JOIN account a ON patient."accountIdFK" = a."accountId"
    WHERE "patientCardId" = $1;
`;

const COUNT_PATIENTS = `
    SELECT COUNT(*)
    FROM patient;
`;

const SELECT_PATIENTS_FROM_PAGE = `
    SELECT *
    FROM patient
    ORDER BY "surname", "firstName", "lastName"
    LIMIT $1 OFFSET $2;
`;

const COUNT_PATIENTS_BY_ID = `
    SELECT COUNT(*)
    FROM patient
    WHERE "patientCardId" = $1;
`

const CREATE_PATIENT = `
    INSERT INTO patient("patientCardId", "firstName", "lastName", "surname",
                        "birthday", "phoneNumber", "accountIdFK")
    VALUES ($1, $2, $3, $4, $5, $6, $7);
`;

const UPDATE_PATIENT = `
    UPDATE patient
    SET "patientCardId" = COALESCE($2, "patientCardId"),
        "firstName"     = COALESCE($3, "firstName"),
        "lastName"      = COALESCE($4, "lastName"),
        "surname"       = COALESCE($5, "surname"),
        "birthday"      = COALESCE($6, "birthday"),
        "phoneNumber"   = COALESCE($7, "phoneNumber"),
        "accountIdFK"   = COALESCE($8, "accountIdFK")
    WHERE "patientCardId" = $1;
`;

const GET_PATIENT_ACCOUNT_ID = `
    SELECT "accountIdFK" as "accountIdFk"
    FROM patient
    WHERE "patientCardId" = $1;
`;

const GET_PATIENT_BY_ACCOUNT_ID = `
    SELECT "patientCardId", "accountEmail", "firstName", "surname", "lastName", "birthday", "phoneNumber"
    FROM patient
             INNER JOIN account a on a."accountId" = patient."accountIdFK"
    WHERE "accountIdFK" = $1;
`;

const ADD_PATIENT_ACCOUNT_ID = `
    UPDATE patient
    SET "accountIdFK" = $2
    WHERE "patientCardId" = $1
      AND "accountIdFK" IS NULL;
`;

const DELETE_PATIENT = `
    DELETE
    FROM patient p
    WHERE "patientCardId" = $1
      AND NOT EXISTS(
            SELECT *
            FROM scheduled_appointment sa
            WHERE sa."patientCardId" = p."patientCardId"
        )
    RETURNING *;
`;

const SELECT_PATIENT_APPOINTMENTS_FROM_PAGE = `
    WITH filtered AS (
        SELECT "appointmentDate",
               "appointmentId",
               "appointmentStatus",
               "firstName",
               "secondName",
               "lastName",
               sa."doctorId"
        FROM scheduled_appointment sa
                 INNER JOIN doctor p ON sa."doctorId" = p."doctorId"
        WHERE sa."patientCardId" = $1
    )
    SELECT *
    FROM (
             TABLE filtered
                 ORDER BY "appointmentDate" DESC
                 LIMIT $2
                 OFFSET $3
         ) limited
             RIGHT JOIN (SELECT COUNT(*) FROM filtered) count("generalCount")
                        ON true;
`;

module.exports = {
    SELECT_ALL,
    SELECT_PATIENT_BY_ACCOUNT_ID,
    COUNT_PATIENTS,
    SELECT_PATIENTS_FROM_PAGE,
    COUNT_PATIENTS_BY_ID,
    CREATE_PATIENT,
    UPDATE_PATIENT,
    GET_PATIENT_ACCOUNT_ID,
    GET_PATIENT_BY_ACCOUNT_ID,
    ADD_PATIENT_ACCOUNT_ID,
    DELETE_PATIENT,
    SELECT_PATIENT_APPOINTMENTS_FROM_PAGE
};
