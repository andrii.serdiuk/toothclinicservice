const COUNT_EQUIPMENT = `
    SELECT COUNT(*)
    FROM equipment;
`;

const GET_EQUIPMENT = `
   SELECT *
   FROM equipment;
`;

const GET_EQUIPMENT_PAGE = `
    SELECT *
    FROM equipment
    ORDER BY "equipmentName"
    LIMIT $1 OFFSET $2;
`;

const CREATE_EQUIPMENT = `
    INSERT INTO equipment("equipmentName")
    VALUES ($1);
`;

const DELETE_EQUIPMENT_BY_ID = `
    DELETE
    FROM equipment
    WHERE "equipmentId" = $1;
`;

const DELETE_EQUIPMENT_BY_NAME = `
    DELETE
    FROM equipment
    WHERE "equipmentName" = $1;
`;

const GET_EQUIPMENT_DEVICES = `
    SELECT "inventoryNumber"
    FROM device
    WHERE "equipmentIdFK" = $1
;`;

const UPDATE_EQUIPMENT = `
    UPDATE equipment
    SET "equipmentName" = $2
    WHERE "equipmentId" = $1
;`;

module.exports = {
    COUNT_EQUIPMENT,
    GET_EQUIPMENT,
    GET_EQUIPMENT_PAGE,
    GET_EQUIPMENT_DEVICES,
    CREATE_EQUIPMENT,
    UPDATE_EQUIPMENT,
    DELETE_EQUIPMENT_BY_ID,
    DELETE_EQUIPMENT_BY_NAME
};
