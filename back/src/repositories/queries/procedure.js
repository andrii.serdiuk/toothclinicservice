const COUNT_PROCEDURES = `
    SELECT COUNT(*)
    FROM procedure;
`;

const GET_PROCEDURES_USED_DURING_APPOINTMENT = `
    SELECT DISTINCT ("performedProcedureId"), "procedureId", "procedureName", "fullPrice" * (1 - pad_full.discount) AS "fullPrice", "minimalCategory"
    FROM pad_full
    WHERE pad_full."scheduledAppointmentId" = $1;
`;

const GET_PERFORMED_PROCEDURE = `
    SELECT "performedProcedureId",
           p."procedureId",
           "procedureName",
           "fullPrice",
           "minimalCategory",
           "discount" * 100 AS "discount"
    FROM procedure_appointment
             INNER JOIN procedure p ON procedure_appointment."procedureId" = p."procedureId"
    WHERE "performedProcedureId" = $1;
`;

const SELECT_ALL_PROCEDURES = `
    SELECT *
    FROM procedure;
`;

const SELECT_PROCEDURE_BY_ID = `
    SELECT *
    FROM procedure
    WHERE "procedureId" = $1;
`;

const SELECT_PROCEDURE_EQUIPMENT = `
    SELECT *
    FROM equipment e
    WHERE e."equipmentId" IN
          (
              SELECT pe."equipmentIdFK"
              FROM procedures_equipment pe
              WHERE pe."procedureIdFK" = $1
          );
`;

const SELECT_PROCEDURES_FROM_PAGE = `
    SELECT *
    FROM procedure
    ORDER BY "procedureId"
    LIMIT $1 OFFSET $2;
`;

const UPDATE_PROCEDURE = `
    UPDATE procedure
    SET "procedureId"     = COALESCE($2, "procedureId"),
        "procedureName"   = COALESCE($3, "procedureName"),
        "currentPrice"    = COALESCE($4, "currentPrice"),
        "minimalCategory" = COALESCE($5, "minimalCategory")
    WHERE "procedureId" = $1
    RETURNING "procedureId";
`;

const CREATE_PROCEDURE = `
    INSERT INTO procedure("procedureName", "currentPrice", "minimalCategory")
    VALUES ($1, $2, $3)
    RETURNING "procedureId";
`;

const DELETE_PROCEDURE = `
    DELETE
    FROM procedure p
    WHERE "procedureId" = $1
      AND NOT EXISTS(
            SELECT *
            FROM procedure_appointment pa
            WHERE pa."procedureId" = p."procedureId"
        )
    RETURNING *;
`;

const CLEAR_PROCEDURE_EQUIPMENTS = `
    DELETE
    FROM procedures_equipment
    WHERE "procedureIdFK" = $1;
`;

const ADD_EQUIPMENT_TO_PROCEDURE = `
    INSERT INTO procedures_equipment("procedureIdFK", "equipmentIdFK")
    VALUES ($1, $2);
`;

module.exports = {
    COUNT_PROCEDURES,
    GET_PROCEDURES_USED_DURING_APPOINTMENT,
    GET_PERFORMED_PROCEDURE,
    SELECT_ALL_PROCEDURES,
    SELECT_PROCEDURES_FROM_PAGE,
    SELECT_PROCEDURE_BY_ID,
    SELECT_PROCEDURE_EQUIPMENT,
    UPDATE_PROCEDURE,
    CLEAR_PROCEDURE_EQUIPMENTS,
    ADD_EQUIPMENT_TO_PROCEDURE,
    CREATE_PROCEDURE,
    DELETE_PROCEDURE
};
