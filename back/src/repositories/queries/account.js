const SAVE_ACCOUNT = `
    INSERT INTO account("accountEmail", "accountPassword", "accountRole")
    VALUES ($1, crypt($2, gen_salt('bf')), $3)
    RETURNING "accountId";
`;

const DELETE_ACCOUNT = `
    DELETE
    FROM account
    WHERE "accountId" = $1;
`;

const UPDATE_ACCOUNT = `
    UPDATE account
    SET "accountEmail"    = COALESCE($2, "accountEmail"),
        "accountPassword" = COALESCE(crypt($3, gen_salt('bf')), "accountPassword")
    WHERE "accountId" = $1;
`

//Look here for more information
//https://www.postgresql.org/docs/current/pgcrypto.html
const AUTHENTICATION = `
    SELECT "accountId", "accountRole"
    FROM account
    WHERE "accountEmail" = $1
      AND "accountPassword" = crypt($2, "accountPassword");
`;

const GET_ACCOUNT_ID_BY_DOCTOR_ID = `
    SELECT "accountId"
    FROM account
        INNER JOIN doctor d 
            ON account."accountId" = d."accountIdFk"
    WHERE "doctorId" = $1;
`;

const GET_ACCOUNT = `
    SELECT *
    FROM account
    WHERE "accountId" = $1;
`;

module.exports = {
    GET_ACCOUNT,
    SAVE_ACCOUNT,
    AUTHENTICATION,
    DELETE_ACCOUNT,
    GET_ACCOUNT_ID_BY_DOCTOR_ID,
    UPDATE_ACCOUNT
};