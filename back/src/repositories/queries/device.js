const CREATE_DEVICE = `
    INSERT INTO device("inventoryNumber", "price", "receiptDate", "equipmentIdFK") 
    VALUES($1, $2, $3, $4);
`;

const CREATE_DEVICE_BIND_BY_EQUIPMENT_NAME = `
    INSERT INTO device("inventoryNumber", "price", "receiptDate", "equipmentIdFK")
    VALUES ($1, $2, $3, (SELECT "equipmentId" FROM equipment WHERE "equipmentName" = $1));
`;

const CREATE_DEVICE_BIND_BY_EQUIPMENT_ID = `
    INSERT INTO device("inventoryNumber", "price", "receiptDate", "equipmentIdFK")
    VALUES ($1, $2, $3, $4);
`;

const SELECT_DEVICE_BY_ID = `
    SELECT *
    FROM device
    WHERE "inventoryNumber" = $1;
`;

const SELECT_ALL_DEVICES = `
    SELECT *
    FROM device
    LIMIT $1
    OFFSET $2;
`;

const COUNT_DEVICES_BY_EQUIPMENT_ID = `
    SELECT COUNT(*)
    FROM device
    WHERE "equipmentIdFK" = $1;
`;

const SELECT_DEVICES_PAGE_BY_EQUIPMENT_ID = `
    SELECT *
    FROM device
    WHERE "equipmentIdFK" = $1
    ORDER BY "inventoryNumber"
    LIMIT $2
    OFFSET $3;
`;

const DELETE_DEVICE = `
    DELETE 
    FROM device
    WHERE "inventoryNumber" = $1;
`;

const UPDATE_DEVICE = `
    UPDATE device
    SET "inventoryNumber" = COALESCE($1, "inventoryNumber"),
        "price"           = COALESCE($2, "price"),
        "receiptDate"     = COALESCE($3, "receiptDate")
    WHERE "inventoryNumber" = $4;
`;

module.exports = {
    CREATE_DEVICE,
    CREATE_DEVICE_BIND_BY_EQUIPMENT_NAME,
    CREATE_DEVICE_BIND_BY_EQUIPMENT_ID,
    COUNT_DEVICES_BY_EQUIPMENT_ID,
    SELECT_DEVICES_PAGE_BY_EQUIPMENT_ID,
    SELECT_ALL_DEVICES,
    SELECT_DEVICE_BY_ID,
    UPDATE_DEVICE,
    DELETE_DEVICE,
};

