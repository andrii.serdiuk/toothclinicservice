const SELECT_ALL_DOCTORS = `SELECT *
                            FROM doctor;`;

const SELECT_BY_PERSONAL_NUMBER = `SELECT *
                                   FROM doctor
                                   WHERE "doctorId" = $1;`;
const SELECT_DOCTORS_FROM_PAGE = `
    SELECT *
    FROM doctor
    ORDER BY "secondName", "firstName", "lastName"
        LIMIT $1
    OFFSET $2;
`;
const SELECT_DOCTORS_FROM_PAGE_WITH_WORKING_HOURS_BOUNDARIES = `
    SELECT *
    FROM doctor d
    WHERE EXISTS(
        SELECT *
        FROM doctor_full_schedule
        WHERE "doctorId" = d."doctorId"
            AND "scheduleDate" BETWEEN COALESCE($1, "scheduleDate") AND COALESCE($2, "scheduleDate")
            AND (COALESCE($3, "beginTime") BETWEEN "beginTime" AND "endTime")
            AND (COALESCE($4, "beginTime") BETWEEN "beginTime" AND "endTime")
        ) 
        AND NOT EXISTS(
            SELECT *
            FROM doctor_scheduled_appointment_patient
            WHERE "doctorId" = d."doctorId"
              AND "appointmentDate" BETWEEN COALESCE($1, "appointmentDate") AND COALESCE($2, "appointmentDate")
                AND ("startTime" BETWEEN COALESCE($3, "startTime") AND COALESCE($4, "startTime") OR
                "approximateEndTime" BETWEEN COALESCE($3, "approximateEndTime") AND COALESCE($4, "approximateEndTime"))
              AND "appointmentStatus" <> 'cancelled'
        )
    ORDER BY "secondName", "firstName", "lastName"
    LIMIT $5
        OFFSET $6;
`;
const COUNT_DOCTORS_FROM_PAGE_WITH_WORKING_HOURS_BOUNDARIES = `
    SELECT COUNT(*)
    FROM doctor d
    WHERE EXISTS(
            SELECT *
            FROM doctor_full_schedule
            WHERE "doctorId" = d."doctorId"
              AND "scheduleDate" BETWEEN COALESCE($1, "scheduleDate") AND COALESCE($2, "scheduleDate")
              AND (COALESCE($3, "beginTime") BETWEEN "beginTime" AND "endTime")
              AND (COALESCE($4, "beginTime") BETWEEN "beginTime" AND "endTime")
        )
      AND NOT EXISTS(
            SELECT *
            FROM doctor_scheduled_appointment_patient
            WHERE "doctorId" = d."doctorId"
              AND "appointmentDate" BETWEEN COALESCE($1, "appointmentDate") AND COALESCE($2, "appointmentDate")
              AND ("startTime" BETWEEN COALESCE($3, "startTime") AND COALESCE($4, "startTime") OR
                   "approximateEndTime" BETWEEN COALESCE($3, "approximateEndTime") AND COALESCE($4, "approximateEndTime"))
              AND "appointmentStatus" <> 'cancelled'
        );
`;
const CREATE_DOCTOR = `INSERT INTO doctor("doctorId", "accountIdFk", "firstName", "secondName",
                                          "lastName", "birthday", "experience", "phoneNumber", "doctorCategory")
                       VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9);`;

const UPDATE_DOCTOR = `UPDATE doctor
                       SET "doctorId"       = COALESCE($2, "doctorId"),
                           "accountIdFk"    = COALESCE($3, "accountIdFk"),
                           "firstName"      = COALESCE($4, "firstName"),
                           "secondName"     = COALESCE($5, "secondName"),
                           "lastName"       = COALESCE($6, "lastName"),
                           "birthday"       = COALESCE($7, "birthday"),
                           "experience"     = COALESCE($8, "experience"),
                           "phoneNumber"    = COALESCE($9, "phoneNumber"),
                           "doctorCategory" = COALESCE($10, "doctorCategory")
                       WHERE "doctorId" = $1;
`;

const DELETE_DOCTOR = `
    DELETE
    FROM doctor d
    WHERE "doctorId" = $1
      AND NOT EXISTS(
            SELECT *
            FROM scheduled_appointment sa
            WHERE sa."doctorId" = d."doctorId"
        )
        RETURNING *;
`;

const GET_DOCTOR_ACCOUNT_ID = `
    SELECT "accountIdFk"
    FROM doctor
    WHERE "doctorId" = $1;
`;

const GET_DOCTOR_BY_ACCOUNT_ID = `
    SELECT "doctorId",
           "accountEmail",
           "firstName",
           "secondName",
           "lastName",
           "birthday",
           "experience",
           "phoneNumber",
           "doctorCategory"
    FROM doctor
             INNER JOIN account a on a."accountId" = doctor."accountIdFk"
    WHERE "accountIdFk" = $1;
    ;`;

const ADD_DOCTOR_ACCOUNT_ID = `
    UPDATE doctor
    SET "accountIdFk" = $2
    WHERE "doctorId" = $1
      AND "accountIdFk" IS NULL;
`;

const SELECT_DOCTOR_SCHEDULE = `
    SELECT *
    FROM doctor_full_schedule
    WHERE "doctorId" = $1
      AND "scheduleDate" BETWEEN $2 AND $3
    ORDER BY "scheduleDate", "beginTime", "endTime";
`;

const SELECT_DOCTOR_PATIENTS = `
    SELECT "patientCardId", "surname", "firstName", "birthday"
    FROM doctor_scheduled_appointment_patient
    WHERE "doctorId" = $1;
`;

const SELECT_DOCTOR_SCHEDULED_APPOINTMENTS = `
    SELECT "appointmentId",
           "appointmentDate",
           "appointmentStatus",
           "patientCardId",
           "surname",
           "firstName",
           "birthday"
    FROM doctor_scheduled_appointment_patient
    WHERE "doctorId" = $1;
`;

const SELECT_DOCTOR_ID_BY_SCHEDULE_ID = `
    SELECT "doctorIdFk"
    FROM doctor_schedule
    WHERE "scheduleId" = $1
    ;`;


// If the specified time period already exists, the table is not updated and the id of
// this period which is in the table is returned. Otherwise, the specified time period
// is created and the new id is returned
const CREATE_HOUR_PERIOD = `
    INSERT INTO doctor_working_hours("beginTime", "endTime")
    VALUES ($1, $2) ON CONFLICT("beginTime", "endTime") DO
    UPDATE SET "beginTime" = excluded."beginTime"
        RETURNING "workingHoursId";
`;

// If the specified date has been already created, this query just returns its id.
// Otherwise, the corresponding date is created and its id is returned.
const CREATE_SCHEDULE_DAY = `
    INSERT INTO doctor_schedule("doctorIdFk", "scheduleDate")
    VALUES ($1, $2) ON CONFLICT("doctorIdFk", "scheduleDate") DO
    UPDATE SET "doctorIdFk" = excluded."doctorIdFk"
        RETURNING "scheduleId";
`;

const BIND_DOCTOR_SCHEDULE_WORKING_HOURS = `
    INSERT INTO doctor_schedule_working_hours("workingHoursIdFk", "scheduleIdFk")
    VALUES ($1, $2) ON CONFLICT("workingHoursIdFk", "scheduleIdFk") DO NOTHING;
`;

const SELECT_CONFLICTING_TIME_PERIODS = `
    SELECT *
    FROM doctor_full_schedule
    WHERE "doctorId" = $1
      AND "scheduleDate" = $2
      AND (("beginTime" > $3 AND "beginTime" < $4) OR ("endTime" > $3 AND "endTime" < $4));
`;

const SELECT_CONFLICTING_TIME_PERIODS_EXCLUDE_SPECIFIED = `
    SELECT *
    FROM doctor
             INNER JOIN doctor_schedule ds on doctor."doctorId" = ds."doctorIdFk"
             INNER JOIN doctor_schedule_working_hours dswh on ds."scheduleId" = dswh."scheduleIdFk"
             INNER JOIN doctor_working_hours dwh on dswh."workingHoursIdFk" = dwh."workingHoursId"
    WHERE "scheduleId" = $1
      AND "workingHoursId" != $2
      AND
        (("beginTime"
        > $3
      AND "beginTime"
        < $4)
       OR ("endTime"
        > $3
      AND "endTime"
        < $4));
`;

const SELECT_WORKING_PERIOD = `
    SELECT "beginTime", "endTime"
    FROM doctor_working_hours
    WHERE "workingHoursId" = $1;
`;

const UPDATE_WORKING_PERIOD = `
    UPDATE doctor_working_hours
    SET "beginTime" = $2,
        "endTime"   = $3
    WHERE "workingHoursId" = $1;
`;

const SELECT_APPOINTMENTS_DURING_WORKING_PERIOD = `
    SELECT *
    FROM doctor_schedule ds
             INNER JOIN scheduled_appointment sa ON ds."doctorIdFk" = sa."doctorId"
    WHERE "scheduleId" = $1
      AND sa."startTime" >= $2
      AND sa."approximateEndTime" <= $3
      AND sa."appointmentStatus" != 'cancelled';
`;

const SELECT_NOT_CANCELED_APPOINTMENTS_NOT_DURING_NEW_WORKING_PERIOD_AND_DURING_OLD_WORKING_PERIOD = `
    SELECT *
    FROM doctor_schedule ds
             INNER JOIN scheduled_appointment sa ON ds."doctorIdFk" = sa."doctorId"
    WHERE "scheduleId" = $1
      AND (sa."startTime" < $2 OR sa."approximateEndTime" > $3)
      AND (sa."startTime" >= $4 AND sa."approximateEndTime" <= $5)
      AND "appointmentStatus" != 'cancelled';
    ;`;

const DELETE_WORKING_PERIOD = `
    DELETE
    FROM doctor_working_hours
    WHERE "workingHoursId" = $1;
    ;`;

const DELETE_SCHEDULE_DAY_BY_ID = `
    DELETE
    FROM doctor_schedule
    WHERE "scheduleId" = $1;
`;

const DELETE_DOCTOR_SCHEDULE_DAY_BY_PERSONAL_NUMBER_AND_DATE = `
    DELETE
    FROM doctor_schedule
    WHERE "doctorIdFk" = $1
      AND "scheduleDate" = $2;
`;

const DELETE_DOCTOR_SCHEDULE_DAY_BY_SCHEDULE_ID = `
    DELETE
    FROM doctor_schedule
    WHERE "scheduleId" = $1;
`;

const DELETE_DOCTOR_SCHEDULE_DAY_BY_DOCTOR_ID_AND_DATE = `
    DELETE
    FROM doctor_schedule
    WHERE "doctorIdFk" = $1
      AND "scheduleDate" = $2;
`;

const DELETE_BIND_WORKING_HOURS_SCHEDULE_DAY_BY_DOCTOR_ID_AND_DAY = `
    DELETE
    FROM doctor_schedule_working_hours
    WHERE "workingHoursIdFk" = $1
      AND "scheduleIdFk" = (SELECT "scheduleId"
                            FROM doctor_schedule
                            WHERE "doctorIdFk" = $2
                              AND "scheduleDate" = $3);
`;

const DELETE_BIND_WORKING_HOURS_SCHEDULE_DAY_BY_SCHEDULE_ID = `
    DELETE
    FROM doctor_schedule_working_hours
    WHERE "workingHoursIdFk" = $1
      AND "scheduleIdFk" = $2;
`;

const SELECT_DOCTOR_BY_ACCOUNT_ID = `
    SELECT "doctorId",
           "firstName",
           "secondName",
           "lastName",
           birthday,
           experience,
           "phoneNumber",
           "doctorCategory",
           "accountEmail"
    FROM doctor
             FULL JOIN account a ON doctor."accountIdFk" = a."accountId"
    WHERE "doctorId" = $1;
`;

const COUNT_DOCTORS = `
    SELECT COUNT(*)
    FROM doctor;
`

const COUNT_DOCTORS_BY_ID = `
    SELECT COUNT(*)
    FROM doctor
    WHERE "doctorId" = $1;
`;

const COUNT_WORKING_HOURS_BY_DOCTOR_ID_DATE_AND_TIME_BOUNDARIES = `
    SELECT COUNT(*)
    FROM doctor_full_schedule
    WHERE "doctorId" = $1
      AND "scheduleDate" = $2
      AND ($3 BETWEEN "beginTime" AND "endTime")
      AND ($4 BETWEEN "beginTime" AND "endTime");
`;

const SELECT_DOCTOR_PATIENTS_FROM_PAGE = `
    WITH filtered AS (
        SELECT DISTINCT "patientCardId",
                        "firstName",
                        "surname",
                        "lastName",
                        "birthday",
                        "phoneNumber",
                        MAX("appointmentDate") as "lastAppointmentDate"
        FROM doctor_scheduled_appointment_patient
        WHERE "doctorId" = $1
        GROUP BY "patientCardId", "firstName", "surname", "lastName", "birthday", "phoneNumber"
    )
    SELECT *
    FROM (
             TABLE filtered
                 ORDER BY "lastAppointmentDate" DESC
                 LIMIT $2
                 OFFSET $3
         ) limited
             RIGHT JOIN (SELECT COUNT(*) FROM filtered) count("generalCount")
    ON true;
`;

const SELECT_DOCTOR_APPOINTMENTS_FROM_PAGE = `
    WITH filtered AS (
        SELECT "appointmentDate",
               "appointmentId",
               "appointmentStatus",
               "firstName",
               "surname",
               "lastName",
               sa."patientCardId"
        FROM scheduled_appointment sa
                 INNER JOIN patient p ON sa."patientCardId" = p."patientCardId"
        WHERE sa."doctorId" = $1
    )
    SELECT *
    FROM (
             TABLE filtered
                 ORDER BY "appointmentDate" DESC
                 LIMIT $2
                 OFFSET $3
         ) limited
             RIGHT JOIN (SELECT COUNT(*) FROM filtered) count("generalCount")
    ON true;
`;

module.exports = {
    SELECT_CONFLICTING_TIME_PERIODS,
    SELECT_NOT_CANCELED_APPOINTMENTS_NOT_DURING_NEW_WORKING_PERIOD_AND_DURING_OLD_WORKING_PERIOD,
    SELECT_WORKING_PERIOD,
    SELECT_APPOINTMENTS_DURING_WORKING_PERIOD,
    DELETE_WORKING_PERIOD,
    UPDATE_WORKING_PERIOD,
    SELECT_CONFLICTING_TIME_PERIODS_EXCLUDE_SPECIFIED,
    SELECT_DOCTOR_ID_BY_SCHEDULE_ID,
    CREATE_DOCTOR,
    CREATE_HOUR_PERIOD,
    CREATE_SCHEDULE_DAY,
    BIND_DOCTOR_SCHEDULE_WORKING_HOURS,
    SELECT_ALL_DOCTORS,
    SELECT_BY_PERSONAL_NUMBER,
    SELECT_DOCTORS_FROM_PAGE,
    SELECT_DOCTOR_SCHEDULE,
    SELECT_DOCTOR_PATIENTS,
    SELECT_DOCTOR_SCHEDULED_APPOINTMENTS,
    UPDATE_DOCTOR,
    DELETE_SCHEDULE_DAY_BY_ID,
    DELETE_DOCTOR_SCHEDULE_DAY_BY_PERSONAL_NUMBER_AND_DATE,
    DELETE_DOCTOR_SCHEDULE_DAY_BY_SCHEDULE_ID,
    DELETE_DOCTOR_SCHEDULE_DAY_BY_DOCTOR_ID_AND_DATE,
    DELETE_BIND_WORKING_HOURS_SCHEDULE_DAY_BY_DOCTOR_ID_AND_DAY,
    DELETE_BIND_WORKING_HOURS_SCHEDULE_DAY_BY_SCHEDULE_ID,
    COUNT_DOCTORS,
    GET_DOCTOR_BY_ACCOUNT_ID,
    SELECT_DOCTOR_BY_ACCOUNT_ID,
    DELETE_DOCTOR,
    COUNT_DOCTORS_BY_ID,
    COUNT_WORKING_HOURS_BY_DOCTOR_ID_DATE_AND_TIME_BOUNDARIES,
    GET_DOCTOR_ACCOUNT_ID,
    ADD_DOCTOR_ACCOUNT_ID,
    SELECT_DOCTOR_PATIENTS_FROM_PAGE,
    SELECT_DOCTOR_APPOINTMENTS_FROM_PAGE,
    SELECT_DOCTORS_FROM_PAGE_WITH_WORKING_HOURS_BOUNDARIES,
    COUNT_DOCTORS_FROM_PAGE_WITH_WORKING_HOURS_BOUNDARIES,
};
