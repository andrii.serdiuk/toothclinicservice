const CREATE_SCHEDULED_APPOINTMENT = `
INSERT INTO scheduled_appointment("doctorId", "patientCardId", "appointmentDate",
                                  "startTime", "approximateEndTime")
VALUES ($1, $2, $3, $4, $5)
RETURNING "appointmentId";
`;

//What happens if there is already a finished appointment with the specified scheduled appointment?
const CREATE_FINISHED_APPOINTMENT = `
    INSERT INTO finished_appointment("scheduledAppointmentId", "diagnosis", "appointmentDate", "appointmentStartTime",
                                     "appointmentEndTime", "patientState", "treatment")
    VALUES ($1, $2, $3, $4, $5, $6, $7)
`;

const SET_APPOINTMENT_STATUS_FINISHED = `
    UPDATE scheduled_appointment
    SET "appointmentStatus" = 'finished'
    WHERE "appointmentId" = $1;
`;

const SET_APPOINTMENT_STATUS_CANCELLED = `
    UPDATE scheduled_appointment
    SET "appointmentStatus" = 'cancelled'
    WHERE "appointmentId" = $1;
`;

const SELECT_ALL_SCHEDULED_APPOINTMENTS = `
    SELECT *
    FROM scheduled_appointment
    WHERE "appointmentDate" BETWEEN $1 AND $2
`;

const SELECT_APPOINTMENT_BY_ID = `
    SELECT *
    FROM scheduled_finished_appointment_patient_doctor
    WHERE "appointmentId" = $1;
`;

const SELECT_SCHEDULED_APPOINTMENTS_BY_DOCTOR_ID = `
    SELECT *
    FROM doctor_scheduled_appointment_patient
    WHERE "doctorId" = $1
      AND "appointmentDate" BETWEEN $2 AND $3;
`;

const SELECT_SCHEDULED_APPOINTMENTS_BY_DOCTOR_ID_FOR_SCHEDULE = `
    SELECT "appointmentId", "appointmentDate", "startTime" AS "beginTime", "approximateEndTime"
    FROM doctor_scheduled_appointment_patient
    WHERE "doctorId" = $1
      AND "appointmentDate" BETWEEN $2 AND $3
      AND "appointmentStatus" <> 'canceled'
    ORDER BY "appointmentDate", "beginTime", "approximateEndTime";
`;

const SELECT_SCHEDULED_APPOINTMENTS_BY_DOCTOR_ID_PAGINATED = `
    SELECT *
    FROM doctor_scheduled_appointment_patient
    WHERE "appointmentId" = $1
      AND "appointmentDate" BETWEEN $2 AND $3
    LIMIT $4 OFFSET $5;
`;

// count all appointments for doctor on date within time boundaries
const COUNT_SCHEDULED_APPOINTMENTS_BY_DOCTOR_ID_AND_DATE_TIME_BOUNDARIES = `
    SELECT COUNT(*)
    FROM doctor_scheduled_appointment_patient
    WHERE "doctorId" = $1
      AND "appointmentDate" = $2
      AND ("startTime" BETWEEN $3 AND $4 OR
           "approximateEndTime" BETWEEN $3 AND $4);
`;

const COUNT_SCHEDULED_APPOINTMENTS_BY_PATIENT_ID_AND_DATE_TIME_BOUNDARIES = `
    SELECT COUNT(*)
    FROM doctor_scheduled_appointment_patient
    WHERE "patientCardId" = $1 AND "appointmentDate" = $2 AND 
          ("startTime" > $3 AND "startTime" < $4 OR 
           "approximateEndTime" > $3 AND "approximateEndTime" < $4);
`;

const GET_PERFORMED_PROCEDURE_DURING_APPOINTMENT = `
    SELECT pad_full."procedureId", pad_full."procedureName", pad_full."fullPrice", pad_full."discount" * 100
    FROM pad_full
    WHERE "performedProcedureId" = $1;
`;

const GET_EQUIPMENT_AND_DEVICE_DURING_APPOINTMENT = `
    SELECT equipment."equipmentName", equipment."equipmentId", pad_full."inventoryNumber"
    FROM pad_full
             INNER JOIN equipment ON pad_full."equipmentIdFK" = equipment."equipmentId"
    WHERE "performedProcedureId" = $1;
`;

const GET_DOCTOR_ACCOUNT_ID_BY_APPOINTMENT_ID = `
    SELECT a."accountId"
    FROM     scheduled_appointment sa
             INNER JOIN doctor d on sa."doctorId" = d."doctorId"
             INNER JOIN account a on d."accountIdFk" = a."accountId"
    WHERE sa."appointmentId" = $1;
`;

const GET_PATIENT_ACCOUNT_ID_BY_APPOINTMENT_ID = `
    SELECT a."accountId"
    FROM     scheduled_appointment sa
             INNER JOIN patient p on sa."patientCardId" = p."patientCardId"
             INNER JOIN account a on p."accountIdFK" = a."accountId"
    WHERE sa."appointmentId" = $1;
`;

const DELETE_PERFORMED_PROCEDURE_BIND = `
    DELETE
    FROM procedure_appointment
    WHERE "performedProcedureId" = $1;
`;

const GET_PROCEDURES_FOR_SPECIFIED_CATEGORY = `
    SELECT "procedureId", "procedureName"
    FROM procedure
    WHERE procedure."minimalCategory" <= $1
      AND NOT EXISTS(
            SELECT *
            FROM equipment
            WHERE EXISTS(
                    SELECT *
                    FROM procedures_equipment pe
                    WHERE procedure."procedureId" = pe."procedureIdFK"
                      AND equipment."equipmentId" = pe."equipmentIdFK"
                )
              AND NOT EXISTS(
                    SELECT *
                    FROM device
                    WHERE device."equipmentIdFK" = equipment."equipmentId"
                )
        )
    ORDER BY "procedureName";
    ;`;

const CREATE_PERFORMED_PROCEDURE = `
    INSERT INTO procedure_appointment("procedureId", "appointmentId", "fullPrice", "discount")
    VALUES ($1, $2, $3, $4)
    RETURNING "performedProcedureId"
    ;`;

const SELECT_FINISHED_APPOINTMENT_ID_BY_SCHEDULED_APPOINTMENT = `
    SELECT "finishedAppointmentId"
    FROM finished_appointment
    WHERE "scheduledAppointmentId" = $1
    ;`;

const BIND_DEVICE_TO_PERFORMED_PROCEDURE = `
    INSERT INTO device_appointment("deviceIdFk", "performedProcedureIdFk")
    VALUES ($1, $2);
`;


const GET_EQUIPMENT_AND_DEVICES_FOR_PROCEDURE = `
    SELECT "equipmentName", "equipmentId", array_agg("inventoryNumber") AS "inventoryNumberList"
    FROM equipment
             INNER JOIN procedures_equipment pe on equipment."equipmentId" = pe."equipmentIdFK"
             INNER JOIN procedure p on p."procedureId" = pe."procedureIdFK"
             INNER JOIN device d on equipment."equipmentId" = d."equipmentIdFK"
    WHERE "procedureId" = $1
    GROUP BY "equipmentName", "equipmentId";
`;

const GET_PROCEDURE_DATA = `
    SELECT "currentPrice", "minimalCategory"
    FROM procedure
    WHERE "procedureId" = $1;
`;

const UPDATE_PERFORMED_PROCEDURE_DISCOUNT = `
    UPDATE procedure_appointment
    SET "discount" = $1
    WHERE "performedProcedureId" = $2;
`;

const UPDATE_DEVICE_APPOINTMENT = `
    UPDATE device_appointment
    SET "deviceIdFk" = $3
    WHERE "performedProcedureIdFk" = $1
      AND "deviceIdFk" = $2;
`;

const UPDATE_APPOINTMENT_INFO = `
    UPDATE finished_appointment
    SET "appointmentDate"      = $2,
        "diagnosis"            = $3,
        "patientState"         = $4,
        "treatment"            = $5,
        "appointmentStartTime" = $6,
        "appointmentEndTime"   = $7
    WHERE "scheduledAppointmentId" = $1;
    ;`;

const SELECT_ALL_APPOINTMENTS_FILTERED = `
    WITH filtered AS (
        SELECT DISTINCT "appointmentId",
                        p."patientCardId",
                        p."firstName" as "patientFirstName",
                        p."surname" as "patientSecondName",
                        p."lastName" as "patientLastName",
                        d."doctorId",
                        d."firstName" as "doctorFirstName",
                        d."secondName" as "doctorSecondName",
                        d."lastName" as "doctorLastName",
                        "appointmentDate",
                        "appointmentStatus"
        FROM scheduled_appointment sa
                 INNER JOIN doctor d on d."doctorId" = sa."doctorId"
                 INNER JOIN patient p on sa."patientCardId" = p."patientCardId"
        WHERE sa."doctorId" = COALESCE($1, sa."doctorId")
          AND sa."patientCardId" = COALESCE($2, sa."patientCardId")
          AND "appointmentDate" >= COALESCE($3, "appointmentDate")
          AND "appointmentDate" <= COALESCE($4, "appointmentDate")
          AND "appointmentStatus" = COALESCE($5, "appointmentStatus")
    )
    SELECT *
    FROM (
             TABLE filtered
                 ORDER BY "appointmentDate" DESC
                 LIMIT $6
                 OFFSET $7
         ) limited
             RIGHT JOIN (SELECT COUNT(*) FROM filtered) count("generalCount")
                        ON true;
`;

module.exports = {
    BIND_DEVICE_TO_PERFORMED_PROCEDURE,
    GET_PATIENT_ACCOUNT_ID_BY_APPOINTMENT_ID,
    SET_APPOINTMENT_STATUS_FINISHED,
    SET_APPOINTMENT_STATUS_CANCELLED,
    SELECT_FINISHED_APPOINTMENT_ID_BY_SCHEDULED_APPOINTMENT,
    UPDATE_APPOINTMENT_INFO,
    UPDATE_DEVICE_APPOINTMENT,
    UPDATE_PERFORMED_PROCEDURE_DISCOUNT,
    GET_PROCEDURES_FOR_SPECIFIED_CATEGORY,
    GET_PROCEDURE_DATA,
    GET_EQUIPMENT_AND_DEVICES_FOR_PROCEDURE,
    CREATE_SCHEDULED_APPOINTMENT,
    CREATE_PERFORMED_PROCEDURE,
    CREATE_FINISHED_APPOINTMENT,
    SELECT_APPOINTMENT_BY_ID,
    SELECT_SCHEDULED_APPOINTMENTS_BY_DOCTOR_ID,
    SELECT_SCHEDULED_APPOINTMENTS_BY_DOCTOR_ID_PAGINATED,
    SELECT_SCHEDULED_APPOINTMENTS_SHORT_BY_DOCTOR_ID: SELECT_SCHEDULED_APPOINTMENTS_BY_DOCTOR_ID_FOR_SCHEDULE,
    GET_PERFORMED_PROCEDURE_DURING_APPOINTMENT,
    GET_EQUIPMENT_AND_DEVICE_DURING_APPOINTMENT,
    GET_DOCTOR_ACCOUNT_ID_BY_APPOINTMENT_ID,
    DELETE_PERFORMED_PROCEDURE_BIND,
    COUNT_SCHEDULED_APPOINTMENTS_BY_DOCTOR_ID_AND_DATE_TIME_BOUNDARIES,
    SELECT_ALL_APPOINTMENTS_FILTERED,
    COUNT_SCHEDULED_APPOINTMENTS_BY_PATIENT_ID_AND_DATE_TIME_BOUNDARIES
};
