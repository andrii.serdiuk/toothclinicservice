const {RoleEnum} = require("../../utils");

const SELECT_RECEPTIONISTS_BY_PAGE = `
    WITH filtered AS (
        SELECT "accountId", "accountEmail"
        FROM account
        WHERE "accountRole" = ${`'${RoleEnum.REGISTRANT.toLowerCase()}'`}
    )
    SELECT *
    FROM (
             TABLE filtered
                 ORDER BY "accountId"
                 LIMIT $1
                 OFFSET $2
         ) limited
             RIGHT JOIN (SELECT COUNT(*) FROM filtered) count("generalCount") ON true;
`;

module.exports = {
    SELECT_RECEPTIONISTS_BY_PAGE
};