// 2.1
// how many times each procedure was performed within last month
// and how much income it gave
const EVERY_PROCEDURE_COUNT_AND_INCOME_WITHIN_A_MONTH = `
    SELECT p."procedureId",
           p."procedureName",
           COALESCE("TimesUsed", 0)          AS "timesUsed",
           COALESCE("TotalIncome", 0::money) AS "totalIncome"
    FROM procedure p
             LEFT OUTER JOIN procedures_report pr
                             ON p."procedureId" = pr."procedureId"
    ORDER BY "procedureName"
    LIMIT $1 OFFSET $2;
`;

// 2.2
// for every equipment count how many times its devices were used within a month
const EVERY_EQUIPMENT_COUNT_AND_INCOME_WITHIN_A_MONTH = `
    SELECT e."equipmentId",
           e."equipmentName",
           COALESCE("TimesUsed", 0)          AS "timesUsed",
           COALESCE("TotalIncome", 0::money) AS "totalIncome"
    FROM equipment e
             LEFT OUTER JOIN equipment_report er
                             ON e."equipmentId" = er."equipmentId"
    ORDER BY "equipmentName"
    LIMIT $1 OFFSET $2;
`;

// 3.2
const WORKAHOLICS = `
    SELECT d."doctorId", d."firstName", d."secondName", d."lastName"
    FROM doctor d
    WHERE EXISTS(
            SELECT *
            FROM doctor_schedule
            WHERE "doctorIdFk" = d."doctorId"
              AND "scheduleDate" BETWEEN $3 AND $4
        )
      AND NOT EXISTS(
            SELECT *
            FROM doctor_schedule ds
            WHERE ds."doctorIdFk" = d."doctorId"
              AND ds."scheduleDate" BETWEEN $3 AND $4
              AND NOT EXISTS(
                    SELECT *
                    FROM doctor_scheduled_appointment_patient dsap
                    WHERE d."doctorId" = dsap."doctorId"
                      AND dsap."appointmentDate" = ds."scheduleDate"
                )
        )
`;

const COUNT_WORKAHOLICS = `
    SELECT COUNT(*)
    FROM doctor d
    WHERE EXISTS(
            SELECT *
            FROM doctor_schedule
            WHERE "doctorIdFk" = d."doctorId"
              AND "scheduleDate" BETWEEN $1 AND $2
        )
      AND NOT EXISTS(
            SELECT *
            FROM doctor_schedule ds
            WHERE ds."doctorIdFk" = d."doctorId"
              AND ds."scheduleDate" BETWEEN $1 AND $2
              AND NOT EXISTS(
                    SELECT *
                    FROM doctor_scheduled_appointment_patient dsap
                    WHERE d."doctorId" = dsap."doctorId"
                      AND dsap."appointmentDate" = ds."scheduleDate"
                )
        )
`;

const WORKAHOLICS_WITH_PAGINATION = `
    ${WORKAHOLICS}
    ORDER BY "secondName", "firstName", "lastName"
    LIMIT $1
    OFFSET $2;
`;

// Serdiuk Andrii

const COUNT_DOCTOR_WORKING_PERIODS_NEXT_WEEK = `
    SELECT COUNT(*) AS count
    FROM doctor_full_schedule
    WHERE "doctorId" = $1 AND "scheduleDate" BETWEEN CURRENT_DATE AND (CURRENT_DATE + 7);
`;

const GET_DOCTOR_WORKING_TIME_NEXT_WEEK = `
    SELECT doctor."doctorId", "scheduleDate", "beginTime", "endTime", "scheduleId", "workingHoursId"
    FROM ((doctor JOIN doctor_schedule ds on doctor."doctorId" = ds."doctorIdFk")
        JOIN doctor_schedule_working_hours ON "scheduleId" = doctor_schedule_working_hours."scheduleIdFk")
              JOIN doctor_working_hours ON "workingHoursIdFk" = doctor_working_hours."workingHoursId"
    WHERE "doctorId" = $1
      AND "scheduleDate" BETWEEN CURRENT_DATE AND (CURRENT_DATE + 7)
    ORDER BY "scheduleDate", "beginTime"
    LIMIT $2
    OFFSET $3
;
`;

const COUNT_UNIQUE_PATIENTS_FOR_EVERY_DOCTOR = `
    SELECT doctor."doctorId", CONCAT(doctor."lastName", ' ',  doctor."firstName", ' ', doctor."secondName") AS name,
           COUNT(DISTINCT patient."patientCardId") AS patientsAmount
    FROM ((patient INNER JOIN scheduled_appointment ON patient."patientCardId" = scheduled_appointment."patientCardId")
        INNER JOIN (SELECT *
                    FROM finished_appointment
                    WHERE "appointmentDate" BETWEEN $1 AND $2) fa
        ON scheduled_appointment."appointmentId" = fa."scheduledAppointmentId")
             RIGHT JOIN doctor ON scheduled_appointment."doctorId" = doctor."doctorId"
    GROUP BY doctor."doctorId"
    ORDER BY patientsAmount DESC, name
    LIMIT $3
    OFFSET $4;
`;

const SELECT_EVERY_DOCTOR_BENEFIT_FOR_COMPANY = `
    SELECT doctor."doctorId",
           CONCAT(doctor."lastName", ' ', doctor."firstName", ' ', doctor."secondName")
                            AS name,
           SUM("fullPrice") AS benefit
    FROM ((doctor INNER JOIN scheduled_appointment sa on doctor."doctorId" = sa."doctorId")
        INNER JOIN finished_appointment fa ON sa."appointmentId" = fa."scheduledAppointmentId")
             INNER JOIN procedure_appointment pa ON fa."finishedAppointmentId" = pa."appointmentId"
    GROUP BY doctor."doctorId", doctor."lastName", doctor."firstName", doctor."secondName"
    ORDER BY benefit DESC
    LIMIT $1
    OFFSET $2;
`;

const SELECT_PROCEDURES_THAT_ALL_PATIENTS_FROM_SPECIFIED_DAY_UNDERWENT_ON_THIS_DAY = `
    SELECT *
    FROM procedure A
    WHERE EXISTS(
                  SELECT *
                  FROM patient_appointment
                  WHERE patient_appointment."appointmentDate" = CURRENT_DATE
              ) AND NOT EXISTS(
            SELECT *
            FROM patient B
            WHERE EXISTS(
                    SELECT *
                    FROM patient_appointment
                    WHERE B."patientCardId" = patient_appointment."patientCardId"
                      AND patient_appointment."appointmentDate" = CURRENT_DATE
                )
              AND NOT EXISTS(
                    SELECT *
                    FROM patient_appointment pa
                             INNER JOIN procedure_appointment pad
                                        ON pa."finishedAppointmentId" = pad."appointmentId"
                    WHERE B."patientCardId" = pa."patientCardId"
                      AND A."procedureId" = pad."procedureId"
                      AND pa."appointmentDate" = CURRENT_DATE
                )
        ) 
    ORDER BY "procedureName"
    LIMIT $1
    OFFSET $2;
`;

const SELECT_AVERAGE_PROCEDURES_AGE = `
    SELECT procedure."procedureId",
           procedure."procedureName",
           COALESCE(AVG(date_part('year', CURRENT_DATE) - date_part('year', "birthday")), 0) as "averageYears"
    FROM procedure
             LEFT JOIN procedure_appointment pad on procedure."procedureId" = pad."procedureId"
             LEFT JOIN patient_appointment pa on pad."appointmentId" = pa."finishedAppointmentId"
             LEFT JOIN patient p on pa."patientCardId" = p."patientCardId"
    GROUP BY procedure."procedureId", procedure."procedureName"
    ORDER BY "procedureName"
    LIMIT $1 OFFSET $2;
`;


//TODO check this query
const SELECT_EQUIPMENT_POPULARITY = `
    SELECT "equipmentId", "equipmentName", COUNT(*) as "timesUsed"
    FROM equipment
             LEFT JOIN device d on equipment."equipmentId" = d."equipmentIdFK"
             LEFT JOIN device_appointment da on da."deviceIdFk" = d."inventoryNumber"
    GROUP BY "equipmentId", "equipmentName"
    ORDER BY "equipmentId"
    LIMIT $1 OFFSET $2;
`;

//TODO check this query
const SELECT_PROCEDURES_WITH_ALL_DEVICES = `
    SELECT *
    FROM procedure AS p
    WHERE NOT EXISTS(
            SELECT *
            FROM equipment AS e
            WHERE EXISTS(SELECT *
                         FROM procedures_equipment as pe
                         WHERE pe."procedureIdFK" = p."procedureId"
                           AND pe."equipmentIdFK" = e."equipmentId")
              --Вибираю девайси, що не використовувалися
              AND EXISTS(
                    SELECT *
                    FROM device AS d
                    WHERE d."equipmentIdFK" = e."equipmentId"
                      AND NOT EXISTS(
                            SELECT *
                            FROM procedure_appointment pad INNER JOIN device_appointment da 
                                ON pad."performedProcedureId" = da."performedProcedureIdFk"
                            WHERE pad."procedureId" = p."procedureId"
                              AND da."deviceIdFk" = d."inventoryNumber"
                        )
                )
        )
    ORDER BY "procedureId"
    LIMIT $1 OFFSET $2;
`;

const COUNT_PATIENT_FINISHED_APPOINTMENTS = `
    SELECT COUNT(*) as "appointmentsNumber"
    FROM (
             SELECT fa."finishedAppointmentId",
                    fa."appointmentDate",
                    fa."diagnosis",
                    fa."appointmentStartTime",
                    fa."appointmentEndTime",
                    fa."patientState",
                    fa."treatment",
                    COUNT(DISTINCT "deviceIdFk") as "devices_used"
             FROM finished_appointment fa
                      INNER JOIN scheduled_appointment sa ON sa."appointmentId" = fa."scheduledAppointmentId"
                      LEFT OUTER JOIN procedure_appointment pad
                                 ON fa."finishedAppointmentId" = pad."appointmentId"
                      LEFT OUTER JOIN device_appointment da on pad."performedProcedureId" = da."performedProcedureIdFk"
             WHERE "patientCardId" = $1
             GROUP BY "finishedAppointmentId"
         ) as "count"
`;

const SELECT_PATIENT_FINISHED_APPOINTMENTS = `
    SELECT fa."finishedAppointmentId" as "appointmentId",
           fa."appointmentDate",
           fa."diagnosis",
           fa."appointmentStartTime",
           fa."appointmentEndTime",
           fa."patientState",
           fa."treatment",
           COUNT(DISTINCT "deviceIdFk") as "devices_used"
    FROM finished_appointment fa
             INNER JOIN scheduled_appointment sa ON sa."appointmentId" = fa."scheduledAppointmentId"
             LEFT OUTER JOIN procedure_appointment pad
                        ON fa."finishedAppointmentId" = pad."appointmentId"
             LEFT OUTER JOIN device_appointment da on pad."performedProcedureId" = da."performedProcedureIdFk"
    WHERE "patientCardId" = $3
    GROUP BY "finishedAppointmentId", fa."appointmentDate", "appointmentStartTime", "appointmentEndTime"
    ORDER BY "appointmentDate", "appointmentStartTime", "appointmentEndTime"
    LIMIT $1 OFFSET $2
`;


module.exports = {
    COUNT_DOCTOR_WORKING_PERIODS_NEXT_WEEK,
    EVERY_PROCEDURE_COUNT_AND_INCOME_WITHIN_A_MONTH,
    WORKAHOLICS,
    COUNT_WORKAHOLICS,
    WORKAHOLICS_WITH_PAGINATION,
    GET_DOCTOR_WORKING_TIME_NEXT_WEEK,
    COUNT_UNIQUE_PATIENTS_FOR_EVERY_DOCTOR,
    SELECT_EVERY_DOCTOR_BENEFIT_FOR_COMPANY,
    SELECT_PROCEDURES_THAT_ALL_PATIENTS_FROM_SPECIFIED_DAY_UNDERWENT_ON_THIS_DAY,
    SELECT_AVERAGE_PROCEDURES_AGE,
    SELECT_EQUIPMENT_POPULARITY,
    SELECT_PROCEDURES_WITH_ALL_DEVICES,
    COUNT_PATIENT_FINISHED_APPOINTMENTS,
    SELECT_PATIENT_FINISHED_APPOINTMENTS,
    EVERY_EQUIPMENT_COUNT_AND_INCOME_WITHIN_A_MONTH,
};
