const doctorQueries = require('./doctor');
const receptionistQueries = require('./receptionist');
const deviceQueries = require('./device');
const equipmentQueries = require('./equipment');
const procedureQueries = require('./procedure');
const reportQueries = require('./report');

module.exports = {
    ...doctorQueries,
    ...receptionistQueries,
    ...deviceQueries,
    ...equipmentQueries,
    ...procedureQueries,
    ...reportQueries
};