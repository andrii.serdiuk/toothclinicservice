const {
    COUNT_PROCEDURES,
    SELECT_ALL_PROCEDURES,
    SELECT_PROCEDURES_FROM_PAGE,
    SELECT_PROCEDURE_BY_ID, SELECT_PROCEDURE_EQUIPMENT, UPDATE_PROCEDURE, CLEAR_PROCEDURE_EQUIPMENTS,
    ADD_EQUIPMENT_TO_PROCEDURE, CREATE_PROCEDURE, DELETE_PROCEDURE
} = require("./queries");
const connection = require("./connection");

class ProcedureRepository {
    async countProcedures() {
        const {rows} = await connection.query(COUNT_PROCEDURES);
        return rows[0].count;
    }

    async getAllProcedures(limit, offset) {
        const {rows} = await connection.query(SELECT_PROCEDURES_FROM_PAGE, [limit, offset]);
        return rows;
    }

    async getProcedureById(id) {
        const {rows} = await connection.query(SELECT_PROCEDURE_BY_ID, [id]);
        return rows[0];
    }

    async getEquipmentForProcedure(id) {
        const {rows} = await connection.query(SELECT_PROCEDURE_EQUIPMENT, [id]);
        return rows;
    }

    async updateProcedure(previousId,
                          {
                              procedureId,
                              procedureName,
                              procedurePrice,
                              procedureMinimalCategory,
                              equipment
                          }) {
        try {
            await connection.query('BEGIN');
            const updatedProcedureId = (await connection.query(UPDATE_PROCEDURE, [previousId, procedureId, procedureName, procedurePrice, procedureMinimalCategory])).rows[0].procedureId;
            await connection.query(CLEAR_PROCEDURE_EQUIPMENTS, [updatedProcedureId]);
            for (const item of equipment) {
                await connection.query(ADD_EQUIPMENT_TO_PROCEDURE, [updatedProcedureId, item.equipmentId]);
            }
            const res = await connection.query('COMMIT');
            return res;
        } catch (e) {
            await connection.query('ROLLBACK');
            throw e;
        }
    }

    async addProcedure({
                           procedureName,
                           procedurePrice,
                           procedureMinimalCategory,
                           equipment
                       }) {
        try {
            await connection.query('BEGIN');
            const procedureId = (await connection.query(CREATE_PROCEDURE, [procedureName, procedurePrice, procedureMinimalCategory])).rows[0].procedureId;
            for (const item of equipment) {
                await connection.query(ADD_EQUIPMENT_TO_PROCEDURE, [procedureId, item.equipmentId]);
            }
            await connection.query('COMMIT');
            return procedureId;
        } catch (e) {
            await connection.query('ROLLBACK');
            throw e;
        }
    }

    async deleteProcedure(id) {
        return (await connection.query(DELETE_PROCEDURE, [id])).rows[0];
    }
}

module.exports = new ProcedureRepository();
