const connection = require('./connection');
const {AuthError} = require("../utils");
const {
    SELECT_DOCTOR_SCHEDULE,
    SELECT_DOCTOR_BY_ACCOUNT_ID,
    CREATE_DOCTOR,
    COUNT_DOCTORS,
    SELECT_DOCTORS_FROM_PAGE
} = require("./queries/doctor");
const {UPDATE_DOCTOR, DELETE_DOCTOR, GET_DOCTOR_ACCOUNT_ID, ADD_DOCTOR_ACCOUNT_ID, COUNT_DOCTORS_BY_ID,
    COUNT_WORKING_HOURS_BY_DOCTOR_ID_DATE_AND_TIME_BOUNDARIES, SELECT_DOCTOR_PATIENTS_FROM_PAGE,
    GET_DOCTOR_BY_ACCOUNT_ID, CREATE_SCHEDULE_DAY, BIND_DOCTOR_SCHEDULE_WORKING_HOURS, CREATE_HOUR_PERIOD,
    SELECT_CONFLICTING_TIME_PERIODS, SELECT_DOCTOR_ID_BY_SCHEDULE_ID, SELECT_CONFLICTING_TIME_PERIODS_EXCLUDE_SPECIFIED,
    UPDATE_WORKING_PERIOD, DELETE_WORKING_PERIOD, SELECT_APPOINTMENTS_DURING_WORKING_PERIOD, SELECT_WORKING_PERIOD,
    SELECT_NOT_CANCELED_APPOINTMENTS_NOT_DURING_NEW_WORKING_PERIOD_AND_DURING_OLD_WORKING_PERIOD,
    SELECT_DOCTOR_APPOINTMENTS_FROM_PAGE, SELECT_ALL_DOCTORS, SELECT_DOCTORS_FROM_PAGE_WITH_WORKING_HOURS_BOUNDARIES,
    COUNT_DOCTORS_FROM_PAGE_WITH_WORKING_HOURS_BOUNDARIES
} = require("./queries");
const accountRepository = require("./accountRepository");
const e = require("express");

class DoctorRepository {

    //Probably should be removed
    async getAll() {
        return await connection.query("SELECT * FROM account");
    }

    async addOne({
                     doctorId, accountId, name, surname, middleName,
                     birthday, experience, category, phone
                 }) {
        return await connection.query(CREATE_DOCTOR, [doctorId, accountId, name, middleName, surname,
            birthday, experience, phone, category]);
    }

    async getOneByAccountId(accountId) {
        const {rows} = await connection.query(SELECT_DOCTOR_BY_ACCOUNT_ID, [accountId]);
        return rows[0] || null;
    }

    async getDoctorsNumber() {
        return await connection.query(COUNT_DOCTORS);
    }

    async getDoctorsWithOffset(limit, offset) {
        return await connection.query(SELECT_DOCTORS_FROM_PAGE, [limit, offset]);
    }

    async getDoctorPatientsWithOffset(id, limit, offset){
        return await connection.query(SELECT_DOCTOR_PATIENTS_FROM_PAGE, [id, limit, offset]);
    }

    async getDoctorAppointmentsWithOffset(id, limit, offset){
        return await connection.query(SELECT_DOCTOR_APPOINTMENTS_FROM_PAGE, [id, limit, offset]);
    }

    async getDoctorSchedule(doctorId, dateFrom, dateTo) {
        const {rows} = await connection.query(SELECT_DOCTOR_SCHEDULE, [doctorId, dateFrom, dateTo]);
        return rows;
    }

    async createWorkingPeriod(doctorId, date, startTime, endTime) {
        const conflicts = await connection.query(SELECT_CONFLICTING_TIME_PERIODS, [doctorId, date, startTime, endTime]);
        if(conflicts.rows.length != 0) {
            throw new Error("Working period intersection");
        }
        const scheduleDayId = (await connection.query(CREATE_SCHEDULE_DAY, [doctorId, date])).rows[0].scheduleId;
        const workingHoursId = (await connection.query(CREATE_HOUR_PERIOD, [startTime, endTime])).rows[0].workingHoursId;
        return await connection.query(BIND_DOCTOR_SCHEDULE_WORKING_HOURS, [workingHoursId, scheduleDayId]);
    }

    async updateWorkingPeriod(scheduleId, workingHoursId, beginTime, endTime) {

        const conflicts = (await connection.query(SELECT_CONFLICTING_TIME_PERIODS_EXCLUDE_SPECIFIED,
            [scheduleId, workingHoursId, beginTime, endTime])).rows;
        if(conflicts.length != 0) {
            throw new Error("Working period intersection");
        }

        const previousValues = (await connection.query(SELECT_WORKING_PERIOD, [workingHoursId])).rows[0];
        const oldBeginTime = previousValues.beginTime;
        const oldEndTime = previousValues.endTime;

        const appointments =
            (await connection.query(SELECT_NOT_CANCELED_APPOINTMENTS_NOT_DURING_NEW_WORKING_PERIOD_AND_DURING_OLD_WORKING_PERIOD,
                [scheduleId, beginTime, endTime, oldBeginTime, oldEndTime])).rows;
       if (appointments.length != 0) {
           throw new Error("Contradiction with the scheduled appointments");
       }
        return await connection.query(UPDATE_WORKING_PERIOD, [workingHoursId, beginTime, endTime]);
    }

    async deleteWorkingPeriod(scheduleId, workingHoursId) {
        const doctorId = (await connection.query(SELECT_DOCTOR_ID_BY_SCHEDULE_ID, [scheduleId])).rows[0].doctorIdFk;
        console.log(doctorId);
        const {beginTime, endTime} = (await connection.query(SELECT_WORKING_PERIOD, [workingHoursId])).rows[0];
        console.log(beginTime, ' ', endTime);
        const appointments =
            (await connection.query(SELECT_APPOINTMENTS_DURING_WORKING_PERIOD, [scheduleId, beginTime, endTime])).rows;
        console.log(appointments);
        if (appointments.length > 0) {
            throw new Error("There is an appointment in the specified working period");
        }
        return await connection.query(DELETE_WORKING_PERIOD, [workingHoursId])
    }

    async getDoctorByAccountId(accountId) {
        const {rows} = await connection.query(GET_DOCTOR_BY_ACCOUNT_ID, [accountId]);
        return rows[0];
    }

    async updateDoctorById(previousId, doctorId,
                           accountIdFk, firstName,
                           secondName, lastName,
                           birthday, experience,
                           phoneNumber, doctorCategory) {
        return await connection.query(UPDATE_DOCTOR, [
            previousId, doctorId, accountIdFk, firstName,
            secondName, lastName, birthday, experience,
            phoneNumber, doctorCategory
        ]);
    }

    async updateDoctorAccountByDoctorId(id, login, password) {
        const {rows} = await connection.query(GET_DOCTOR_ACCOUNT_ID, [id]);
        return await accountRepository.updateUser(rows[0].accountIdFk, login, password);
    }

    async deleteDoctorById(id) {
        try {
            await connection.query('BEGIN');
            const res = (await connection.query(DELETE_DOCTOR, [id])).rows[0];
            if (res) {
                await accountRepository.removeUser(res.accountIdFk);
            }
            await connection.query('COMMIT');
            return res;
        } catch (e) {
            await connection.query('ROLLBACK');
            throw e;
        }
    }

    async deleteAccountByDoctorId(id) {
        try {
            await connection.query('BEGIN');
            const accountIdFk = (await connection.query(GET_DOCTOR_ACCOUNT_ID, [id])).rows[0].accountIdFk;
            const res = await accountRepository.removeUser(accountIdFk);
            await connection.query('COMMIT');
            return res;
        } catch (e) {
            await connection.query('ROLLBACK');
            throw e;
        }
    }

    async addDoctorAccount(doctorId, login, password) {
        try {
            await connection.query('BEGIN');
            const accountId = (await accountRepository.addUser(login, password, 'doctor')).rows[0].accountId;
            const res = await connection.query(ADD_DOCTOR_ACCOUNT_ID, [doctorId, accountId]);
            await connection.query('COMMIT');
            return res;
        } catch (e) {
            await connection.query('ROLLBACK');
            throw e;
        }
    }

    async existsById(id) {
        const {rows} = await connection.query(COUNT_DOCTORS_BY_ID, [id]);
        return !!Number.parseInt(rows[0]?.count);
    }

    async existWorkingHours(doctorId, date, beginTime, endTime) {
        const {rows} = await connection.query(COUNT_WORKING_HOURS_BY_DOCTOR_ID_DATE_AND_TIME_BOUNDARIES,
            [doctorId, date, beginTime, endTime]);
        return !!Number.parseInt(rows[0]?.count);
    }

    async getAllDoctors(){
        const {rows} = await connection.query(SELECT_ALL_DOCTORS);
        return rows;
    }

    async getDoctorsWithWorkingHoursBoundaries(dateFrom, dateTo, timeFrom, timeTo, limit, page) {
        const { rows } = await connection.query(SELECT_DOCTORS_FROM_PAGE_WITH_WORKING_HOURS_BOUNDARIES, [dateFrom, dateTo, timeFrom, timeTo, limit, page]);
        return rows;
    }

    async countDoctorsWithWorkingHoursBoundaries(dateFrom, dateTo, timeFrom, timeTo) {
        const {rows} = await connection.query(COUNT_DOCTORS_FROM_PAGE_WITH_WORKING_HOURS_BOUNDARIES,
            [dateFrom, dateTo, timeFrom, timeTo]);
        return rows[0]?.count;
    }
}

module.exports = new DoctorRepository();
