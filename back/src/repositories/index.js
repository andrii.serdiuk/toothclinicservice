const doctorRepository = require('./doctorRepository');
const patientRepository = require('./patientRepository');
const receptionistRepository = require('./receptionistRepository');
const accountRepository = require('./accountRepository');
const appointmentRepository = require('./appointmentRepository');
const equipmentRepository = require('./equipmentRepository');
const procedureRepository = require('./procedureRepository');
const reportRepository = require('./reportRepository');
const deviceRepository = require('./deviceRepository');
const connection = require('./connection');
const commonRepository = require('./commonRepository');


module.exports = {
    doctorRepository,
    patientRepository,
    receptionistRepository,
    accountRepository,
    appointmentRepository,
    equipmentRepository,
    procedureRepository,
    reportRepository,
    deviceRepository,
    connection,
    commonRepository
};
