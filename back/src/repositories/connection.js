// Getting access to the configuration file
const config = require('config');

//Reading the necessary data to connect to the database
const herokuUser = config.get("heroku.user");
const herokuPassword = config.get("heroku.password");
const herokuHost = config.get("heroku.host");
const herokuPort = config.get("heroku.port");
const herokuDatabase = config.get("heroku.database");

//Setting up the connection to the heroku database
const pg = require('pg');
const Client = pg.Client;

//Disable automatic date parsing
pg.types.setTypeParser(1082, str => str);

const connection = new Client({
    user: herokuUser,
    password: herokuPassword,
    host: herokuHost,
    port: herokuPort,
    database: herokuDatabase,
    dialect: "postgres",
    ssl: {
        require: true,
        rejectUnauthorized: false
    }
});

connection.connect();

module.exports = connection;

// Remove the "finally" clause if you don't want to close the connection
// connection.connect()
//     .then(() => console.log("Connected to the database successfully"))
//     .then(() => connection.query("SELECT * FROM account"))
//     .then(results => console.table(results.rows))
//     .catch(e => console.log(e))
//     .finally(() => connection.end().
//     then(() => console.log("Connection has been closed")));
