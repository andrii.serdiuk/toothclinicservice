const connection = require('./connection');

class CommonRepository {
    async transaction(queriesFunction) {
        try {
            await connection.query('BEGIN');
            const res = await queriesFunction();
            await connection.query('COMMIT');
            return res;
        } catch (e) {
            await connection.query('ROLLBACK');
            throw e;
        }
    }
}

module.exports = new CommonRepository();