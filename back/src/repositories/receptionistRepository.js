const connection = require("./connection");
const {SELECT_RECEPTIONISTS_BY_PAGE} = require("./queries");

class ReceptionistRepository {
    async getReceptionistsByPage(limit, offset) {
        const {rows} = await connection.query(SELECT_RECEPTIONISTS_BY_PAGE, [limit, offset]);
        return rows;
    }
}

module.exports = new ReceptionistRepository();