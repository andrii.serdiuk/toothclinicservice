const {equipmentRepository, deviceRepository} = require("../repositories");

class EquipmentService {

    async countEquipment() {
        return await equipmentRepository.countEquipment();
    }

    async getEquipment() {
        return await equipmentRepository.getEquipment();
    }

    async getEquipmentPage(limit, page) {
        return await equipmentRepository.getEquipmentPage(limit, limit * page);
    }

    async getEquipmentPageAndCount(limit, page) {
        const data = await this.getEquipmentPage(limit, page);
        const count = await this.countEquipment();
        return {equipment: data, count: count};
    }

    async getDevicesPageAndCountById(equipmentId, limit, page) {
        const count = await this.getDevicesCountById(equipmentId);
        const data = await this.getDevicesPageById(equipmentId, limit, page);
        return {data: data, count: count};
    }

    async getDevicesCountById(equipmentId) {
        return await deviceRepository.countDevicesByEquipmentId(equipmentId);
    }

    async getDevicesPageById(equipmentId, limit, page) {
        return await deviceRepository.getDevicesPageByEquipmentId(equipmentId, limit, page * limit);
    }

    async addEquipment(equipmentName) {
        return await equipmentRepository.addEquipment(equipmentName);
    }

    async getAllEquipmentDevices(equipmentId) {
        return await equipmentRepository.getAllEquipmentDevices(equipmentId);
    }

    async updateEquipment(equipmentId, equipmentName) {
        return await equipmentRepository.updateEquipment(equipmentId, equipmentName);
    }

    async deleteEquipmentById(equipmentId) {
        return await equipmentRepository.deleteEquipmentById(equipmentId);
    }

    async deleteEquipmentByName(equipmentName) {
        return await equipmentRepository.deleteEquipmentByName(equipmentName);
    }

}

module.exports = new EquipmentService();
