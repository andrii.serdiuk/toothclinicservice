const {deviceRepository} = require('../repositories');

class DeviceService {

    async addDeviceBindByEquipmentName(equipment) {
        return await deviceRepository.createDeviceBindByEquipmentName(equipment);
    }

    async addDeviceBindByEquipmentId(inventoryNumber, price, receiptDate, equipmentId) {
        return await deviceRepository.createDeviceBindByEquipmentId(inventoryNumber, price, receiptDate, equipmentId);
    }

    async updateDevice(inventoryNumber, receiptDate, price, oldInventoryNumber) {
        //Coalesce works correctly with undefined values
        return await deviceRepository.updateDevice(inventoryNumber, receiptDate, price, oldInventoryNumber);
    }

    async getDeviceById(id) {
        return await deviceRepository.getOneById(id);
    }

    async deleteDevice(id) {
        return await deviceRepository.delete(id);
    }

}

module.exports = new DeviceService();