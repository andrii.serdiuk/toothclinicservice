const dbService = require('./dbService');
const doctorService = require('./doctorService');
const patientService = require('./patientService');
const receptionistService = require('./receptionistService');
const userService = require('./userService');
const tokenService = require('./tokenService');
const appointmentService = require('./appointmentService');
const equipmentService = require('./equipmentService');
const procedureService = require('./procedureService');
const accountService = require('./accountService');
const reportService = require('./reportService');
const deviceService = require('./deviceService');
const testDataService = require('./testDataService');
const scheduleService = require('./scheduleService');


module.exports = {
    dbService,
    doctorService,
    patientService,
    receptionistService,
    userService,
    tokenService,
    appointmentService,
    equipmentService,
    procedureService,
    accountService,
    deviceService,
    reportService,
    testDataService,
    scheduleService,
};
