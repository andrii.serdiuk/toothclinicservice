const {toDayDate, ValidationError} = require("../utils");
const {appointmentRepository} = require("../repositories");
const doctorService = require("./doctorService");
const patientService = require("./patientService");

class AppointmentService {
    async getAllByDoctorIdPaginated(doctorId, dateFrom, dateTo, limit, offset) {
        return await appointmentRepository.getAllByDoctorIdPaginated(doctorId, dateFrom, dateTo, offset, limit);
    }

    async getAllByDoctorId(doctorId, dateFrom, dateTo) {
        return await appointmentRepository.getAllByDoctorId(doctorId, dateFrom, dateTo);
    }

    async getAllByDoctorIdForSchedule(doctorId, dateFrom, dateTo) {
        return await appointmentRepository.getAllByDoctorIdForSchedule(doctorId, dateFrom, dateTo);
    }

    async getProceduresDoneDuringAppointment(appointmentId) {
        return await appointmentRepository.getProceduresDoneDuringAppointment(appointmentId);
    }

    async getPerformedProcedure(performedProcedureId) {
        const procedure = await appointmentRepository.getPerformedProcedureDuringAppointment(performedProcedureId);
        procedure.equipment = await this.getEquipmentAndDeviceForPerformedProcedure(performedProcedureId);
        //console.log("Service: ", procedure);
        return procedure;
    }

    async getDoctorAccountIdByAppointmentId(appointmentId) {
        return await appointmentRepository.getDoctorAccountIdByAppointmentId(appointmentId);
    }

    async getPatientAccountIdByAppointmentId(appointmentId) {
        return await appointmentRepository.getPatientAccountIdByAppointmentId(appointmentId);
    }

    async getOneById(appointmentId) {
        const appointment = await appointmentRepository.getOneById(appointmentId)
            // filtering null values, in case appointment is not finished
            .then(res => Object.fromEntries(
                Object.entries(res).filter(([key, value]) => value != null)
            ));
        const parsedAppointment = {
            patient: {},
            doctor: {},
        };

        Object.entries(appointment)
            .forEach(([key, value]) => {
                if (key.startsWith("patient")) parsedAppointment.patient[key] = value;
                else if (key.startsWith("doctor")) parsedAppointment.doctor[key] = value;
                else parsedAppointment[key] = value;
            });
        const pBirthday = parsedAppointment.patient.patientBirthday;
        const dBirthday = parsedAppointment.doctor.doctorBirthday;
        delete parsedAppointment.doctor.doctorBirthday;
        delete parsedAppointment.patient.patientBirthday;

        // parsedAppointment.patient.birthday = `${pBirthday.getDate()}.${pBirthday.getMonth()}.${pBirthday.getYear()}`;
        parsedAppointment.patient.birthday = pBirthday;

        parsedAppointment.doctor.birthday = dBirthday;
        const doctorAccountId = await this.getDoctorAccountIdByAppointmentId(appointmentId);
        const patientAccountId = await this.getPatientAccountIdByAppointmentId(appointmentId);

        if (doctorAccountId !== undefined) {
            parsedAppointment.doctor.accountId = doctorAccountId.accountId;
        }

        if(patientAccountId !== undefined) {
            parsedAppointment.patient.accountId = patientAccountId.accountId;
        }

        return parsedAppointment;
    }

    async existsByPatientIdDateAndTime(patientCardId, date, beginTime, endTime) {
        return await appointmentRepository.existsByPatientIdDateAndTimeBoundaries(patientCardId, date, beginTime, endTime);
    }

    async existsByDoctorIdDateAndTime(doctorId, date, beginTime, endTime) {
        return await appointmentRepository.existsByDoctorIdDateAndTime(doctorId, date, beginTime, endTime);
    }

    async createAppointment(doctorId, patientCardId, date, beginTime, endTime) {
        // check if doctor exists
        if (!await doctorService.existsById(doctorId)) {
            throw new ValidationError(`No doctor with id ${doctorId}`);
        }
        // check if patient exists
        if (!await patientService.existsById(patientCardId)) {
            throw new ValidationError(`No patient with id ${patientCardId}`);
        }
        const appBeginFullDate = new Date(date);
        const beginTimeSplitted = beginTime.split(':');
        appBeginFullDate.setHours(beginTimeSplitted[0]);
        appBeginFullDate.setMinutes(beginTimeSplitted[1]);
        if (appBeginFullDate < Date.now()) {
            throw new ValidationError(`date cannot be in the past ${date}`)
        }
        // check if beginTime < endTime
        if (beginTime >= endTime) {
            throw new ValidationError(`beginTime ${beginTime} cannot be more than endTime ${endTime}`)
        }
        // check if patient doesn't have appointment at that time
        if (await this.existsByPatientIdDateAndTime(patientCardId, date, beginTime, endTime)) {
            throw new ValidationError(`Patient with id ${patientCardId} has appointment on ${date} at time ${beginTime} ${endTime}`);
        }

        // check if free working hours exist
        if (!await doctorService.existWorkingHours(doctorId, date, beginTime, endTime)) {
            throw new ValidationError(`Doctor with id ${doctorId} does not work on ${date} from ${beginTime} to ${endTime}`)
        }

        // check if doctor doesn't have appointment at that time
        if (await this.existsByDoctorIdDateAndTime()) {
            throw new ValidationError(`Doctor ${doctorId} has appointment on ${date} at time ${beginTime} ${endTime}`);
        }

        return await appointmentRepository.create(doctorId, patientCardId, date, beginTime, endTime);
    }

    async getEquipmentAndDeviceForPerformedProcedure(performedProcedureId) {
        return await appointmentRepository.getEquipmentAndDeviceForPerformedProcedure(performedProcedureId);
    }

    async deletePerformedProcedure(performedProcedureId) {
        return await appointmentRepository.deletePerformedProcedure(performedProcedureId);
    }

    async getProceduresForTheSpecifiedCategory(category) {
        return await appointmentRepository.getProceduresForTheSpecifiedCategory(category);
    }

    async createPerformedProcedure(procedureId, appointmentId, devices, fullPrice, discount) {
        return await appointmentRepository.createPerformedProcedure(procedureId, appointmentId, devices, fullPrice, discount);
    }

    async getEquipmentAndDevicesForProcedure(procedureId) {
        return await appointmentRepository.getEquipmentAndDevicesForProcedure(procedureId);
    }

    async getProcedureData(procedureId) {
        return await appointmentRepository.getProcedureData(procedureId);
    }

    async updatePerformedProcedure(performedProcedureId, data) {
        return await appointmentRepository.updatePerformedProcedure(performedProcedureId, data);
    }

    async updateAppointmentInfo(appointmentId, date, diagnosis, patientState, treatment, beginTime, endTime) {
        return await appointmentRepository.updateAppointmentInfo(appointmentId, date, diagnosis, patientState, treatment, beginTime, endTime);
    }

    async createFinishedAppointment(scheduledAppointmentId, data) {
        await appointmentRepository.createFinishedAppointment(scheduledAppointmentId, data);
        return await appointmentRepository.setAppointmentFinished(scheduledAppointmentId);
    }

    async cancelAppointment(appointmentId) {
        await appointmentRepository.setAppointmentCancelled(appointmentId);
    }

    async getAppointments(limit, page, filter) {
        return await appointmentRepository.getFilteredAppointments(limit, page, filter.doctorId, filter.patientId,
            filter.dateFrom, filter.dateTo, filter.appointmentStatus);
    }
}

module.exports = new AppointmentService();
