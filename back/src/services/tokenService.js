const jwt = require('jsonwebtoken');
const { RoleEnum } = require('../utils');

class TokenService {
    static async generateJwt(accountId, role) {
        return jwt.sign(
            { accountId, role },
            process.env.JWT_SECRET,
            { expiresIn: '24h' }
        );
    }

    async createToken({ accountId, role }) {
        if (!Object.values(RoleEnum).includes(role))
            throw new Error(`Invalid value for user role: ${role}`);

        return await TokenService.generateJwt(accountId, role);
    }
}

module.exports = new TokenService();
