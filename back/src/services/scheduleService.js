const {doctorRepository} = require("../repositories");
const appointmentService = require("./appointmentService");
const {InternalServerError, SchedulePeriodTypeEnum, splitInto45MinPeriods} = require("../utils");

class ScheduleService {
    // returns full schedule list with appointments on according days
    async getFullSchedule(doctorId, dateFrom, dateTo) {
        const schedule = await doctorRepository.getDoctorSchedule(doctorId, dateFrom, dateTo);

        const appointments = await appointmentService.getAllByDoctorIdForSchedule(doctorId, dateFrom, dateTo);

        return await this.mergeScheduleAndAppointments(schedule, appointments, { full: true });
    }

    async getShortSchedule(doctorId, dateFrom, dateTo) {
        let schedule = await doctorRepository.getDoctorSchedule(doctorId, dateFrom, dateTo);

        const appointments = await appointmentService.getAllByDoctorIdForSchedule(doctorId, dateFrom, dateTo);

        return await this.mergeScheduleAndAppointments(schedule, appointments);
    }

    async getShortScheduleForPatient(doctorId, dateFrom, dateTo) {
        let schedule = await doctorRepository.getDoctorSchedule(doctorId, dateFrom, dateTo);

        const appointments = await appointmentService.getAllByDoctorIdForSchedule(doctorId, dateFrom, dateTo);

        return await this.mergeScheduleAndAppointments(schedule, appointments, { splitFree: true });
    }

    async parseSchedule(schedule) {
        let nextScheduleDay;
        const parsedSchedule = [];

        schedule.forEach(({ scheduleDate, beginTime, endTime }) => {
            if (scheduleDate.getTime() === nextScheduleDay?.date.getTime()) {
                nextScheduleDay.workingHours.push({ beginTime, endTime });
                return;
            }

            if (nextScheduleDay) parsedSchedule.push(nextScheduleDay);
            nextScheduleDay = {
                date: scheduleDate,
                workingHours: [
                    { beginTime, endTime }
                ]
            };
        });

        return parsedSchedule;
    }

    // choose whether to pass parsed
    // schedule - sorted list of [date, beginTime, endTime]
    // appointments - sorted list of [date, beginTime, endTime]
    // full - true in order to include appointments info
    async mergeScheduleAndAppointments(schedule, appointments, options = {}) {
        const { splitFree, full} = options;
        let scheduleIndex = 0;
        let appointmentsIndex = 0;
        const mergedSchedule = [];

        // to save current date
        let mergedDay;
        let daySchedule;

        while (scheduleIndex < schedule.length) {
            // as schedule working periods must include appointments time boundaries
            const { scheduleDate, beginTime : workingPeriodBegin, endTime: workingPeriodEnd } = schedule[scheduleIndex];
            if (scheduleDate !== mergedDay?.date) {
                if (mergedDay) {
                    mergedDay.daySchedule = daySchedule;
                    mergedSchedule.push(mergedDay);
                    const mergedDayD = new Date(mergedDay.date);
                    mergedDayD.setDate(mergedDayD.getDate() + 1);
                    const scheduleD = new Date(scheduleDate);
                    while (mergedDayD.getDate() !== scheduleD.getDate()) {
                        mergedSchedule.push({
                            date: mergedDayD.toDateString().substring(4, 15)
                        });
                        mergedDayD.setDate(mergedDayD.getDate() + 1);
                    }
                }
                mergedDay = { date: scheduleDate };
                daySchedule = [];
            }

            const partialDaySchedule = [];
            let lastFreeTime = workingPeriodBegin;

            // date is the same and begin in boundaries of working period
            // parsing work period and all appointments "in it"
            if (appointments[appointmentsIndex] && appointments[appointmentsIndex].appointmentDate < scheduleDate)
                throw new InternalServerError(`Appointment is on the date or time, when there are no working hours`)

            while (appointments[appointmentsIndex]?.appointmentDate === scheduleDate &&
            appointments[appointmentsIndex].beginTime < workingPeriodEnd) {
                const { appointmentId, beginTime : appointmentBeginTime, approximateEndTime : appointmentEndTime } = appointments[appointmentsIndex];

                // appointment "breaks" working periods, should never happen
                if (appointmentEndTime > workingPeriodEnd) {
                    throw new InternalServerError(`Appointment is on the edge of working periods`);
                }

                if (lastFreeTime !== appointmentBeginTime) {
                    // pushing part of working period [from last free time to appointment begin time]
                    if (splitFree) {
                        partialDaySchedule.push(...splitInto45MinPeriods(lastFreeTime, appointmentBeginTime));
                    } else {
                        partialDaySchedule.push({
                            type: SchedulePeriodTypeEnum.FREE,
                            beginTime: lastFreeTime,
                            endTime: appointmentBeginTime,
                        });
                    }
                }

                if (full) {
                    // pushing appointment
                    partialDaySchedule.push({
                        type: SchedulePeriodTypeEnum.OCCUPIED_BY_APPOINTMENT,
                        beginTime: appointmentBeginTime,
                        endTime: appointmentEndTime,
                        appointmentId,
                    });
                }

                // resetting last free time point
                lastFreeTime = appointmentEndTime;
                ++appointmentsIndex;
            }

            // if appointments ended for working period, but there is still free time
            if (lastFreeTime < workingPeriodEnd) {
                if (splitFree) {
                    partialDaySchedule.push(...splitInto45MinPeriods(lastFreeTime, workingPeriodEnd));
                } else {
                    partialDaySchedule.push({
                        type: SchedulePeriodTypeEnum.FREE,
                        beginTime: lastFreeTime,
                        endTime: workingPeriodEnd,
                    });
                }
            }

            daySchedule = daySchedule.concat(partialDaySchedule);
            ++scheduleIndex;
        }

        if (mergedDay) {
            mergedDay.daySchedule = daySchedule;
            mergedSchedule.push(mergedDay);
        }

        return mergedSchedule;
    }
}

module.exports = new ScheduleService();
