const {AuthError, RoleEnum, BaseError} = require('../utils');
const {accountRepository} = require('./../repositories');
const {duplicateProcessing} = require("./utils/dbError");

class UserService {
    async login(login, password) {
        const {rows} = await accountRepository.authenticate(login, password);
        if (rows.length === 0)
            throw new AuthError('Login or password is incorrect');

        return {
            accountId: rows[0].accountId,
            role: rows[0].accountRole.toUpperCase()
        };
    }

    async addAccount(login, password, role) {
        const resp = await accountRepository.addUser(login, password, role);
        return resp.rows[0].accountId;
    }

    async removeAccount(id) {
        try {
            await accountRepository.removeUser(id);
        } catch (e) {
            throw new BaseError('Remove account failure', 500, "Failed to remove account");
        }
    }

    async getAccount(id) {
        const result = await accountRepository.getAccount(id);
        console.log(id);
        console.log(result);
        return result;
    }
}

module.exports = new UserService;
