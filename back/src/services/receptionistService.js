const {receptionistRepository} = require("../repositories");
const userService = require("./userService");
const accountService = require("./accountService");

class ReceptionistService {
    async getReceptionistsByPage(limit, page) {
        const rows = await receptionistRepository.getReceptionistsByPage(limit, limit * page);
        if (parseInt(rows[0].generalCount) === 0) {
            return {count: 0, receptionists: []}
        }
        return {
            count: rows[0].generalCount,
            receptionists: rows.map(item => {
                delete item.generalCount;
                return item;
            })
        }
    }

    async addReceptionist(receptionist) {
        return await userService.addAccount(receptionist.accountEmail, receptionist.password, 'registrant');
    }

    async editReceptionist(id, receptionist) {
        return await accountService.updateAccount(id, receptionist);
    }

    async deleteReceptionist(id) {
        return await userService.removeAccount(id);
    }
}

module.exports = new ReceptionistService();