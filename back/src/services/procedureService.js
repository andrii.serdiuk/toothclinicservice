const {procedureRepository} = require("../repositories");

class ProcedureService {
    async countProcedures() {
        return await procedureRepository.countProcedures();
    }

    async getProceduresByPage(limit, page) {
        return await procedureRepository.getAllProcedures(limit, limit * page);
    }

    async getProcedureById(id) {
        return await procedureRepository.getProcedureById(id);
    }

    async getEquipmentForProcedure(id) {
        return await procedureRepository.getEquipmentForProcedure(id);
    }

    async updateProcedure(previousId, procedure) {
        return await procedureRepository.updateProcedure(previousId, procedure);
    }

    async addProcedure(procedure) {
        return await procedureRepository.addProcedure(procedure);
    }

    async deleteProcedure(id) {
        return await procedureRepository.deleteProcedure(id);
    }
}

module.exports = new ProcedureService();
