const {reportRepository} = require("../repositories");

class ReportService {
    async getProceduresReport(limit, offset) {
        return await reportRepository.getProceduresReport(limit, offset);
    }

    async getEquipmentReport(limit, offset) {
        return await reportRepository.getEquipmentReport(limit, offset);
    }

    async getWorkaholics(limit, offset, dateFrom, dateTo) {
        return await reportRepository.getWorkaholics(limit, offset, dateFrom, dateTo);
    }

    async countWorkaholics(dateFrom, dateTo) {
        return await reportRepository.countWorkaholics(dateFrom, dateTo);
    }

    async getDoctorScheduleNextWeek(id, itemsOnPage, pageCount) {
        const rows = await reportRepository.getDoctorWorkingTime(id, itemsOnPage, itemsOnPage * pageCount);
        const count = (await reportRepository.countDoctorWorkingPeriods(id)).rows[0].count;
        return {
            rows: rows,
            count: count
        };
    }

    async getUniquePatientsAmountForEveryDoctor(dateFrom, dateTo, page, limit) {
        return await reportRepository.getUniquePatientsAmountForEveryDoctor(dateFrom, dateTo, limit, page * limit);
    }

    async getDoctorBenefitForCompany(page, limit) {
        return await reportRepository.getBenefitOfEveryDoctor(limit, page * limit);
    }

    async getProceduresThatAllPatientsThisDayUnderwent(page, limit) {
        return await reportRepository.getImportantProcedures(limit, page * limit);
    }

    async getAverageProceduresAge(limit, page){
        return await reportRepository.getAverageProceduresAge(limit, limit*page);
    }

    async getEquipmentPopularity(limit, page){
        return await reportRepository.getEquipmentPopularity(limit, limit * page);
    }

    async getProceduresWithAllDevices(limit, page){
        return await reportRepository.getProceduresWithAllDevices(limit, limit * page);
    }

    async getPatientFinishedAppointments(limit, offset, id){
        return await reportRepository.getPatientFinishedAppointments(limit, offset, id);;
    }
}

module.exports = new ReportService();
