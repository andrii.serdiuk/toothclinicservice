const { connection } = require('../repositories');
const {
    CREATE_APPLICATION_STATUS_ENUM,
    CREATE_ROLE_ENUM,
    CREATE_TABLE_ACCOUNT,
    CREATE_TABLE_DOCTOR,
    CREATE_TABLE_DOCTOR_SCHEDULE,
    CREATE_TABLE_DOCTOR_WORKING_HOURS,
    CREATE_TABLE_DOCTOR_SCHEDULE_WORKING_HOURS,
    CREATE_TABLE_PATIENT,
    CREATE_TABLE_DEVICE,
    CREATE_TABLE_EQUIPMENT,
    CREATE_TABLE_PROCEDURE,
    CREATE_TABLE_PROCEDURE_EQUIPMENT,
    CREATE_TABLE_SCHEDULED_APPOINTMENT,
    CREATE_TABLE_DOCTOR_APPOINTMENT,
    /*CREATE_TABLE_PROCEDURE_APPOINTMENT_DEVICE,*/
    CREATE_DOCTOR_FULL_SCHEDULE_VIEW, CREATE_DOCTOR_SCHEDULED_APPOINTMENT_PATIENT_VIEW
} = require('../repositories/schema');


class DBService {

    async init() {
        await this.createEnums()
        await this.createTables();
        await this.createViews();
    }

    async createTables() {
        await connection.query(CREATE_TABLE_ACCOUNT);
        await connection.query(CREATE_TABLE_DOCTOR);
        await connection.query(CREATE_TABLE_DOCTOR_SCHEDULE);
        await connection.query(CREATE_TABLE_DOCTOR_WORKING_HOURS);
        await connection.query(CREATE_TABLE_DOCTOR_SCHEDULE_WORKING_HOURS);
        await connection.query(CREATE_TABLE_PATIENT);
        await connection.query(CREATE_TABLE_EQUIPMENT);
        await connection.query(CREATE_TABLE_DEVICE);
        await connection.query(CREATE_TABLE_PROCEDURE);
        await connection.query(CREATE_TABLE_PROCEDURE_EQUIPMENT);
        await connection.query(CREATE_TABLE_SCHEDULED_APPOINTMENT);
        await connection.query(CREATE_TABLE_DOCTOR_APPOINTMENT);
    }

    async createEnums() {
        await connection.query(CREATE_APPLICATION_STATUS_ENUM);
        await connection.query(CREATE_ROLE_ENUM);
    }

    async createViews() {
        await connection.query(CREATE_DOCTOR_FULL_SCHEDULE_VIEW);
        await connection.query(CREATE_DOCTOR_SCHEDULED_APPOINTMENT_PATIENT_VIEW);
    }
}

module.exports = new DBService();
