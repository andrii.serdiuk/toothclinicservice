const {patientRepository, doctorRepository, commonRepository} = require('../repositories');

class PatientService {
    async getAll() {
        try {
            return await patientRepository.findAll();
        } catch (e) {
            throw new Error(`Unexpected error occurred: ${e}`)
        }
    }

    async getPatientById(id) {
        return await patientRepository.getPatientById(id);
    }

    async getPatientsNumber() {
        return await patientRepository.getPatientNumber();
    }

    async getAllPatients() {
        return await patientRepository.getAllPatients();
    }

    async getPatientsByPage(limit, page) {
        return await patientRepository.getPatientsWithOffset(limit, limit * page);
    }

    async addPatient(patient) {
        return await patientRepository.addPatientWithAccount(patient)
    }

    async updatePatientById(previousId, patient) {
        const {data} = await patientRepository.updatePatientById(previousId, patient);
        return data;
    }

    async getPatientAppointmentByAccountByPage(accountId, limit, page){
        return await commonRepository.transaction(async () => {
            const patientCardId = (await patientRepository.getPatientByAccountId(accountId)).patientCardId;
            const appointments = (await patientRepository.getPatientAppointmentsWithOffset(patientCardId, limit, limit * page)).rows;
            if (parseInt(appointments[0].generalCount) === 0) {
                return {count: 0, appointments: []};
            }
            return {
                count: appointments[0].generalCount,
                appointments: appointments.map(appointment => {
                    delete appointment.generalCount;
                    return appointment;
                })
            };
        });
    }

    async updatePatientAccountByPatientId(id, account) {
        const {data} = await patientRepository.updatePatientAccountByPatientId(id,
            account.email,
            account.password);
        return data;
    }

    async deletePatientById(id) {
        const {data} = await patientRepository.deletePatientById(id);
        return data;
    }

    async deleteAccountByPatientId(id) {
        const {data} = await patientRepository.deleteAccountByPatientId(id);
        return data;
    }

    async addPatientAccount(id, login, password) {
        const {data} = await patientRepository.addPatientAccount(id, login, password);
        return data;
    }

    async existsById(id) {
        return await patientRepository.existsById(id);
    }

    async getPatientByAccountId(id) {
        return await patientRepository.getPatientByAccountId(id);
    }

}

module.exports = new PatientService();