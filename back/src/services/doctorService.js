const {doctorRepository, commonRepository} = require('../repositories');
const {InternalServerError, ValidationError, SchedulePeriodTypeEnum} = require("../utils");
const userService = require("./userService");
const {duplicateProcessing} = require("./utils/dbError");

class DoctorService {
    async getAll() {
        try {
            return await doctorRepository.getAll();
        } catch (e) {
            throw Error(`Unexpected error occurred: ${e}`);
        }
    }

    //Create account for doctor and add doctor information to the system
    async addDoctor(doctor) {
        // Transaction required
        try {
            //TODO: Transaction and doctor's data validation before creating an account
            //Add account
            doctor.accountId = await userService.addAccount(doctor.email, doctor.password, 'doctor');
            //Add doctor
            return await doctorRepository.addOne(doctor);
        } catch (e) {
            //Remove account if it cannot create doctor
            await userService.removeAccount(doctor.accountId);
            throw e;
        }
    }

    async getDoctorsByPage(limit, page) {
        return await doctorRepository.getDoctorsWithOffset(limit, limit * page);
    }

    async getDoctorsWithWorkingHoursBoundaries(dateFrom, dateTo, timeFrom, timeTo, limit, page) {
        return await doctorRepository.getDoctorsWithWorkingHoursBoundaries(dateFrom, dateTo, timeFrom, timeTo, limit, page);
    }

    async countDoctorsWithWorkingHoursBoundaries(dateFrom, dateTo, timeFrom, timeTo) {
        return await doctorRepository.countDoctorsWithWorkingHoursBoundaries(dateFrom, dateTo, timeFrom, timeTo);
    }

    async getDoctorAppointmentByAccountByPage(accountId, limit, page) {
        return await commonRepository.transaction(async () => {
            const doctorId = (await doctorRepository.getDoctorByAccountId(accountId)).doctorId;
            const appointments = (await doctorRepository.getDoctorAppointmentsWithOffset(doctorId, limit, limit * page)).rows;
            if (parseInt(appointments[0].generalCount) === 0) {
                return {count: 0, appointments: []};
            }
            return {
                count: appointments[0].generalCount,
                appointments: appointments.map(appointment => {
                    delete appointment.generalCount;
                    return appointment;
                })
            };
        });
    }

    async getDoctorPatientsByPage(id, limit, page) {
        const {rows} = (await doctorRepository.getDoctorPatientsWithOffset(id, limit, limit * page));
        return {
            count: rows[0].generalCount,
            patients: rows.map(item => {
                delete item.generalCount;
                return item;
            })
        }
    }

    async getDoctorPatientsByAccountByPage(accountId, limit, page) {
        return await commonRepository.transaction(async () => {
            const doctorId = (await doctorRepository.getDoctorByAccountId(accountId)).doctorId;
            const patients = (await doctorRepository.getDoctorPatientsWithOffset(doctorId, limit, limit * page)).rows;
            if (parseInt(patients[0].generalCount) === 0) {
                return {count: 0, patients: []};
            }
            return {
                count: patients[0].generalCount,
                patients: patients.map(patient => {
                    delete patient.generalCount;
                    return patient;
                })
            };
        });
    }

    async getDoctorsNumber() {
        return await doctorRepository.getDoctorsNumber();
    }

    async getDoctorById(id) {
        return await doctorRepository.getOneByAccountId(id);
    }

    async getDoctorByAccountId(id) {
        return await doctorRepository.getDoctorByAccountId(id);
    }

    async updateDoctorById(previousId, doctor) {
        const {data} = await doctorRepository.updateDoctorById(
            previousId, doctor.doctorId,
            doctor.accountIdFk, doctor.firstName,
            doctor.secondName, doctor.lastName,
            doctor.birthday, doctor.experience,
            doctor.phoneNumber, doctor.doctorCategory
        );
        return data;
    }

    async updateDoctorAccountByDoctorId(id, account) {
        const {data} = await doctorRepository.updateDoctorAccountByDoctorId(id,
            account.email,
            account.password);
    }

    async updateWorkingPeriod(scheduleId, workingHoursId, beginTime, endTime) {
        return await doctorRepository.updateWorkingPeriod(scheduleId, workingHoursId, beginTime, endTime);
    }

    async deleteWorkingPeriod(scheduleId, workingHoursId) {
        return await doctorRepository.deleteWorkingPeriod(scheduleId, workingHoursId);
    }

    async deleteDoctorById(id) {
        const data = await doctorRepository.deleteDoctorById(id);
        return data;
    }

    async deleteAccountByDoctorId(id) {
        const {data} = await doctorRepository.deleteAccountByDoctorId(id);
        return data;
    }

    async addDoctorAccount(id, login, password) {
        const {data} = await doctorRepository.addDoctorAccount(id, login, password);
        return data;
    }

    async existsById(id) {
        return await doctorRepository.existsById(id);
    }

    async createWorkingPeriod(doctorId, date, beginTime, endTime) {
        return await doctorRepository.createWorkingPeriod(doctorId, date, beginTime, endTime);
    }

    async existWorkingHours(doctorId, date, beginTime, endTime) {
        return await doctorRepository.existWorkingHours(doctorId, date, beginTime, endTime);
    }

    async getAllDoctors() {
        return await doctorRepository.getAllDoctors();
    }
}

module.exports = new DoctorService();