const {accountRepository} = require("../repositories");

class AccountService {
    async getAccountIdByDoctorId(doctorId) {
        return await accountRepository.getAccountIdByDoctorId(doctorId);
    }

    async updateAccount(accountId, account){
        return await accountRepository.updateUser(accountId, account.accountEmail, account.password);
    }
}

module.exports = new AccountService();
