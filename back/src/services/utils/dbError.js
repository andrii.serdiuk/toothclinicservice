const {ValidationError, InternalServerError} = require("../../utils");

const duplicateProcessing = e => {
    if (e.routine === '_bt_check_unique') {
        return new ValidationError('Duplication error');
    }
    return new InternalServerError(e);
};

module.exports = {
    duplicateProcessing
}