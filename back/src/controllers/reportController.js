const {reportService, procedureService, equipmentService} = require("../services");
const {httpStatusCodes, parseDate, normalize_page, normalize_limit} = require("../utils");
const {validationResult} = require("express-validator");
const {getLimitAndPage} = require("./utilities");
const {duplicateProcessing} = require("../services/utils/dbError");
const {LIMIT_DEFAULT, PAGE_DEFAULT} = require("../utils").pagination;


class ReportController {

    async getProceduresReport(req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return next(errors);
        }
        try {
            let { page, limit } = req.query;

            const count = await procedureService.countProcedures();

            if (limit) {
                page = normalize_page(page);
                limit = normalize_limit(limit);
            } else {
                limit = count;
                page = 0;
            }
            const offset = page * limit;

            const proceduresReport = await reportService.getProceduresReport(limit, offset);
            return res.status(httpStatusCodes.OK).json({
                count, proceduresReport
            });
        } catch (e) {
            next(e);
        }
    }

    async getEquipmentReport(req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return next(errors);
        }
        try {
            let { page, limit } = req.query;

            const count = await equipmentService.countEquipment();

            if (limit) {
                page = normalize_page(page);
                limit = normalize_limit(limit);
            } else {
                limit = count;
                page = 0;
            }

            const offset = page * limit;

            const equipmentReport = await reportService.getEquipmentReport(limit, offset);
            return res.status(httpStatusCodes.OK).json({
                count, equipmentReport
            });
        } catch (e) {
            next(e);
        }
    }

    async getWorkaholics(req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return next(errors);
        }
        try {
            let { page, limit, dateFrom, dateTo } = req.query;

            // default is a month ago
            const monthAgo = new Date();
            monthAgo.setDate(monthAgo.getDate() - 30);
            dateFrom = parseDate(dateFrom, monthAgo);
            // default is now
            dateTo = parseDate(dateTo, new Date());

            const count = await reportService.countWorkaholics(dateFrom, dateTo);

            if (limit) {
                page = normalize_page(page);
                limit = normalize_limit(limit);
            } else {
                limit = count;
                page = 0;
            }

            const offset = page * limit;

            const workaholics = await reportService.getWorkaholics(limit, offset, dateFrom, dateTo);
            return res.status(httpStatusCodes.OK).json({
                workaholics,
                count
            });
        } catch (e) {
            next(e);
        }
    }

    async getDoctorScheduleNextWeek(req, res, next) {
        const {id, itemsOnPage, pageCount} = req.query;
        try {
            const resultData = await reportService.getDoctorScheduleNextWeek(id, itemsOnPage, pageCount);
            return res.status(200).send({ data: resultData });
        } catch (e) {
            console.log(e);
            throw e;
        } 
    }

    async getUniquePatientsAmountForEveryDoctor(req, res, next) {
        const {dateFrom, dateTo} = req.query;
        const {limit, page} = getLimitAndPage(req.query);
        try {
            const resultData = await reportService.getUniquePatientsAmountForEveryDoctor(dateFrom, dateTo, page, limit);
            return res.status(200).send({ data: resultData });
        } catch (e) {
            console.log(e);
            throw e;
        }
    }

    async getDoctorBenefitForCompany(req, res, next) {
        const {limit, page} = getLimitAndPage(req.query);
        try {
            const resultData = await reportService.getDoctorBenefitForCompany(page, limit);
            return res.status(200).send({ data: resultData });
        } catch (e) {
            console.log(e);
            throw e;
        }
    }

    async getProceduresThatAllPatientsThisDayUnderwent(req, res, next) {
        const {limit, page} = getLimitAndPage(req.query);
        try {
            const resultData = await reportService.getProceduresThatAllPatientsThisDayUnderwent(page, limit);
            return res.status(200).send({ data: resultData });
        } catch (e) {
            console.log(e);
            throw e;
        }
    }

    async getAverageProceduresAge(req, res, next){
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return next(errors);
        }
        try {
            let { page, limit } = req.query;

            const count = await procedureService.countProcedures();

            if (limit) {
                page = normalize_page(page);
                limit = normalize_limit(limit);
            } else {
                limit = count;
                page = 0;
            }

            const averageProceduresAge = await reportService.getAverageProceduresAge(limit, page);
            return res.status(httpStatusCodes.OK).json({
                count, averageProceduresAge: averageProceduresAge.rows
            });
        } catch (e) {
            next(e);
        }
    }

    async getEquipmentPopularity(req, res, next){
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return next(errors);
        }
        try {
            let { page, limit } = req.query;

            const count = await equipmentService.countEquipment();

            if (limit) {
                page = normalize_page(page);
                limit = normalize_limit(limit);
            } else {
                limit = count;
                page = 0;
            }

            const equipmentPopularity = await reportService.getEquipmentPopularity(limit, page);
            return res.status(httpStatusCodes.OK).json({
                count, equipmentPopularity: equipmentPopularity.rows
            });
        } catch (e) {
            next(e);
        }
    }

    async getProceduresWithAllDevices(req, res, next){
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return next(errors);
        }
        try {
            let { page, limit } = req.query;

            const count = (await reportService.getProceduresWithAllDevices(9999999, 0)).rows.length;

            if (limit) {
                page = normalize_page(page);
                limit = normalize_limit(limit);
            } else {
                limit = count;
                page = 0;
            }

            const proceduresWithAllDevices = await reportService.getProceduresWithAllDevices(limit, page);
            return res.status(httpStatusCodes.OK).json({
                count, proceduresWithAllDevices: proceduresWithAllDevices.rows
            });
        } catch (e) {
            next(e);
        }
    }

    async getPatientFinishedAppointments(req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return next(errors);
        }
        try {
            const {id} = req.params;
            let { page, limit } = req.query;

            page = normalize_page(page);
            limit = normalize_limit(limit);

            const proceduresWithAllDevices = await reportService.getPatientFinishedAppointments(limit, page * limit, id);
            return res.status(httpStatusCodes.OK).json(proceduresWithAllDevices);
        } catch (e) {
            next(e);
        }
    }
}

module.exports = new ReportController();
