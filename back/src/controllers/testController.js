
const {ValidationError} = require("../utils");

class TestController {
    async test(req, res) {
        return res.json({message: "Test complete"});
    }

    async error(req, res, next) {
        return next(new ValidationError('This endpoint should throw an error'));
    }
}

module.exports = new TestController();
