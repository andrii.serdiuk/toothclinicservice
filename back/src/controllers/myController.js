const {doctorRepository, appointmentRepository} = require("../repositories");
const {AuthError, httpStatusCodes, toDayDate, parseDate} = require("../utils");
const {appointmentService, patientService} = require("../services");
const {validationResult} = require("express-validator");

class MyController {
    async myAppointments(req, res, next) {
        try {
            const user = req.user;

            const doctor = await doctorRepository.getOneByAccountId(user.accountId);
            if (!doctor) {
                return next(new AuthError(`No doctor with account id: ${user.accountId}`));
            }

            // validating query string values
            let {dateFrom, dateTo, page, limit} = req.query;
            // default is "now()"
            dateFrom = parseDate(dateFrom, new Date());
            dateFrom = toDayDate(dateFrom);
            // default is "now() + 14 days"
            const inTwoWeeks = new Date();
            inTwoWeeks.setDate(inTwoWeeks.getDate() + 14);
            dateTo = dateTo = parseDate(dateTo, inTwoWeeks);
            dateTo = toDayDate(dateTo);

            page = page || 1;
            // default is "20"
            limit = limit && limit > 0 && limit < 50 ? limit : 20;
            // console.log(new Date().toDateString());
            const offset = (page - 1) * limit;

            const appointments = await appointmentService.getAllByDoctorIdPaginated(doctor.doctorId, dateFrom, dateTo, limit, offset);
            return res.status(httpStatusCodes.OK).json(appointments);
        } catch (e) {
            next(e);
        }
    }

    async createMyAppointment(req, res, next) {
        const validationErrors = validationResult(req);
        if (!validationErrors.isEmpty()) {
            return next(validationErrors);
        }
        try {
            const { accountId } = req.user;
            const { patientCardId } = await patientService.getPatientByAccountId(accountId)
            const { doctorId, date, beginTime, endTime } = req.body;

            const appointmentId = await appointmentService.createAppointment(doctorId, patientCardId, date, beginTime, endTime);

            return res.status(httpStatusCodes.OK).json({ appointmentId });
        } catch (e) {
            next(e);
        }
    }
}

module.exports = new MyController();
