const {patientService} = require("../services");
const {InternalServerError, ValidationError, httpStatusCodes} = require("../utils");
const {duplicateProcessing} = require("../services/utils/dbError");
const {LIMIT_DEFAULT, PAGE_DEFAULT} = require("../utils").pagination;

class PatientController {
    async show_patient(req, res, next) {
        const {id} = req.params;
        try {
            const patient = (await patientService.getPatientById(id));
            res.status(200).json(patient);
        } catch (e) {
            console.log(`Error has happened: ${e}`);
            next(new InternalServerError(`Error has happened while trying to get the patient(${id})`))
        }
    }

    async show_patients(req, res, next) {
        try {
            const patients = (await patientService.getAllPatients());
            res.status(200).json({
                patients: patients.map(item => {
                    item.patientCardId = item.patientCardId.trim();
                    return item;
                })
            });
        } catch (e) {
            console.log(`Error has happened: ${e}`);
            next(new InternalServerError(`Error has happened while trying to get the patient(${id})`))
        }
    }

    async show_patients_by_page(req, res, next) {
        let {limit, page} = req.query;
        try {
            const {count} = (await patientService.getPatientsNumber()).rows[0];
            limit = limit === undefined ? LIMIT_DEFAULT : limit;
            page = page === undefined || page < 1 || page > Math.ceil(count / limit) ? PAGE_DEFAULT : page;

            const patients = (await patientService.getPatientsByPage(limit, page)).rows;
            res.status(200).send({
                count: count, patients: patients.map(item => {
                    item.patientCardId = item.patientCardId.trim();
                    return item;
                })
            });
        } catch (e) {
            console.log(`Error has happened: ${e}`);
            next(new InternalServerError("Error has happened"));
        }
    }

    async add_patient(req, res, next) {
        const patient = req.body;
        //TODO: change hardcoded pass
        patient.password = '1';
        try {
            await patientService.addPatient(patient);
            return res.status(200).end();
        } catch (e) {
            console.log(e);
            next(duplicateProcessing(e));
        }
    }

    async add_patient_account(req, res, next) {
        const {id} = req.params;
        const {email, password} = req.body;
        try {
            console.log(id);
            await patientService.addPatientAccount(id, email, password);
            res.status(200).end();
        } catch (e) {
            console.log(e);
            next(duplicateProcessing(e));
        }
    }

    async update_patient(req, res, next) {
        const {id} = req.params;
        const patient = req.body;
        try {
            if (parseInt(id) !== parseInt(patient.patientId)) {
                console.log("Ids are different");
                next(new ValidationError("Ids are different"));
                return;
            }
            await patientService.updatePatientById(id, patient);
            res.status(200).end();
        } catch (e) {
            console.log(`Error has happened: ${e}`);
            next(duplicateProcessing(e));
        }
    }

    async update_patient_account(req, res, next) {
        const {id} = req.params;
        const account = req.body;
        account.email = account.email ? account.email : null;
        account.password = account.password ? account.password : null;
        try {
            const result = await patientService.updatePatientAccountByPatientId(id, account);
            res.status(200).end();
        } catch (e) {
            console.log(`Error has happened: ${e}`);
            next(duplicateProcessing(e));
        }
    }

    async delete_patient(req, res, next) {
        const {id} = req.params;
        try {
            await patientService.deletePatientById(id);
            res.status(httpStatusCodes.OK).end();
        } catch (e) {
            console.log(e);
            res.status(httpStatusCodes.BAD_REQUEST);
        }
    }

    async delete_patient_account(req, res, next) {
        const {id} = req.params;
        try {
            await patientService.deleteAccountByPatientId(id);
            res.status(200).end();
        } catch (e) {
            console.log(e);
            next(new InternalServerError(e));
        }
    }

    async getPatientByAccountId(req, res, next) {
        const {id} = req.params;
        try {
            const data = await patientService.getPatientByAccountId(id);
            res.status(200).send(data);
        } catch (e) {
            console.log(e);
            next(e);
        }
    }

}

module.exports = new PatientController();