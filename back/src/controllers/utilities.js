const {LIMIT_DEFAULT, PAGE_DEFAULT} = require("../utils").pagination;

class Utilities {

    getLimitAndPage(params) {
        let {limit, page} = params;
        if (limit === undefined) {
            limit = LIMIT_DEFAULT;
        }
        if (page === undefined || page < 1) {
            page = PAGE_DEFAULT;
        }
        return {limit, page};
    }

}

module.exports = new Utilities();