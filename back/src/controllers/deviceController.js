const {deviceService} = require('../services');
const {duplicateProcessing} = require("../services/utils/dbError");
const {InternalServerError} = require("../utils");

class DeviceController {

    async addDeviceByEquipmentName(req, res, next) {
        const equipment = req.body;
        try {
            await deviceService.addDeviceBindByEquipmentName(equipment);
            return res.status(200).end();
        } catch (e) {
            next(duplicateProcessing(e));
        }
    }

    async getDevice(req, res, next) {
        const { id } = req.params;
        try {
            const device = (await deviceService.getDeviceById(id)).rows[0];
            res.status(200).send(device);
        } catch (e){
            next(new InternalServerError(`Error has happened while trying to get the device(${id})`));
        }
    }

    async addDeviceByEquipmentId(req, res, next) {
        const {inventoryNumber, price, receiptDate, equipmentId} = req.body;
        try {
            await deviceService.addDeviceBindByEquipmentId(inventoryNumber, price, receiptDate, equipmentId);
            res.status(200).end();
        } catch (e) {
            console.log(e);
            next(new InternalServerError(`Error has happened while trying to create the device(${inventoryNumber})`));
        }
    }

    async updateDevice(req, res, next) {
        const {inventoryNumber, receiptDate, price, oldInventoryNumber} = req.body;
        try {
            await deviceService.updateDevice(inventoryNumber, receiptDate, price, oldInventoryNumber);
            res.status(200).end();
        } catch (e) {
            console.log(e);
            next(new InternalServerError(`Error has happened while trying to update the device(${inventoryNumber})`));
        }
    }

    async deleteDevice(req, res, next) {
        const {deviceId} = req.params;
        try {
            await deviceService.deleteDevice(deviceId);
            res.status(200).end();
        } catch (e) {
            next(new InternalServerError("Device has already been used"));
        }
    }

}

module.exports = new DeviceController();