const {doctorService, scheduleService} = require("../services");
const {httpStatusCodes, parseDate} = require("../utils");
const {validationResult} = require("express-validator");

class ScheduleController {
    async getMySchedule(req, res, next) {
        const validationErrors = validationResult(req);
        if (!validationErrors.isEmpty()) {
            return next(validationErrors);
        }

        try {
            const { accountId } = req.user;
            let { dateFrom, dateTo } = req.query;

            // default is now
            dateFrom = parseDate(dateFrom, new Date());

            // default is in two weeks
            const inTwoWeeks = new Date();
            inTwoWeeks.setDate(inTwoWeeks.getDate() + 14);
            dateTo = parseDate(dateTo, inTwoWeeks);

            const { doctorId } = await doctorService.getDoctorByAccountId(accountId);
            const schedule = await scheduleService.getFullSchedule(doctorId, dateFrom, dateTo);

            return res.status(httpStatusCodes.OK).json(schedule);
        } catch (e) {
            next(e);
        }
    }
}

module.exports = new ScheduleController();
