const {appointmentService} = require("../services");
const {httpStatusCodes} = require("../utils");
const {validationResult} = require("express-validator");

class AppointmentController {

    async getOneById(req, res, next) {
        try {
            const { appointmentId } = req.params;
            const appointment = await appointmentService.getOneById(appointmentId);
            appointment.procedures = await appointmentService.getProceduresDoneDuringAppointment(appointmentId);
            res.status(httpStatusCodes.OK).json(appointment);
        } catch (e) {
            next(e);
        }
    }

    async getAppointments(req, res, next) {
        const filter = req.query;
        try {
            const appointments = await appointmentService.getAppointments(filter.limit, filter.page, filter);
            // console.log(appointments);
            res.status(httpStatusCodes.OK).json(appointments);
        } catch (e) {
            next(e);
        }
    }

    async createAppointment(req, res, next) {
        const validationErrors = validationResult(req);
        if (!validationErrors.isEmpty()) {
            return next(validationErrors);
        }
        try {
            const { doctorId, patientCardId, date, beginTime, endTime } = req.body;

            const appointmentId = await appointmentService.createAppointment(doctorId, patientCardId, date, beginTime, endTime);

            return res.status(httpStatusCodes.OK).json({ appointmentId });
        } catch (e) {
            next(e);
        }
    }

    async createFinishedAppointment(req, res, next) {
        const {scheduledAppointmentId, data} = req.body;
        try {
            await appointmentService.createFinishedAppointment(scheduledAppointmentId, data);
            res.status(httpStatusCodes.OK).send();
        } catch (e) {
            next(e);
        }
    }

    async getPerformedProcedure(req, res, next) {
        try {
            const {performedProcedureId} = req.params;
            const procedure = await appointmentService.getPerformedProcedure(performedProcedureId);
            res.status(httpStatusCodes.OK).json(procedure);
        } catch (e) {
            next(e);
        }
    }

    async deletePerformedProcedure(req, res, next) {
        try {
            const {performedProcedureId} = req.params;
            await appointmentService.deletePerformedProcedure(performedProcedureId);
            res.status(httpStatusCodes.OK).send();
        } catch (e) {
            console.log(e);
            next(e);
        }
    }

    async getAllProceduresForTheSpecifiedCategory(req, res, next) {
        const {category} = req.params;
        try {
            const result = (await appointmentService.getProceduresForTheSpecifiedCategory(category)).rows;
            res.status(httpStatusCodes.OK).json(result);
        } catch (e) {
            console.log(e);
            next(e);
        }
    }

    async createPerformedProcedure(req, res, next) {
        const {procedureId, appointmentId, devices, price, discount} = req.body;
        try {
            await appointmentService.createPerformedProcedure(procedureId, appointmentId,
                devices, price, discount);
            res.status(httpStatusCodes.OK).send();
        } catch (e) {
            console.log(e);
            next(e);
        }
    }

    async getEquipmentAndDevicesForProcedure(req, res, next) {
        const {procedureId} = req.params;
        try {
            const result = (await appointmentService.getEquipmentAndDevicesForProcedure(procedureId)).rows;
            for(const element of result) {
                element.inventoryNumberList.sort(function(a, b) {
                    return a - b;
                });
            }
            res.status(httpStatusCodes.OK).json(result);
        } catch (e) {
            console.log(e);
            next(e);
        }
    }

    async getProcedureData(req, res, next) {
        const {procedureId} = req.params;
        try {
            const result = (await appointmentService.getProcedureData(procedureId)).rows[0];
            res.status(httpStatusCodes.OK).json(result);
        } catch (e) {
            console.log(e);
            next(e);
        }
    }

    async updatePerformedProcedure(req, res, next) {
        const {performedProcedureId} = req.params;
        const object = req.body;
        try {
            await appointmentService.updatePerformedProcedure(performedProcedureId, object);
            res.status(httpStatusCodes.OK).send();
        } catch (e) {
            console.log(e);
            next(e);
        }
    }

    async updateAppointmentInfo(req, res, next) {
        const {appointmentId} = req.params;
        const {date, diagnosis, patientState, treatment, beginTime, endTime} = req.body;
        try {
            await appointmentService.updateAppointmentInfo(appointmentId, date, diagnosis, patientState, treatment, beginTime, endTime);
            res.status(httpStatusCodes.OK).send();
        } catch (e) {
            console.log(e);
            next(e);
        }
    }

    async cancelAppointment(req, res, next) {
        const {appointmentId} = req.params;
        try {
            await appointmentService.cancelAppointment(appointmentId);
            res.status(httpStatusCodes.OK).end();
        } catch (e) {
            console.log(e);
            next(e);
        }
    }

}

module.exports = new AppointmentController();

