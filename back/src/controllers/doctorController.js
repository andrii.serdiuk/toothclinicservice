const {ValidationError, httpStatusCodes, RoleEnum, parseDate} = require("../utils");
const {doctorService, accountService, scheduleService} = require('../services')
const {InternalServerError} = require("../utils/errors")
const {duplicateProcessing} = require("../services/utils/dbError");
const {accountRepository} = require("../repositories");
const {validationResult} = require("express-validator");
const {LIMIT_DEFAULT, PAGE_DEFAULT} = require("../utils").pagination;

class DoctorController {
    async test(req, res, next) {
        return res.status(httpStatusCodes.OK).json({message: "test complete"});
    }

    async add_doctor(req, res, next) {
        const doctor = req.body;
        //TODO: change hardcoded pass
        doctor.password = '1';
        try {
            await doctorService.addDoctor(doctor);
            return res.status(200).end();
        } catch (e) {
            next(duplicateProcessing(e));
        }
    }

    async show_doctors(req, res, next) {
        let {limit, page, dateFrom, dateTo, timeFrom, timeTo} = req.query;
        const {role} = req.user;
        try {
            let count;
            if (role === RoleEnum.REGISTRANT && (dateFrom || dateTo || timeFrom || timeTo)) {

                count = await doctorService.countDoctorsWithWorkingHoursBoundaries(dateFrom || null, dateTo || null, timeFrom || null, timeTo || null);
            } else {
                count = (await doctorService.getDoctorsNumber()).rows[0]?.count;
            }
            limit = (limit === undefined ? LIMIT_DEFAULT : limit);
            page = page === undefined || page < 1 || page > Math.ceil(count / limit) ? PAGE_DEFAULT : page;

            // dateFrom = parseDate(dateFrom, null);
            // dateTo = parseDate(dateTo, null);
            let doctors;
            if (role === RoleEnum.REGISTRANT && (dateFrom || dateTo || timeFrom || timeTo)) {
                doctors = await doctorService.getDoctorsWithWorkingHoursBoundaries(dateFrom, dateTo, timeFrom, timeTo);
            } else {
                doctors = (await doctorService.getDoctorsByPage(limit, page)).rows;
            }
            res.status(200).send({count: count, doctors: doctors});
        } catch (e) {
            console.log(`Error has happened: ${e}`);
            next(new InternalServerError("Error has happened"));
        }
    }

    async get_all_doctors(req, res, next) {
        try{
            const doctors = await doctorService.getAllDoctors();
            res.status(200).send(doctors);
        } catch (e) {
            console.log(`Error has happened: ${e}`);
            next(new InternalServerError("Error has happened"));
        }
    }

    async show_doctor(req, res, next) {
        const {id} = req.params;
        try {
            const doctor = await doctorService.getDoctorById(id);
            if (doctor) {
                res.status(200).send(doctor);
            } else {
                res.status(404).send("Not found");
            }
        } catch (e) {
            console.log(`Error has happened: ${e}`);
            next(new InternalServerError(`Error has happened while trying to get the doctor(${id})`));
        }
    }

    async get_doctor_patients(req, res, next) {
        const {id} = req.params;
        let {limit, page} = req.query;
        try {
            limit = (limit === undefined ? LIMIT_DEFAULT : limit);
            page = page === undefined || page < 1 ? PAGE_DEFAULT : page;

            const patients = (await doctorService.getDoctorPatientsByPage(id, limit, page));
            res.status(200).json(patients);
        } catch (e) {
            console.log(`Error has happened: ${e}`);
            next(new InternalServerError(`Error has happened while trying to get the doctor(${id})`));
        }
    }

    async getSchedule(req, res, next) {
        const validationErrors = validationResult(req);
        if (!validationErrors.isEmpty()) {
            console.log("Validation wasn't passed");
            return next(validationErrors);
        }
        try {
            const {user} = req;
            const {doctorId} = req.params;
            let {dateFrom, dateTo} = req.query;

            // default is now
            dateFrom = parseDate(dateFrom, new Date());

            // default is in two weeks
            const inTwoWeeks = new Date();
            inTwoWeeks.setDate(inTwoWeeks.getDate() + 14);
            dateTo = parseDate(dateTo, inTwoWeeks);

            let schedule;
            if (user.role === RoleEnum.DOCTOR
                    && user.accountId !== await accountService.getAccountIdByDoctorId(doctorId)) {
                // for another doctor return only free hours in schedule
                schedule = await scheduleService.getShortSchedule(doctorId, dateFrom, dateTo);
            } else if (user.role === RoleEnum.PATIENT) {
                // for patient return only free hours splitted into 45-minute periods
                schedule = await scheduleService.getShortScheduleForPatient(doctorId, dateFrom, dateTo);
            } else {
                // for director, registrant and doctor himself return full schedule
                schedule = await scheduleService.getFullSchedule(doctorId, dateFrom, dateTo);
            }

            return res.status(httpStatusCodes.OK).json(schedule);
        } catch (e) {
            console.log(e);
            next(e);
        }
    }

    async createWorkingPeriod(req, res, next) {
        const {doctorId, date, startTime, endTime} = req.body.params;
        try {
            await doctorService.createWorkingPeriod(doctorId, date, startTime, endTime);
            res.status(httpStatusCodes.OK).end();
        } catch (e) {
            next(e);
        }
    }

    async update_doctor(req, res, next) {
        const {id} = req.params;
        const doctor = req.body;
        try {
            if (parseInt(id) !== parseInt(doctor.doctorId)) {
                console.log("Ids are different");
                next(new ValidationError("Ids are different"));
                return;
            }
            const result = (await doctorService.updateDoctorById(id, doctor));
            res.status(200).end();
        } catch (e) {
            console.log(`Error has happened: ${e}`);
            next(duplicateProcessing(e));
        }
    }

    async update_doctor_account(req, res, next) {
        const {id} = req.params;
        const account = req.body;
        account.email = account.email ? account.email : null;
        account.password = account.password ? account.password : null;
        try {
            const result = await doctorService.updateDoctorAccountByDoctorId(id, account);
            res.status(200).end();
        } catch (e) {
            console.log(`Error has happened: ${e}`);
            next(duplicateProcessing(e));
        }
    }

    async delete_doctor(req, res, next) {
        const {id} = req.params;
        try {
            const result = await doctorService.deleteDoctorById(id);
            if (result) {
                res.status(httpStatusCodes.OK).end();
            } else {
                next(new ValidationError("You cannot delete doctor who already has appointments"));
            }
        } catch (e) {
            console.log(e);
            res.status(httpStatusCodes.BAD_REQUEST);
        }
    }

    async delete_doctor_account(req, res, next) {
        const {id} = req.params;
        try {
            await doctorService.deleteAccountByDoctorId(id);
            res.status(200).end();
        } catch (e) {
            console.log(e);
            next(new InternalServerError(e));
        }
    }

    async add_doctor_account(req, res, next) {
        const {id} = req.params;
        const {email, password} = req.body;
        try {
            await doctorService.addDoctorAccount(id, email, password);
            res.status(httpStatusCodes.OK).end();
        } catch (e) {
            console.log(e);
            next(duplicateProcessing(e));
        }
    }

    async updateWorkingPeriod(req, res, next) {
        const {scheduleId, workingHoursId, beginTime, endTime} = req.body.params;
        console.log(scheduleId, workingHoursId, beginTime, endTime);
        try {
            await doctorService.updateWorkingPeriod(scheduleId, workingHoursId, beginTime, endTime);
            res.status(httpStatusCodes.OK).end();
        } catch (e) {
            if (e.message === 'Working period intersection') {
                console.log("Time periods");
                next(new ValidationError('Working periods intersection'));
            } else {
                console.log("Appointment");
                next(new ValidationError('There is an appointment in the specified working period'));
            }
        }
    }

    async deleteWorkingPeriod(req, res, next) {
        const {workingHoursId, scheduleId} = req.params;
        console.log(scheduleId);
        try {
            await doctorService.deleteWorkingPeriod(scheduleId, workingHoursId);
            res.status(httpStatusCodes.OK).end();
        } catch (e) {
            console.log(e);
            next(new ValidationError('Working periods intersection'));
        }
    }

    async getDoctorByAccountId(req, res, next) {
        const {id} = req.params;
        try {
            const data = await doctorService.getDoctorByAccountId(id);
            res.status(200).send(data);
        } catch (e) {
            console.log(e);
            next(e);
        }
    }
}

module.exports = new DoctorController();
