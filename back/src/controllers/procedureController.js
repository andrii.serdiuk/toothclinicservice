const {procedureService} = require("../services");
const {InternalServerError, ValidationError} = require("../utils");
const {duplicateProcessing} = require("../services/utils/dbError");
const {LIMIT_DEFAULT, PAGE_DEFAULT} = require("../utils").pagination;

class ProcedureController {
    async getProceduresByPage(req, res, next) {
        let {limit, page} = req.query;
        try {
            const count = await procedureService.countProcedures();
            limit = limit === undefined ? LIMIT_DEFAULT : limit;
            page = page === undefined || page < 1 || page > Math.ceil(count / limit) ? PAGE_DEFAULT : page;

            const procedures = await procedureService.getProceduresByPage(limit, page);
            res.status(200).json({
                count: count,
                procedures: procedures
            });
        } catch (e) {
            console.log(`Error has happened: ${e}`);
            next(new InternalServerError(`Error has happened while trying to get procedures`))
        }
    }

    async getProcedure(req, res, next) {
        const {id} = req.params;
        try {
            const procedure = await procedureService.getProcedureById(id);
            res.status(200).send(procedure);
        } catch (e) {
            console.log(`Error has happened: ${e}`);
            next(new InternalServerError(`Error has happened while trying to get the patient(${id})`))
        }
    }

    async addProcedure(req, res, next) {
        const procedure = req.body;
        try {
            const procedureId = await procedureService.addProcedure(procedure);
            res.status(200).json({procedureId: procedureId});
        } catch (e) {
            console.log(`Error has happened: ${e}`);
            next(duplicateProcessing(e));
        }
    }

    async updateProcedure(req, res, next) {
        const {id} = req.params;
        const procedure = req.body;
        try {
            const result = await procedureService.updateProcedure(id, procedure);
            res.status(200).end();
        } catch (e) {
            console.log(`Error has happened: ${e}`);
            next(duplicateProcessing(e));
        }
    }

    async deleteProcedure(req, res, next) {
        const {id} = req.params;
        try {
            const result = await procedureService.deleteProcedure(id);
            if(result) {
                res.status(200).end();
            } else{
                next(new ValidationError("You cannot delete procedure if it has been used in an appointment."));
            }
        } catch (e) {
            console.log(`Error has happened: ${e}`);
            next(new InternalServerError(`Error has happened while trying to get the patient(${id})`))
        }
    }

    async getEquipmentForProcedure(req, res, next) {
        const {id} = req.params;
        try {
            const result = await procedureService.getEquipmentForProcedure(id);
            res.status(200).json({equipment: result});
        } catch (e) {
            console.log(`Error has happened: ${e}`);
            next(new InternalServerError(`Error has happened while trying to get the patient(${id})`))
        }
    }
}

module.exports = new ProcedureController();