const {httpStatusCodes} = require("../utils");
const {equipmentService} = require('../services')
const {InternalServerError} = require("../utils/errors")
const {duplicateProcessing} = require("../services/utils/dbError");
const {LIMIT_DEFAULT, PAGE_DEFAULT} = require("../utils").pagination;
const {getLimitAndPage} = require("./utilities");

class EquipmentController {

    async addEquipment(req, res, next) {
        const {equipmentName} = req.body;
        try {
            await equipmentService.addEquipment(equipmentName);
            return res.status(200).end();
        } catch (e) {
            next(duplicateProcessing(e));
        }
    }

    async getAllEquipment(req, res, next) {
        try {
            const data = await equipmentService.getEquipment();
            return res.status(200).send({ equipment: data });
        } catch (e) {
            next(duplicateProcessing(e));
        }
    }

    async getEquipmentPage(req, res, next) {
        const {limit, page} = getLimitAndPage(req.query);
        try {
            const { equipment, count } = await equipmentService.getEquipmentPageAndCount(limit, page);
            return res.status(200).send({ equipment: equipment.rows, count: count });
        } catch (e) {
            next(duplicateProcessing(e));
        }
    }

    async getDevicePage(req, res, next) {
        const {limit, page} = getLimitAndPage(req.query);
        const {id} = req.params;
        try {
            const { data, count } = await equipmentService.getDevicesPageAndCountById(id, limit, page);
            return res.status(200).send({ data: data.rows, count: count });
        } catch (e) {
            next(duplicateProcessing(e));
        }
    }

    async getAllEquipmentDevices(req, res, next) {
        const {id} = req.params;
        try {
            const { data } = await equipmentService.getAllEquipmentDevices(id);
            return res.status(200).send({ data: data.rows });
        } catch (e) {
            next(duplicateProcessing(e));
        }
    }

    async deleteEquipment(req, res, next) {
        const {id} = req.params;
        try {
            await equipmentService.deleteEquipmentById(id);
            res.status(httpStatusCodes.OK).end();
        } catch (e) {
            next(new InternalServerError("Equipment has been used"));
        }
    }

    async updateEquipment(req, res, next) {
        const {id} = req.params;
        const equipmentName = req.body.params;
        try {
            await equipmentService.updateEquipment(id, equipmentName);
            res.status(httpStatusCodes.OK).end();
        } catch (e) {
            console.log(e);
            next(new InternalServerError("Equipment name duplicate"));
        }
    }

}

module.exports = new EquipmentController();