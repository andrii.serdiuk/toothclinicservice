const {ValidationError, httpStatusCodes, RoleEnum, normalize_limit, normalize_page} = require("../utils");
const {userService, tokenService, doctorService, patientService} = require("../services");
const {InternalServerError} = require("../utils/errors");
const {LIMIT_DEFAULT, PAGE_DEFAULT} = require("../utils").pagination;

class UserController {
    async login(req, res, next) {
        try {
            const {login, password} = req.body;

            if (!login || !password)
                return next(new ValidationError('Login or password is incorrect'));

            const account = await userService.login(login, password);
            const token = await tokenService.createToken(account);

            return res.status(httpStatusCodes.OK).json({token});
        } catch (e) {
            next(e);
        }
    }


    async get_my_patients_by_page(req, res, next) {
        let {limit, page} = req.query;
        try {
            limit = (limit === undefined ? LIMIT_DEFAULT : limit);
            page = page === undefined || page < 1 ? PAGE_DEFAULT : page;
            let patients;
            if (req.user.role === RoleEnum.DOCTOR) {
                patients = (await doctorService.getDoctorPatientsByAccountByPage(req.user.accountId, limit, page));
            }
            res.status(200).json(patients);
        } catch (e) {
            console.log(`Error has happened: ${e}`);
            next(new InternalServerError(`Error has happened while trying to get the doctor(${id})`));
        }
    }


    async get_my_appointments_by_page(req, res, next) {
        let {limit, page} = req.query;
        try {
            limit = normalize_limit(limit);
            page = normalize_page(page);
            let appointments;
            if (req.user.role === RoleEnum.DOCTOR) {
                appointments = await doctorService.getDoctorAppointmentByAccountByPage(req.user.accountId, limit, page);
            } else if (req. user.role === RoleEnum.PATIENT) {
                appointments = await patientService.getPatientAppointmentByAccountByPage(req.user.accountId, limit, page);
            }
            res.status(200).json(appointments);
        } catch (e) {
            console.log(`Error has happened: ${e}`);
            next(new InternalServerError(`Error has happened while trying to get appointments`));
        }
    }

    async getAccountInformation(req, res, next) {
        const {accountId} = req.params;
        try {
            const result = (await userService.getAccount(accountId)).rows[0];
            console.log(result);
            res.status(httpStatusCodes.OK).json(result);
        } catch (e) {
            next(e);
        }
    }
}

module.exports = new UserController();
