const testController = require('./testController');
const userController = require('./userController');
const doctorController = require('./doctorController');
const myController = require('./myController');
const reportController = require('./reportController');
const appointmentController = require('./appointmentController');
const patientController = require('./patientController');
const receptionistController = require('./receptionistController');
const equipmentController = require('./equipmentController');
const deviceController = require('./deviceController');
const procedureController = require('./procedureController');
const scheduleController = require('./scheduleController');
const utilities = require('./utilities');


module.exports = {
    testController,
    userController,
    doctorController,
    reportController,
    myController,
    appointmentController,
    patientController,
    equipmentController,
    deviceController,
    procedureController,
    scheduleController,
    utilities,
    receptionistController,
};
