const {LIMIT_DEFAULT, PAGE_DEFAULT} = require("../utils").pagination;
const {normalize_limit, normalize_page, InternalServerError} = require("../utils")
const {receptionistService} = require("../services");
const {duplicateProcessing} = require("../services/utils/dbError");

class ReceptionistController {
    async get_receptionists_by_page(req, res, next) {
        let {limit, page} = req.query;
        try {
            limit = normalize_limit(limit);
            page = normalize_page(page);

            const receptionists = await receptionistService.getReceptionistsByPage(limit, page);
            res.status(200).json(receptionists);
        } catch (e) {
            console.log(`Error has happened: ${e}`);
            next(new InternalServerError(`Error has happened while trying to get receptionists`))
        }
    }

    async add_receptionist(req, res, next) {
        let receptionist = req.body;
        try {
            const accountId = await receptionistService.addReceptionist(receptionist);
            res.status(200).json({accountId: accountId});
        } catch (e) {
            console.log(`Error has happened: ${e}`);
            next(new InternalServerError(`Error has happened while trying to get receptionists`))
        }
    }

    async edit_receptionist(req, res, next){
        const {id} = req.params;
        const receptionist = req.body;
        try{
            await receptionistService.editReceptionist(id, receptionist);
            res.status(200).end();
        } catch (e) {
            console.log(`Error has happened: ${e}`);
            next(duplicateProcessing(e));
        }
    }

    async delete_receptionist(req, res, next){
        const {id} = req.params;
        try{
            await receptionistService.deleteReceptionist(id);
            res.status(200).end();
        } catch (e) {
            console.log(`Error has happened: ${e}`);
            next(new InternalServerError(`Error has happened while trying to delete receptionists`));
        }
    }

}

module.exports = new ReceptionistController();