const addDoctor = require('./addDoctorValidation');
const addPatient = require('./addPatientValidation');
const common = require('./commonValidation');
const doctorValidation = require('./doctor');
const addEquipment = require('./addEquipmentValidation');
const appointment = require('./appointment');

module.exports = {
    //Three dots(spread operator) needs to unpack all fields of object
    //To avoid writing object.field
    ...addDoctor,
    ...addPatient,
    ...common,
    ...doctorValidation,
    ...addEquipment,
    ...appointment,
};