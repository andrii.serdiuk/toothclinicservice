const {queryDayDate, bodyDayDate, bodyTime, bodyIntId, bodyChar13Id} = require("../../utils/commonValidation");

const createMyAppointmentValidation = [
    bodyDayDate('date'),
    bodyTime('beginTime'),
    bodyTime('endTime'),
    bodyIntId('doctorId'),
];

const createAppointmentValidation = [
    ...createMyAppointmentValidation,
    bodyChar13Id('patientCardId'),
];

module.exports = {
    createMyAppointmentValidation,
    createAppointmentValidation,
};
