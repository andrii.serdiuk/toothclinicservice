const {check, body} = require('express-validator');
const {regex} = require('../../utils/regex');
const {stringNotNullOrEmpty} = require("../../utils/commonValidation");

const addPatient = [
    stringNotNullOrEmpty('doctorId'),
    stringNotNullOrEmpty('name'),
    stringNotNullOrEmpty('surname'),
    stringNotNullOrEmpty('middleName'),
    stringNotNullOrEmpty('email'),
    body('email').isEmail(),
    stringNotNullOrEmpty('birthday'),
    stringNotNullOrEmpty('experience'),
    stringNotNullOrEmpty('category'),
    stringNotNullOrEmpty('phone'),
];

module.exports = {
    addPatient
}