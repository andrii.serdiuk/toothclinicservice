const {stringNotNullOrEmpty} = require("../../utils/commonValidation");

const addEquipment = [
    stringNotNullOrEmpty('equipmentName')
];

module.exports = {
    addEquipment
}