const { queryDayDate, queryDayDateOptional} = require("../../utils/commonValidation");

const scheduleValidation = [
    queryDayDateOptional('dateFrom'),
    queryDayDateOptional('dateTo'),
];

module.exports = {
    scheduleValidation,
};
