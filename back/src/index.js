const express = require('express');
const cors = require('cors');

const { dbService } = require('./services');
const router = require('./routes/index');
const { errorHandlingMiddleware, notFoundMiddleware } = require('./middleware');
const {httpStatusCodes} = require("./utils");

const PORT = process.env.PORT || 5001;

const app = express();

app.use(cors());
app.use(express.json());

app.use('/api', router);
app.use('*', notFoundMiddleware);
// catching all errors sent to "next": next(new BaseError)
app.use(errorHandlingMiddleware);

const start = async () => {
    try {
        await dbService.init();

        app.listen(PORT, () => console.log(`App is listening on port ${PORT}`));
    } catch(e) {
        console.error(e);
    }
}

start();
