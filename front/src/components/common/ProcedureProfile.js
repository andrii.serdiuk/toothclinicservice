const ProcedureProfile = ({procedureId, procedureName, currentPrice, minimalCategory}) => {
    return (
        <>
            <h1 className="text-center"><b>{procedureName}</b></h1>
            <table className="table table-sm table-borderless">
                <tbody>
                <tr>
                    <td><b>Id:</b></td>
                    <td>{procedureId}</td>
                </tr>
                <tr>
                    <td><b>Price:</b></td>
                    <td>{currentPrice}</td>
                </tr>
                <tr>
                    <td><b>Minimal category:</b></td>
                    <td>{minimalCategory}</td>
                </tr>
                </tbody>
            </table>
            <div className="d-flex">


                <div className="d-flex">


                </div>
                <div className="d-flex">

                </div>
            </div>
            <div className="d-flex">


            </div>
        </>
    );
}

export default ProcedureProfile;