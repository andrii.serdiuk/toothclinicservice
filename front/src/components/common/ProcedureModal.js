import {Modal, ModalBody, ModalHeader} from "react-bootstrap";
import ProcedureForm from "./ProcedureForm";
import React from "react";

const ProcedureModal = ({
                            showModal,
                            buttonLabel,
                            closeModal,
                            submit,
                            errorMessage,
                            procedureId,
                            procedureName,
                            currentPrice,
                            minimalCategory,
                            currentEquipment,
                            setErrorMessage
                        }) => {
    return (
        <Modal show={showModal} onHide={closeModal}>
            <ModalHeader closeButton>
                <Modal.Title>Edit</Modal.Title>
            </ModalHeader>
            <ModalBody>
                <div className="row">
                    {showModal && <ProcedureForm
                        buttonLabel={buttonLabel}
                        procedureId={procedureId}
                        procedureName={procedureName}
                        procedurePrice={currentPrice}
                        procedureMinimalCategory={minimalCategory}
                        currentEquipment={currentEquipment}
                        submit={submit}
                        errorMessage={errorMessage}
                        setErrorMessage={setErrorMessage}
                    />}
                </div>
            </ModalBody>
        </Modal>);
}

export default ProcedureModal;