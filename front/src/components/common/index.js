import PersonProfile from "./personProfile";
import PaginationList from "./PaginationList";
import PaginatedList from "./PaginatedList";
import PatientForm from "./PatientForm";
import DoctorDropDown from "./DoctorDropDown";
import PatientDropDown from "./PatientDropDown";
import ProcedureProfile from "./ProcedureProfile";
import ProcedureForm from "./ProcedureForm";
import ProcedureModal from "./ProcedureModal";
import DoctorForm from "./DoctorForm";
import YesNoModal from "./YesNoModal";
import InformationModal from "./InformationModal";
import AccountModal from "./AccountModal"

export {
    PersonProfile,
    PaginationList,
    PaginatedList,
    DoctorDropDown,
    PatientDropDown,
    PatientForm,
    ProcedureProfile,
    ProcedureForm,
    ProcedureModal,
    DoctorForm,
    YesNoModal,
    InformationModal,
    AccountModal
};