import React from 'react';

const TableList = ({ headers, items }) => {
  console.log(headers)
  return (
    <div style={{flex: "1 1 auto"}} className="overflow-visible  pagination_list">
        <table className="table">
            <thead className="thead-dark">
            <tr className="cursor-pointer">
                {
                    headers &&
                    headers.map(item => <th key={item} scope="col">{item}</th>)
                }
            </tr>
            </thead>
            <tbody>
            {
                items && items.map(item =>
                    <tr
                        className="cursor-pointer"
                        key={item.key}
                        onClick={item.onClick}
                    >
                        {
                            item.cells && item.cells.map((cell, index) => {
                                if (index !== 0) {
                                    return <td key={item.key + ' ' + index + ' ' + ' ' + cell}>{cell}</td>
                                } else {
                                    return <th key={item.key + ' ' + index + ' ' + ' ' + cell} scope="row">{cell}</th>
                                }
                            })
                        }
                    </tr>
                )
            }
            </tbody>
        </table>
    </div>
  );
};

export default TableList;