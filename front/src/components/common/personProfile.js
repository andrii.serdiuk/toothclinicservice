import React, {useEffect, useState} from "react";
import YesNoModal from "./YesNoModal";

const PersonProfile = ({
                           roleName,
                           personFields,
                           editingAllowed,
                           onProfileEdit,
                           onProfileDelete,
                           onAccountAdd,
                           onAccountEdit,
                           onAccountDelete,
                           imageSource
                       }) => {

    const [showYesNoConfirmation, setShowYesNoConfirmation] = useState(false);
    const [yesNoConfirmationHeader, setYesNoConfirmationHeader] = useState('');
    const [yesNoConfirmationBody, setYesNoConfirmationBody] = useState('');
    const [deleteButtonFunction, setDeleteButtonFunction] = useState(null);

    const profileDelete = () => {
        setYesNoConfirmationHeader("Delete " + roleName);
        setYesNoConfirmationBody("Do you really want to delete the " + roleName + "?");
        setShowYesNoConfirmation(true);
        //Works only with 2 lambdas
        //Look here: https://stackoverflow.com/questions/59040989/usestate-with-a-lambda-invokes-the-lambda-when-set
        setDeleteButtonFunction(() => () => {
            setShowYesNoConfirmation(false);
            onProfileDelete();
        });
    }

    const accountDelete = () => {
        setYesNoConfirmationHeader("Delete " + roleName + " account");
        setYesNoConfirmationBody("Do you really want to delete the " + roleName + " account?");
        setShowYesNoConfirmation(true);
        setDeleteButtonFunction(() => () => {
            setShowYesNoConfirmation(false);
            onAccountDelete();
        });
    }

    return (
        <div className="col-5 w-100 d-flex flex-column align-items-center">
            <img className="mb-3"
                 src={imageSource}
                 alt="Profile picture"
                 style={{
                     maxWidth: '150px',
                 }}/>
            <div
                className="text-center mb-2 fs-1">{personFields.secondName + ' ' + personFields.firstName + ' ' + personFields.lastName}
            </div>
            <div className="text-center">
                {personFields.fields.map(f => <div key={f.title + f.content} className="doctor-info mt-1">
                    <h5 className="title fs-4">{f.title}</h5>
                    <div className="content">{f.content}</div>
                </div>)}
            </div>
            {
                editingAllowed && (onProfileEdit || onProfileDelete || onAccountEdit || onAccountDelete || onAccountAdd) &&
                <div className="d-flex flex-column mt-3">
                    <div className="btn-group pb-2 gap-2">
                        {onProfileEdit &&
                            <button
                                style={{borderRadius: "5px"}}
                                className="btn btn-warning"
                                onClick={onProfileEdit}>Edit profile</button>}
                        {onProfileDelete &&
                            <button
                                style={{borderRadius: "5px"}}
                                className="btn btn-danger"
                                onClick={profileDelete}>Delete profile</button>}
                    </div>
                    <hr/>
                    {
                        (onProfileEdit || onProfileDelete) && (onAccountEdit || onAccountDelete || onAccountAdd) &&
                            <div className="btn-group pt-2 gap-2">
                                {onAccountEdit &&
                                    <button
                                        style={{borderRadius: "5px"}}
                                        className="btn btn-warning"
                                        onClick={onAccountEdit}>Edit account</button>}
                                {onAccountAdd &&
                                    <button
                                        style={{borderRadius: "5px"}}
                                        className="btn btn-success"
                                        onClick={onAccountAdd}>Add account</button>}
                                {onAccountDelete &&
                                    <button
                                        style={{borderRadius: "5px"}}
                                        className="btn btn-danger"
                                        onClick={accountDelete}>Delete account</button>}
                            </div>
                    }

                    <YesNoModal
                        showYesNoConfirmation={showYesNoConfirmation}
                        setShowYesNoConfirmation={setShowYesNoConfirmation}
                        yesNoConfirmationHeader={yesNoConfirmationHeader}
                        yesNoConfirmationBody={yesNoConfirmationBody}
                        deleteButtonFunction={deleteButtonFunction}
                    />

                </div>
            }
        </div>
    );
};

export default PersonProfile;