import React from 'react';
import TableList from './TableList';

const TableListWithHeader = ({ headers, items, title }) => {
  return (
    <div style={{padding: "0 100px", marginTop: "50px"}} className='d-flex flex-column h-100'>
      <h2 className="mt-5 mb-3">{title}</h2>
      <TableList headers={headers} items={items}/>
    </div>
  );
};

export default TableListWithHeader;