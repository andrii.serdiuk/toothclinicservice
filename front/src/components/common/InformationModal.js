import {Button, Modal} from "react-bootstrap";
import React from "react";

const InformationModal = ({show, setShow, header, body}) => {
    return (
        <Modal show={show} centered>
            <Modal.Header closeButton={true} onHide={() => setShow(false)}>
                <Modal.Title>{header}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {body}
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => setShow(false)}>Ok</Button>
            </Modal.Footer>
        </Modal>
    );
}

export default InformationModal;