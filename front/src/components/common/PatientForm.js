import React, {useState} from "react";
import {Button, Form, Modal, ModalBody, ModalFooter, ModalHeader} from "react-bootstrap";
import {getCurrentDate} from "../../utils/common";

const PatientForm = ({
                         patientIdInput, firstNameInput, surnameInput,
                         lastNameInput, emailInput,
                         birthdayInput, phoneInput,
                         submitForm, reloadOnClose,
                         modalBody, modalHeader, buttonLabel
                     }) => {
    const [modalIsOpen, setModalVisibility] = useState(false);
    const [validated, setValidated] = useState(false);
    const showModal = () => setModalVisibility(true);
    const closeModal = () => {
        setModalVisibility(false);
        if (reloadOnClose) {
            console.log("Reload");
            window.location.reload();
        }
    }

    const handleSubmit = async e => {
        e.preventDefault();
        e.stopPropagation();
        setValidated(true);

        const form = e.currentTarget;
        if(!form.checkValidity()){
            return;
        }

        await submitForm();
        showModal();
    }

    return (
        <>
            <Form className="col-8 p-3 border border-3 rounded border-primary needs-validation"
                  validated={validated}
                  onSubmit={handleSubmit}
                  noValidate>
                {
                    (patientIdInput.value || !patientIdInput.isDisabled) &&
                    <Form.Group className="mt-2">
                        <Form.Label htmlFor="inputPatientId">Patient id</Form.Label>
                        <Form.Control type="text"
                                      className="mt-1"
                                      id="inputPatientId"
                                      placeholder="1234567890123"
                                      value={patientIdInput.value}
                                      pattern="[0-9]{13}"
                                      onChange={e => patientIdInput.setValue(e.target.value)}
                                      disabled={patientIdInput.isDisabled}
                                      required
                        />
                        <Form.Control.Feedback type="invalid">Please type valid patient id, <b>13
                            digit</b></Form.Control.Feedback>
                    </Form.Group>
                }
                {
                    (firstNameInput.value || !firstNameInput.isDisabled) &&
                    <Form.Group className="mt-2">
                        <Form.Label htmlFor="nameInput">First name</Form.Label>
                        <Form.Control type="text"
                                      className="mt-1"
                                      id="inputName"
                                      placeholder="John"
                                      value={firstNameInput.value}
                                      onChange={e => firstNameInput.setValue(e.target.value)}
                                      disabled={firstNameInput.isDisabled}
                                      required
                        />
                        <Form.Control.Feedback type="invalid">Please type valid name</Form.Control.Feedback>
                    </Form.Group>
                }
                {
                    (surnameInput.value || !surnameInput.isDisabled) &&
                    <Form.Group className="mt-2">
                        <Form.Label htmlFor="surnameInput">Second name</Form.Label>
                        <Form.Control type="text"
                                      className="mt-1"
                                      id="inputSurname"
                                      placeholder="Doe"
                                      value={surnameInput.value}
                                      onChange={e => surnameInput.setValue(e.target.value)}
                                      disabled={surnameInput.isDisabled}
                                      required
                        />
                        <Form.Control.Feedback type="invalid">Please type valid surname</Form.Control.Feedback>
                    </Form.Group>
                }
                {
                    (lastNameInput.value || !lastNameInput.isDisabled) &&
                    <Form.Group className="mt-2">
                        <Form.Label htmlFor="inputMiddleName">Last name</Form.Label>
                        <Form.Control type="text"
                                      className="mt-1"
                                      id="inputMiddleName"
                                      placeholder="Peter"
                                      value={lastNameInput.value}
                                      onChange={e => lastNameInput.setValue(e.target.value)}
                                      disabled={lastNameInput.isDisabled}
                                      required
                        />
                        <Form.Control.Feedback type="invalid">Please type valid name</Form.Control.Feedback>
                    </Form.Group>
                }
                {
                    (emailInput.value || !emailInput.isDisabled) &&
                    <Form.Group className="mt-2">
                        <Form.Label htmlFor="inputEmail">Email</Form.Label>
                        <Form.Control type="email"
                                      className="mt-1"
                                      id="inputEmail"
                                      placeholder="Type email"
                                      value={emailInput.value}
                                      onChange={e => emailInput.setValue(e.target.value)}
                                      disabled={emailInput.isDisabled}
                                      required
                        />
                        <Form.Control.Feedback type="invalid">Please type valid email</Form.Control.Feedback>
                    </Form.Group>
                }
                {
                    (birthdayInput.value || !birthdayInput.isDisabled) &&
                    <Form.Group className="mt-2">
                        <Form.Label htmlFor="birthday" className="col-form-label">Birthdate</Form.Label>
                        <Form.Control type="date"
                                      className="mt-1"
                                      id="birthday"
                                      min="1900-01-01" max={getCurrentDate()}
                                      value={birthdayInput.value}
                                      onChange={e => birthdayInput.setValue(e.target.value)}
                                      disabled={birthdayInput.isDisabled}
                                      required
                        />
                        <Form.Control.Feedback type="invalid">Please type valid birthday</Form.Control.Feedback>
                    </Form.Group>
                }
                {
                    (phoneInput.value || !phoneInput.isDisabled) &&
                    <Form.Group className="mt-2">
                        <Form.Label htmlFor="phone" className="col-form-label">Phone</Form.Label>
                        <Form.Control type="phone"
                                      className="mt-1"
                                      id="phone"
                                      placeholder="+380675687560"
                                      value={phoneInput.value}
                                      pattern="\+380[0-9]{9}"
                                      onChange={e => phoneInput.setValue(e.target.value)}
                                      disabled={phoneInput.isDisabled}
                                      required
                        />
                        <Form.Control.Feedback type="invalid">Please type valid phone</Form.Control.Feedback>
                    </Form.Group>
                }
                <Button type="submit" className="mt-3">{buttonLabel}</Button>
            </Form>

            <Modal show={modalIsOpen} onHide={closeModal}>
                <ModalHeader closeButton>{modalHeader}</ModalHeader>
                <ModalBody>{modalBody}</ModalBody>
                <ModalFooter>
                    <Button onClick={closeModal}>Close</Button>
                </ModalFooter>
            </Modal>
        </>
    );
};

export default PatientForm;