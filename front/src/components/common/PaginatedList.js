import React, {useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import {PaginationList} from "./index";
import {getDoctorAccountPatientsByPage} from "../../http/doctorApi";

const PaginatedList = ({getPatients, fieldsMap, keyMap, headers, onItemClick}) => {
    //Keep track of length separately from data
    const [currentItems, setCurrentItems] = useState(null);
    const [itemsOnPage, _] = useState(20);
    const [pageCount, setPageCount] = useState(0);
    const [currentPage, setCurrentPage] = useState(0);

    const navigate = useNavigate();

    useEffect(async () => {
        const res = await getPatients(itemsOnPage, currentPage);
        setPageCount(Math.ceil(res.count / itemsOnPage));
        setCurrentItems(res.items.map(item => {
                return {
                    onClick: () => onItemClick(item),
                    key: keyMap(item),
                    cells: fieldsMap(item)
                };
            }
        ));
    }, [currentPage]);


    return (
        <PaginationList
            choosePage={newPage => setCurrentPage(newPage)}
            headers={headers}
            pageCount={pageCount}
            items={currentItems}
        />
    );
}

export default PaginatedList;