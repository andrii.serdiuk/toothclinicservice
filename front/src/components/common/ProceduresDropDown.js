import {useEffect, useState} from "react";
import {Dropdown, Form} from "react-bootstrap";
import {getAllProceduresForTheSpecifiedCategory} from "../../http/appointmentApi";

const ProceduresDropDown = ({setItem, doctorCategory}) => {

    const [items, setItems] = useState(null);

    useEffect(async () => {
        const {data} = await getAllProceduresForTheSpecifiedCategory(doctorCategory);
        const transformData = data.map(item => {
            return {
                value: item.procedureId,
                content: item.procedureName
            }
        });
        setItems(transformData);
        setItem(transformData[0]?.value);
    }, []);

    return (
        <Dropdown>
            <Form.Select onChange={e => setItem(e.target.value)}>
                {items && items.map(item =>
                    <option key={item.value} value={item.value}>{item.content}</option>
                )}
            </Form.Select>
        </Dropdown>
    );
};

export default ProceduresDropDown;