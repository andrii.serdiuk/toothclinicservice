import {Button, Form} from "react-bootstrap";
import {getCurrentDate} from "../../utils/common";
import React, {useEffect, useState} from "react";
import {addEquipment, getAllEquipment} from "../../http/equipmentApi";
import Select from "react-select";

const ProcedureForm = ({
                           buttonLabel, procedureId,
                           procedureName, procedurePrice,
                           procedureMinimalCategory, currentEquipment,
                           submit, errorMessage, setErrorMessage
                       }) => {
    const [validated, setValidated] = useState(false);

    const [availableEquipment, setAvailableEquipment] = useState([]);
    const [selectedEquipment, setSelectedEquipment] = useState(currentEquipment.slice());
    const [selectedEquipmentElement, setSelectedEquipmentElement] = useState(null);
    const [procedureIdInput, setProcedureIdInput] = useState(procedureId.value);
    const [procedureNameInput, setProcedureNameInput] = useState(procedureName.value);
    const [procedurePriceInput, setProcedurePriceInput] = useState(procedurePrice.value);
    const [procedureMinimalCategoryInput, setProcedureMinimalCategoryInput] = useState(procedureMinimalCategory.value);
    const [equipmentErrorMessage, setEquipmentErrorMessage] = useState('');

    const handleSubmit = e => {
        e.preventDefault();
        e.stopPropagation();
        setValidated(true);
        if (!e.target.checkValidity()) {
            return;
        }
        if (selectedEquipment.length < 1) {
            setEquipmentErrorMessage('There must be at least 1 equipment.');
            setTimeout(() => setEquipmentErrorMessage(''), 500);
            return;
        }
        const res = {
            equipment: selectedEquipment,
            procedureId: procedureIdInput,
            procedureName: procedureNameInput,
            procedurePrice: procedurePriceInput,
            procedureMinimalCategory: procedureMinimalCategoryInput
        }
        submit(res);
    }

    const removeEquipment = item => {
        if (selectedEquipment.length <= 1) {
            setEquipmentErrorMessage('There must be at least 1 equipment.');
            setTimeout(() => setEquipmentErrorMessage(''), 500);
            return;
        }
        setSelectedEquipment(selectedEquipment.filter(el => el.equipmentId !== item.equipmentId));
        availableEquipment.push(item);
        setAvailableEquipment(availableEquipment);
    }

    const addEquipment = () => {
        if (!selectedEquipmentElement) {
            return;
        }
        selectedEquipment.push({
            equipmentId: selectedEquipmentElement.value,
            equipmentName: selectedEquipmentElement.label
        });
        setAvailableEquipment(availableEquipment.filter(item => item.equipmentId.toString() !== selectedEquipmentElement.value));
        setSelectedEquipmentElement(null);
    }

    useEffect(async () => {
        const {data: {equipment}} = await getAllEquipment();
        const filtered = equipment.filter(item => !(selectedEquipment.some(cItem => cItem.equipmentId === item.equipmentId)));
        setAvailableEquipment(filtered);
        setErrorMessage('');
    }, []);

    return (
        <div className="w-100">
            <Form validated={validated}
                  onSubmit={handleSubmit}
                  noValidate>
                {
                    (procedureIdInput || !procedureId.isDisabled) &&
                    <Form.Group className="mt-2">
                        <Form.Label>Procedure id</Form.Label>
                        <Form.Control type="text"
                                      placeholder="1234567890123"
                                      value={procedureIdInput}
                                      pattern="[0-9]{13}"
                                      onChange={e => setProcedureIdInput(e.target.value)}
                                      disabled={procedureId.isDisabled}
                                      required
                        />
                        <Form.Control.Feedback type="invalid">Please type a valid patient id, <b>13
                            digit</b></Form.Control.Feedback>
                    </Form.Group>
                }
                {
                    (procedureNameInput || !procedureName.isDisabled) &&
                    <Form.Group className="mt-2">
                        <Form.Label>Procedure name</Form.Label>
                        <Form.Control type="text"
                                      placeholder="John"
                                      value={procedureNameInput}
                                      onChange={e => setProcedureNameInput(e.target.value)}
                                      disabled={procedureName.isDisabled}
                                      required
                        />
                        <Form.Control.Feedback type="invalid">Please type a valid name</Form.Control.Feedback>
                    </Form.Group>
                }
                {
                    (procedurePriceInput || !procedurePrice.isDisabled) &&
                    <Form.Group className="mt-2">
                        <Form.Label>Procedure price</Form.Label>
                        <Form.Control type="text"
                                      placeholder="$234.45"
                                      value={procedurePriceInput}
                                      onChange={e => setProcedurePriceInput(e.target.value)}
                                      disabled={procedurePrice.isDisabled}
                                      pattern="\$?[\d,.]+"
                                      required
                        />
                        <Form.Control.Feedback type="invalid">Please type a valid price</Form.Control.Feedback>
                    </Form.Group>
                }
                {
                    (procedureMinimalCategoryInput || !procedureMinimalCategory.isDisabled) &&
                    <Form.Group className="mt-2">
                        <Form.Label>Minimal category</Form.Label>
                        <Form.Select value={procedureMinimalCategoryInput}
                                     onChange={e => setProcedureMinimalCategoryInput(e.target.value)}
                                     disabled={procedureMinimalCategory.isDisabled}
                                     required
                        >
                            <option disabled value={""}>Select</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                        </Form.Select>
                        <Form.Control.Feedback type="invalid">Please select category</Form.Control.Feedback>
                    </Form.Group>
                }
                <Form.Group className="mt-3">
                    <div style={
                        {
                            height: "30vh",
                            "overflowX": "hidden",
                            "overflowY": "auto"
                        }}
                         className="border border-dark border-2">
                        <table className="table table-bordered m-0">
                            <thead>
                            <tr>
                                <th className="col">Equipment id</th>
                                <th className="col">Equipment name</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                selectedEquipment && selectedEquipment.map(item =>
                                    <tr key={item.equipmentId}>
                                        <th>{item.equipmentId}</th>
                                        <td>{item.equipmentName}</td>
                                        <td>
                                            <button
                                                type="button"
                                                onClick={() => removeEquipment(item)}
                                                className="btn btn-danger text-center">-
                                            </button>
                                        </td>
                                    </tr>
                                )
                            }
                            </tbody>
                        </table>
                    </div>
                    <div className="text-danger mt-1">{equipmentErrorMessage}</div>
                    <div className="mt-3 d-flex gap-3">
                        <Select
                            className="w-100"
                            value={selectedEquipmentElement}
                            onChange={e => setSelectedEquipmentElement(e)}
                            options={
                                availableEquipment && availableEquipment.map(item => {
                                    return {value: item.equipmentId, label: item.equipmentName}
                                })
                            }
                        />
                        <button type="button" className="btn btn-primary" onClick={addEquipment}>Add</button>
                    </div>
                </Form.Group>
                <div className="text-danger text-center fs-5">
                    {errorMessage}
                </div>
                <div className="w-100 d-flex justify-content-center">
                    <Button type="submit" className="btn btn-success mt-3 w-25">{buttonLabel}</Button>
                </div>
            </Form>
        </div>
    );
}

export default ProcedureForm;