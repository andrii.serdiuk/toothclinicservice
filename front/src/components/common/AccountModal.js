import {Button, Form, Modal} from "react-bootstrap";
import React, {useState} from "react";

const AccountModal = ({
                          showAccountModal,
                          close,
                          modalSubmit,
                          modalHeader,
                          modalButtonLabel,
                          passwordNotEmpty,
                          emailErrorMessage,
                          setEmailErrorMessage,
                          buttonType,
                          startAccountEmail,
                          setStartAccountEmail
                      }) => {
    const [accountEmail, setAccountEmail] = useState('');
    const [password, setPassword] = useState('');
    const [passwordConfirmation, setPasswordConfirmation] = useState('');
    const [passwordConfirmationErrorMessage, setPasswordConfirmationErrorMessage] = useState(null);
    const [passwordErrorMessage, setPasswordErrorMessage] = useState(null);

    const checkEmail = (email) => {
        if (!/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
            setEmailErrorMessage('Email isn\'t valid');
            return false;
        }
        setEmailErrorMessage(null);
        return true;
    }

    const checkPasswordEquality = (password, passwordConfirmation) => {
        if (password !== passwordConfirmation) {
            setPasswordConfirmationErrorMessage('Passwords aren\'t equal');
            setPasswordErrorMessage('Passwords aren\'t equal');
        } else {
            setPasswordConfirmationErrorMessage(null);
            setPasswordErrorMessage(null);
        }
    }

    const modalSuccessClick = async (accountEmail, password, passwordConfirmation) => {
        setEmailErrorMessage(accountEmail ? '' : 'This field cannot be empty');
        if (!password && passwordNotEmpty) {
            setPasswordErrorMessage('This field cannot be empty');
        }
        if (!accountEmail || !password && passwordNotEmpty) {
            return;
        }
        await modalSubmit(accountEmail, password, passwordConfirmation)
    }

    const clearFields = () => {
        setAccountEmail(null);
        setPassword(null);
        setPasswordConfirmation(null);
        setStartAccountEmail(null);
        setEmailErrorMessage(null);
        setPasswordErrorMessage(null);
        setPasswordConfirmationErrorMessage(null);
    }

    return (
        <Modal onExited={clearFields} onShow={() => setAccountEmail(startAccountEmail)} show={showAccountModal}
               centered>
            <Modal.Header closeButton={true} onHide={close}>
                <Modal.Title>{modalHeader}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Email</Form.Label>
                        <Form.Control
                            onChange={e => {
                                setAccountEmail(e.target.value);
                                checkEmail(e.target.value);
                            }}
                            type="email"
                            placeholder="Type email"
                            value={accountEmail ? accountEmail : ''}
                        />
                        <div className="text-danger" hidden={!emailErrorMessage}>{emailErrorMessage}</div>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            onChange={e => {
                                setPassword(e.target.value);
                                checkPasswordEquality(e.target.value, passwordConfirmation);
                            }}
                            type="password"
                        />
                        <div className="text-danger"
                             hidden={!passwordErrorMessage}>{passwordErrorMessage}</div>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPasswordConfirmation">
                        <Form.Label>Confirm password</Form.Label>
                        <Form.Control
                            onChange={e => {
                                setPasswordConfirmation(e.target.value);
                                checkPasswordEquality(password, e.target.value);
                            }}
                            type="password"
                        />
                        <div className="text-danger"
                             hidden={!passwordConfirmationErrorMessage}>{passwordConfirmationErrorMessage}</div>
                    </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant={buttonType ? buttonType : 'success'}
                        onClick={() => modalSuccessClick(accountEmail, password, passwordConfirmation)}>{modalButtonLabel}</Button>
                <Button variant="secondary" onClick={close}>Cancel</Button>
            </Modal.Footer>
        </Modal>
    );
}

export default AccountModal;