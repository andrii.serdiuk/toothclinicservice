import {Button, Modal} from "react-bootstrap";
import React from "react";

const YesNoModal = ({
                        showYesNoConfirmation,
                        setShowYesNoConfirmation,
                        yesNoConfirmationHeader,
                        yesNoConfirmationBody,
                        deleteButtonFunction
                    }) => {
    return (
        <Modal show={showYesNoConfirmation} centered>
            <Modal.Header closeButton={true} onHide={() => setShowYesNoConfirmation(false)}>
                <Modal.Title>{yesNoConfirmationHeader}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {yesNoConfirmationBody}
            </Modal.Body>
            <Modal.Footer>
                <Button variant="danger" onClick={deleteButtonFunction}>Delete</Button>
                <Button variant="success" onClick={() => setShowYesNoConfirmation(false)}>Cancel</Button>
            </Modal.Footer>
        </Modal>
    );
}

export default YesNoModal;