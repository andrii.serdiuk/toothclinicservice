import React, {useState} from "react";
import {add as addDoctor} from "../../http/doctorApi";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "react-bootstrap";
import CenterInnerLayout from "../layout/CenterInnerLayoutV";

const DoctorForm = ({
                        doctorIdInput, firstNameInput, surnameInput,
                        lastNameInput, emailInput,
                        birthdayInput, experienceInput,
                        categoryInput, phoneInput,
                        submitForm, reloadOnClose,
                        modalBody, modalHeader, buttonLabel
                    }) => {
    const [modalIsOpen, setModalVisibility] = useState(false);
    const [birthdayIsValid, setBirthdayIsValid] = useState(true);
    const showModal = () => setModalVisibility(true);
    const closeModal = () => {
        setModalVisibility(false);
        if (reloadOnClose) {
            console.log("Reload");
            window.location.reload();
        }
    }

    const click = async e => {
        e.preventDefault();
        e.stopPropagation();
        await submitForm();
        showModal();
    }

    function getCurrentDate() {
        const date = new Date();
        let day = date.getDate();
        day = day < 10 ? '0' + day : day;
        //Here month starts from 0
        let month = date.getMonth() + 1;
        month = month < 10 ? '0' + month : month;
        let year = date.getFullYear();
        year = year < 10 ? '0' + year : year;
        return [year, month, day].join('-');
    }

    window.onload = () => {
        const forms = document.querySelectorAll('.needs-validation');
        Array.prototype.filter.call(forms, form => {
            form.onsubmit = event => {
                if (!form.checkValidity()) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }
        })
    }

    return (
        <>
            <form className="col-8 p-3 border border-3 rounded border-primary needs-validation" onSubmit={click}
                  noValidate>
                {
                    (doctorIdInput.value || !doctorIdInput.isDisabled) &&
                    <div className="form-group mt-2">
                        <label htmlFor="inputDoctorId">Doctor id</label>
                        <input type="text" className="form-control mt-1" id="inputDoctorId"
                               placeholder="783459" value={doctorIdInput.value} pattern="[0-9]{6}"
                               onChange={e => doctorIdInput.setValue(e.target.value)} required
                               disabled={doctorIdInput.isDisabled}
                        />
                        <div className="invalid-feedback">Please type valid doctor id, <b>6 digit</b></div>
                    </div>
                }
                {
                    (firstNameInput.value || !firstNameInput.isDisabled) &&
                    <div className="form-group mt-2">
                        <label htmlFor="nameInput">First name</label>
                        <input type="text" className="form-control mt-1" id="inputName"
                               placeholder="John" value={firstNameInput.value}
                               onChange={e => firstNameInput.setValue(e.target.value)} required
                               disabled={firstNameInput.isDisabled}
                        />
                        <div className="invalid-feedback">Please type valid name</div>
                    </div>
                }
                {
                    (surnameInput.value || !surnameInput.isDisabled) &&
                    <div className="form-group mt-2">
                        <label htmlFor="surnameInput">Second name</label>
                        <input type="text" className="form-control mt-1" id="inputSurname"
                               placeholder="Doe" value={surnameInput.value}
                               onChange={e => surnameInput.setValue(e.target.value)} required
                               disabled={surnameInput.isDisabled}
                        />
                        <div className="invalid-feedback">Please type valid surname</div>
                    </div>
                }
                {
                    (lastNameInput.value || !lastNameInput.isDisabled) &&
                    <div className="form-group mt-2">
                        <label htmlFor="inputMiddleName">Last name</label>
                        <input type="text" className="form-control mt-1" id="inputMiddleName"
                               placeholder="Peter" value={lastNameInput.value}
                               onChange={e => lastNameInput.setValue(e.target.value)} required
                               disabled={lastNameInput.isDisabled}
                        />
                        <div className="invalid-feedback">Please type valid name</div>
                    </div>
                }
                {
                    (emailInput.value || !emailInput.isDisabled) &&
                    <div className="form-group mt-2">
                        <label htmlFor="inputEmail">Email</label>
                        <input type="email" className="form-control mt-1" id="inputEmail"
                               placeholder="Type email" value={emailInput.value}
                               onChange={e => emailInput.setValue(e.target.value)}
                               required disabled={emailInput.isDisabled}
                        />
                        <div className="invalid-feedback">Please type valid email</div>
                    </div>
                }
                {
                    (birthdayInput.value || !birthdayInput.isDisabled) &&
                    <div className="form-group mt-2">
                        <label htmlFor="birthday" className="col-form-label">Birthdate</label>
                        <input type="date" className="form-control mt-1" id="birthday"
                               min="1900-01-01" max={`${new Date().getFullYear() - 18 - Number(experienceInput.value)}-${new Date().getMonth() + 1}-${new Date().getDate()}`} value={birthdayInput.value}
                               onChange={e => {
                                   birthdayInput.setValue(e.target.value);

                               }} required
                               disabled={birthdayInput.isDisabled}
                        />
                        <div className="invalid-feedback">Please type valid birthday</div>
                        <div style={{color: "#dc3545"}}
                             hidden={birthdayIsValid}>
                             Invalid birthday
                        </div>
                    </div>
                }
                {
                    (experienceInput.value || !experienceInput.isDisabled) &&
                    <div className="form-group mt-2">
                        <label htmlFor="experience" className="col-form-label">Years of experience</label>
                        <input type="number" className="form-control mt-1" id="experience" placeholder="0"
                               min="0" max="80" value={experienceInput.value}
                               onChange={e => {experienceInput.setValue(e.target.value);
                                   console.log(experienceInput);
                               }}
                               required disabled={experienceInput.isDisabled}
                        />
                        <div className="invalid-feedback">Please type valid experience</div>
                    </div>
                }
                {
                    (categoryInput.value || !categoryInput.isDisabled) &&
                    <div className="form-group mt-2">
                        <label htmlFor="category" className="col-form-label">Category</label>
                        <input type="number" className="form-control mt-1" id="category" placeholder="0"
                               min="0" max="4" value={categoryInput.value}
                               onChange={e => categoryInput.setValue(e.target.value)}
                               required disabled={categoryInput.isDisabled}
                        />
                        <div className="invalid-feedback">Please type valid category</div>
                    </div>
                }
                {
                    (phoneInput.value || !phoneInput.isDisabled) &&
                    <div className="form-group mt-2">
                        <label htmlFor="phone" className="col-form-label">Phone</label>
                        <input type="phone" className="form-control mt-1" id="phone" placeholder="+380675687560"
                               value={phoneInput.value} pattern="\+380[0-9]{9}"
                               onChange={e => phoneInput.setValue(e.target.value)} required
                               disabled={phoneInput.isDisabled}
                        />
                        <div className="invalid-feedback">Please type valid phone</div>
                    </div>
                }
                <CenterInnerLayout>
                    <button className="btn btn-primary mt-3">{buttonLabel}</button>
                </CenterInnerLayout>
            </form>

            <Modal show={modalIsOpen} onHide={closeModal}>
                <ModalHeader closeButton>{modalHeader}</ModalHeader>
                <ModalBody>{modalBody}</ModalBody>
                <ModalFooter>
                    <Button onClick={closeModal}>Close</Button>
                </ModalFooter>
            </Modal>
        </>
    );
};

export default DoctorForm;