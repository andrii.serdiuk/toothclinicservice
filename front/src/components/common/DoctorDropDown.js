import React, {useEffect, useState} from "react";
import {Dropdown, Form} from "react-bootstrap";
import {getAllPatients} from "../../http/patientApi";
import {getAllDoctors} from "../../http/doctorApi";
import Select from "react-select";

const DoctorDropDown = ({changeDoctor}) => {
    const [currentItems, setCurrentItems] = useState(null);
    const [selectedItem, setSelectedItem] = useState(null);

    useEffect(async () => {
        const doctors = await getAllDoctors();
        const transformData = doctors.map(item => {
            return {
                value: item.doctorId,
                content: item.secondName + ' ' + item.firstName + ' ' + item.lastName + '(' + item.doctorId + ')'
            }
        });
        transformData.push({value: null, label: 'Select....'});
        setCurrentItems(transformData);
        changeDoctor(transformData[0]?.value);
    }, []);

    return (
        <Select
            className="w-100"
            value={selectedItem}
            onChange={e => {
                console.log(e);
                setSelectedItem(e);
                changeDoctor(e);
            }}
            options={
                currentItems && currentItems.map(item => {
                    return {value: item.value, label: item.content}
                })
            }
            isClearable
        />
    );
};

export default DoctorDropDown;