import ReactPaginate from "react-paginate";
import React, {useRef, useState} from "react";
import CenterInnerLayout from "../layout/CenterInnerLayoutV";
import {BaseSpinner} from "../base";
import '../assets/css/pagination_list.css';

const PaginationList = ({choosePage, headers, pageCount, items}) => {
    const css = `
        .cursor-pointer:hover{
            cursor:pointer;
        }
    `;

    return (
        <div className="h-100 d-flex flex-column">
            <style>{css}</style>
            <div style={{flex: "1 1 auto"}} className="overflow-auto">
                <table className="table">
                    <thead className="thead-dark">
                    <tr className="cursor-pointer">
                        {
                            headers &&
                            headers.map(item => <th key={item} scope="col">{item}</th>)
                        }
                    </tr>
                    </thead>
                    <tbody>
                    {
                        items && items.map(item =>
                            <tr
                                className="cursor-pointer"
                                key={item.key}
                                onClick={item.onClick}
                            >
                                {
                                    item.cells && item.cells.map((cell, index) => {
                                        if (index !== 0) {
                                            return <td key={item.key + ' ' + index + ' ' + ' ' + cell}>{cell}</td>
                                        } else {
                                            return <th key={item.key + ' ' + index + ' ' + ' ' + cell} scope="row">{cell}</th>
                                        }
                                    })
                                }
                            </tr>
                        )
                    }
                    </tbody>
                </table>
            </div>
            <div>
                <div className="pagination-list-print-hidden">
                    <ReactPaginate
                        breakLabel="..."
                        nextLabel=">"
                        onPageChange={e => choosePage(e.selected)}
                        pageRangeDisplayed={5}
                        pageCount={pageCount}
                        previousLabel="<"
                        renderOnZeroPageCount={null}
                        pageClassName="page-item"
                        pageLinkClassName="page-link"
                        previousClassName="page-item"
                        previousLinkClassName="page-link"
                        nextClassName="page-item"
                        nextLinkClassName="page-link"
                        breakClassName="page-item"
                        breakLinkClassName="page-link"
                        containerClassName="pagination justify-content-center"
                        activeClassName="active"
                    />
                </div>
            </div>
        </div>
    );
}

export default PaginationList;