import React, {useEffect, useState} from "react";
import {Dropdown, Form} from "react-bootstrap";
import {getAllPatients} from "../../http/patientApi";
import Select from "react-select";

const PatientDropDown = ({changePatient}) => {
    const [currentItems, setCurrentItems] = useState(null);
    const [selectedItem, setSelectedItem] = useState(null);

    useEffect(async () => {
        const {data} = await getAllPatients();
        const transformData = data.patients.map(item => {
            return {
                value: item.patientCardId,
                content: item.surname + ' ' + item.firstName + ' ' + item.lastName + '(' + item.patientCardId + ')'
            }
        });
        setCurrentItems(transformData);
        changePatient(transformData[0]?.value);
    }, []);

    return (
        <Select
            className="w-100"
            value={selectedItem}
            onChange={e => {
                setSelectedItem(e);
                changePatient(e);
            }}
            options={
                currentItems && currentItems.map(item => {
                    return {value: item.value, label: item.content}
                })
            }
            isClearable
        />
    );
};

export default PatientDropDown;