import React from 'react';
import { Button } from 'react-bootstrap';
import "./../assets/css/print_button.css";

const PrintButton = ({ style, className, onClick }) => {
  return (
    <button
      onClick={onClick}
      style={{
        ...style,
      }}
      className={className + "  print_button"}
    >
    </button>
  );
};

export default PrintButton;