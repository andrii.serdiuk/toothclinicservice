import {Document, Page} from "react-pdf";

const styles = StyleSheet.create({
    page: {
        fontFamily: 'Helvetica',
        fontSize: 11,
        paddingTop: 30,
        paddingLeft: 60,
        paddingRight: 60,
        lineHeight: 1.5,
        flexDirection: 'column'
    },
    logo: {
        width: 74,
        height: 66,
        marginLeft: 'auto',
        marginRight: 'auto'
    }
});

const ReportPDF = ({title, report}) => (
    <Document>
        <Page>
            <ReportTitle title={title}/>
            <ReportTable report={report}/>
        </Page>
    </Document>
)

export default ReportPDF;