import React, { useRef } from 'react';
import { useReactToPrint } from 'react-to-print';
import TableListWithHeader from '../common/TableListWithHeader';
import PrintButton from './PrintButton';

const ReportWrapper = ({ header, title, children, itemsToPrint, headersToPrint }) => {
  const componentRef = useRef();
  const handlePrint = useReactToPrint({
      content: () => componentRef.current,
      documentTitle: "Average"
  })

  return (
    <div className="d-flex flex-column h-100">
      <div className="report__wrapper  d-flex flex-column">
        <h2 className="text-center mt-5 mb-5">{title}</h2>
        <div className='pagination-list-print-hidden'>{header}</div>
        {children}
      </div>
      <PrintButton
        // className="btn"
        onClick={handlePrint}
      />
      <div style={{display: "none"}}>
      <div ref={componentRef} className="component_to_print">
        <TableListWithHeader title={title} items={itemsToPrint} headers={headersToPrint}/>
      </div>
      </div>
    </div>
  );
};

export default ReportWrapper;