import React, { useEffect, useState } from 'react';
import { getFullSchedule } from '../../../http/doctorApi';
import ScheduleDay from './ScheduleDay';
import '../../assets/css/app_schedule.css';
import { CreateAppointmentModal } from './CreateAppointmentModal';
import { createAppointment } from '../../../http/appointmentApi';
import { useNavigate } from 'react-router-dom';
import { NOT_FOUND_ROUTE } from '../../../utils/routesNames';
import { getUserFromToken } from '../../../logic/auth';
import { UserRolesEnum } from '../../../utils/enums';
import { createMyAppointment } from '../../../http/myApi';

const Schedule = ({ doctorId }) => {
  const [show, setShow] = useState(false);
  const [dateFrom, setDateFrom] = useState(null);
  const [dateTo, setDateTo] = useState(null);
  const [appointmentDate, setAppointmentDate] = useState(null);
  const [beginTime, setBeginTime] = useState(null);
  const [endTime, setEndTime] = useState(null);
  const [schedule, setSchedule] = useState(null);
  // const [patientId, setPatientId] = useState(null);
  const navigate = useNavigate();
  const [user, setUser] = useState(null);

  const getSchedule = async () => {
    try {
      const fullSchedule = await getFullSchedule(doctorId, '2022-05-01');
      setSchedule(fullSchedule);
    } catch (e) {
      navigate(NOT_FOUND_ROUTE);
    }
  }

  useEffect(() => {
    getUserFromToken().then((user) => setUser(user)).catch(() => navigate('/oops'))
    getSchedule();
  }, []);

  const handleClose = ({ update }) => {
    if (update) {
      getSchedule();
    }

    setShow(false);
  };

  const handleShow = (appointmentDate, beginTime, endTime) => {
    setShow(true);
    setBeginTime(beginTime);
    setEndTime(endTime);
    setAppointmentDate(appointmentDate);
  }

  const onSubmitCreateAppointment = async (patientId) => {
    if (user.role !== UserRolesEnum.PATIENT)
      return await createAppointment(doctorId, patientId, appointmentDate, beginTime, endTime);
    return await createMyAppointment(doctorId, appointmentDate, beginTime, endTime);
  }

  return (
    <div
      className="app_schedule"
    >
      <h1>Doctor schedule</h1>
      <div
        className='app_schedule__days_list'
      >
        {schedule && schedule.map((day) =>
          <ScheduleDay
            key={day.date}
            day={day}
            onTimeChosen={(beginTime, endTime) => handleShow(day.date, beginTime, endTime)}
          />
        )}
      </div>
      <CreateAppointmentModal
        // patientId={patientId}
        beginTime={beginTime}
        endTime={endTime}
        date={dateTo}
        handleClose={handleClose}
        onSubmit={onSubmitCreateAppointment}
        show={show}
      />
    </div>
  );
};

export default Schedule;