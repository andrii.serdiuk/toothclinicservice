import React, { useEffect, useRef, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { dayIndex } from '../../../utils/dayIndex';
import TimePicker from './TimePicker';

const HEIGHT_PER_30_MIN = 60;
const HEIGHT_PER_15_MIN = 20;
const HEIGHT_PER_30_MIN_INACTIVE = 25;
const MIN_HEIGHT = 80;


function useOutsideAlerter(ref, cb) {
  useEffect(() => {
    /**
     * Alert if clicked on outside of element
     */
    function handleClickOutside(event) {
      if (ref.current && !ref.current.contains(event.target)) {
        console.log('callback!')
        cb();
        // document.removeEventListener("mousedown", handleClickOutside);
      }
    }
    console.log('callback init!')
    // Bind the event listener
    document.addEventListener("mousedown", handleClickOutside);
    return [() => {
      // Unbind the event listener on clean up
      document.removeEventListener("mousedown", handleClickOutside);
    }, () => document.addEventListener("mousedown", handleClickOutside)];
  }, [ref]);
}

const ScheduleDayPeriod = ({ beginTime, endTime, type, appointmentId, onTimeChosen }) => {
  const navigate = useNavigate();
  const [addApp, setAddApp] = useState(false);
  console.log(`rerendering, ${addApp}: ${beginTime}`);
  const bTimeDate = new Date(Date.parse(`2022-05-01T${beginTime}`));
  const eTimeDate = new Date(Date.parse(`2022-05-01T${endTime}`));

  const div30Min = (eTimeDate.getTime() - bTimeDate.getTime()) / (60 * 60 * 500) + 1;
  let height = addApp ? div30Min * HEIGHT_PER_30_MIN : div30Min * HEIGHT_PER_30_MIN_INACTIVE;
  console.log(beginTime, endTime, div30Min, height)
  let maxHeight = div30Min * HEIGHT_PER_30_MIN_INACTIVE;
  if (height < MIN_HEIGHT) {
    height = MIN_HEIGHT;
    maxHeight = MIN_HEIGHT;
  }

  const addAppRef = useRef(null);
  const itemRef = useRef(null);
  const [parentRef, setParentRef] = useState(null);

  useOutsideAlerter(addAppRef, () => {
    // if (addApp)
      setAddApp(false);
      // removeOutsideListener();
  });

  const onAddAppClick = () => {
    setAddApp(true);
    console.log(`onAddAppClick: ${addApp}`);
    console.log(document.onmousedown);
    // addOutsideListener();
  }

  useEffect(() => {
    setParentRef(itemRef?.current);
  }, []);

  console.log(`Max Height: ${div30Min * HEIGHT_PER_30_MIN_INACTIVE}`)
  return (
    // <div className="app_schedule__day_periods_item_wrapper"
    //   style={{
    //     // maxHeight: a
    //   }}
    // >
      <li
        className={'app_schedule__day_periods_item  ' + (type === 'FREE' ? 'app_schedule__day_periods_item_free' : 'app_schedule__day_periods_item_busy')}
        style={{
          height: height,
        }}
        onMouseDown={type === 'FREE' ? onAddAppClick : () => {}}
        ref={itemRef}
      >
        <div className='app_schedule__day_periods_item__time_wrapper'>
          <div className='app_schedule__day_periods_item__begin_time'>{beginTime.substring(0, 5)}</div>
          <div className='app_schedule__day_periods_item__end_time'>{endTime.substring(0, 5)}</div>
        </div>
        {(() => {console.log(addApp);return addApp})()
        ? <div className='app_schedule__day_periods_item__body  app_schedule__day_periods_item__body_add_app'
            ref={addAppRef}
          >
            <TimePicker
              initBeginTime={bTimeDate}
              initEndTime={eTimeDate}
              height={height}
              heightPer30Min={HEIGHT_PER_30_MIN}
              parentRef={itemRef}
              onSubmit={(beginTime, endTime) => {onTimeChosen(beginTime, endTime); setAddApp(false)}}
            />
          </div>
        : <div className='app_schedule__day_periods_item__body'>
          {type === "FREE"
            ? <div className='app_schedule__day_periods_item__body_text'>Available</div>
            : <div className='app_schedule__day_periods_item__body_text  app_schedule__day_periods_item__body_text_appointment'
                onClick={() => navigate(`/appointments/${appointmentId}`)}
              >
                Appointment
              </div>
              }
            <button
              className='app_schedule__day_periods_item__body_button'
              // onBlur(() => }
            >+</button>
          </div>}
      </li>
    // </div>
  );
}


const ScheduleDay = ({ day: { date, daySchedule }, onTimeChosen }) => {
  const dateD = new Date(date).toDateString();
  return (
    <div className="app_schedule__day">
      <div className="app_schedule__day_header">
        <div>{dateD.substring(0, 3)}</div>
        <div>{dateD.substring(3, 10)}</div>
      </div>
      <ul className="app_schedule__day_periods_list">
        {daySchedule?.length && daySchedule.map((period) =>
          <ScheduleDayPeriod {...{key: period.beginTime, ...period, onTimeChosen}}></ScheduleDayPeriod>)
        ||  <div
              style={{display: 'flex', justifyContent: "center"}}
            >
              No empty slots
            </div>}
      </ul>
    </div>
  );
};

export default ScheduleDay;
