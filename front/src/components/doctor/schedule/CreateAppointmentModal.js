import { useEffect, useState } from "react";
import { useNavigate } from 'react-router-dom';
import { Alert, Button, Form, FormSelect, Modal } from "react-bootstrap";
import Select from 'react-select';
import { getAllPatients } from "../../../http/patientApi";
import { getUserFromToken } from "../../../logic/auth";
import { UserRolesEnum } from "../../../utils/enums";

export const CreateAppointmentModal = ({ show, handleClose, beginTime, endTime, date, patientId, onSubmit }) => {
  // const [show, setShow] = useState(false);
  const [patients, setPatients] = useState([]);
  const [patientsOptions, setPatientsOptions] = useState([]);
  const [selectedPatient, setSelectedPatient] = useState(null);
  const [isValidationMessageVisible, setIsValidationMessageVisible] = useState(false);
  const [validationMessage, setValidationMessage] = useState(null);
  const [user, setUser] = useState(null);

  const navigate = useNavigate();

  const showValidationMessage = (message) => {
    setValidationMessage(message)
    setIsValidationMessageVisible(true);
    setTimeout(() => setIsValidationMessageVisible(false), 2000);
  }

  const onCreateAppointment = async () => {
    try {
      if (user?.role !== UserRolesEnum.PATIENT && !selectedPatient) {
        showValidationMessage('Please choose patient');
        return;
      }
      const { appointmentId } = await onSubmit(selectedPatient?.value);
      navigate(`/appointments/${appointmentId}`);

      handleClose({update: true});
    } catch (e) {
      if (e?.response?.status === 400) {
        const { data: { errors: [error]}} = e.response;
        showValidationMessage(error?.description);
      } else {
        showValidationMessage('Oops. Sorry. Something went wrong :(');
      }
    }
  }

  const onPatientChange = (selectedPatient) => {
    if (isValidationMessageVisible) {
      setIsValidationMessageVisible(false);
    }
    setSelectedPatient(selectedPatient);
  }

  useEffect(() => {
    getUserFromToken()
      .then((user) => setUser(user))
      .catch(e => navigate('/oops'));
    getAllPatients().
      then(({data: { patients }}) => {
        patients = patients.map(({patientCardId, firstName, lastName, surname}) => ({
          label: `${firstName} ${lastName} ${surname}`,
          value: patientCardId
        }));
        setPatients(patients);
        setPatientsOptions(patients);
      })
      .catch(e => navigate('/oops'));
  }, [])

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>New Appointment</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group className="mb-3 d-flex flex-column" controlId="exampleForm.ControlInput1">
            <Form.Label>
              Date:
              <span style={{fontWeight: '600'}}>&nbsp;{beginTime?.toDateString()?.substring(0, 15)}</span>
            </Form.Label>
            <Form.Label>
              Begin Time:
              <span style={{fontWeight: '600'}}>&nbsp;{beginTime?.toTimeString().substring(0, 5)}</span>
            </Form.Label>
            <Form.Label>
              End Time:
              <span style={{fontWeight: '600'}}>&nbsp;{endTime?.toTimeString().substring(0, 5)}</span>
            </Form.Label>
          </Form.Group>
          {isValidationMessageVisible &&
            <Alert variant="danger">
              {validationMessage}
            </Alert>
          }

          {user?.role !== UserRolesEnum.PATIENT &&
            <Form.Group
              className="mb-3"
              controlId="createAppointment.ControlTextarea1"
            >
            <Form.Label className="">
              Choose patient:
            </Form.Label>
              <Select onChange={onPatientChange} options={patientsOptions} value={selectedPatient}/>

            </Form.Group>
          }
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
        <Button variant="primary" onClick={onCreateAppointment}>
          Create appointment
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
