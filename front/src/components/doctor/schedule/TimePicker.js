import React, { useEffect, useRef, useState } from 'react';
import '../../assets/css/time_boundaries_picker.css';

const INIT_5_MINUTES_INTERVAL = 1;

const TimePicker = ({ initBeginTime, initEndTime, initHeight, heightPer30Min, parentRef, onSubmit }) => {
  // const intervalsFit = Math.ceil((initEndTime.getTime() - initBeginTime.getTime()) / 1000 / 60 / 4);
  // const INIT_15_MINUTES_INTERVAL = Math.min(6, intervalsFit);
  const heightStep = heightPer30Min / 6;
  const [beginTime, setBeginTime] = useState(initBeginTime);
  // const [numberOf5MinGaps, setNumberOf5MinGaps] = useState(INIT_15_MINUTES_INTERVAL);
  const [endTime, setEndTime] = useState(new Date(initBeginTime.getTime() + INIT_5_MINUTES_INTERVAL*1000*60*5));
  const [beginMouseDown, setBeginMouseDown] = useState(false);
  const [marginTop, setMarginTop] = useState(5);
  const [height, setHeight] = useState((INIT_5_MINUTES_INTERVAL+3) * heightStep);
  // count how many 5-minute breaks can fit into boundaries

  // (begin - init) / (1000*60*15) * heightStep
  // round((y - y0) / heightStep)  - 5minutesDiff / 5
  // 5minutesDiff
  const pickerRef = useRef();

  const submitTimeBoundaries = () => {
    onSubmit(beginTime, endTime);
  }

  const beginDraggerOnMouseDown = (e) => {
    if (!pickerRef.current)
      return;

    const onmousemove = (e, initBeginTime, getParentY, heightStep, beginTime, endTime) => {
      if (e.clientY < getParentY() + 21) {
        if (beginTime !== initBeginTime) {
          setBeginTime(initBeginTime);
          setMarginTop(5);
          const newHeight = ((endTime().getTime() - initBeginTime.getTime())/1000/60/5) * heightStep;
          setHeight(newHeight);
        }
        return;
      }
      const numberOf5MinGaps = Math.round((e.clientY - 21 - getParentY()) / heightStep); // - 5minutesDiff / 5
      let newBeginTime = new Date(initBeginTime.getTime() + numberOf5MinGaps * 5 * 60 * 1000);
      if (newBeginTime === beginTime() && beginTime() !== initBeginTime) {
        return;
      }
      const intervalsNum = ((endTime().getTime() - newBeginTime.getTime())/1000/60/5);

      if (intervalsNum < 1) {
        return;
      }

      setBeginTime(newBeginTime);
      setMarginTop(numberOf5MinGaps * heightStep + 5);
      const newHeight = (intervalsNum+3) * heightStep;
      setHeight(newHeight);
    }

    document.onmousemove = (e) => onmousemove(e, initBeginTime, () => parentRef?.current?.getBoundingClientRect().y, heightStep, () => beginTime, () => endTime);

    document.onmouseup = (e) => document.onmousemove = null;
  }

  const endDraggerOnMouseDown = (e) => {
    if (!pickerRef.current)
      return;

    const onmousemove = (e, initEndTime, getParentY, getBeginY, heightStep, beginTime, endTime) => {
      if (e.clientY > getParentY() - 21) {
        if (endTime !== initEndTime) {
          setEndTime(initEndTime);
          const newHeight = ((initEndTime.getTime() - beginTime().getTime())/1000/60/5 + 3) * heightStep;
          setHeight(newHeight);
        }
        return;
      }
      const numberOf5MinGaps = Math.round((e.clientY - getBeginY() - 31) / heightStep); // - 5minutesDiff / 5
      let newEndTime = new Date(beginTime().getTime() + numberOf5MinGaps * 5 * 60 * 1000);
      if (newEndTime === endTime() && endTime() !== initBeginTime) {
        return;
      }
      const intervalsNum = ((newEndTime.getTime() - beginTime().getTime())/1000/60/5);

      if (intervalsNum < 1) {
        return;
      }

      setEndTime(newEndTime);
      const newHeight = (intervalsNum+3) * heightStep;
      setHeight(newHeight);
    }

    document.onmousemove = (e) => onmousemove(
      e, initEndTime,
      () => parentRef?.current?.getBoundingClientRect().y + parentRef?.current?.getBoundingClientRect().height,
      () => parentRef?.current?.getBoundingClientRect().y + marginTop,
      heightStep, () => beginTime, () => endTime
    );

    document.onmouseup = (e) => document.onmousemove = null;
  }

  return (
    <div ref={pickerRef} style={{marginTop: marginTop, height: height}} className='time_boundaries_picker'>
      <div className='time_boundaries_picker__begin'>
        <div className='time_boundaries_picker__begin_time'>{beginTime.toTimeString().substring(0, 5)}</div>
        <div
          className='time_boundaries_picker__dragger  time_boundaries_picker__begin_dragger'
          onMouseDown={beginDraggerOnMouseDown}
        />
      </div>
      <div className='time_boundaries_picker__end'>
        <div className='time_boundaries_picker__end_time'>{endTime.toTimeString().substring(0, 5)}</div>
        <div
          className='time_boundaries_picker__dragger  time_boundaries_picker__end_dragger'
          onMouseDown={endDraggerOnMouseDown}
        />
      </div>
      <button
        className='time_boundaries_picker__button'
        onClick={submitTimeBoundaries}
      >
        +
      </button>
      <div className='time_boundaries_picker__line'></div>
    </div>
  );
};

export default TimePicker;