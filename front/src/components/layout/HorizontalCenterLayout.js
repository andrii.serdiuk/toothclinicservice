import React from 'react';
import {Container} from 'react-bootstrap';

const HorizontalCenterLayout = ({children, style = {}}) => {
    return (
        <div className="d-flex w-100 flex-column align-items-center"
             style={{
                 height: '100vh',
                 ...style,
             }}
        >
            {children}
        </div>
    );
};

export default HorizontalCenterLayout;