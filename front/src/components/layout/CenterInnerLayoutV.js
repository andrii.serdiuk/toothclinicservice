import React from 'react';
import { Container } from 'react-bootstrap';

const CenterInnerLayout = ({ children, style={} }) => {
  return (
    <Container
      style={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        flexGrow: 1,
        ...style
      }}
    >
      {children}
    </Container>
  );
};

export default CenterInnerLayout;