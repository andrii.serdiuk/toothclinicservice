import React from 'react';
import {Col, Container, Row} from 'react-bootstrap';
// import Header from './Header';
import {Navbar} from './navbar';

const NavbarWrapper = ({children, user}) => {
    if (!user) {
        return children;
    }

    return (
        <div className="d-flex w-100 h-100">
            <Navbar user={user}/>
            {/* <Component user={user}/> */}
            <div style={{flex: "1 1 auto"}} className="d-flex flex-column h-100 w-100">{children}</div>
        </div>
    );
};

export default NavbarWrapper;
