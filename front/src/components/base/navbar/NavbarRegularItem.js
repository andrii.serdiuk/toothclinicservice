import React from 'react';
import { useNavigate } from 'react-router-dom';
import dotIcon from '../../assets/img/navbar/dot_24.png';

const NavbarRegularItem = ({ isActive, icon, name, path }) => {
  const navigate = useNavigate();
  let itemClass = 'navbar__item  navbar__item_regular';
  const imageClassName = name?.toLowerCase().includes('patients') ? 'navbar__item_image  navbar__item_image_patient' : 'navbar__item_image';
  if (isActive) itemClass += '  navbar__item_regular--active';

  return (
    <div
      className={itemClass}
      onClick={() => navigate(path)}
    >
      {isActive && <img className='navbar__item_dropdown_dot' src={dotIcon}/>}
      <img className={imageClassName} src={icon}/>
      <span className='navbar__item_name'>{name}</span>
    </div>
  );
};

export default NavbarRegularItem;