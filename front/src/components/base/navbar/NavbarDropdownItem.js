import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import arrowIcon from '../../assets/img/navbar/arrow-down_32.png';
import dotIcon from '../../assets/img/navbar/dot_24.png';


const DropdownList = ({ items }) => {
  const location = useLocation();

  return (
    <div className='navbar__item_dropdown_list'>
      {items.map(({ path, name }) =>
        <DropdownItem key={path} path={path} isActive={location.pathname.startsWith(path)} name={name}/>
      )}
    </div>
  );
}

const DropdownItem = ({ name, path, isActive }) => {
  const navigate = useNavigate();
  let itemClassName = 'navbar__item_dropdown_item';
  if (isActive) itemClassName += '  navbar__item_dropdown_item--active';

  return (
    <div
      className={itemClassName}
      onClick={() => !isActive && navigate(path)}
    >
      {name}
    </div>);
}

const NavbarDropdownItem = ({ isActive, icon, name, items }) => {
  let itemClass = 'navbar__item  navbar__item_dropdown';
  const imageClassName = name?.toLowerCase().includes('patients') ? 'navbar__item_image  navbar__item_image_patient' : 'navbar__item_image';
  if (isActive) itemClass += '  navbar__item_dropdown--active';

  const [dropdownArrowClass, setDropdownArrowClass] = useState('');

  const [isVisible, setVisible] = useState(false);

  useEffect(() => {
    setVisible(isActive);
    setDropdownArrowClass(isActive ? 'navbar__item_dropdown_arrow  navbar__item_dropdown_arrow--up' : 'navbar__item_dropdown_arrow');
  } ,[isActive]);

  const onChange = (newVisible) => {
    if (isActive)
      return;

    setVisible(newVisible);

    const newDropdownArrowClass = newVisible ? 'navbar__item_dropdown_arrow  navbar__item_dropdown_arrow--up' : 'navbar__item_dropdown_arrow';
    setDropdownArrowClass(newDropdownArrowClass);
  }

  return (
    <div
      className={itemClass}
      onMouseEnter={() => onChange(true)}
      onMouseLeave={() => onChange(false)}
    >
      <div className='navbar__item_dropdown_info'>
        {isActive && <img className='navbar__item_dropdown_dot' src={dotIcon}/>}
        <img className={imageClassName} src={icon}/>
        <span className='navbar__item_name'>{name}</span>
        <img className={dropdownArrowClass} src={arrowIcon}/>
      </div>
      {isVisible && <DropdownList items={items}/>}
    </div>
  );
};

export default NavbarDropdownItem;