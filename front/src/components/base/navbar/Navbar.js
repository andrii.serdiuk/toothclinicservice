import React, {useEffect, useState} from 'react';
import {Dropdown} from 'react-bootstrap';
import {Link, useLocation} from 'react-router-dom';
import {NavbarItemTypeEnum} from '../../../utils/enums';
import {navbarPages} from '../../../utils/navbarPages';
import NavbarDropdownItem from './NavbarDropdownItem';
import NavbarRegularItem from './NavbarRegularItem';
import '../../assets/css/navbar.css';
import BaseSpinner from '../BaseSpinner';

const Navbar = ({user}) => {
    const userNavbarPages = navbarPages(user.role);
    const [toggle, setToggle] = useState(false);
    const location = useLocation();
    const [active, setActive] = useState(null);
    const [children, setChildren] = useState(null);

    const buildNavbarItem = ({icon, path, name, type, items}) => {
        const isActive = location.pathname.startsWith(path);

        const regular = () => <NavbarRegularItem isActive={isActive} key={path} toggle={toggle} icon={icon} name={name}
                                                 path={path}/>;

        if (type === NavbarItemTypeEnum.REGULAR) {
            return regular();
        }

        if (type === NavbarItemTypeEnum.DROPDOWN) {
            return <NavbarDropdownItem isActive={isActive} key={name} toggle={toggle} icon={icon} name={name}
                                       items={items}/>
        }

        return regular()
    }

    // useEffect(() => {
    //   setChildren(userNavbarPages.map((item) => buildNavbarItem(item)));
    // }, [userNavbarPages]);

    return (
        <div className='navbar__wrapper'>
            <div
                className='navbar__main'
                onMouseEnter={() => setToggle(true)}
                onMouseLeave={() => setToggle(false)}
            >
                <div className='navbar__icon'>
                    <div className='navbar__icon_tooth'>
                        <div className='navbar__icon_dot'/>
                    </div>
                </div>

                {/* {children !== null
      ? children
      : <BaseSpinner/>} */}
                {userNavbarPages.map((item) => buildNavbarItem(item))}
            </div>
            {/* <div className='navbar__curtain'></div> */}
        </div>
    );
};

export default Navbar;
