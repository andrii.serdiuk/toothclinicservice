import jwt_decode from 'jwt-decode';
import { customLocalStorage } from '../utils/localStorage';

export const getUserFromToken = async () => {
  const token = customLocalStorage.getAuthToken();
  const decoded = jwt_decode(token);

  if (Date.now() >= decoded.exp * 1000)
    return null;

  return decoded;
};
