import React from 'react';
import {Card} from 'react-bootstrap';
import {useNavigate} from "react-router-dom";
import CenterInnerLayout from "../../components/layout/CenterInnerLayoutV";


const DeviceItem = ({inventoryNumber, receiptDate, price}) => {

    const navigate = useNavigate();

    return (

        <Card className="cardFlex"
              onClick={() => navigate('/device/' + inventoryNumber, {replace: false})}
              style={{
                  display: "inline-block",
                  width: "18rem",
                  cursor: "pointer",
                  padding: "10px",
                  flexBasis: "32%",
                  height: "140px"
              }}>
            <Card.Body>
                <CenterInnerLayout>
                    <Card.Title style={{cursor: "pointer"}}>
                        {inventoryNumber}
                    </Card.Title>
                    <Card.Body>
                        Receipt date: {receiptDate}
                    <br/>
                        Price: {price}
                    </Card.Body>
                </CenterInnerLayout>
            </Card.Body>
        </Card>

    )

};

export default DeviceItem;