import AllEquipmentPage from "./AllEquipmentPage";
import SpecificEquipmentPage from "./SpecificEquipmentPage";

export {
  AllEquipmentPage,
  SpecificEquipmentPage
};
