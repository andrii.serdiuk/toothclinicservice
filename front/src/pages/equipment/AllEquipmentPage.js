import React, {useEffect, useState} from 'react';
import CenterInnerLayout from '../../components/layout/CenterInnerLayoutV';
import {getEquipmentPage} from "../../http/equipmentApi";
import SaveEquipmentModal from "./SaveEquipmentModal";
import {useNavigate} from "react-router-dom";
import EquipmentItem from "./EquipmentItem";
import ReactPaginate from "react-paginate";
import {getUserFromToken} from "../../logic/auth";
import DIRECTOR from "../../utils/enums/userRoles.enum";

const AllEquipmentPage = () => {

    //Keep track of length separately from data
    const [currentItems, setCurrentItems] = useState(null);
    const [itemsOnPage, _] = useState(9);
    const [pageCount, setPageCount] = useState(0);
    const [currentPage, setCurrentPage] = useState(0);
    const [loading, setLoading] = useState(true);
    const [showButton, setShowButton] = useState(false);

    const navigate = useNavigate();
    useEffect(async () => {
        setShowButton((await getUserFromToken()).role === DIRECTOR.DIRECTOR );
        const res = (await getEquipmentPage(itemsOnPage, currentPage)).data;
        setLoading(false);
        setPageCount(Math.ceil(res.count / itemsOnPage));
        setCurrentItems(res.equipment);
    }, [itemsOnPage, currentPage]);

    const handlePageClick = event => {
        setCurrentPage(event.selected);
    }

    return (
        <>
            <CenterInnerLayout>
                <div
                    style={{
                        display: "inline-flex",
                        alignItems: "flex-start",
                        alignContent: "flex-start",
                        flexFlow: "row wrap",
                        margin: "30px",
                        position: "relative",
                        gap: "20px",
                        height: "500px",
                        width: "100%"
                    }}>
                    {
                        currentItems && currentItems.map(item => (
                            <EquipmentItem
                                key={item.equipmentId}
                                equipmentName={item.equipmentName}
                                equipmentId={item.equipmentId}/>
                        ))
                    }
                </div>
                {
                    showButton && <SaveEquipmentModal/>
                }
                <ReactPaginate
                    breakLabel="..."
                    nextLabel=">"
                    onPageChange={handlePageClick}
                    pageRangeDisplayed={5}
                    pageCount={pageCount}
                    previousLabel="<"
                    renderOnZeroPageCount={null}
                    pageClassName="page-item"
                    pageLinkClassName="page-link"
                    previousClassName="page-item"
                    previousLinkClassName="page-link"
                    nextClassName="page-item"
                    nextLinkClassName="page-link"
                    breakClassName="page-item"
                    breakLinkClassName="page-link"
                    containerClassName="pagination justify-content-center"
                    activeClassName="active"
                />
            </CenterInnerLayout>
        </>
    );

};

export default AllEquipmentPage;
