import React, {useEffect, useState} from 'react';
import {Button, Form, Modal, Spinner} from "react-bootstrap";
import {addEquipment, updateEquipment} from "../../http/equipmentApi";

const EditEquipmentModal = ({show, setShow, equipmentId, equipmentName}) => {

    const [equipmentNameValue, setEquipmentName] = useState('');
    const [isLoading, setLoading] = useState(false);
    const [duplicateNameError, setDuplicateNameError] = useState(false);
    const [duplicateNameErrorText, setDuplicateNameErrorText] = useState(null);
    const [emptyInput, setEmptyInput] = useState(true);

    useEffect(
        () => {
            setEquipmentName(equipmentName);
        }, []);

    const handleClose = () => {
        setEquipmentName(equipmentName);
        setDuplicateNameError(false);
        setDuplicateNameErrorText(null);
        setEmptyInput(true);
        setShow(false);
    };

    const handleDuplicateNameError = () => {
        setDuplicateNameError(true);
        setDuplicateNameErrorText(equipmentNameValue.trim());
    }

    const submit = async e => {
        setLoading(true);
        e.stopPropagation();
        e.preventDefault();
        try {
            await updateEquipment(equipmentId, equipmentNameValue.trim());
            return true;
        } catch (e) {
            handleDuplicateNameError();
            return false;
        } finally {
            setLoading(false);
        }
    }

    const handleSave = (e) => {
        submit(e).then(success => {
            if(success) {
                handleClose();
                window.location.reload();
            }
        });
    };

    return (
        <>
            <Modal show={show} onHide={handleClose} centered>

                <Modal.Header closeButton={true}>
                    Edit equipment
                </Modal.Header>

                <Modal.Body>
                    <div className="d-flex justify-content-center">
                        <Form onSubmit={submit}>
                            <Form.Group className="mb-3" controlId="controlInput">
                                <Form.Label>Equipment name</Form.Label>
                                <Form.Control type="text"
                                              value={equipmentNameValue} placeholder="Name"
                                              onChange={e => {
                                                  setEquipmentName(e.target.value);
                                                  setDuplicateNameError(duplicateNameErrorText !== null &&
                                                      e.target.value.trim() === duplicateNameErrorText);
                                                  setEmptyInput(e.target.value.trim() === '');
                                              }}
                                              autoFocus/>
                                <div hidden={!duplicateNameError} style={{color: "#dc3545"}}>
                                    Duplicate name
                                </div>
                            </Form.Group>
                        </Form>
                    </div>
                </Modal.Body>
                <Modal.Footer style={{
                    display: "flex",
                    justifyContent: "center",
                }}>
                    <Button style={{backgroundColor: "#FF8C00", borderColor: "#FF8C00"}} disabled={isLoading || emptyInput || duplicateNameError}
                            onClick={!isLoading && !emptyInput && !duplicateNameError ? handleSave : null} centered>
                        <span hidden={isLoading}> Edit </span>
                        <Spinner
                            as="span"
                            animation="border"
                            size="sm"
                            role="status"
                            hidden={!isLoading}
                        />

                    </Button>
                </Modal.Footer>

            </Modal>

        </>
    );


};

export default EditEquipmentModal;