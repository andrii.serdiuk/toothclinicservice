import React, {useState} from 'react';
import {Button, Card} from 'react-bootstrap';
import {useNavigate} from "react-router-dom";
import CenterInnerLayout from "../../components/layout/CenterInnerLayoutV";
import {deleteEquipment} from "../../http/equipmentApi";
import EditEquipmentModal from "./EditEquipmentModal";
import {set} from "mobx";


const EquipmentItem = ({equipmentName, equipmentId}) => {

    const navigate = useNavigate();

    const [removingForbidden, setRemovingForbidden] = useState(false);
    const [showEdit, setShowEdit] = useState(false);

    const removeEquipment = async () => {
        try {
            await deleteEquipment(equipmentId);
            console.log("Reloading");
            window.location.reload();
        } catch (e) {
            setRemovingForbidden(true);
        }
    };

    return (
        <>
            <Card className="cardFlex"
                  style={{
                      display: "inline-block",
                      width: "18rem",
                      cursor: "pointer",
                      padding: "10px",
                      flexBasis: "32%",
                      height: "160px"
                  }}>
                <Card.Body>
                    <CenterInnerLayout>
                        <Card.Title>{equipmentName}</Card.Title>
                        <Card.Body style={{display: "flex", flexDirection: "column", alignItems: "center"}}>
                            <div style={{display: "flex", marginTop: "10px", gap: "15px", marginBottom: "5px"}}>
                                <Button variant="primary" onClick={
                                    () => navigate('/equipment/' + equipmentId + '/devices', {replace: false})}>
                                    Open
                                </Button>
                                <Button onClick={() => {
                                    setShowEdit(true);
                                }}
                                        style={{backgroundColor: "#FF8C00", borderColor: "#FF8C00"}}
                                >
                                    Edit
                                </Button>

                                <Button onClick={() => {
                                    removeEquipment();
                                }}
                                        style={{backgroundColor: "#b72339", borderColor: "#b72339"}}
                                        disabled={removingForbidden}
                                >
                                    Remove
                                </Button>
                            </div>
                            <div style={{color: "#dc3545"}}
                                 hidden={!removingForbidden}>
                                Equipment has been used
                            </div>
                        </Card.Body>
                    </CenterInnerLayout>
                </Card.Body>
            </Card>
            <EditEquipmentModal
                show={showEdit}
                setShow={setShowEdit}
                equipmentId={equipmentId}
                equipmentName={equipmentName}
            />
        </>
    )

};

export default EquipmentItem;