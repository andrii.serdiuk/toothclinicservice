import React, {useEffect, useState} from 'react';
import CenterInnerLayout from '../../components/layout/CenterInnerLayoutV';

import {useParams} from "react-router-dom";
import ReactPaginate from "react-paginate";
import DeviceItem from "./DeviceItem";
import {getSpecificEquipmentDeviceList} from "../../http/equipmentApi";
import {getUserFromToken} from "../../logic/auth";
import DIRECTOR from "../../utils/enums/userRoles.enum";
import SaveDeviceModal from "../device/SaveDeviceModal";


const SpecificEquipmentPage = () => {

    //Keep track of length separately from data
    const [currentItems, setCurrentItems] = useState(null);
    const [itemsOnPage, _] = useState(9);
    const [pageCount, setPageCount] = useState(0);
    const [currentPage, setCurrentPage] = useState(0);
    const [loading, setLoading] = useState(true);


    const [showButton, setShowButton] = useState(false);

    const {equipmentId} = useParams();

    const [noDevices, setNoDevices] = useState(false);

    useEffect(async () => {
        setShowButton((await getUserFromToken()).role === DIRECTOR.DIRECTOR );

        const res = (await getSpecificEquipmentDeviceList(equipmentId, itemsOnPage, currentPage)).data;
        setLoading(false);
        setPageCount(Math.ceil(res.count / itemsOnPage));
        setCurrentItems(res.data);
        console.log(res.data.length === 0);
        setNoDevices(res.data.length === 0);
    }, [itemsOnPage, currentPage]);

    const handlePageClick = event => {
        setCurrentPage(event.selected);
    }

    return (
        <>
            {noDevices && <CenterInnerLayout style={{position: "absolute", left: "10%", top: "40%"}}>
                <h2> There are no devices in this group </h2>
            </CenterInnerLayout>
            }

            <CenterInnerLayout>
                <div
                    style={{
                        display: "inline-flex",
                        alignItems: "flex-start",
                        alignContent: "flex-start",
                        flexFlow: "row wrap",
                        margin: "30px",
                        position: "relative",
                        gap: "20px",
                        height: "500px",
                        width: "100%"}}>
                    {
                        currentItems && currentItems.map(item => (
                            <DeviceItem
                                key={item.inventoryNumber}
                                inventoryNumber={item.inventoryNumber}
                                receiptDate={item.receiptDate}
                                price={item.price}/>
                        ))
                    }
                </div>


                {
                    showButton && <SaveDeviceModal/>
                }
            </CenterInnerLayout>

            {
                <ReactPaginate
                    breakLabel="..."
                    nextLabel=">"
                    onPageChange={handlePageClick}
                    pageRangeDisplayed={5}
                    pageCount={pageCount}
                    previousLabel="<"
                    renderOnZeroPageCount={null}
                    pageClassName="page-item"
                    pageLinkClassName="page-link"
                    previousClassName="page-item"
                    previousLinkClassName="page-link"
                    nextClassName="page-item"
                    nextLinkClassName="page-link"
                    breakClassName="page-item"
                    breakLinkClassName="page-link"
                    containerClassName="pagination justify-content-center"
                    activeClassName="active"
                />
            }
        </>
    );

};

export default SpecificEquipmentPage;
