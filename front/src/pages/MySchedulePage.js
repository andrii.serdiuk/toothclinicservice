import React, {useEffect, useState} from 'react';
import CenterInnerLayout from '../components/layout/CenterInnerLayoutV';
import {PaginationList} from "../components/common";
import {doctorScheduleNextWeek} from "../http/reportsApi";
import {getDoctorByAccountId} from "../http/doctorApi";
import {getUserFromToken} from "../logic/auth";
import {CenterInnerLayoutH} from "../components/layout";

const MySchedulePage = () => {

    const [currentItems, setCurrentItems] = useState(null);
    const [itemsOnPage, _] = useState(5);
    const [pageCount, setPageCount] = useState(0);
    const [currentPage, setCurrentPage] = useState(0);

    const [working, setWorking] = useState(true);

    const choosePage = async (newPage) => {
        setCurrentPage(newPage);
    };


    useEffect(async () => {
        const accountId = (await getUserFromToken()).accountId;
        const doctorId = (await getDoctorByAccountId(accountId)).data.doctorId;

        const schedule = (await doctorScheduleNextWeek(doctorId, itemsOnPage, currentPage)).data;

        setWorking(Number(schedule.count) !== 0);

        setPageCount(Math.ceil(schedule.count / itemsOnPage));
        setCurrentItems(schedule.rows.rows.map(item => {
                return {
                    onClick: (() => {

                    }),
                    key: `${item.scheduleDate}${item.beginTime}`,
                    cells: [item.scheduleDate, item.beginTime, item.endTime]
                };
            }
        ));


    }, [currentPage]);

    return (
        <>
        <CenterInnerLayoutH style={{marginTop: "35px"}}>
            <h3>My schedule</h3>
        </CenterInnerLayoutH>
        <CenterInnerLayout style={{marginTop: "200px"}}>

            <hr/>
            {working && <PaginationList
                choosePage={choosePage}
                headers={["Day", "Begin time", "End time"]}
                pageCount={pageCount}
                items={currentItems}
            />
            }
            {!working &&
                <CenterInnerLayout style={{position: "absolute"}}>
                    <img width="130px" src="https://cdn-icons-png.flaticon.com/512/3919/3919475.png"/>
                    <h4 style={{marginTop: "40px"}}>You are free</h4>
                </CenterInnerLayout>}
        </CenterInnerLayout>
        </>
    );
};

export default MySchedulePage;