import React, {useEffect, useState} from 'react';
import {useNavigate, useParams} from 'react-router-dom';
import {cancelAppointment, getAppointment, getPerformedProcedureData} from '../../http/appointmentApi';
import PatientInfo from "./PatientInfo";
import DoctorInfo from "./DoctorInfo";
import {Button, Form, Modal} from "react-bootstrap";
import ProceduresInfo from "./ProceduresInfo";
import EquipmentDeviceReadOnly from "./EquipmentDeviceReadOnly";
import {getUserFromToken} from "../../logic/auth";
import DIRECTOR from "../../utils/enums/userRoles.enum";
import PATIENT from "../../utils/enums/userRoles.enum";
import REGISTRANT from "../../utils/enums/userRoles.enum";
import DOCTOR from "../../utils/enums/userRoles.enum";
import CenterInnerLayout from "../../components/layout/CenterInnerLayoutV";

const AppointmentPage = () => {

    const {appointmentId} = useParams();

    const [appointment, setAppointment] = useState();

    const [procedureName, setProcedureName] = useState();
    const [procedurePerformedPrice, setProcedurePerformedPrice] = useState();
    const [procedureMinimalCategory, setProcedureMinimalCategory] = useState();
    const [procedureDiscount, setProcedureDiscount] = useState();
    const [procedureEquipment, setProcedureEquipment] = useState();
    const [showEditButton, setShowEditButton] = useState(false);

    const [showModal, setShowModal] = useState(false);
    const [enableEditButton, setEnableEditButton] = useState(false);

    const [showAppointment, setShowAppointment] = useState(true);
    const [cancelingAllowed, setCancelingAllowed] = useState(false);
    const [appointmentCanceled, setAppointmentCanceled] = useState(false);

    const [appointmentFinished, setAppointmentFinished] = useState(false);

    async function showProcedureModal(performedProcedureId) {
        const procedureData = (await getPerformedProcedureData(performedProcedureId)).data;
        setProcedureName(procedureData.procedureName);
        setProcedurePerformedPrice(procedureData.fullPrice.substring(1));
        setProcedureMinimalCategory(procedureData.minimalCategory);
        setProcedureDiscount(procedureData.discount);
        setProcedureEquipment(procedureData.equipment);
        setShowModal(true);
    }

    const handleClose = () => {
        setShowModal(false);
    };

    const navigate = useNavigate();

    const cancelThisAppointment = async () => {
        await cancelAppointment(appointmentId);
        window.location.reload();
    }

    useEffect(async () => {
        const token = await getUserFromToken();
        const role = token.role;
        const userAccountId = token.accountId;
        const appointmentData = (await getAppointment(appointmentId)).data;
        appointmentData.doctor.category = appointmentData.category;
        console.log(appointmentData);
        console.log(userAccountId);
        setAppointment(appointmentData);
        setAppointmentCanceled(appointmentData.appointmentStatus === "cancelled" || appointmentData.appointmentStatus === "canceled");
        setCancelingAllowed(appointmentData.appointmentStatus === "scheduled");

        setAppointmentFinished(appointmentData.appointmentStatus === "finished");
        setShowEditButton(role === DIRECTOR.DIRECTOR || role === DIRECTOR.DOCTOR);
        setEnableEditButton(role === DIRECTOR.DIRECTOR ||
            (appointmentData.doctor.accountId === userAccountId));
        setShowAppointment(role === DIRECTOR.DIRECTOR || role === REGISTRANT ||
            (role === DOCTOR.DOCTOR && appointmentData.doctor.accountId === userAccountId) ||
            (role === PATIENT.PATIENT && appointmentData.patient.accountId === userAccountId));
    }, []);

    return (
        <>
            {!showAppointment &&

                <CenterInnerLayout style={{marginTop: "100px"}}>
                    <img style={{width: "300px"}} src="https://cdn-icons-png.flaticon.com/512/3064/3064197.png"/>
                    <h2 style={{marginTop: "25px"}}>You don't have access to this page</h2>
                </CenterInnerLayout>}

            {
                showAppointment &&
                <div style={{
                    marginTop: "35px",
                    marginBottom: "25px",
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center"
                }}>
                    <div style={{display: "flex", flexDirection: "row", justifyContent: "space-around",}}>
                        {appointment && <DoctorInfo doctor={appointment.doctor}/>}
                        {appointment && <PatientInfo patient={appointment.patient}/>}
                    </div>
                    <br/>
                    <hr/>
                    {appointment &&

                        <CenterInnerLayout>
                            <br/>
                            <Form>

                                <h3>Appointment information</h3>
                                <br/>

                                <CenterInnerLayout>

                                    {/* Scheduled date */}
                                    <Form.Group className="mb-3" controlId="controlInput">
                                        <Form.Label>Appointment scheduled</Form.Label>
                                        <Form.Control style={{width: "200px", borderRadius: "10px"}}
                                                      readOnly={true}
                                                      type={"date"}
                                                      value={appointment.scheduledAppointmentDate}
                                                      placeholder="Scheduled date"
                                                      autoFocus/>
                                    </Form.Group>

                                    {/* Approximate begin time */}
                                    <Form.Group className="mb-3" controlId="controlInput">
                                        <Form.Label>Approximate begin time</Form.Label>
                                        <Form.Control style={{width: "200px", borderRadius: "10px"}}
                                                      readOnly={true}
                                                      type={"time"}
                                                      value={appointment.scheduledAppointmentStartTime}
                                                      placeholder="Approximate start time"
                                                      autoFocus/>
                                    </Form.Group>

                                    {/* Approximate end time */}
                                    <Form.Group className="mb-3" controlId="controlInput">
                                        <Form.Label>Approximate end time</Form.Label>
                                        <Form.Control style={{width: "200px", borderRadius: "10px"}}
                                                      readOnly={true}
                                                      type={"time"}
                                                      value={appointment.approximateEndTime}
                                                      placeholder="Approximate end time"
                                                      autoFocus/>
                                    </Form.Group>

                                    {/* Diagnosis */}
                                    <Form.Group className="mb-3" controlId="controlInput">
                                        <Form.Label>Diagnosis</Form.Label>
                                        <Form.Control
                                            size="lg"
                                            style={{width: "320px", borderRadius: "10px"}}
                                            readOnly={true}
                                            value={appointment.diagnosis ? appointment.diagnosis : '—'}
                                            autoFocus/>
                                    </Form.Group>

                                    {/* Date */}

                                    <Form.Group className="mb-3" controlId="controlInput">
                                        <Form.Label>Date</Form.Label>
                                        <Form.Control style={{width: "200px", borderRadius: "10px"}}
                                                      readOnly={true}
                                                      type={"date"}
                                                      value={appointment.finishedAppointmentDate ?
                                                          appointment.finishedAppointmentDate : '—'}
                                        />
                                    </Form.Group>

                                    {/* Begin time */}
                                    <Form.Group className="mb-3" controlId="controlInput">
                                        <Form.Label>Begin time</Form.Label>
                                        <br/>
                                        <div style={{display: "inline-flex"}}>
                                            <Form.Control
                                                readOnly={true}
                                                style={{
                                                    width: "200px",
                                                    borderRadius: "10px"
                                                }}
                                                type="time"
                                                value={appointment.finishedAppointmentStartTime ?
                                                    appointment.finishedAppointmentStartTime : '—'}
                                            />
                                        </div>
                                    </Form.Group>

                                    {/* End time */}
                                    <Form.Group className="mb-3" controlId="controlInput">
                                        <Form.Label>End time</Form.Label>
                                        <br/>
                                        <div style={{display: "inline-flex"}}>
                                            <Form.Control
                                                readOnly={true}
                                                style={{width: "200px", borderRadius: "10px"}}
                                                type="time"
                                                value={appointment.finishedAppointmentEndTime ? appointment.finishedAppointmentEndTime : '—'}
                                            />
                                        </div>
                                    </Form.Group>

                                    {/* Treatment */}
                                    <Form.Group className="mb-3" controlId="controlInput">
                                        <Form.Label>Treatment</Form.Label>
                                        <br/>
                                        <div style={{display: "inline-flex"}}>

                                            <Form.Control
                                                size="lg"
                                                readOnly={true}
                                                style={{width: "320px", borderRadius: "10px"}}
                                                type="text"
                                                value={appointment.treatment ? appointment.treatment : '—'}
                                            />

                                        </div>
                                    </Form.Group>

                                    {/* Patient state */}
                                    <Form.Group className="mb-3" controlId="controlInput">
                                        <Form.Label>Patient state</Form.Label>
                                        <br/>
                                        <div style={{display: "inline-flex"}}>

                                            <Form.Control
                                                size="lg"
                                                readOnly={true}
                                                style={{width: "320px", borderRadius: "10px"}}
                                                type="text"
                                                value={appointment.patient.patientState ? appointment.patient.patientState : '—'}
                                            />

                                        </div>
                                    </Form.Group>

                                </CenterInnerLayout>
                            </Form>
                        </CenterInnerLayout>
                    }
                    <CenterInnerLayout>
                        <h4 style={{color: "#dc3545"}}
                            hidden={!appointmentCanceled}>
                            The appointment has been canceled
                        </h4>
                    </CenterInnerLayout>
                    {appointment && appointmentFinished && <ProceduresInfo
                        appointmentId={appointmentId}
                        onRowClickAction={showProcedureModal}
                    />}
                    <Modal scrollable={true} show={showModal} onHide={handleClose}>
                        <Modal.Header closeButton={true}>
                            Performed procedure
                        </Modal.Header>
                        <Modal.Body>
                            <div style={{display: "flex", flexDirection: "column", justifyContent: "center"}}>

                                <h3>Appointment information</h3>

                                <Form>

                                    {/* Procedure name */}
                                    <Form.Group className="mb-3" controlId="controlInput">
                                        <Form.Label>ProcedureName</Form.Label>
                                        <Form.Control readOnly={true}
                                                      value={procedureName} placeholder="Inventory number"/>
                                    </Form.Group>

                                    {/* Minimal category */}

                                    <Form.Group className="mb-3" controlId="controlInput">
                                        <Form.Label>Minimal category</Form.Label>
                                        <Form.Control readOnly={true}
                                                      value={procedureMinimalCategory} placeholder="Minimal category"
                                        />
                                    </Form.Group>

                                    {/* Price */}
                                    <Form.Group className="mb-3" controlId="controlInput">
                                        <Form.Label>Price</Form.Label>
                                        <br/>
                                        <div style={{display: "inline-flex"}}>
                                    <span style={{borderRadius: "10px 0 0 10px"}} className="input-group-text">
                                        $
                                    </span>
                                            <Form.Control readOnly={true}
                                                          style={{borderRadius: "0 10px 10px 0"}}
                                                          type="text"
                                                          value={procedurePerformedPrice} placeholder="Price"
                                            />
                                        </div>

                                    </Form.Group>

                                    {/* Discount */}
                                    <Form.Group className="mb-3" controlId="controlInput">
                                        <Form.Label>Discount</Form.Label>
                                        <br/>
                                        <div style={{display: "inline-flex"}}>
                                    <span style={{borderRadius: "10px 0 0 10px"}} className="input-group-text">
                                        %
                                    </span>
                                            <Form.Control readOnly={true}
                                                          style={{borderRadius: "0 10px 10px 0"}}
                                                          type="text"
                                                          value={procedureDiscount} placeholder="Discount"
                                            />
                                        </div>
                                    </Form.Group>
                                </Form>

                                <div style={{display: "flex", flexDirection: "column", justifyContent: "center"}}>
                                    <CenterInnerLayout style={{marginBottom: "20px"}}>
                                        <h2>Equipment</h2>
                                    </CenterInnerLayout>
                                    {
                                        procedureEquipment && procedureEquipment.map(
                                            item => (
                                                <EquipmentDeviceReadOnly
                                                    equipmentId={item.equipmentId}
                                                    equipmentName={item.equipmentName}
                                                    inventoryNumber={item.inventoryNumber}
                                                />
                                            )
                                        )
                                    }
                                </div>

                            </div>
                        </Modal.Body>
                    </Modal>
                    {showEditButton && appointment && !appointmentCanceled &&
                        <CenterInnerLayout>
                            <div style={{display: "flex", gap: "25px"}}>
                                <Button
                                    hidden={!enableEditButton}
                                    style={{backgroundColor: "#FF8C00", borderColor: "#FF8C00"}}
                                    onClick={() => navigate('/appointments/' + appointmentId + '/edit', {replace: false})}>
                                    Edit
                                </Button>
                                <Button
                                    hidden={!enableEditButton || !cancelingAllowed}
                                    style={{backgroundColor: "#b72339", borderColor: "#b72339"}}
                                    onClick={() => {
                                        cancelThisAppointment();
                                    }
                                    }
                                >
                                    Cancel
                                </Button>
                            </div>
                        </CenterInnerLayout>
                    }
                </div>
            }
        </>
    );
};

export default AppointmentPage;