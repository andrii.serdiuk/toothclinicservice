import React, {useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import {getUserFromToken} from "../../logic/auth";
import {getAllAppointments, getMyAppointments} from "../../http/userApi";
import {pointerOnHover} from "../../utils/stylesConstants";
import ReactPaginate from "react-paginate";
import Select from "react-select";
import {DoctorDropDown, PatientDropDown} from "../../components/common";
import {Form} from "react-bootstrap";
import {getCurrentDate} from "../../utils/common";
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

const cssFixDate = `
    .react-datepicker__input-container{
        width: inherit;
    }

    .react-datepicker__input-container input{
        width: inherit;
    }

    .react-datepicker-wrapper{
        width: 100%;
    }
`;

const AllAppointmentsPage = () => {
    const [items, setItems] = useState(null);
    const [currentPage, setCurrentPage] = useState(0);
    const [itemsOnPage, _] = useState(20);
    const [pageCount, setPageCount] = useState(0);
    const [userRole, setUserRole] = useState(null);
    const [selectedDoctorId, setSelectedDoctorId] = useState(null);
    const [selectedPatientId, setSelectedPatientId] = useState(null);
    const [dateFrom, setDateFrom] = useState(null);
    const [dateTo, setDateTo] = useState(null);
    const [appointmentStatus, setAppointmentStatus] = useState(null);

    const navigate = useNavigate();

    const dateTransform = date => {
        return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
    }

    const initList = async () => {
        try {
            const role = (await getUserFromToken()).role.toLowerCase();
            setUserRole(role);
            const res = await getAllAppointments(itemsOnPage, currentPage,
                selectedDoctorId, selectedPatientId,
                dateFrom, dateTo, appointmentStatus);
            setPageCount(Math.ceil(res.count / itemsOnPage));
            setItems(res.appointments);
        } catch (e) {
            console.log(e);
        }
    }

    useEffect(async () => {
        await initList();
    }, [currentPage, selectedDoctorId, selectedPatientId,
        dateFrom, dateTo, appointmentStatus]);

    const getRowFormatting = ({appointmentStatus}) => {
        if (appointmentStatus === 'finished') {
            return "table-success";
        } else if (appointmentStatus === 'scheduled') {
            return "table-warning";
        } else if (appointmentStatus === 'cancelled') {
            return "table-danger";
        } else if (appointmentStatus === 'unattended') {
            return "table-primary";
        }

        return '';
    }

    return (
        <div className="h-100 d-flex flex-column">
            <style>{cssFixDate}</style>
            <div className="row w-100">
                <div className="col-3">
                    <div className="mb-2">Doctor</div>
                    <DoctorDropDown
                        changeDoctor={e => setSelectedDoctorId(e ? e.value : e)}
                    />
                </div>
                <div className="col-3">
                    <div className="mb-2">Patient</div>
                    <PatientDropDown
                        changePatient={e => setSelectedPatientId(e ? e.value : e)}
                    />
                </div>
                <div className="col-3 d-flex gap-3">
                    <div>
                        <div className="mb-2">Date from</div>
                        <DatePicker
                            selected={dateFrom}
                            onChange={date => setDateFrom(date)}
                            dateFormat={"dd.MM.yyyy"}
                            isClearable
                        />
                    </div>
                    <div>
                        <div className="mb-2">Date to</div>
                        <DatePicker
                            selected={dateTo}
                            onChange={date => setDateTo(date)}
                            dateFormat={"dd.MM.yyyy"}
                            isClearable
                        />
                    </div>
                </div>
                <div className="col-3">
                    <div className="mb-2">Status</div>
                    <Select
                        onChange={e => setAppointmentStatus(e ? e.value : e)}
                        options={
                            [
                                {value: "finished", label: "Finished"},
                                {value: "scheduled", label: "Scheduled"},
                                {value: "cancelled", label: "Cancelled"},
                            ]
                        }
                        isClearable
                    />
                </div>

            </div>
            <style>{pointerOnHover}</style>
            <div style={{flex: "1 1 auto"}} className="overflow-auto">
                <table className="table w-100">
                    <thead className="thead-dark">
                    <tr>
                        <th className="col-1" scope="col">Appointment id</th>
                        <th className="col-2" scope="col">Appointment date</th>
                        <th className="col-2" scope="col">Patient full name</th>
                        <th className="col-2" scope="col">Doctor full name</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        items && items.map(item =>
                            <tr onClick={() => {
                                navigate(`/appointments/${item.appointmentId}`)
                            }} key={item.appointmentId}
                                className={"cursor-pointer " + getRowFormatting(item)}>
                                <th>{item.appointmentId}</th>
                                <td>{item.appointmentDate}</td>
                                <td>{`${item.patientSecondName} ${item.patientFirstName[0]}. ${item.patientLastName[0]}`}</td>
                                <td>{`${item.doctorSecondName} ${item.doctorFirstName[0]}. ${item.doctorLastName[0]}`}</td>
                            </tr>
                        )
                    }
                    </tbody>
                </table>
            </div>
            <div>
                <div className="pagination-list-print-hidden">
                    <ReactPaginate
                        breakLabel="..."
                        nextLabel=">"
                        onPageChange={e => setCurrentPage(e.selected)}
                        pageRangeDisplayed={5}
                        pageCount={pageCount}
                        previousLabel="<"
                        renderOnZeroPageCount={null}
                        pageClassName="page-item"
                        pageLinkClassName="page-link"
                        previousClassName="page-item"
                        previousLinkClassName="page-link"
                        nextClassName="page-item"
                        nextLinkClassName="page-link"
                        breakClassName="page-item"
                        breakLinkClassName="page-link"
                        containerClassName="pagination justify-content-center"
                        activeClassName="active"
                    />
                </div>
            </div>
        </div>
    );
}

export default AllAppointmentsPage;