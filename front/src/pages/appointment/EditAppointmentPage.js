import {useParams} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {
    createFinishedAppointment,
    getAppointment,
    getEquipmentAndAllDevicesForProcedure,
    getPerformedProcedureData, removePerformedProcedureById, updateAppointmentInfo, updatePerformedProcedure
} from "../../http/appointmentApi";
import PatientInfo from "./PatientInfo";
import DoctorInfo from "./DoctorInfo";
import AddProcedureToAppointmentModal from "./AddProcedureToAppointmentModal";
import ProceduresInfo from "./ProceduresInfo";
import {Button, Form, Modal, Spinner} from "react-bootstrap";
import EquipmentListDevicesSelectedSpecified from "./EquipmentListDevicesSelectedSpecified";
import {getCurrentDate} from "../../utils/common";
import CenterInnerLayout from "../../components/layout/CenterInnerLayoutV";


const EditAppointmentPage = () => {
    const {appointmentId} = useParams();

    const [appointment, setAppointment] = useState();

    const [performedProcedureId, setPerformedProcedureId] = useState('');
    const [procedureName, setProcedureName] = useState('');
    const [procedurePerformedPrice, setProcedurePerformedPrice] = useState('');
    const [procedureMinimalCategory, setProcedureMinimalCategory] = useState('');
    const [procedureDiscount, setProcedureDiscount] = useState(0);
    const [equipmentRequired, setEquipmentRequired] = useState(null);
    const [map, _] = useState(new Map());

    const [invalidDiscount, setInvalidDiscount] = useState(false);
    const [emptyDiscount, setEmptyDiscount] = useState(false);
    const [discountNotInRange, setDiscountNotInRange] = useState(false);

    const [scheduledDate, setScheduledDate] = useState();
    const [approximateStartTime, setApproximateStartTime] = useState();
    const [approximateEndTime, setApproximateEndTime] = useState();

    const [diagnosis, setDiagnosis] = useState('');
    const [date, setDate] = useState('');
    const [patientState, setPatientState] = useState('');
    const [treatment, setTreatment] = useState('');
    const [beginTime, setBeginTime] = useState('');
    const [endTime, setEndTime] = useState('');

    const [wrongTime, setWrongTime] = useState(false);
    const [emptyBeginTime, setEmptyBeginTime] = useState(false);
    const [emptyEndTime, setEmptyEndTime] = useState(false);
    const [emptyDiagnosis, setEmptyDiagnosis] = useState(false);
    const [emptyTreatment, setEmptyTreatment] = useState(false);
    const [emptyPatientState, setEmptyPatientState] = useState(false);

    const [hideEmptyBeginTime, setHideEmptyBeginTime] = useState(false);
    const [hideEmptyEndTime, setHideEmptyEndTime] = useState(false);
    const [hideEmptyDiagnosisError, setHideEmptyDiagnosisError] = useState(false);
    const [hideEmptyTreatmentError, setHideEmptyTreatmentError] = useState(false);
    const [hideEmptyPatientStateError, setHideEmptyPatientStateError] = useState(false);

    const [hasAlreadyBeenStarted, setHasAlreadyBeenStarted] = useState(false);

    const [wrongDate, setWrongDate] = useState(false);

    const discountRegex = /^\d*$/g;

    const discountIsValid = (string) => {
        return discountRegex.test(string);
    };

    const notInRange = (str) => {
        return str > 100;
    }

    function setChosenDevice(equipmentId, deviceId) {
        map.set(Number(equipmentId), Number(deviceId));
    }

    const [isLoading, setIsLoading] = useState(false);

    const [showModal, setShowModal] = useState(false);

    useEffect(async () => {
        const appointmentData = (await getAppointment(appointmentId)).data;
        setHasAlreadyBeenStarted(appointmentData.finishedAppointmentId !== undefined);
        setAppointment(appointmentData);
        appointmentData.doctor.category = appointmentData.category;
        console.log(appointmentData);
        setScheduledDate(appointmentData.scheduledAppointmentDate);
        setApproximateStartTime(appointmentData.scheduledAppointmentStartTime);
        setApproximateEndTime(appointmentData.approximateEndTime);
        const beginTime = appointmentData.finishedAppointmentStartTime;
        const endTime = appointmentData.finishedAppointmentEndTime;

        if (beginTime !== undefined) {
            setBeginTime(beginTime);
        }

        if (endTime !== undefined) {
            setEndTime(endTime);
        }

        setDiagnosis(appointmentData.diagnosis);
        const finishedDate = appointmentData.finishedAppointmentDate;
        if (finishedDate !== undefined) {
            setDate(finishedDate);
        } else {
            setDate(getCurrentDate());
        }
        setPatientState(appointmentData.patient.patientState);
        setTreatment(appointmentData.treatment);

        const beginTimeIsUndefined = (beginTime === undefined);
        const endTimeIsUndefined = (endTime === undefined);
        const treatmentIsUndefined = (appointmentData.treatment === undefined);
        const diagnosisIsUndefined = (appointmentData.diagnosis === undefined);
        const patientStateUndefined = (appointmentData.patient.patientState === undefined);

        setEmptyBeginTime(beginTimeIsUndefined);
        setEmptyEndTime(endTimeIsUndefined);
        setEmptyTreatment(treatmentIsUndefined);
        setEmptyDiagnosis(diagnosisIsUndefined);
        setEmptyPatientState(patientStateUndefined);

        setHideEmptyBeginTime(beginTimeIsUndefined);
        setHideEmptyEndTime(endTimeIsUndefined);
        setHideEmptyTreatmentError(treatmentIsUndefined);
        setHideEmptyDiagnosisError(diagnosisIsUndefined);
        setHideEmptyPatientStateError(patientStateUndefined);
    }, []);

    async function showEditProcedureModal(performedProcedureId) {
        setPerformedProcedureId(performedProcedureId);
        const procedureData = (await getPerformedProcedureData(performedProcedureId)).data;
        const procedureId = procedureData.procedureId;
        const data = (await getEquipmentAndAllDevicesForProcedure(procedureId)).data;

        const procedureEquipment = procedureData.equipment;

        for (const element of data) {
            let foundElement = {};
            for (const procedureEquipmentElement of procedureEquipment) {
                if (procedureEquipmentElement.equipmentId === element.equipmentId) {
                    foundElement = procedureEquipmentElement;
                }
            }
            element.selectedInventoryNumber = foundElement.inventoryNumber;
        }

        setProcedureName(procedureData.procedureName);
        setProcedurePerformedPrice(procedureData.fullPrice.substring(1));
        setProcedureMinimalCategory(procedureData.minimalCategory);
        setProcedureDiscount(procedureData.discount);
        setEquipmentRequired(data);

        setShowModal(true);
    }

    const updateProcedures = async e => {
        e.stopPropagation();
        e.preventDefault();

        try {

            const array = [];
            let i = 0;
            for (const object of map) {
                array[i++] = object;
            }

            const obj = {
                performedProcedureId: performedProcedureId,
                discount: procedureDiscount,
                equipmentDevices: equipmentRequired,
                toChange: array
            };

            console.log(obj);
            await updatePerformedProcedure(performedProcedureId, obj);
            return true;
        } catch (e) {
            return false;
        } finally {
            handleClose();
            setIsLoading(false);
        }

    };

    const removePerformedProcedure = async e => {
        e.stopPropagation();
        e.preventDefault();
        await removePerformedProcedureById(performedProcedureId);
        handleClose();
    }

    const handleClose = () => {
        setPerformedProcedureId('');
        setProcedureName('');
        setProcedurePerformedPrice('');
        setProcedureMinimalCategory('');
        setProcedureDiscount(0);
        setInvalidDiscount(false);
        setEmptyDiscount(false);
        setDiscountNotInRange(false);

        setShowModal(false);
        window.location.reload();
    };

    const updateAppointment = async (e) => {
        e.stopPropagation();
        e.preventDefault();
        const obj = {
            date: date,
            diagnosis: diagnosis,
            patientState: patientState,
            treatment: treatment,
            beginTime: beginTime,
            endTime: endTime
        };
        try {
            if (!hasAlreadyBeenStarted) {
                await createFinishedAppointment(appointmentId, obj);
            } else {
                await updateAppointmentInfo(appointmentId, obj);
            }
            return true;
        } catch (e) {
            return false;
        } finally {
            handleClose();
            setIsLoading(false);
            window.location.reload();
        }
    };

    return (
        <CenterInnerLayout>

            <div style={{
                display: "flex", flexDirection: "column", justifyContent: "center",
                gap: "40px", marginTop: "40px"
            }}>
                <div style={{display: "flex", flexDirection: "row", justifyContent: "space-between", gap: "240px"}}>
                    {appointment && <DoctorInfo doctor={appointment.doctor}/>}
                    {appointment && <PatientInfo patient={appointment.patient}/>}
                </div>
                <hr/>
                <CenterInnerLayout>
                    {appointment &&

                        <Form onSubmit={updateAppointment}>

                            <h3>Appointment information</h3>
                            <br/>
                            <CenterInnerLayout>

                                {/* Scheduled date */}
                                <Form.Group className="mb-3" controlId="controlInput">
                                    <Form.Label>Appointment scheduled</Form.Label>
                                    <Form.Control style={{width: "200px", borderRadius: "10px"}}
                                                  readOnly={true}
                                                  type={"date"}
                                                  value={scheduledDate} placeholder="Scheduled date"
                                                  autoFocus/>
                                </Form.Group>

                                {/* Approximate begin time */}
                                <Form.Group className="mb-3" controlId="controlInput">
                                    <Form.Label>Approximate begin time</Form.Label>
                                    <Form.Control style={{width: "200px", borderRadius: "10px"}}
                                                  readOnly={true}
                                                  type={"time"}
                                                  value={approximateStartTime}
                                                  placeholder="Approximate start time"
                                                  autoFocus/>
                                </Form.Group>

                                {/* Approximate end time */}
                                <Form.Group className="mb-3" controlId="controlInput">
                                    <Form.Label>Approximate end time</Form.Label>
                                    <Form.Control style={{width: "200px", borderRadius: "10px"}}
                                                  readOnly={true}
                                                  type={"time"}
                                                  value={approximateEndTime}
                                                  placeholder="Approximate end time"
                                                  autoFocus/>
                                </Form.Group>

                                {/* Diagnosis */}
                                <Form.Group className="mb-3" controlId="controlInput">
                                    <Form.Label>Diagnosis</Form.Label>
                                    <Form.Control
                                        size="lg"
                                        style={{width: "320px", borderRadius: "10px"}}
                                        onChange={e => {
                                            const value = e.target.value;
                                            setDiagnosis(value);
                                            setEmptyDiagnosis(value.trim() === '');
                                            setHideEmptyDiagnosisError(false);
                                        }}
                                        value={diagnosis}
                                        placeholder="Diagnosis"
                                        autoFocus/>
                                    <div style={{color: "#dc3545"}}
                                         hidden={hideEmptyDiagnosisError || !emptyDiagnosis}>
                                        Empty diagnosis
                                    </div>
                                </Form.Group>

                                {/* Date */}

                                <Form.Group className="mb-3" controlId="controlInput">
                                    <Form.Label>Date</Form.Label>
                                    <Form.Control
                                                style={{width: "200px", borderRadius: "10px"}}
                                                  onChange={(e) => {
                                                      setDate(e.target.value);
                                                      setWrongDate(e.target.value > getCurrentDate());
                                                  }
                                                  }
                                                  type={"date"}
                                                  value={date} placeholder="Date"
                                    />
                                    <div style={{color: "#dc3545"}}
                                         hidden={!wrongDate}>
                                        Wrong date
                                    </div>
                                </Form.Group>

                                {/* Begin time */}
                                <Form.Group className="mb-3" controlId="controlInput">
                                    <Form.Label>Begin time</Form.Label>
                                    <br/>
                                    <div style={{display: "inline-flex"}}>
                                        <Form.Control
                                            onChange={e => {
                                                const value = e.target.value;
                                                setWrongTime(endTime <= value);
                                                setBeginTime(value);
                                                setEmptyBeginTime(value.trim() === '');
                                                setHideEmptyBeginTime(false);
                                            }}
                                            style={{
                                                width: "200px",
                                                borderRadius: "10px"
                                            }}
                                            type="time"
                                            value={beginTime}
                                        />
                                    </div>
                                    <div style={{color: "#dc3545"}}
                                         hidden={hideEmptyBeginTime || !emptyBeginTime}>
                                        Empty begin time
                                    </div>
                                </Form.Group>

                                {/* End time */}
                                <Form.Group className="mb-3" controlId="controlInput">
                                    <Form.Label>End time</Form.Label>
                                    <br/>
                                    <div style={{display: "inline-flex"}}>
                                        <Form.Control
                                            onChange={e => {
                                                const value = e.target.value;
                                                setEndTime(value);
                                                setWrongTime(value <= beginTime);
                                                setEmptyEndTime(e.target.value.trim() === '');
                                                setHideEmptyEndTime(false);
                                            }}
                                            style={{width: "200px", borderRadius: "10px"}}
                                            type="time"
                                            value={endTime}
                                        />
                                    </div>
                                    <div style={{color: "#dc3545"}}
                                         hidden={hideEmptyEndTime || !emptyEndTime}>
                                        Empty end time
                                    </div>
                                    <div style={{color: "#dc3545"}} hidden={emptyEndTime || !wrongTime}>
                                        Wrong end time
                                    </div>
                                </Form.Group>

                                {/* Treatment */}
                                <Form.Group className="mb-3" controlId="controlInput">
                                    <Form.Label>Treatment</Form.Label>
                                    <br/>
                                    <div style={{display: "inline-flex"}}>

                                        <Form.Control
                                            size="lg"
                                            onChange={e => {
                                                const value = e.target.value;
                                                setTreatment(value);
                                                setEmptyTreatment(value.trim() === '');
                                                setHideEmptyTreatmentError(false);
                                            }}
                                            style={{width: "320px", borderRadius: "10px"}}
                                            type="text"
                                            value={treatment}
                                            placeholder="Treatment"
                                        />

                                    </div>
                                    <div style={{color: "#dc3545"}}
                                         hidden={hideEmptyTreatmentError || !emptyTreatment}>
                                        Empty treatment
                                    </div>
                                </Form.Group>

                                {/* Patient state */}
                                <Form.Group className="mb-3" controlId="controlInput">
                                    <Form.Label>Patient state</Form.Label>
                                    <br/>
                                    <div style={{display: "inline-flex"}}>

                                        <Form.Control
                                            size="lg"
                                            style={{width: "320px", borderRadius: "10px"}}
                                            type="text"
                                            value={patientState}
                                            placeholder="Patient state"
                                            onChange={(e) => {
                                                setPatientState(e.target.value.trim());
                                                setHideEmptyPatientStateError(false);
                                                setEmptyPatientState(e.target.value.trim() === '');
                                            }
                                            }
                                        />
                                    </div>
                                    <div style={{color: "#dc3545"}}
                                         hidden={hideEmptyPatientStateError || !emptyPatientState}>
                                        Empty patient state
                                    </div>
                                </Form.Group>


                                <Button onClick={(e) => updateAppointment(e)}
                                        disabled={wrongTime || emptyTreatment || emptyDiagnosis
                                            || emptyPatientState || emptyBeginTime || emptyEndTime || wrongDate}>
                                    Save
                                </Button>
                            </CenterInnerLayout>
                        </Form>
                    }
                </CenterInnerLayout>

                {hasAlreadyBeenStarted &&
                    <CenterInnerLayout>
                        <hr/>
                        <br/>
                        <h3>Procedures</h3>
                        <br/>
                    </CenterInnerLayout>
                }
                {hasAlreadyBeenStarted && appointment && <ProceduresInfo

                    appointmentId={appointmentId}
                    onRowClickAction={showEditProcedureModal}

                />}
                {hasAlreadyBeenStarted && appointment &&
                    <CenterInnerLayout>
                        <AddProcedureToAppointmentModal
                            appoinmentId={appointmentId}
                            doctorCategory={appointment.doctor.category}
                        />
                    </CenterInnerLayout>
                }
                <Modal show={showModal} onHide={() => setShowModal(false)}>
                    <Modal.Header closeButton={true}>
                        Performed procedure
                    </Modal.Header>
                    <Modal.Body>
                        <div className="d-flex justify-content-center">
                            <Form method="put" onSubmit={updatePerformedProcedure}>

                                {/* Procedure name */}
                                <Form.Group className="mb-3" controlId="controlInput">
                                    <Form.Label>ProcedureName</Form.Label>
                                    <Form.Control readOnly={true}
                                                  onChange={e => {
                                                      const value = e.target.value;
                                                      setProcedureName(value);
                                                  }}
                                                  value={procedureName} placeholder="Procedure name"
                                                  autoFocus/>
                                </Form.Group>

                                {/* Minimal category */}

                                <Form.Group className="mb-3" controlId="controlInput">
                                    <Form.Label>Minimal category</Form.Label>
                                    <Form.Control readOnly={true}
                                                  onChange={e => {
                                                      const value = e.target.value;
                                                      setProcedureMinimalCategory(value);
                                                  }}
                                                  value={procedureMinimalCategory} placeholder="Minimal category"
                                    />
                                </Form.Group>

                                {/* Price */}
                                <Form.Group className="mb-3" controlId="controlInput">
                                    <Form.Label>Price</Form.Label>
                                    <br/>
                                    <div style={{display: "inline-flex"}}>
                                    <span style={{borderRadius: "10px 0 0 10px"}} className="input-group-text">
                                        $
                                    </span>
                                        <Form.Control readOnly={true}
                                                      onChange={e => {
                                                          const value = e.target.value;
                                                          setProcedurePerformedPrice(value);
                                                      }}
                                                      style={{borderRadius: "0 10px 10px 0"}}
                                                      type="text"
                                                      value={procedurePerformedPrice} placeholder="Price"
                                        />
                                    </div>
                                </Form.Group>

                                {/* Discount */}
                                <Form.Group className="mb-3" controlId="controlInput">
                                    <Form.Label>Discount</Form.Label>
                                    <br/>
                                    <div style={{display: "inline-flex"}}>
                                    <span style={{borderRadius: "10px 0 0 10px"}} className="input-group-text">
                                        %
                                    </span>
                                        <Form.Control readOnly={false}
                                                      onChange={e => {
                                                          const value = e.target.value;
                                                          console.log(value);
                                                          setProcedureDiscount(value);
                                                          setInvalidDiscount(!discountIsValid(value));
                                                          setEmptyDiscount(value.trim() === '');
                                                          setDiscountNotInRange(notInRange(value));
                                                      }}
                                                      style={{borderRadius: "0 10px 10px 0"}}
                                                      type="text"
                                                      value={procedureDiscount} placeholder="Discount"
                                        />

                                    </div>
                                    <div style={{color: "#dc3545"}} hidden={!emptyDiscount}>
                                        Empty discount
                                    </div>
                                    <div style={{color: "#dc3545"}} hidden={!invalidDiscount}>
                                        Incorrect discount
                                    </div>
                                    <div style={{color: "#dc3545"}} hidden={!discountNotInRange}>
                                        Discount must be less than 101
                                    </div>
                                </Form.Group>
                            </Form>
                        </div>

                        <div
                            style={{
                                display: "flex", flexDirection: "column",
                                justifyContent: "center", gap: "20px"
                            }}>
                            {/*Listing all the required equipment*/}
                            {
                                equipmentRequired && equipmentRequired.map(item => (
                                    <EquipmentListDevicesSelectedSpecified
                                        onChangeFunction={setChosenDevice}
                                        key={item.equipmentId}
                                        equipmentName={item.equipmentName}
                                        equipmentId={item.equipmentId}
                                        inventoryNumberList={item.inventoryNumberList}
                                        selectedInventoryNumber={item.selectedInventoryNumber}
                                    />
                                ))
                            }
                        </div>
                    </Modal.Body>
                    <Modal.Footer style={{
                        display: "flex",
                        justifyContent: "center",
                    }}>
                        <div style={{display: "flex", flexDirection: "row", gap: "20px", marginTop: "30px"}}>
                            <Button
                                style={{backgroundColor: "#FF8C00", borderColor: "#FF8C00"}}
                                disabled={invalidDiscount || emptyDiscount}
                                onClick={(e) => {
                                    updateProcedures(e);
                                }}>
                                <span hidden={isLoading}> Edit </span>
                                <Spinner
                                    as="span"
                                    animation="border"
                                    size="sm"
                                    role="status"
                                    hidden={!isLoading}
                                />
                            </Button>
                            <Button
                                style={{backgroundColor: "#b72339", borderColor: "#b72339"}}
                                disabled={false}
                                onClick={(e) => {
                                    removePerformedProcedure(e);
                                }}>
                                <span hidden={isLoading}> Remove </span>
                                <Spinner
                                    as="span"
                                    animation="border"
                                    size="sm"
                                    role="status"
                                    hidden={!isLoading}
                                />
                            </Button>
                        </div>

                    </Modal.Footer>
                </Modal>
            </div>

            <br/>
            <br/>

        </CenterInnerLayout>
    );
}

export default EditAppointmentPage;