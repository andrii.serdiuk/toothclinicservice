import InfoField from "./InfoField";
import React from "react";
import {Card, ListGroup, ListGroupItem} from "react-bootstrap";
import {useNavigate} from "react-router-dom";

const DoctorInfo = ({ doctor }) => {

    const navigate = useNavigate();

    return (

            <Card onClick={() => navigate('/doctors/' + doctor.doctorId, {replace: false})}
                style={{ width: '18rem', cursor: "pointer" }}>
                <Card.Img variant="top"
                          src="https://cdn-icons-png.flaticon.com/512/921/921059.png" />
                <Card.Body>
                    <Card.Title>Doctor</Card.Title>
                    <Card.Text>
                        {doctor.doctorSecondName + ' ' + doctor.doctorFirstName}
                    </Card.Text>
                </Card.Body>
                <ListGroup className="list-group-flush">
                    <ListGroupItem>Birthday: {doctor.birthday}</ListGroupItem>
                    <ListGroupItem>Category: {doctor.category}</ListGroupItem>

                </ListGroup>
            </Card>

    );
};

export default DoctorInfo;