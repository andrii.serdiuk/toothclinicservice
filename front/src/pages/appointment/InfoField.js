import React from "react";

const InfoField = ({ name, value }) => {
    return (
        <div style={{display: "flex", flexDirection: "column", marginTop: "10px"}}>
            <div style={{fontSize: "18px"}}>{name}</div>
            <div style={{fontSize: "15px", marginTop: "3px"}}>{value}</div>
        </div>
    );
};

export default InfoField;