import {Button, ButtonGroup} from "react-bootstrap";
import ReactPaginate from "react-paginate";
import React, {useEffect, useState} from "react";
import {pointerOnHover} from "../../utils/stylesConstants";
import {getReceptionists} from "../../http/receptionistsApi";
import {getMyAppointments} from "../../http/userApi";
import {useNavigate} from "react-router-dom";
import {getUserFromToken} from "../../logic/auth";

const MyAppointmentsPage = () => {
    const [items, setItems] = useState(null);
    const [currentPage, setCurrentPage] = useState(0);
    const [itemsOnPage, _] = useState(20);
    const [pageCount, setPageCount] = useState(0);
    const [userRole, setUserRole] = useState(null);

    const navigate = useNavigate();
    const initList = async () => {
        try {
            const role = (await getUserFromToken()).role.toLowerCase();
            setUserRole(role);
            const res = await getMyAppointments(itemsOnPage, currentPage);
            if (role === 'doctor') {
                res.appointments.map(item => {
                    item.userId = item.patientCardId;
                    delete item.patientCardId;
                    item.secondName = item.surname;
                    delete item.surname;
                    return item;
                })
            } else {
                res.appointments.map(item => {
                    item.userId = item.doctorId;
                    delete item.doctorId;
                    return item;
                })
            }

            setPageCount(Math.ceil(res.count / itemsOnPage));
            setItems(res.appointments);
        } catch (e) {
            console.log(e);
        }
    }

    useEffect(async () => {
        await initList();
    }, [currentPage]);

    const getRowFormatting = ({appointmentStatus}) => {
        if (appointmentStatus === 'finished') {
            return "table-success";
        } else if (appointmentStatus === 'scheduled') {
            return "table-warning";
        } else if (appointmentStatus === 'cancelled') {
            return "table-danger";
        } else if (appointmentStatus === 'unattended') {
            return "table-primary";
        }

        return '';
    }

    return (
        <div className="h-100 d-flex flex-column">
            <style>{pointerOnHover}</style>
            <div style={{flex: "1 1 auto"}} className="overflow-auto">
                <table className="table w-100">
                    <thead className="thead-dark">
                    <tr>
                        <th className="col-1" scope="col">Appointment id</th>
                        <th className="col-1" scope="col">{userRole === 'doctor' ? 'Patient' : 'Doctor'} id</th>
                        <th className="col-2" scope="col">Second name</th>
                        <th className="col-2" scope="col">First name</th>
                        <th className="col-2" scope="col">Last name</th>
                        <th className="col-2" scope="col">Appointment date</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        items && items.map(item =>
                            <tr onClick={() => {
                                navigate(`/appointments/${item.appointmentId}`)
                            }} key={item.appointmentId}
                                className={"cursor-pointer " + getRowFormatting(item)}>
                                <th>{item.appointmentId}</th>
                                <td>{item.userId}</td>
                                <td>{item.secondName}</td>
                                <td>{item.firstName}</td>
                                <td>{item.lastName}</td>
                                <td>{item.appointmentDate}</td>
                            </tr>
                        )
                    }
                    </tbody>
                </table>
            </div>
            <div>
                <div className="pagination-list-print-hidden">
                    <ReactPaginate
                        breakLabel="..."
                        nextLabel=">"
                        onPageChange={e => setCurrentPage(e.selected)}
                        pageRangeDisplayed={5}
                        pageCount={pageCount}
                        previousLabel="<"
                        renderOnZeroPageCount={null}
                        pageClassName="page-item"
                        pageLinkClassName="page-link"
                        previousClassName="page-item"
                        previousLinkClassName="page-link"
                        nextClassName="page-item"
                        nextLinkClassName="page-link"
                        breakClassName="page-item"
                        breakLinkClassName="page-link"
                        containerClassName="pagination justify-content-center"
                        activeClassName="active"
                    />
                </div>
            </div>
        </div>
    );
}

export default MyAppointmentsPage;