import InfoField from "./InfoField";
import React from "react";

const AppointmentInfo = ({ appointment }) => (
    <div
        className="appointment__appointment_info"
        style={{
            display: "flex",
            flexDirection: "column",
            paddingLeft: "20px",
            marginTop: "20px",
            width: "50%"
        }}
    >
        <h2
            style={{
                marginTop: "20px",
                marginBottom: "30px"
            }}
        >
            Appointment
        </h2>
        <InfoField name="Id" value={appointment.appointmentId}/>
        <InfoField name="Status" value={appointment.appointmentStatus}/>
        {/* <InfoField name="Date" value={appscheduledAppointmentDate}/> */}
        {/* <InfoField name="Start Time" value={appointment.scheduledAppointmentStartTime}/> */}
        {/* <InfoField name="Finished" value={appointment.finishedAppointmentId}/> */}
        <InfoField name="Date" value={appointment.finishedAppointmentDate}/>
        <InfoField name="Start Time" value={appointment.finishedAppointmentStartTime}/>
        <InfoField name="End Time" value={appointment.finishedAppointmentEndTime}/>
        <InfoField name="Diagnosis" value={appointment.diagnosis}/>
        <InfoField name="Treatment" value={appointment.treatment}/>
    </div>
);

export default AppointmentInfo;