import {Button, Form, Modal, Spinner} from "react-bootstrap";
import React, {useEffect, useState} from "react";
import ProceduresDropDown from "../../components/common/ProceduresDropDown";
import {
    createPerformedProcedure, getAllProcedures,
    getEquipmentAndAllDevicesForProcedure,
    getProcedureData
} from "../../http/appointmentApi";
import EquipmentListDevices from "./EquipmentListDevices";

const AddProcedureToAppointmentModal = ({appoinmentId, doctorCategory}) => {

    //Set to true if the user is waiting for a response from the server
    const [isLoading, setLoading] = useState(false);
    const [showModal, setShowModal] = useState(false);
    //Equipment and all the devices related to this equipment
    const [equipmentRequired, setEquipmentRequired] = useState(null);
    const [map, _] = useState(new Map());

    const [procedureId, setProcedureId] = useState('');
    const [procedurePrice, setProcedurePrice] = useState('');
    const [procedureMinimalCategory, setProcedureMinimalCategory] = useState('');
    const [procedureDiscount, setProcedureDiscount] = useState(0);

    const [discountNotInRange, setDiscountNotInRange] = useState(false);
    const [emptyDiscount, setEmptyDiscount] = useState(false);
    const [invalidDiscount, setInvalidDiscount] = useState(false);

    const discountRegex = /^\d*$/g;

    const discountIsValid = (string) => {
        return discountRegex.test(string);
    };

    const notInRange = (str) => {
        return str > 100;
    }


    function handleSave(e) {
        submit(e).then(success => {
            if(success) {
                handleClose();
                window.location.reload();
            }
        });
    }

    const handleShow = () => setShowModal(true);

    function handleClose() {
        //Set fields to empty strings
        setShowModal(false);
    }

    function setChosenDevice(equipmentId, deviceId) {
        map.set(Number(equipmentId), Number(deviceId));
    }

    async function changeItem(item)  {
      //Get equipment required for this procedure
      setProcedureId(item);
      const data = (await getEquipmentAndAllDevicesForProcedure(item)).data;
      console.log(data);
      const procedure = (await getProcedureData(item)).data;
      setProcedurePrice(procedure.currentPrice.substring(1));
      setProcedureMinimalCategory(procedure.minimalCategory);
      setProcedureDiscount(0);
      setEquipmentRequired(data);
      for (const elem of data) {
          const equipmentId =  Number(elem.equipmentId);
          const firstInventoryNumber =  Number(elem.inventoryNumberList[0]);
          map.set(equipmentId, firstInventoryNumber);
      }
      console.log(map);
    }

    const submit = async e => {
        setLoading(true);
        e.stopPropagation();
        e.preventDefault();
        try {
            //Bind all the devices
            //await createPerformedProcedure(,);
            console.log("Procedure id: ", procedureId);
            console.log("Appointment id: ", appoinmentId);
            console.log("Discount: ", procedureDiscount);
            let array = [];
            let i = 0;
            for(const element of map) {
                 array[i++] = (element[1]);
            }
            const data = {
                procedureId: procedureId,
                appointmentId: appoinmentId,
                devices: array,
                discount: procedureDiscount,
                price: procedurePrice
            };
            await createPerformedProcedure(data);
            return true;
        } catch (e) {
            console.log(e);
            throw e;
        } finally {
            setLoading(false);
        }
    };

    return(
        <>
            <Modal scrollable={true} show={showModal} onHide={handleClose}>
                <Modal.Header closeButton={true}>
                    Add new procedure
                </Modal.Header>
                <Modal.Body>
                    <div className="d-flex justify-content-center">
                        <Form>

                            {/* Choosing the procedure */}
                            <Form.Group className="mb-3" controlId="controlInput">
                                <Form.Label>Procedure</Form.Label>
                                <ProceduresDropDown
                                doctorCategory={doctorCategory}
                                setItem={changeItem}
                                />

                            </Form.Group>

                            {/* Procedure's minimal category */}
                            <Form.Group className="mb-3" controlId="controlInput">
                                <Form.Label>Minimal category</Form.Label>
                                <br/>
                                <div style={{display: "inline-flex"}}>
                                    <span style={{borderRadius: "10px 0 0 10px"}} className="input-group-text">
                                        $
                                    </span>
                                    <Form.Control readOnly={true}
                                                  style={{borderRadius: "0 10px 10px 0"}}
                                                  type="text"
                                                  value={procedureMinimalCategory} placeholder="Minimal category"
                                    />
                                </div>
                            </Form.Group>

                            {/* Procedure's full price */}
                            <Form.Group className="mb-3" controlId="controlInput">
                                <Form.Label>Price</Form.Label>
                                <br/>
                                <div style={{display: "inline-flex"}}>
                                    <span style={{borderRadius: "10px 0 0 10px"}} className="input-group-text">
                                        $
                                    </span>
                                    <Form.Control readOnly={true}
                                                  style={{borderRadius: "0 10px 10px 0"}}
                                                  type="text"
                                                  value={procedurePrice} placeholder="Price"
                                    />
                                </div>
                            </Form.Group>

                            {/* Discount */}
                            <Form.Group className="mb-3" controlId="controlInput">
                                <Form.Label>Discount</Form.Label>
                                <br/>
                                <div style={{display: "inline-flex"}}>
                                    <span style={{borderRadius: "10px 0 0 10px"}} className="input-group-text">
                                        %
                                    </span>
                                    <Form.Control readOnly={false}
                                                  onChange={e => {
                                                      const value = e.target.value;
                                                      setProcedureDiscount(value);
                                                      setInvalidDiscount(!discountIsValid(value));
                                                      setEmptyDiscount(value.trim() === '');
                                                      setDiscountNotInRange(notInRange(value));
                                                  }}
                                                  style={{borderRadius: "0 10px 10px 0"}}
                                                  type="text"
                                                  value={procedureDiscount} placeholder="Discount"
                                    />

                                </div>
                                <div style={{color: "#dc3545"}} hidden={!emptyDiscount}>
                                    Empty discount
                                </div>
                                <div style={{color: "#dc3545"}} hidden={!invalidDiscount}>
                                    Incorrect discount
                                </div>
                                <div style={{color: "#dc3545"}} hidden={!discountNotInRange}>
                                    Discount must be less than 101
                                </div>
                            </Form.Group>
                        </Form>
                    </div>

                    <div
                         style={{display: "flex", flexDirection: "column",
                             justifyContent: "center", gap: "20px"}}>
                        {/*Listing all the required equipment*/}
                        {
                            equipmentRequired && equipmentRequired.map(item => (
                                <EquipmentListDevices
                                    onChangeFunction={setChosenDevice}
                                    key={item.equipmentId}
                                    equipmentName={item.equipmentName}
                                    equipmentId={item.equipmentId}
                                    inventoryNumberList={item.inventoryNumberList}
                                />
                            ))
                        }
                    </div>
                </Modal.Body>
                <Modal.Footer style={{
                    display: "flex",
                    justifyContent: "center",
                }}>
                    <Button style={{marginBottom: "20px", width: "90px", height: "40px"}} variant="success"
                            disabled={emptyDiscount || invalidDiscount || discountNotInRange}
                            onClick={handleSave}>
                        <span hidden={isLoading}> Save </span>
                        <Spinner
                            as="span"
                            animation="border"
                            size="sm"
                            role="status"
                            hidden={!isLoading}
                        />
                    </Button>
                </Modal.Footer>
            </Modal>


            <Button style={{marginBottom: "20px", width: "150px", height: "40px"}} variant="success"
                    onClick={handleShow}>
                <b>Add procedure</b>
            </Button>
        </>

    );

};

export default AddProcedureToAppointmentModal;