import {PaginationList} from "../../components/common";
import React, {useEffect, useState} from "react";
import {getAppointment} from "../../http/appointmentApi";

const ProceduresInfo = ({appointmentId, onRowClickAction}) => {

    const [currentShownProcedures, setCurrentShownProcedures] = useState(null);
    const [itemsOnPage] = useState(20);
    const [pageCount, setPageCount] = useState(0);
    const [currentPage, setCurrentPage] = useState(0);

    useEffect(async () => {

        const procedures = (await getAppointment(appointmentId)).data.procedures;
        console.log(procedures);
        //Showing procedures
        setCurrentShownProcedures(procedures.map(item => {

            return {
                onClick: (() => {
                    onRowClickAction(item.performedProcedureId);
                }),
                key: Number(item.performedProcedureId),
                cells: [item.procedureName, item.fullPrice]
            };
        }));
        //Calculating the amount of pages
        const proceduresCount = procedures.length;
        setPageCount(Math.ceil(proceduresCount / itemsOnPage));

        console.log(currentShownProcedures);
    }, [currentPage]);

    return(
        <PaginationList
        choosePage={setCurrentPage}
        headers={["Procedure name", "Price"]}
        pageCount={pageCount}
        items={currentShownProcedures}
    />);
};

export default ProceduresInfo;