import React from "react";
import {useNavigate} from "react-router-dom";
import {Card, Form} from "react-bootstrap";

const EquipmentDeviceReadOnly = ({equipmentName, equipmentId, inventoryNumber}) => {

    const navigate = useNavigate();

    return (

        <Card style={{display: "flex", flexDirection: "row",
            outline: "1px solid black", height: "40px", marginBottom: "20px"}}>
            <div onClick={() => navigate('/equipment/' + equipmentId + '/devices', {replace: false})}
                 style={{flexGrow: "1", cursor: "pointer", display: "flex", justifyContent: "center",
                 alignItems: "center", width: "100%"}}>
                <h5>{equipmentName}</h5>
            </div>
            <div className="vr"></div>
            <div onClick={() => navigate('/device/' + inventoryNumber, {replace: false})}
                 style={{flexGrow: "1", width: "100%", cursor: "pointer", display: "flex", justifyContent: "center",
                     alignItems: "center"}}>
                <h5>
                    {inventoryNumber}
                </h5>
            </div>
        </Card>
    )
};

export default EquipmentDeviceReadOnly;