import React from "react";
import InfoField from "./InfoField";
import {Card, ListGroup, ListGroupItem} from "react-bootstrap";
import {useNavigate} from "react-router-dom";

const PatientInfo = ({ patient }) => {

    const navigate = useNavigate();

    return(
    <Card onClick={() => navigate('/patients/' + patient.patientCardId, {replace: false})}
        style={{width: '18rem', cursor: "pointer"}}>
        <Card.Img variant="top"
                  src="https://cdn-icons-png.flaticon.com/512/3034/3034851.png"/>
        <Card.Body>
            <Card.Title>Patient</Card.Title>
            <Card.Text>
                {patient.patientSurname + ' ' + patient.patientFirstName + ' ' + patient.patientLastName}
            </Card.Text>
        </Card.Body>
        <ListGroup className="list-group-flush">
            <ListGroupItem>Card number: {patient.patientCardId}</ListGroupItem>
            <ListGroupItem>Birthday: {patient.birthday}</ListGroupItem>
        </ListGroup>
    </Card>
    )
};

export default PatientInfo;