import React from "react";
import {useNavigate} from "react-router-dom";
import {Dropdown, Form} from "react-bootstrap";

const EquipmentListDevicesSelectedSpecified = ({equipmentName, equipmentId, inventoryNumberList, selectedInventoryNumber,
                                                   onChangeFunction}) => {

    const navigate = useNavigate();

    return (

        <div style={{display: "flex"}}>
            <div onClick={() => navigate('/equipment/' + equipmentId + '/devices', {replace: false})}
                 style={{flexGrow: "1", cursor: "pointer", width: "50%",
                     textAlign: "center"}}><b>{equipmentName}</b>
            </div>

            <Dropdown style={{flexGrow: 1, width: "50%"}}>
                <Form.Select onChange={
                    e => onChangeFunction(equipmentId , e.target.value)
                }>
                    {inventoryNumberList && inventoryNumberList.map(item =>
                        <option selected={item === selectedInventoryNumber} key={item} value={item}>{item}</option>
                    )}
                </Form.Select>
            </Dropdown>

        </div>
    )
};

export default EquipmentListDevicesSelectedSpecified;