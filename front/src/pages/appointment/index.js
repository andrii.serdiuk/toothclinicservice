import AppointmentPage from "./AppointmentPage";
import EditAppointmentPage from "./EditAppointmentPage";
import MyAppointmentsPage from "./MyAppointmentsPage";
import AllAppointmentsPage from "./AllAppointmentsPage";

export {
    AppointmentPage,
    EditAppointmentPage,
    MyAppointmentsPage,
    AllAppointmentsPage
};
