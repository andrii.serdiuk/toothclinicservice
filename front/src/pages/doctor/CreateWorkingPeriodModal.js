import React, {useState} from 'react';
import {Button, Form, Modal, Spinner} from "react-bootstrap";
import {createWorkingPeriod} from "../../http/doctorApi";
import CenterInnerLayout from "../../components/layout/CenterInnerLayoutV";

const CreateWorkingPeriodModal = ({doctorId}) => {

    const [day, setDay] = useState('');
    const [startTime, setStartTime] = useState('');
    const [endTime, setEndTime] = useState('');

    const [show, setShow] = useState(false);
    const [isLoading, setLoading] = useState(false);

    const [emptyDay, setEmptyDay] = useState(true);
    const [emptyStartTime, setEmptyStartTime] = useState(true);
    const [emptyEndTime, setEmptyEndTime] = useState(true);

    const [hideEmptyDay, setHideEmptyDay] = useState(true);
    const [hideEmptyStartTime, setHideEmptyStartTime] = useState(true);
    const [hideEmptyEndTime, setHideEmptyEndTime] = useState(true);

    const [wrongTime, setWrongTime] = useState(false);

    const [workingPeriodIntersection, setWorkingPeriodIntersection] = useState(false);

    const handleClose = () => {
        setDay('');
        setStartTime('');
        setEndTime('');

        setEmptyDay(true);
        setEmptyStartTime(true);
        setEmptyEndTime(true);

        setHideEmptyDay(true);
        setHideEmptyStartTime(true);
        setHideEmptyEndTime(true);

        setWrongTime(false);
        setWorkingPeriodIntersection(false);

        setShow(false);
        window.location.reload();
    };
    const handleShow = () => setShow(true);

    const submit = async e => {
        setLoading(true);
        e.stopPropagation();
        e.preventDefault();
        try {
            await createWorkingPeriod(doctorId, day, startTime, endTime);
            return true;
        } catch (e) {
            setWorkingPeriodIntersection(true);
            return false;
        } finally {
            setLoading(false);
        }
    }

    const handleSave = (e) => {
        submit(e).then(success => {
            if (success) {
                handleClose();
            }
        });
    };

    return (
        <>
            <Modal show={show} onHide={handleClose} centered>

                <Modal.Header closeButton={true}>
                    Add working period
                </Modal.Header>

                <Modal.Body>
                    <div className="d-flex justify-content-center">
                        <Form onSubmit={submit}>

                            {/* Working day */}
                            <Form.Group className="mb-3" controlId="controlInput">
                                <Form.Label>Day</Form.Label>
                                <Form.Control type="date"
                                              value={day} placeholder="Day"
                                              onChange={e => {
                                                  const value = e.target.value;
                                                  setDay(value);
                                                  setHideEmptyDay(false);
                                                  setEmptyDay(value.trim() === '');
                                                  setWorkingPeriodIntersection(false);
                                              }}
                                              autoFocus/>
                                <div style={{color: "#dc3545"}}
                                     hidden={hideEmptyDay || !emptyDay}>
                                    Empty day
                                </div>
                            </Form.Group>

                            {/* Start time */}
                            <Form.Group className="mb-3" controlId="controlInput">
                                <Form.Label>Begin time</Form.Label>
                                <br/>
                                <div style={{display: "inline-flex"}}>
                                    <Form.Control
                                        onChange={e => {
                                            const value = e.target.value;
                                            setStartTime(value);
                                            setHideEmptyStartTime(false);
                                            setEmptyStartTime(value.trim() === '');
                                            setWorkingPeriodIntersection(false);
                                            setWrongTime(endTime < value);
                                        }}
                                        style={{
                                            width: "200px",
                                            borderRadius: "10px"
                                        }}
                                        type="time"
                                        value={startTime}
                                    />
                                </div>
                                <div style={{color: "#dc3545"}}
                                     hidden={hideEmptyStartTime || !emptyStartTime}>
                                    Empty start time
                                </div>
                            </Form.Group>

                            {/* End time */}
                            <Form.Group className="mb-3" controlId="controlInput">
                                <Form.Label>End time</Form.Label>
                                <br/>
                                <div style={{display: "inline-flex"}}>
                                    <Form.Control
                                        onChange={e => {
                                            const value = e.target.value;
                                            setEndTime(value);
                                            setWrongTime(value < startTime);
                                            setHideEmptyEndTime(false);
                                            setEmptyEndTime(value.trim() === '');
                                            setWorkingPeriodIntersection(false);
                                        }}
                                        style={{width: "200px", borderRadius: "10px"}}
                                        type="time"
                                        value={endTime}
                                    />
                                </div>
                                <div style={{color: "#dc3545"}}
                                     hidden={hideEmptyEndTime || !emptyEndTime}>
                                    Empty end time
                                </div>
                                <div style={{color: "#dc3545"}} hidden={emptyEndTime || !wrongTime}>
                                    Wrong end time
                                </div>
                            </Form.Group>

                            <div style={{color: "#dc3545", width: "70%"}} hidden={!workingPeriodIntersection}>
                                Periods intersection
                            </div>
                        </Form>
                    </div>
                </Modal.Body>
                <Modal.Footer style={{
                    display: "flex",
                    justifyContent: "center",
                }}>

                    <Button variant="success" disabled={isLoading || emptyDay || emptyStartTime || wrongTime
                        || emptyEndTime || workingPeriodIntersection}
                            onClick={!isLoading ? handleSave : null} centered>
                        <span hidden={isLoading}> Save </span>
                        <Spinner
                            as="span"
                            animation="border"
                            size="sm"
                            role="status"
                            hidden={!isLoading}
                        />

                    </Button>
                </Modal.Footer>

            </Modal>
            <CenterInnerLayout>
                <Button style={{marginBottom: "20px", width: "90px", height: "40px"}}
                        variant="success"
                        onClick={handleShow}>
                    <b>Add</b>
                </Button>
            </CenterInnerLayout>
        </>
    );


};

export default CreateWorkingPeriodModal;