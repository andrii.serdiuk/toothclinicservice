import React, {useEffect, useState} from "react";
import {add as addDoctor, editDoctor, getDoctor} from "../../http/doctorApi";
import {PatientForm} from "../../components/common";
import {useNavigate, useParams} from "react-router-dom";
import DoctorForm from "../../components/common/DoctorForm";

const EditDoctorPage = () => {
    const [modalBody, setModalBody] = useState('');
    const [modalHeader, setModalHeader] = useState('');
    const [reloadOnClose, setReloadOnClose] = useState(false);

    const [doctorId, setDoctorId] = useState('');
    const [firstName, setFirstName] = useState('');
    const [secondName, setSecondName] = useState('');
    const [lastName, setLastName] = useState('');
    const [birthday, setBirthday] = useState('2001-01-01');
    const [experience, setExperience] = useState('');
    const [category, setCategory] = useState('');
    const [phone, setPhone] = useState('');

    const {doctorId : id} = useParams();
    const navigate = useNavigate();

    useEffect(async () => {
        const doctor = await getDoctor(id);
        console.log(doctor);
        setDoctorId(doctor.doctorId);
        setFirstName(doctor.firstName);
        setSecondName(doctor.secondName);
        setLastName(doctor.lastName);
        setBirthday(doctor.birthday);
        setExperience(doctor.experience);
        setCategory(doctor.doctorCategory);
        setPhone(doctor.phoneNumber) ;
    }, [])

    const submitForm = async () => {
        try {
            const doctor = {
                doctorId: parseInt(doctorId),
                firstName: firstName,
                secondName: secondName,
                lastName: lastName,
                birthday: birthday,
                experience: parseInt(experience),
                doctorCategory: parseInt(category),
                phone: phone
            };
            const res = await editDoctor(doctor.doctorId, doctor);
            setModalHeader('Success');
            setModalBody('Successfully edited');
            navigate(-1);
        } catch (e) {
            const errors = e.response.data.errors;
            setModalHeader('Failure');
            setReloadOnClose(false);
            if (errors.filter(a => a.description.toLowerCase().includes("duplicat")).length !== 0) {
                setModalBody('Doctor id, email and phone must be unique in the whole system');
            } else {
                setModalBody('Error has happened');
            }
        }
    }

    return (
        <>
            <h1 className="text-center mt-1">Edit doctor</h1>
            <div className="d-flex justify-content-center w-100">
                <DoctorForm
                    submitForm={submitForm}
                    reloadOnClose={reloadOnClose}
                    modalBody={modalBody}
                    modalHeader={modalHeader}
                    buttonLabel="Edit"
                    doctorIdInput={{value: doctorId, isDisabled: true, setValue: setDoctorId}}
                    firstNameInput={{value: firstName, isDisabled: false, setValue: setFirstName}}
                    surnameInput={{value: secondName, isDisabled: false, setValue: setSecondName}}
                    lastNameInput={{value: lastName, isDisabled: false, setValue: setLastName}}
                    emailInput={{value: null, isDisabled: true, setValue: null}}
                    birthdayInput={{value: birthday, isDisabled: false, setValue: setBirthday}}
                    experienceInput={{value: experience, isDisabled: false, setValue: setExperience}}
                    categoryInput={{value: category, isDisabled: false, setValue: setCategory}}
                    phoneInput={{value: phone, isDisabled: false, setValue: setPhone}}
                />
            </div>
        </>
    );
};

export default EditDoctorPage;