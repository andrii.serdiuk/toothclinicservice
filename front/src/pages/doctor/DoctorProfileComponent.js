import React, {useEffect, useState} from 'react';
import {useNavigate} from "react-router-dom";
import {
    addDoctorAccount,
    deleteDoctor,
    deleteDoctorAccount,
    updateDoctorAccount
} from "../../http/doctorApi";
import {PaginationList, PersonProfile} from "../../components/common";
import {doctorScheduleNextWeek} from "../../http/reportsApi";
import {Button, Form, Modal} from "react-bootstrap";
import {Schedule} from '../../components/doctor';
import {getUserFromToken} from "../../logic/auth";
import DIRECTOR from "../../utils/enums/userRoles.enum";
import CenterInnerLayout from "../../components/layout/CenterInnerLayoutV";
import EditWorkingPeriodModal from "./EditWorkingPeriodModal";

const DoctorProfileComponent = ({doctorId, getData, editingAllowedNotOnlyForDirector}) => {

    const [doctorItem, setDoctorItem] = useState(null);

    const [currentItems, setCurrentItems] = useState(null);
    const [itemsOnPage, _] = useState(5);
    const [pageCount, setPageCount] = useState(0);
    const [currentPage, setCurrentPage] = useState(0);

    const [showAccountModal, setShowAccountModal] = useState(false);
    const [accountEmail, setAccountEmail] = useState(null);
    const [password, setPassword] = useState('');
    const [passwordConfirmation, setPasswordConfirmation] = useState('');
    const [modalSuccessClick, setModalSuccessClick] = useState(null);
    const [accountModalHeader, setAccountModalHeader] = useState('');
    const [accountModalButtonLabel, setAccountModalButtonLabel] = useState('');
    const [emailErrorMessage, setEmailErrorMessage] = useState(null);
    const [passwordConfirmationErrorMessage, setPasswordConfirmationErrorMessage] = useState(null);
    const [passwordErrorMessage, setPasswordErrorMessage] = useState(null);
    const [hasAccount, setHasAccount] = useState(false);

    const [editingEnabled, setEditingEnabled] = useState(false);

    const [showEditModal, setShowEditModal] = useState(false);
    const [editedScheduleId, setEditedScheduleId] = useState(null);
    const [editedWorkingHoursId, setEditedWorkingHoursId] = useState(null);
    const [editedDate, setEditedDate] = useState(null);
    const [editedStartTime, setEditedStartTime] = useState(null);
    const [editedEndTime, setEditedEndTime] = useState(null);

    const [showErrorModal, setShowErrorModal] = useState(false);

    const navigate = useNavigate();
    const choosePage = async (newPage) => {
        setCurrentPage(newPage);
    };

    const setEditModal = (scheduleId, workingHoursId, date, startTime, endTime) => {

        setEditedScheduleId(scheduleId);
        setEditedWorkingHoursId(workingHoursId);
        setEditedDate(date);
        setEditedStartTime(startTime);
        setEditedEndTime(endTime);
        setShowEditModal(true);
    };

    useEffect(async () => {
        await initDoctor();

        const token = await getUserFromToken();
        const role = token.role;
        //Doctor should use their profile to change information
        const editingAllowed = editingAllowedNotOnlyForDirector || (role === DIRECTOR.DIRECTOR);
        setEditingEnabled(editingAllowed);

        const schedule = (await doctorScheduleNextWeek(doctorId, itemsOnPage, currentPage)).data;
        setPageCount(Math.ceil(schedule.count / itemsOnPage));
        setCurrentItems(schedule.rows.rows.map(item => {
                return {
                    onClick: (() => {
                        setEditModal(item.scheduleId, item.workingHoursId, item.scheduleDate, item.beginTime, item.endTime);
                    }),
                    key: `${item.scheduleDate}${item.beginTime}`,
                    cells: [item.scheduleDate, item.beginTime, item.endTime]
                };
            }
        ));
    }, [currentPage]);

    const initDoctor = async () => {
        const res = await getData();

        setDoctorItem({
            firstName: res.firstName,
            secondName: res.secondName,
            lastName: res.lastName,
            fields: [
                {title: "Doctor id", content: res.doctorId},
                {title: "Email", content: res.accountEmail ? res.accountEmail : "None"},
                {title: "Birthday", content: res.birthday},
                {title: "Experience", content: res.experience + ' ' + (res.experience > 1 ? "years" : "year")},
                {title: "Phone number", content: res.phoneNumber},
                {title: "Category", content: res.doctorCategory}
            ]
        });
        setAccountEmail(res.accountEmail);
        setHasAccount(!!res.accountEmail);
    }


    const deleteProfile = async () => {
        try {
            const res = await deleteDoctor(doctorId);
            if (res.status === 200) {
                navigate('/doctors', {replace: true});
            }
        } catch (e) {
            setShowErrorModal(true);
        }
    }

    const deleteAccount = async () => {
        const res = await deleteDoctorAccount(doctorId);
        if (res.status === 200) {
            await initDoctor();
        } else {
            console.log("Failure");
        }
    }

    const doctorSendAccount = (sendData, checkAttributes) => async (emailInput, passwordInput, passwordConfirmationInput) => {
        if (!checkAttributes(emailInput, passwordInput, passwordConfirmationInput) ||
            passwordInput !== passwordConfirmationInput || !checkEmail(emailInput)) {
            return;
        }
        const data = {email: emailInput};
        if (passwordInput) {
            data.password = passwordInput;
        }
        try {
            await sendData(doctorId, data);
            await initDoctor();
            setShowAccountModal(false);
        } catch (e) {
            const errors = e.response.data.errors;
            if (errors.filter(a => a.description.toLowerCase().includes("duplicat")).length !== 0) {
                setEmailErrorMessage('Doctor id, email and phone must be unique in the whole system');
            }
        }
    }

    const showAccountEditModal = async () => {
        setModalSuccessClick(() => doctorSendAccount(updateDoctorAccount, () => true));
        setAccountModalHeader('Edit account');
        setAccountModalButtonLabel('Edit');
        setShowAccountModal(true);
        await initDoctor();
        modalResetErrors();
    }

    const addAccountModal = async () => {
        setModalSuccessClick(() => doctorSendAccount(addDoctorAccount, (emailInput, passwordInput, passwordConfirmationInput) => {
            setEmailErrorMessage(emailInput ? '' : 'This field cannot be empty');
            setPasswordConfirmationErrorMessage(passwordInput ? '' : 'This field cannot be empty');
            setPasswordErrorMessage(passwordConfirmationInput ? '' : 'This field cannot be empty');

            return emailInput && passwordInput && passwordConfirmationInput;
        }));
        setAccountModalHeader('Add account');
        setAccountModalButtonLabel('Add');
        setShowAccountModal(true);
        await initDoctor();
        modalResetErrors();
    }

    const checkPasswordEquality = (password, passwordConfirmation) => {
        if (password !== passwordConfirmation) {
            setPasswordConfirmationErrorMessage('Passwords aren\'t equal');
            setPasswordErrorMessage('Passwords aren\'t equal');
        } else {
            setPasswordConfirmationErrorMessage(null);
            setPasswordErrorMessage(null);
        }
    }

    const modalResetErrors = () => {
        setEmailErrorMessage(null);
        setPasswordConfirmationErrorMessage(null);
    }

    const checkEmail = (email) => {
        if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
            setEmailErrorMessage('Email isn\'t valid');
            return false;
        }
        setEmailErrorMessage(null);
        return true;
    }

    return (
        <div style={{flexDirection: 'column', gap: '80px', maxWidth: '100%'}}
             className="doctor_page  d-flex justify-content-center mt-5">
            <div
                className="doctor_profile__person_profile_full_schedule_wrapper"
                style={{
                    display: 'flex',
                    maxWidth: "100%",
                    justifyContent: "space-between",
                    width: "100%",
                    padding: '0 40px'
                }}
            >
                <div
                    className="doctor_profile__full_schedule_wrapper"
                    style={{
                        maxWidth: "calc(100% - 300px)",
                        width: '100%',
                        height: "100%",
                    }}
                >
                    <Schedule doctorId={doctorId}/>
                </div>

                <div
                    className="doctor_profile__person_profile_wrapper"
                    style={{
                        maxWidth: "250px",
                        minWidth: "150px",
                    }}
                >
                    {
                        doctorItem && <PersonProfile
                            roleName="doctor"
                            personFields={doctorItem}
                            editingAllowed={editingEnabled}
                            onProfileDelete={deleteProfile}
                            onProfileEdit={() => navigate(`/doctors/${+doctorId}/edit`)}
                            onAccountDelete={hasAccount ? deleteAccount : null}
                            onAccountEdit={hasAccount ? showAccountEditModal : null}
                            onAccountAdd={!hasAccount ? addAccountModal : null}
                            imageSource="https://cdn-icons-png.flaticon.com/512/921/921059.png"
                        />
                    }
                </div>
            </div>

            <CenterInnerLayout style={{width: "400px"}}>
                <PaginationList
                    choosePage={choosePage}
                    headers={["Day", "Begin time", "End time"]}
                    pageCount={pageCount}
                    items={currentItems}
                />
            </CenterInnerLayout>

            <Modal show={showErrorModal}>

                <Modal.Header closeButton>
                    <CenterInnerLayout>
                    <Modal.Title>
                    <CenterInnerLayout>
                        Error
                    </CenterInnerLayout>
                    </Modal.Title>
                    </CenterInnerLayout>
                </Modal.Header>

                <Modal.Body>
                    <p style={{color: "red"}}>This doctor has history, consequently, the profile cannot be removed</p>
                </Modal.Body>

                <Modal.Footer>
                    <CenterInnerLayout>
                        <Button onClick={() => setShowErrorModal(false)} variant="primary">OK</Button>
                    </CenterInnerLayout>
                </Modal.Footer>
            </Modal>

            <Modal show={showAccountModal} centered>
                <Modal.Header closeButton={true} onHide={() => setShowAccountModal(false)}>
                    {accountModalHeader}
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control
                                onChange={e => {
                                    setAccountEmail(e.target.value);
                                    checkEmail(e.target.value);
                                }}
                                type="email"
                                placeholder="Type email"
                                value={accountEmail ? accountEmail : ''}
                            />
                            <div className="text-danger" hidden={!emailErrorMessage}>{emailErrorMessage}</div>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                onChange={e => {
                                    setPassword(e.target.value);
                                    checkPasswordEquality(e.target.value, passwordConfirmation);
                                }}
                                type="password"
                            />
                            <div className="text-danger"
                                 hidden={!passwordErrorMessage}>{passwordErrorMessage}</div>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicPasswordConfirmation">
                            <Form.Label>Confirm password</Form.Label>
                            <Form.Control
                                onChange={e => {
                                    setPasswordConfirmation(e.target.value);
                                    checkPasswordEquality(password, e.target.value);
                                }}
                                type="password"
                            />
                            <div className="text-danger"
                                 hidden={!passwordConfirmationErrorMessage}>{passwordConfirmationErrorMessage}</div>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="success"
                            onClick={() => modalSuccessClick(accountEmail, password, passwordConfirmation)}>{accountModalButtonLabel}</Button>
                    <Button variant="danger" onClick={() => setShowAccountModal(false)}>Cancel</Button>
                </Modal.Footer>
            </Modal>
            <EditWorkingPeriodModal
                show={showEditModal}
                setShow={setShowEditModal}
                scheduleId={editedScheduleId}
                workingHoursId={editedWorkingHoursId}
                date={editedDate}
                passedStartTime={editedStartTime}
                passedEndTime={editedEndTime}
            />
        </div>
    );
};

export default DoctorProfileComponent;