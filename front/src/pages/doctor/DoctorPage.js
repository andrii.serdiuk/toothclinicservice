import {useParams} from "react-router-dom";
import DoctorProfileComponent from "./DoctorProfileComponent";
import {getDoctor} from "../../http/doctorApi";
import CreateWorkingPeriodModal from "./CreateWorkingPeriodModal";
import {useEffect, useState} from "react";
import {getUserFromToken} from "../../logic/auth";
import DIRECTOR from "../../utils/enums/userRoles.enum";

const DoctorPage = () => {

    const {doctorId} = useParams();
    const [isDirector, setDirector] = useState(false);

    useEffect(async () => {
        const token = await getUserFromToken();
        const role = token.role;
        setDirector(role === DIRECTOR.DIRECTOR);
    }, []);

    const getData = async () => {
        const result = await getDoctor(doctorId);
        console.log(result);
        return result;
    };

    return (
        <>
            <DoctorProfileComponent
                doctorId={doctorId}
                getData={getData}
                editingAllowedNotOnlyForDirector={false}
            />
            {isDirector &&
                <CreateWorkingPeriodModal
                doctorId={doctorId}
                />
            }
        </>
    );
};

export default DoctorPage;