import React, {useState} from 'react';
import {add as addDoctor} from "../../http/doctorApi";
import {DoctorForm} from "../../components/common";
import {useNavigate} from "react-router-dom";

const AddDoctorPage = () => {
    const [modalBody, setModalBody] = useState('');
    const [modalHeader, setModalHeader] = useState('');
    const [reloadOnClose, setReloadOnClose] = useState(false);

    const [doctorId, setDoctorId] = useState('');
    const [firstName, setFirstName] = useState('');
    const [secondName, setSecondName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [birthday, setBirthday] = useState('2001-01-01');
    const [experience, setExperience] = useState('');
    const [category, setCategory] = useState('');
    const [phone, setPhone] = useState('');

    const navigate = useNavigate();

    const submitForm = async () => {
        try {
            const doctor = {
                doctorId: parseInt(doctorId),
                name: firstName,
                surname: secondName,
                middleName: lastName,
                email: email,
                birthday: birthday,
                experience: parseInt(experience),
                category: parseInt(category),
                phone: phone
            };
            await addDoctor(doctor);
            setModalHeader('Success');
            setModalBody('Successfully added');
            navigate('/doctors/' + doctor.doctorId, {replace: true})
        } catch (e) {
            const errors = e.response.data.errors;
            setModalHeader('Failure');
            setReloadOnClose(false);
            if (errors && errors.filter(a => a.description.toLowerCase().includes("duplicat")).length !== 0) {
                setModalBody('Doctor id, email and phone must be unique in the whole system');
            } else {
                setModalBody('Error has happened');
            }
        }
    }

    return (
        <>
            <h1 className="text-center mt-1">Add doctor</h1>
            <div className="d-flex justify-content-center w-100">
                <DoctorForm
                    submitForm={submitForm}
                    reloadOnClose={reloadOnClose}
                    modalBody={modalBody}
                    modalHeader={modalHeader}
                    buttonLabel="Add doctor"
                    doctorIdInput={{value: doctorId, isDisabled: false, setValue: setDoctorId}}
                    firstNameInput={{value: firstName, isDisabled: false, setValue: setFirstName}}
                    surnameInput={{value: secondName, isDisabled: false, setValue: setSecondName}}
                    lastNameInput={{value: lastName, isDisabled: false, setValue: setLastName}}
                    emailInput={{value: email, isDisabled: false, setValue: setEmail}}
                    birthdayInput={{value: birthday, isDisabled: false, setValue: setBirthday}}
                    experienceInput={{value: experience, isDisabled: false, setValue: setExperience}}
                    categoryInput={{value: category, isDisabled: false, setValue: setCategory}}
                    phoneInput={{value: phone, isDisabled: false, setValue: setPhone}}
                />
            </div>
        </>
    );
};


export default AddDoctorPage;