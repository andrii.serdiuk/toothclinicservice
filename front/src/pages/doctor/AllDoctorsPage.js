import React, {useCallback, useEffect, useState} from 'react';
import {NavbarWrapper, Header, BaseSpinner} from '../../components/base';
import ReactPaginate from "react-paginate";
import {getDoctors} from "../../http/doctorApi";
import {useNavigate} from "react-router-dom";
import {PaginationList} from "../../components/common";
import {Button} from "react-bootstrap";
import {getUserFromToken} from "../../logic/auth";
import { UserRolesEnum } from '../../utils/enums';
import DatePicker from 'react-datepicker';
import TimePicker from 'react-time-picker';

const AllDoctorsPage = () => {
    //Keep track of length separately from data
    const [currentItems, setCurrentItems] = useState(null);
    const [itemsOnPage, setItemsOnPage] = useState(20);
    const [pageCount, setPageCount] = useState(0);
    const [currentPage, setCurrentPage] = useState(0);
    const [isDirector, setIsDirector] = useState(false);
    const [isRegistrant, setIsRegistrant] = useState(false);
    const [dateFrom, setDateFrom] = useState(null);
    const [dateTo, setDateTo] = useState(null);
    const [timeFrom, setTimeFrom] = useState(null);
    const [timeTo, setTimeTo] = useState(null);

    const navigate = useNavigate();

    const dateTransform = date => {
      console.log(date);
      if (!date) return null;
        return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
    }

    useEffect(async () => {
        await getUserFromToken().then(({role}) => {
          setIsDirector(role === UserRolesEnum.DIRECTOR);
          setIsRegistrant(role === UserRolesEnum.REGISTRANT)
        });
        let res;
        if (isRegistrant) {
          console.log(dateTransform(dateFrom), dateTransform(dateTo), timeFrom, timeTo);
          res = (await getDoctors(dateTransform(dateFrom), dateTransform(dateTo), timeFrom, timeTo, itemsOnPage, currentPage)).data;
        } else {
          res = (await getDoctors(null, null, null, null, itemsOnPage, currentPage)).data
        }
        console.log(res);
        setPageCount(Math.ceil(res.count / itemsOnPage));
        setCurrentItems(res.doctors.map(item => {
                return {
                    onClick: (() => navigate('/doctors/' + item.doctorId, {replace: false})),
                    key: item.doctorId,
                    cells: [item.doctorId, item.secondName, item.firstName, item.lastName]
                };
            }
        ));
    }, [currentPage, dateFrom, dateTo, timeFrom, timeTo]);

    const dateTimeBoundaries = () => (
      <div style={{marginRight: "30px"}} className="col-4 d-flex gap-3">
      <div>
          <div className="mb-2">Date from</div>
          <DatePicker
              selected={dateFrom}
              onChange={date => setDateFrom(date)}
              dateFormat={"dd.MM.yyyy"}
              isClearable
          />
      </div>
      <div>
          <div className="mb-2">Date to</div>
          <DatePicker
              selected={dateTo}
              onChange={date => setDateTo(date)}
              dateFormat={"dd.MM.yyyy"}
              isClearable
          />
      </div>
      <div>
          <div className="mb-2">Time from</div>
          <TimePicker
              value={timeFrom}
              onChange={time => setTimeFrom(time)}
              isClearable
              locale="pl-PL"
          />
      </div>
      <div style={{marginLeft: "30px"}} >
          <div className="mb-2">Time to</div>
          <TimePicker
              value={timeTo}
              onChange={time => setTimeTo(time)}
              isClearable
              locale="pl-PL"
          />
      </div>
    </div>
    )

    return (
        <div className="d-flex flex-column h-100">
            {isDirector && <Button onClick={() => navigate('add')} variant="success" className="w-100">Add</Button>}
            {isRegistrant && dateTimeBoundaries()}
            <PaginationList
                choosePage={setCurrentPage}
                headers={["Doctor id", "Second name", "First Name", "LastName"]}
                pageCount={pageCount}
                items={currentItems}
            />
        </div>
    );
};

export default AllDoctorsPage;