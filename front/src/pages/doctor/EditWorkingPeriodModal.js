import React, {useEffect, useState} from 'react';
import {Button, Form, Modal, Spinner} from "react-bootstrap";
import {deleteWorkingPeriod, updateWorkingPeriod} from "../../http/doctorApi";

const EditWorkingPeriodModal = ({show, setShow, scheduleId, workingHoursId, date, passedStartTime, passedEndTime}) => {

    const [day, setDay] = useState('');
    const [startTime, setStartTime] = useState('');
    const [endTime, setEndTime] = useState('');

    const [isLoading, setLoading] = useState(false);

    const [emptyDay, setEmptyDay] = useState(false);
    const [emptyStartTime, setEmptyStartTime] = useState(false);
    const [emptyEndTime, setEmptyEndTime] = useState(false);

    const [wrongTime, setWrongTime] = useState(false);

    const [workingPeriodIntersection, setWorkingPeriodIntersection] = useState(false);
    const [lostAppointmentError, setLostAppointmentError] = useState(false);

    useEffect(() => {
            setDay(date);
            setStartTime(passedStartTime);
            setEndTime(passedEndTime);
        }
        , [show]);

    const handleClose = () => {
        setDay('');
        setStartTime('');
        setEndTime('');
        setEmptyDay(false);
        setEmptyStartTime(false);
        setEmptyEndTime(false);
        setWrongTime(false);
        setWorkingPeriodIntersection(false);
        setLostAppointmentError(false);
        window.location.reload();
        setShow(false);
    };

    const submit = async e => {
        setLoading(true);
        e.stopPropagation();
        e.preventDefault();
        try {
            await updateWorkingPeriod(scheduleId, workingHoursId, startTime, endTime);
            return true;
        } catch (e) {
            const error = e.response.data.errors[0].description;
            if (error === 'Working periods intersection') {
                setWorkingPeriodIntersection(true);
            } else {
                setLostAppointmentError(true);
            }
            return false;
        } finally {
            setLoading(false);
        }
    }

    const handleEdit = (e) => {
        console.log("Editing ", scheduleId, ' ', workingHoursId);
        submit(e).then(success => {
            if (success) {
                handleClose();
            }
        });
    };

    const handleRemove = async (e) => {
        try {
            await deleteWorkingPeriod(scheduleId, workingHoursId);
            handleClose();
        } catch (e) {
            setLostAppointmentError(true);
        }
    }

    return (
        <>
            <Modal show={show} onHide={handleClose} centered>

                <Modal.Header closeButton={true}>
                   Working period
                </Modal.Header>

                <Modal.Body>
                    <div className="d-flex justify-content-center">
                        <Form onSubmit={submit}>

                            {/* Working day */}
                            <Form.Group className="mb-3" controlId="controlInput">
                                <Form.Label>Day</Form.Label>
                                <Form.Control type="date"
                                              readOnly={true}
                                              value={day} placeholder="Day"
                                              onChange={e => {
                                                  const value = e.target.value;
                                                  setDay(value);
                                                  setEmptyDay(value.trim() === '');
                                                  setWorkingPeriodIntersection(false);
                                              }}
                                              autoFocus/>
                                <div style={{color: "#dc3545"}}
                                     hidden={!emptyDay}>
                                    Empty day
                                </div>
                            </Form.Group>

                            {/* Start time */}
                            <Form.Group className="mb-3" controlId="controlInput">
                                <Form.Label>Begin time</Form.Label>
                                <br/>
                                <div style={{display: "inline-flex"}}>
                                    <Form.Control
                                        onChange={e => {
                                            const value = e.target.value;
                                            setStartTime(value);
                                            setEmptyStartTime(value.trim() === '');
                                            setWorkingPeriodIntersection(false);
                                            setWrongTime(endTime < value);
                                        }}
                                        style={{
                                            width: "200px",
                                            borderRadius: "10px"
                                        }}
                                        type="time"
                                        value={startTime}
                                    />
                                </div>
                                <div style={{color: "#dc3545"}}
                                     hidden={!emptyStartTime}>
                                    Empty start time
                                </div>
                            </Form.Group>

                            {/* End time */}
                            <Form.Group className="mb-3" controlId="controlInput">
                                <Form.Label>End time</Form.Label>
                                <br/>
                                <div style={{display: "inline-flex"}}>
                                    <Form.Control
                                        onChange={e => {
                                            const value = e.target.value;
                                            setEndTime(value);
                                            setWrongTime(value < startTime);
                                            setEmptyEndTime(value.trim() === '');
                                            setWorkingPeriodIntersection(false);
                                        }}
                                        style={{width: "200px", borderRadius: "10px"}}
                                        type="time"
                                        value={endTime}
                                    />
                                </div>
                                <div style={{color: "#dc3545"}}
                                     hidden={!emptyEndTime}>
                                    Empty end time
                                </div>
                                <div style={{color: "#dc3545"}} hidden={emptyEndTime || !wrongTime}>
                                    Wrong end time
                                </div>
                            </Form.Group>

                            <div style={{color: "#dc3545", width: "70%"}} hidden={!workingPeriodIntersection}>
                                Periods intersection
                            </div>
                            <div style={{color: "#dc3545", width: "70%"}} hidden={!lostAppointmentError}>
                                There is an appointment during this time
                            </div>
                        </Form>
                    </div>
                </Modal.Body>
                <Modal.Footer style={{
                    display: "flex",
                    justifyContent: "center",
                }}>

                    <Button variant="success" disabled={isLoading || emptyDay || emptyStartTime || wrongTime
                        || emptyEndTime || workingPeriodIntersection}
                            onClick={!isLoading ? handleEdit : null} centered>
                        <span hidden={isLoading}> Edit </span>
                        <Spinner
                            as="span"
                            animation="border"
                            size="sm"
                            role="status"
                            hidden={!isLoading}
                        />

                    </Button>
                    <Button variant="danger" disabled={isLoading}
                            onClick={!isLoading ? handleRemove : null} centered>
                        <span hidden={isLoading}> Remove </span>
                        <Spinner
                            as="span"
                            animation="border"
                            size="sm"
                            role="status"
                            hidden={!isLoading}
                        />

                    </Button>

                </Modal.Footer>

            </Modal>
        </>
    );


};

export default EditWorkingPeriodModal;