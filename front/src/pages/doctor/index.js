import AddDoctorPage from './AddDoctorPage';
import AllDoctorsPage from './AllDoctorsPage';
import DoctorPage from './DoctorPage';
import DoctorProfileComponent from './DoctorProfileComponent';

export {
    AddDoctorPage,
    AllDoctorsPage,
    DoctorPage,
    DoctorProfileComponent
};
