import {useNavigate} from "react-router-dom";
import {AccountModal, PaginatedList, YesNoModal} from "../../components/common";
import {getPatients} from "../../http/patientApi";
import {Button, ButtonGroup} from 'react-bootstrap';
import React, {useEffect, useState} from "react";
import {addReceptionist, deleteReceptionist, getReceptionists, updateReceptionist} from "../../http/receptionistsApi";
import {addDoctorAccount} from "../../http/doctorApi";
import {set} from "mobx";
import {addEquipment} from "../../http/equipmentApi";
import ReactPaginate from "react-paginate";
import {getMyPatients} from "../../http/userApi";
import {getUserFromToken} from "../../logic/auth";

const AllReceptionistsPage = () => {
    const [showAccountModal, setShowAccountModal] = useState(false);
    const [emailErrorMessage, setEmailErrorMessage] = useState(null);
    const [modalFunction, setModalFunction] = useState(null);
    const [modalHeader, setModalHeader] = useState('');
    const [buttonLabel, setButtonLabel] = useState('');
    const [buttonType, setButtonType] = useState(null);
    const [emptyPassword, setEmptyPassword] = useState(false);
    const [startModalEmail, setStartModalEmail] = useState('');
    const [showDeleteConfirmation, setShowDeleteConfirmation] = useState(false);
    const [currentDeleteItem, setCurrentDeleteItem] = useState(null);
    const [isDirector, setIsDirector] = useState(false);

    const [items, setItems] = useState(null);
    const [itemsOnPage, _] = useState(20);
    const [pageCount, setPageCount] = useState(0);
    const [currentPage, setCurrentPage] = useState(0);

    const navigate = useNavigate();

    const initList = async () => {
        const res = await getReceptionists(itemsOnPage, currentPage);
        setPageCount(Math.ceil(res.count / itemsOnPage));
        setItems(res.receptionists)
    }

    useEffect(async () => {
        setIsDirector((await getUserFromToken()).role === 'DIRECTOR');
        await initList();
    }, [currentPage]);

    const modalAddReceptionist = async (accountEmail, password) => {
        try {
            await addReceptionist({accountEmail, password});
            setShowAccountModal(false);
            await initList();
            return true;
        } catch (e) {
            setEmailErrorMessage("Such email already registered in the system");
            return false;
        }
    }

    const editReceptionistClick = (e, item) => {
        setModalFunction(() => async (email, password) => {
            try {
                if (!password) {
                    await updateReceptionist(item.accountId, {accountEmail: email, password});
                } else {
                    await updateReceptionist(item.accountId, {accountEmail: email});
                }
                setShowAccountModal(false);
                await initList();
                return true;
            } catch (e) {
                console.log(e);
                setEmailErrorMessage("Such email already registered in the system");
                return false;
            }
        });
        setModalHeader("Edit the receptionist");
        setButtonLabel("Edit");
        setButtonType("warning")
        setEmptyPassword(true);
        setStartModalEmail(item.accountEmail);
        setShowAccountModal(true)
    }

    const deleteReceptionistClick = async () => {
        try {
            console.log(currentDeleteItem);
            await deleteReceptionist(currentDeleteItem.accountId);
            setShowDeleteConfirmation(false);
            setCurrentDeleteItem(null);
            await initList();
            return true;
        } catch (e) {
            return false;
        }
    }

    const css = `
        .cursor-pointer:hover{
            cursor:pointer;
        }
    `;

    return (
        <div className="h-100 d-flex flex-column">
            {
                isDirector &&
                <Button onClick={() => {
                    setModalFunction(() => (email, password, confirmation) => modalAddReceptionist(email, password, confirmation));
                    setModalHeader("Add a receptionist");
                    setButtonLabel("Add");
                    setButtonType("success");
                    setEmptyPassword(false);
                    setShowAccountModal(true)
                }} className="w-100" variant="success">Add</Button>}
            <div className="h-100 d-flex flex-column">
                <style>{css}</style>
                <div style={{flex: "1 1 auto"}} className="overflow-auto">
                    <table className="table">
                        <thead className="thead-dark">
                        <tr className="cursor-pointer">
                            <th scope="col">Account id</th>
                            <th scope="col">Email</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            items && items.map(item =>
                                <tr key={item.accountId} className="align-middle">
                                    <th className="col-1">{item.accountId}</th>
                                    <td className="col-6">{item.accountEmail}</td>
                                    <td className="col-3">
                                        <ButtonGroup className="d-flex gap-2 w-50">
                                            <Button onClick={e => editReceptionistClick(e, item)}
                                                    variant="warning">Edit</Button>
                                            <Button onClick={() => {
                                                setShowDeleteConfirmation(true);
                                                setCurrentDeleteItem(item);
                                            }}
                                                    variant="danger">Delete</Button>
                                        </ButtonGroup>
                                    </td>
                                </tr>
                            )
                        }
                        </tbody>
                    </table>
                </div>
                <div>
                    <div className="pagination-list-print-hidden">
                        <ReactPaginate
                            breakLabel="..."
                            nextLabel=">"
                            onPageChange={e => setCurrentPage(e.selected)}
                            pageRangeDisplayed={5}
                            pageCount={pageCount}
                            previousLabel="<"
                            renderOnZeroPageCount={null}
                            pageClassName="page-item"
                            pageLinkClassName="page-link"
                            previousClassName="page-item"
                            previousLinkClassName="page-link"
                            nextClassName="page-item"
                            nextLinkClassName="page-link"
                            breakClassName="page-item"
                            breakLinkClassName="page-link"
                            containerClassName="pagination justify-content-center"
                            activeClassName="active"
                        />
                    </div>
                </div>
            </div>
            <AccountModal
                showAccountModal={showAccountModal}
                close={() => setShowAccountModal(false)}
                modalHeader={modalHeader}
                modalButtonLabel={buttonLabel}
                passwordNotEmpty={!emptyPassword}
                modalSubmit={modalFunction}
                emailErrorMessage={emailErrorMessage}
                setEmailErrorMessage={setEmailErrorMessage}
                buttonType={buttonType}
                startAccountEmail={startModalEmail}
                setStartAccountEmail={setStartModalEmail}
            />
            <YesNoModal
                deleteButtonFunction={deleteReceptionistClick}
                setShowYesNoConfirmation={setShowDeleteConfirmation}
                showYesNoConfirmation={showDeleteConfirmation}
                yesNoConfirmationBody={"Are you really would like to delete"}
                yesNoConfirmationHeader={"Sure?"}
            />
        </div>
    );
}

export default AllReceptionistsPage;