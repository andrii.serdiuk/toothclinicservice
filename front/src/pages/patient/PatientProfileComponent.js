import React, {useEffect, useState} from "react";
import {useNavigate, useParams} from "react-router-dom";
import {PersonProfile} from "../../components/common";
import {
    addPatientAccount,
    deletePatient,
    deletePatientAccount,
    updatePatientAccount
} from "../../http/patientApi";
import {Button, Form, Modal} from "react-bootstrap";
import {getUserFromToken} from "../../logic/auth";
import DIRECTOR from "../../utils/enums/userRoles.enum";

const PatientProfileComponent = ({patientId, getData, editingAllowedNotOnlyForDirector}) => {
    const [patientItem, setPatientItem] = useState(null);

    const [showAccountModal, setShowAccountModal] = useState(false);
    const [accountEmail, setAccountEmail] = useState(null);
    const [password, setPassword] = useState('');
    const [passwordConfirmation, setPasswordConfirmation] = useState('');
    const [modalSuccessClick, setModalSuccessClick] = useState(null);
    const [accountModalHeader, setAccountModalHeader] = useState('');
    const [accountModalButtonLabel, setAccountModalButtonLabel] = useState('');
    const [emailErrorMessage, setEmailErrorMessage] = useState(null);
    const [passwordConfirmationErrorMessage, setPasswordConfirmationErrorMessage] = useState(null);
    const [passwordErrorMessage, setPasswordErrorMessage] = useState(null);
    const [hasAccount, setHasAccount] = useState(false);

    const [editingEnabled, setEditingEnabled] = useState(false);

    const navigate = useNavigate();

    useEffect(async () => {
        await initPatient();
        const token = await getUserFromToken();
        const role = token.role;
        const editingAllowed = editingAllowedNotOnlyForDirector || (role === DIRECTOR.DIRECTOR);
        setEditingEnabled(editingAllowed);
    }, []);

    const deleteProfile = async () => {
        const res = await deletePatient(patientId);
        if (res.status === 200) {
            navigate('/patients', {replace: true});
        }
    }

    const deleteAccount = async () => {
        const res = await deletePatientAccount(patientId);
        if (res.status === 200) {
            await initPatient();
        } else {
            console.log("Failure");
        }
    }

    const patientSendAccount = (sendData, checkAttributes) => async (emailInput, passwordInput, passwordConfirmationInput) => {
        if (!checkAttributes(emailInput, passwordInput, passwordConfirmationInput) ||
            passwordInput !== passwordConfirmationInput || !checkEmail(emailInput)) {
            return;
        }
        const data = {email: emailInput};
        if (passwordInput) {
            data.password = passwordInput;
        }
        try {
            await sendData(patientId, data);
            await initPatient();
            setShowAccountModal(false);
        } catch (e) {
            const errors = e.response.data.errors;
            if (errors.filter(a => a.description.toLowerCase().includes("duplicat")).length !== 0) {
                setEmailErrorMessage('Patient id, email and phone must be unique in the whole system');
            }
        }
    }

    const showAccountEditModal = async () => {
        setModalSuccessClick(() => patientSendAccount(updatePatientAccount, () => true));
        setAccountModalHeader('Edit account');
        setAccountModalButtonLabel('Edit');
        setShowAccountModal(true);
        await initPatient();
        modalResetErrors();
    }

    const addAccountModal = async () => {
        setModalSuccessClick(() => patientSendAccount(addPatientAccount, (emailInput, passwordInput, passwordConfirmationInput) => {
            setEmailErrorMessage(emailInput ? '' : 'This field cannot be empty');
            setPasswordConfirmationErrorMessage(passwordInput ? '' : 'This field cannot be empty');
            setPasswordErrorMessage(passwordConfirmationInput ? '' : 'This field cannot be empty');

            return emailInput && passwordInput && passwordConfirmationInput;
        }));
        setAccountModalHeader('Add account');
        setAccountModalButtonLabel('Add');
        setShowAccountModal(true);
        await initPatient();
        modalResetErrors();
    }

    const initPatient = async () => {
        const res = await getData();
        setPatientItem({
            firstName: res.firstName,
            secondName: res.surname,
            lastName: res.lastName,
            fields: [
                {title: "Patient card id", content: res.patientCardId},
                {title: "Email", content: res.accountEmail ? res.accountEmail : "None"},
                {title: "Birthday", content: res.birthday},
                {title: "Phone number", content: res.phoneNumber},
            ]
        })

        setAccountEmail(res.accountEmail);
        setHasAccount(!!res.accountEmail);
    }

    const checkPasswordEquality = (password, passwordConfirmation) => {
        if (password !== passwordConfirmation) {
            setPasswordConfirmationErrorMessage('Passwords aren\'t equal');
            setPasswordErrorMessage('Passwords aren\'t equal');
        } else {
            setPasswordConfirmationErrorMessage(null);
            setPasswordErrorMessage(null);
        }
    }

    const modalResetErrors = () => {
        setEmailErrorMessage(null);
        setPasswordConfirmationErrorMessage(null);
    }

    const checkEmail = (email) => {
        if (!/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
            setEmailErrorMessage('Email isn\'t valid');
            return false;
        }
        setEmailErrorMessage(null);
        return true;
    }

    return (
        <div style={{flexDirection: 'column', gap: '80px'}} className="d-flex justify-content-center mt-5">
            <div>
                {
                    patientItem && <PersonProfile
                        roleName="patient"
                        personFields={patientItem}
                        editingAllowed = {editingEnabled}
                        onProfileDelete={deleteProfile}
                        onProfileEdit={() => navigate(`/patients/${patientId}/edit`)}
                        onAccountDelete={hasAccount ? deleteAccount : null}
                        onAccountEdit={hasAccount ? showAccountEditModal : null}
                        onAccountAdd={!hasAccount ? addAccountModal : null}
                        imageSource="https://cdn-icons-png.flaticon.com/512/3034/3034851.png"
                    />
                }
            </div>

            <Modal show={showAccountModal} centered>
                <Modal.Header closeButton={true} onHide={() => setShowAccountModal(false)}>
                    {accountModalHeader}
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control
                                onChange={e => {
                                    setAccountEmail(e.target.value);
                                    checkEmail(e.target.value);
                                }}
                                type="email"
                                placeholder="Type email"
                                value={accountEmail ? accountEmail : ''}
                            />
                            <div className="text-danger" hidden={!emailErrorMessage}>{emailErrorMessage}</div>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                onChange={e => {
                                    setPassword(e.target.value);
                                    checkPasswordEquality(e.target.value, passwordConfirmation);
                                }}
                                type="password"
                            />
                            <div className="text-danger"
                                 hidden={!passwordErrorMessage}>{passwordErrorMessage}</div>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicPasswordConfirmation">
                            <Form.Label>Confirm password</Form.Label>
                            <Form.Control
                                onChange={e => {
                                    setPasswordConfirmation(e.target.value);
                                    checkPasswordEquality(password, e.target.value);
                                }}
                                type="password"
                            />
                            <div className="text-danger"
                                 hidden={!passwordConfirmationErrorMessage}>{passwordConfirmationErrorMessage}</div>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="success"
                            onClick={() => modalSuccessClick(accountEmail, password, passwordConfirmation)}>{accountModalButtonLabel}</Button>
                    <Button variant="danger" onClick={() => setShowAccountModal(false)}>Cancel</Button>
                </Modal.Footer>
            </Modal>

        </div>
    );
};

export default PatientProfileComponent;