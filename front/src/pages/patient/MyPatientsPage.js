import React from 'react';
import {getMyPatients} from "../../http/userApi";
import {useNavigate} from "react-router-dom";
import {PaginatedList} from "../../components/common";

const MyPatientsPage = () => {
    const navigate = useNavigate();

    return (
        <PaginatedList
            getPatients={async (limit, page) => {
                const res = await getMyPatients(limit, page);
                res.items = res.patients;
                delete res.patients;
                return res;
            }}
            headers={["Patient id", "Second name", "First Name", "Last Name", "Birthday", "Last appointment"]}
            fieldsMap={item => [item.patientCardId, item.surname, item.firstName, item.lastName, item.birthday, item.lastAppointmentDate]}
            onItemClick={item => navigate('/patients/' + item.patientCardId, {replace: false})}
            keyMap={item => item.patientCardId}
        />
    );
};

export default MyPatientsPage;