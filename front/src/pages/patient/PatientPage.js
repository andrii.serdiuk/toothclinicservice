import {useParams} from "react-router-dom";
import PatientProfileComponent from "./PatientProfileComponent";
import {getPatient} from "../../http/patientApi";

const PatientPage = () => {

    const {patientId} = useParams();

    const getData = async () => {
        return (await getPatient(patientId)).data;
    };

    return (
        <PatientProfileComponent
        patientId={patientId}
        getData={getData}
        editingAllowedNotOnlyForDirector={false}
        />
    );
};

export default PatientPage;