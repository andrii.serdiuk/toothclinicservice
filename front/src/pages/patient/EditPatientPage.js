import React, {useEffect, useState} from "react";
import {PatientForm} from "../../components/common";
import {useNavigate, useParams} from "react-router-dom";
import {editPatient, getPatient} from "../../http/patientApi";

const EditPatientPage = () => {
    const [modalBody, setModalBody] = useState('');
    const [modalHeader, setModalHeader] = useState('');
    const [reloadOnClose, setReloadOnClose] = useState(false);

    const [patientId, setPatientId] = useState('');
    const [firstName, setFirstName] = useState('');
    const [secondName, setSecondName] = useState('');
    const [lastName, setLastName] = useState('');
    const [birthday, setBirthday] = useState('2001-01-01');
    const [phone, setPhone] = useState('');

    const {patientId : id} = useParams();

    useEffect(async () => {
        const {data: patient} = await getPatient(id);
        setPatientId(patient.patientCardId);
        setFirstName(patient.firstName);
        setSecondName(patient.surname);
        setLastName(patient.lastName);
        setBirthday(patient.birthday);
        setPhone(patient.phoneNumber) ;
    }, [])

    const navigate = useNavigate();

    const submitForm = async () => {
        try {
            const patient = {
                patientId: parseInt(patientId),
                firstName: firstName,
                secondName: secondName,
                lastName: lastName,
                birthday: birthday,
                phone: phone
            };
            await editPatient(id, patient);
            setModalHeader('Success');
            setModalBody('Successfully edited');
            navigate(-1);
        } catch (e) {
            const errors = e.response.data.errors;
            console.log("Http code:", e.response.status);
            setModalHeader('Failure');
            setReloadOnClose(false);
            if (errors && errors.filter(a => a.description.toLowerCase().includes("duplicat")).length !== 0) {
                setModalBody('Patient id, email and phone must be unique in the whole system');
            } else {
                setModalBody('Error has happened');
            }
        }
    }

    return (
        <>
            <h1 className="text-center mt-1">Edit patient</h1>
            <div className="d-flex justify-content-center w-100">
                <PatientForm
                    submitForm={submitForm}
                    reloadOnClose={reloadOnClose}
                    modalBody={modalBody}
                    modalHeader={modalHeader}
                    buttonLabel="Edit"
                    patientIdInput={{value: patientId, isDisabled: true, setValue: setPatientId}}
                    firstNameInput={{value: firstName, isDisabled: false, setValue: setFirstName}}
                    surnameInput={{value: secondName, isDisabled: false, setValue: setSecondName}}
                    lastNameInput={{value: lastName, isDisabled: false, setValue: setLastName}}
                    emailInput={{value: null, isDisabled: true, setValue: null}}
                    birthdayInput={{value: birthday, isDisabled: false, setValue: setBirthday}}
                    phoneInput={{value: phone, isDisabled: false, setValue: setPhone}}
                />
            </div>
        </>
    );
};

export default EditPatientPage;