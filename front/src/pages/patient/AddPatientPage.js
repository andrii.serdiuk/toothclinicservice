import React, {useState} from "react";
import {PatientForm} from "../../components/common";
import {addPatient} from "../../http/patientApi";
import {useNavigate} from "react-router-dom";

const AddPatientPage = () => {
    const [modalBody, setModalBody] = useState('');
    const [modalHeader, setModalHeader] = useState('');
    const [reloadOnClose, setReloadOnClose] = useState(false);

    const [patientId, setPatientId] = useState('');
    const [firstName, setFirstName] = useState('');
    const [secondName, setSecondName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [birthday, setBirthday] = useState('2001-01-01');
    const [phone, setPhone] = useState('');

    const navigate = useNavigate();

    const submitForm = async () => {
        try {
            const patient = {
                patientId: parseInt(patientId),
                firstName: firstName,
                secondName: secondName,
                lastName: lastName,
                email: email,
                birthday: birthday,
                phone: phone
            };
            await addPatient(patient);
            setModalHeader('Success');
            setModalBody('Successfully added');
            navigate(`/patients/${patientId}`, {replace: true});
        } catch (e) {
            const errors = e.response.data.errors;
            console.log(e.response);
            setModalHeader('Failure');
            setReloadOnClose(false);
            if (errors && errors.filter(a => a.description.toLowerCase().includes("duplicat")).length !== 0) {
                setModalBody('Patient id, email and phone must be unique in the whole system');
            } else {
                setModalBody('Error has happened');
            }
        }
    }

    return (
        <>
            <h1 className="text-center mt-1">Add patient</h1>
            <div className="d-flex justify-content-center w-100">
                <PatientForm
                    submitForm={submitForm}
                    reloadOnClose={reloadOnClose}
                    modalBody={modalBody}
                    modalHeader={modalHeader}
                    buttonLabel="Add patient"
                    patientIdInput={{value: patientId, isDisabled: false, setValue: setPatientId}}
                    firstNameInput={{value: firstName, isDisabled: false, setValue: setFirstName}}
                    surnameInput={{value: secondName, isDisabled: false, setValue: setSecondName}}
                    lastNameInput={{value: lastName, isDisabled: false, setValue: setLastName}}
                    emailInput={{value: email, isDisabled: false, setValue: setEmail}}
                    birthdayInput={{value: birthday, isDisabled: false, setValue: setBirthday}}
                    phoneInput={{value: phone, isDisabled: false, setValue: setPhone}}
                />
            </div>
        </>
    );
};

export default AddPatientPage;