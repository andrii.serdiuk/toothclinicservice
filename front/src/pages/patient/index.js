import MyPatientsPage from "./MyPatientsPage";
import PatientPage from "./PatientPage";
import AddPatientPage from "./AddPatientPage";
import AllPatientsPage from "./AllPatientsPage";
import EditPatientPage from "./EditPatientPage";
import PatientProfileComponent from "./PatientProfileComponent";

export {
    PatientPage,
    AllPatientsPage,
    AddPatientPage,
    MyPatientsPage,
    EditPatientPage,
    PatientProfileComponent
}
