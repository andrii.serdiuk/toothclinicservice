import React, {useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import {getPatients} from "../../http/patientApi";
import {PaginatedList} from "../../components/common";
import {getUserFromToken} from "../../logic/auth";
import {Button} from "react-bootstrap";

const AllPatientsPage = () => {
    const navigate = useNavigate();
    const [isDirector, setIsDirector] = useState(false);

    useEffect(async () => {
        setIsDirector((await getUserFromToken()).role === 'DIRECTOR');
    }, []);

    return (
        <div className="d-flex flex-column h-100">
            {isDirector && <Button onClick={() => navigate('add')} variant="success" className="w-100">Add</Button>}
            <PaginatedList
                getPatients={async (limit, page) => {
                    const res = (await getPatients(limit, page)).data;
                    res.items = res.patients;
                    delete res.patients;
                    return res;
                }}
                headers={["Patient id", "Second name", "First Name", "Last Name", "Birthday"]}
                fieldsMap={item => [item.patientCardId, item.surname, item.firstName, item.lastName, item.birthday]}
                onItemClick={item => navigate('/patients/' + item.patientCardId, {replace: false})}
                keyMap={item => item.patientCardId}
            />
        </div>
    );
};

export default AllPatientsPage;