import {useState} from "react";
import {addEquipment} from "../../http/equipmentApi";
import {ProcedureForm} from "../../components/common";
import {useNavigate} from "react-router-dom";
import {addProcedure} from "../../http/procedureApi";

const AddProcedurePage = () => {
    const [errorMessage, setErrorMessage] = useState('');

    const navigate = useNavigate();

    const addEquipment = async procedure => {
        delete procedure.procedureId;
        try {
            const {data} = await addProcedure(procedure);
            console.log(data.procedureId);
            navigate(`/procedures/${data.procedureId}`, {replace: true});
        } catch (e) {
            console.log(e);
        }
    }

    return (
        <div className="row d-flex justify-content-center w-100 mt-3">
            <div className="col-8">
                <h1 className="text-center mb-5">Add procedure</h1>
                <ProcedureForm
                    buttonLabel={"Add"}
                    procedureId={{value: '', isDisabled: true}}
                    procedureName={{value: '', isDisabled: false}}
                    procedurePrice={{value: '', isDisabled: false}}
                    procedureMinimalCategory={{value: '', isDisabled: false}}
                    currentEquipment={[]}
                    submit={addEquipment}
                    errorMessage={errorMessage}
                    setErrorMessage={setErrorMessage}/>
            </div>
        </div>
    );
}

export default AddProcedurePage;