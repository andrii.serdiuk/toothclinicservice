import AddProcedurePage from './AddProcedurePage';
import ProcedurePage from './ProcedurePage'
import ProceduresPage from './ProceduresPage';

export {
    AddProcedurePage,
    ProcedurePage,
    ProceduresPage
}
