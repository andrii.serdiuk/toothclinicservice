import ProcedureProfile from "../../components/common/ProcedureProfile";
import React, {useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import {PaginationList} from "../../components/common";
import {getProcedures} from "../../http/procedureApi";
import {getUserFromToken} from "../../logic/auth";
import {Button} from "react-bootstrap";

const ProceduresPage = () => {
    const [currentItems, setCurrentItems] = useState(null);
    const [itemsOnPage, _] = useState(20);
    const [pageCount, setPageCount] = useState(0);
    const [currentPage, setCurrentPage] = useState(0);
    const [isDirector, setIsDirector] = useState(false);

    const navigate = useNavigate();

    useEffect(async () => {
        setIsDirector((await getUserFromToken()).role === 'DIRECTOR');
        const res = (await getProcedures(itemsOnPage, currentPage)).data;
        setPageCount(Math.ceil(res.count / itemsOnPage));
        setCurrentItems(res.procedures.map(item => {
            return {
                onClick: (() => navigate(`/procedures/${item.procedureId}`, {replace: false})),
                key: item.procedureId,
                cells: [item.procedureId, item.procedureName, item.currentPrice, item.minimalCategory]
            }
        }));
    }, [currentPage]);

    return (
        <div className="d-flex flex-column h-100">
            {isDirector && <Button onClick={() => navigate('add')} variant="success" className="w-100">Add</Button>}
            <PaginationList
                choosePage={async newPage => {
                    setCurrentPage(newPage)
                }}
                headers={["Id", "Name", "Price", "Minimal category"]}
                pageCount={pageCount}
                items={currentItems}
            />
        </div>
    );
}

export default ProceduresPage;