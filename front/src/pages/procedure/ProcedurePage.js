import ProcedureProfile from "../../components/common/ProcedureProfile";
import React, {useEffect, useState} from "react";
import {deleteProcedure, getEquipmentForProcedure, getProcedure, updateProcedure} from "../../http/procedureApi";
import {useNavigate, useParams} from "react-router-dom";
import ProcedureModal from "../../components/common/ProcedureModal";
import YesNoModal from "../../components/common/YesNoModal";
import InformationModal from "../../components/common/InformationModal";
import {getUserFromToken} from "../../logic/auth";
import DIRECTOR from "../../utils/enums/userRoles.enum";

const ProcedurePage = () => {
    const [procedureId, setProcedureId] = useState(-1);
    const [procedureName, setProcedureName] = useState('');
    const [currentPrice, setCurrentPrice] = useState('');
    const [minimalCategory, setMinimalCategory] = useState(0);
    const [equipment, setEquipment] = useState(null);
    const [showEditModal, setShowEditModal] = useState(false);
    const [showDeleteModal, setShowDeleteModal] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');

    const [showYesNoConfirmation, setShowYesNoConfirmation] = useState(false);
    const [yesNoConfirmationHeader, setYesNoConfirmationHeader] = useState('');
    const [yesNoConfirmationBody, setYesNoConfirmationBody] = useState('');
    const [showInformationModal, setShowInformationModal] = useState(false);
    const [informationHeader, setInformationHeader] = useState(null);
    const [informationBody, setInformationBody] = useState(null);

    const [editingAllowed, setEditingAllowed] = useState(false);

    const {procedureId: id} = useParams();
    const navigate = useNavigate();

    const onProcedureEdit = async procedure => {
        try {
            await updateProcedure(procedure.procedureId, procedure);
            await initProcedure();
            setShowEditModal(false);
        } catch (e) {
            setErrorMessage("Error has happened");
        }
    }

    const modalDeleteButtonFunction = async () => {
        try {
            const {data} = await deleteProcedure(id);
            navigate('/procedures', {replace: true});
        } catch (e) {
            const errors = e.response.data.errors.map(item => item.description).join('\n');
            setShowYesNoConfirmation(false);
            setInformationHeader("Error while deleting");
            setInformationBody(errors);
            setShowInformationModal(true);
        }
    }

    const onProcedureDelete = () => {
        setShowYesNoConfirmation(true);
        setYesNoConfirmationHeader("Delete");
        setYesNoConfirmationBody("Do you want to delete procedure?");
    }

    useEffect(async () => {
        setEditingAllowed((await getUserFromToken()).role === DIRECTOR.DIRECTOR );
        await initProcedure();

        }, []);

    const initProcedure = async () => {
        const {data: procedure} = await getProcedure(id);
        setProcedureId(procedure.procedureId);
        setProcedureName(procedure.procedureName);
        setCurrentPrice(procedure.currentPrice);
        setMinimalCategory(procedure.minimalCategory);
        const {data: {equipment}} = await getEquipmentForProcedure(id);
        setEquipment(equipment);
    }

    return (
        <>
            <div className="row w-100 h-100 d-flex justify-content-center">
                <div className="col-5 h-100 d-flex align-items-center flex-column pt-5">
                    <div>
                        <ProcedureProfile
                            procedureId={procedureId}
                            procedureName={procedureName}
                            currentPrice={currentPrice}
                            minimalCategory={minimalCategory}
                        />
                    </div>
                    <h3 className="text-center">Equipment</h3>
                    <div className="col-8 h-50 border border-dark border-2 overflow-auto">
                        <table className="table table-bordered m-0">
                            <thead>
                            <tr>
                                <th className="col-6">Equipment id</th>
                                <th className="col-6">Equipment name</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                equipment && equipment.map(item =>
                                    <tr key={item.equipmentId}>
                                        <th>{item.equipmentId}</th>
                                        <td>{item.equipmentName}</td>
                                    </tr>
                                )
                            }
                            </tbody>
                        </table>
                    </div>
                    <div className="row w-50">
                        <div className="btn-group pt-2 gap-2 mt-4">
                            <button hidden={!editingAllowed} className="btn btn-warning" onClick={() => setShowEditModal(true)}>Edit</button>
                            <button hidden={!editingAllowed} className="btn btn-danger" onClick={onProcedureDelete}>Delete</button>
                        </div>
                    </div>
                </div>
            </div>

            <ProcedureModal
                showModal={showEditModal}
                closeModal={() => setShowEditModal(false)}
                submit={onProcedureEdit}
                errorMessage={errorMessage}
                setErrorMessage={setErrorMessage}
                procedureId={{value: procedureId, isDisabled: true}}
                procedureName={{value: procedureName, isDisabled: false}}
                currentPrice={{value: currentPrice, isDisabled: false}}
                minimalCategory={{value: minimalCategory, isDisabled: false}}
                currentEquipment={equipment}
                buttonLabel="Edit"
            />
            <YesNoModal
                showYesNoConfirmation={showYesNoConfirmation}
                setShowYesNoConfirmation={setShowYesNoConfirmation}
                yesNoConfirmationHeader={yesNoConfirmationHeader}
                yesNoConfirmationBody={yesNoConfirmationBody}
                deleteButtonFunction={modalDeleteButtonFunction}
            />
            <InformationModal
                show={showInformationModal}
                setShow={setShowInformationModal}
                header={informationHeader}
                body={informationBody}
            />
        </>
    );
}

export default ProcedurePage;