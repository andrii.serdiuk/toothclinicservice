import NotFoundPage from './NotFoundPage';
import MyProfilePage from './MyProfilePage';
import HomePage from './HomePage';
import LoginPage from './LoginPage'
import MySchedulePage from './MySchedulePage';
import ProceduresPage from './procedure/ProceduresPage';
import ProcedurePage from "./procedure/ProcedurePage";

export {
    NotFoundPage,
    MyProfilePage,
    HomePage,
    LoginPage,
    MySchedulePage,
    ProceduresPage,
    ProcedurePage
};
