import React, {useState} from "react";
import {useNavigate, useParams} from "react-router-dom";
import {addDevice} from "../../http/deviceApi";
import {Modal, Button, Form, Spinner} from "react-bootstrap";
import {getCurrentDate} from "../../utils/common";

const SaveDeviceModal = () => {

    const {equipmentId} = useParams();

    const [showModal, setShowModal] = useState(false);
    //New data given by the user
    const [deviceInventoryNumber, setDeviceInventoryNumber] = useState('');
    const [deviceReceiptDate, setDeviceReceiptDate] = useState('');

    const [devicePrice, setDevicePrice] = useState('');

    //Inventory number field
    //Duplicate value
    const [duplicateInventoryNumberError, setDuplicateInventoryNumberError] = useState(false);
    const [duplicateInventoryNumberText, setDuplicateInventoryNumberText] = useState(null);
    //Empty field
    const [inventoryNumberEmpty, setInventoryNumberEmpty] = useState(false);
    //Wrong format
    const [inventoryNumberFormatIsValid, setInventoryNumberFormatIsValid] = useState(true);
    const inventoryNumberRegex = /^\d*$/g;

    const [receiptDateEmpty, setReceiptDateEmpty] = useState(true);

    //Receipt date field
    const [receiptDateValid, setReceiptDateValid] = useState(true);

    //Device price field
    //Empty field
    const [devicePriceEmpty, setDevicePriceEmpty] = useState(false);
    //Negative value
    const [priceIsNegative, setNegativePrice] = useState(false);
    //Invalid format
    const priceRegex = /^((\d+\.\d{2})|(\d+))$/g;
    const [priceFormatIsValid, setPriceFormatIsValid] = useState(true);

    //Set to true if the user is waiting for a response from the server
    const [isLoading, setLoading] = useState(false);

    const priceIsValid = (string) => {
        return priceRegex.test(string);
    };

    const inventoryNumberIsValid = (string) => {
        return inventoryNumberRegex.test(string);
    }

    const handleDuplicateInventoryNumber = () => {
        setDuplicateInventoryNumberError(true);
        setDuplicateInventoryNumberText(deviceInventoryNumber);
    };

    const handleShow = () => setShowModal(true);

    const submit = async e => {
        setLoading(true);
        e.stopPropagation();
        e.preventDefault();
        try {
            const device = {
                equipmentId: equipmentId,
                inventoryNumber: deviceInventoryNumber,
                price: devicePrice,
                receiptDate: deviceReceiptDate
            }
            await addDevice(device);
            return true;
        } catch (e) {
            handleDuplicateInventoryNumber();
            return false;
        } finally {
            setLoading(false);
        }
    }

    const handleClose = () => {
        setDeviceInventoryNumber('');
        setDuplicateInventoryNumberError(false);
        setDuplicateInventoryNumberText(null);
        setInventoryNumberEmpty(false);
        setInventoryNumberFormatIsValid(true);

        setDevicePrice('');
        setDevicePriceEmpty(true);
        setNegativePrice(false);
        setPriceFormatIsValid(true);

        setDeviceReceiptDate('');
        setDevicePriceEmpty(true);
        setReceiptDateValid(true);

        setShowModal(false);
    };

    const handleSave = (e) => {
        submit(e).then(success => {
            if(success) {
                handleClose();
                window.location.reload();
            }
        });
    };

    return(
        <>
            <Modal show={showModal} onHide={handleClose}>
                <Modal.Header closeButton={true}>
                    Create new device
                </Modal.Header>
                <Modal.Body>
                    <div className="d-flex justify-content-center">
                        <Form method={"post"} onSubmit={submit}>

                            {/* Inventory number */}
                            <Form.Group className="mb-3" controlId="controlInput">
                                <Form.Label>Inventory number</Form.Label>
                                <Form.Control value={deviceInventoryNumber} placeholder="Inventory number"
                                              onChange={e => {
                                                  setDeviceInventoryNumber(e.target.value);
                                                  setInventoryNumberFormatIsValid(inventoryNumberIsValid(e.target.value));
                                                  if (inventoryNumberFormatIsValid) {
                                                      setDuplicateInventoryNumberError(setDuplicateInventoryNumberText !== null &&
                                                          e.target.value.trim() === duplicateInventoryNumberText);
                                                      setInventoryNumberEmpty(e.target.value.trim() === '');
                                                  }
                                              }}
                                              autoFocus/>
                                <div hidden={!duplicateInventoryNumberError} style={{color: "#dc3545"}}>
                                    Duplicate inventory number
                                </div>
                                <div hidden={inventoryNumberFormatIsValid} style={{color: "#dc3545"}}>
                                    Invalid format
                                </div>
                            </Form.Group>

                            {/* Receipt date */}

                            <Form.Group className="mb-3" controlId="controlInput">
                                <Form.Label>Receipt date</Form.Label>
                                <Form.Control type="date"
                                              value={deviceReceiptDate} placeholder="Receipt date"
                                              onChange={e => {
                                                  setReceiptDateEmpty(e.target.value === '');
                                                  setDeviceReceiptDate(e.target.value);
                                                  setReceiptDateValid(e.target.value <= getCurrentDate());
                                              }}
                                              />
                                <div hidden={receiptDateValid} style={{color: "#dc3545"}}>
                                    Invalid receipt date
                                </div>
                            </Form.Group>

                            {/* Price */}
                            <Form.Group className="mb-3" controlId="controlInput">
                                <Form.Label>Price</Form.Label>
                                <br/>
                                <div style={{display: "inline-flex"}}>
                        <span style={{borderRadius: "10px 0 0 10px"}} className="input-group-text">
                            $
                        </span>
                                    <Form.Control style={{borderRadius: "0 10px 10px 0"}}
                                                  type="text"
                                                  value={devicePrice} placeholder="Price"
                                                  onChange={e => {
                                                      const value = e.target.value;

                                                      setPriceFormatIsValid(priceIsValid(value));
                                                      setDevicePrice(value);
                                                      if (priceFormatIsValid) {
                                                          setNegativePrice(value < 0);
                                                      }

                                                      setDevicePriceEmpty(value.trim() === '');
                                                  }}
                                                  />
                                </div>
                                <div hidden={devicePriceEmpty || priceFormatIsValid} style={{color: "#dc3545"}}>
                                    Price format is invalid
                                </div>
                                <div hidden={!priceIsNegative} style={{color: "#dc3545"}}>
                                    Price must not negative!
                                </div>
                            </Form.Group>
                        </Form>
                    </div>
                </Modal.Body>
                <Modal.Footer style={{
                    display: "flex",
                    justifyContent: "center",
                }}>
                    <Button
                        style={{backgroundColor: "#36bb27", borderColor: "#36bb27"}}
                        disabled={isLoading || receiptDateEmpty
                            || duplicateInventoryNumberError || inventoryNumberEmpty
                            || devicePriceEmpty || priceIsNegative || !priceFormatIsValid
                            || !receiptDateValid || !inventoryNumberFormatIsValid
                        }
                        onClick={handleSave}>
                        <span hidden={isLoading}> Save </span>
                        <Spinner
                            as="span"
                            animation="border"
                            size="sm"
                            role="status"
                            hidden={!isLoading}
                        />
                    </Button>

                </Modal.Footer>
            </Modal>

            <Button style={{marginBottom: "20px", width: "90px", height: "40px"}} variant="success"
                    onClick={handleShow}>
                <b>Save</b>
            </Button>
        </>
    );
};

export default SaveDeviceModal;