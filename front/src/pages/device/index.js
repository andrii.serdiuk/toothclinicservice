import DevicePage from "./DevicePage";
import DeviceEditPage from "./DeviceEditPage";

export {
    DevicePage,
    DeviceEditPage
};
