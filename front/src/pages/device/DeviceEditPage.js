import {useNavigate, useParams} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {getDevice, updateDevice} from "../../http/deviceApi";
import CenterInnerLayout from "../../components/layout/CenterInnerLayoutV";
import {Button, Form, Spinner} from 'react-bootstrap';
import {getCurrentDate} from "../../utils/common";

const DeviceEditPage = () => {

    //Old data stored for comparison
    const [deviceData, setDeviceData] = useState(null);

    //Inventory number is used to fetch the data from the server
    const {inventoryNumber} = useParams();

    //New data given by the user
    const [deviceInventoryNumber, setDeviceInventoryNumber] = useState(inventoryNumber);
    const [deviceReceiptDate, setDeviceReceiptDate] = useState('');

    const [devicePrice, setDevicePrice] = useState('');
    const [devicePriceCurrency, setDevicePriceCurrency] = useState('');

    //Inventory number field
    //Duplicate value
    const [duplicateInventoryNumberError, setDuplicateInventoryNumberError] = useState(false);
    const [duplicateInventoryNumberText, setDuplicateInventoryNumberText] = useState(null);
    //Empty field
    const [inventoryNumberEmpty, setInventoryNumberEmpty] = useState(false);
    //Wrong format
    const [inventoryNumberFormatIsValid, setInventoryNumberFormatIsValid] = useState(true);
    const inventoryNumberRegex = /^\d*$/g;

    //Receipt date field
    const [receiptDateValid, setReceiptDateValid] = useState(true);

    //Device price field
    //Empty field
    const [devicePriceEmpty, setDevicePriceEmpty] = useState(false);
    //Negative value
    const [priceIsNegative, setNegativePrice] = useState(false);
    //Invalid format
    const priceRegex = /^((\d+\.\d{2})|(\d+))$/g;
    const [priceFormatIsValid, setPriceFormatIsValid] = useState(true);

    //Set to true if the user is waiting for a response from the server
    const [isLoading, setLoading] = useState(false);


    useEffect(async () => {
        const result = (await getDevice(inventoryNumber)).data;
        setDeviceData(result);
        setDeviceReceiptDate(result.receiptDate);
        const price = result.price.replace(',', '');
        setDevicePriceCurrency(price.charAt(0));
        setDevicePrice(price.substring(1));
    }, [inventoryNumber]);

    const getDeviceData = () => {
        const inventoryNumberToUpdate = deviceData.inventoryNumber !== deviceInventoryNumber;
        const receiptDateToUpdate = deviceData.receiptDate !== deviceReceiptDate;
        const priceToUpdate = deviceData.price !== devicePrice;
        let result = {oldInventoryNumber: inventoryNumber};
        if (inventoryNumberToUpdate) {
            result.inventoryNumber = deviceInventoryNumber;
        }
        if (receiptDateToUpdate) {
            result.receiptDate = deviceReceiptDate;
        }
        if (priceToUpdate) {
            result.price = devicePrice;
        }
        return result;
    };

    const priceIsValid = (string) => {
        return priceRegex.test(string);
    };

    const inventoryNumberIsValid = (string) => {
        return inventoryNumberRegex.test(string);
    }

    const handleDuplicateInventoryNumber = () => {
        setDuplicateInventoryNumberError(true);
        setDuplicateInventoryNumberText(deviceInventoryNumber);
    };

    const submit = async e => {
        setLoading(true);
        e.stopPropagation();
        e.preventDefault();
        try {
            await updateDevice(getDeviceData());
            return true;
        } catch (e) {
            handleDuplicateInventoryNumber();
            return false;
        } finally {
            setLoading(false);
        }

    }

    const navigate = useNavigate();

    return(
        deviceData &&
        <CenterInnerLayout>

            <Form method={"put"} onSubmit={submit}>

                {/* Inventory number */}
                <Form.Group className="mb-3" controlId="controlInput">
                    <Form.Label>Inventory number</Form.Label>
                    <Form.Control value={deviceInventoryNumber} placeholder="Inventory number"
                                  onChange={e => {
                                      setDeviceInventoryNumber(e.target.value);
                                      setInventoryNumberFormatIsValid(inventoryNumberIsValid(e.target.value));
                                      if(inventoryNumberFormatIsValid) {
                                          setDuplicateInventoryNumberError(setDuplicateInventoryNumberText !== null &&
                                              e.target.value.trim() === duplicateInventoryNumberText);
                                          setInventoryNumberEmpty(e.target.value.trim() === '');
                                      }
                                  }}
                                  autoFocus/>
                    <div hidden={!duplicateInventoryNumberError} style={{color: "#dc3545"}}>
                        Duplicate inventory number
                    </div>
                    <div hidden={inventoryNumberFormatIsValid} style={{color: "#dc3545"}}>
                        Invalid format
                    </div>
                </Form.Group>

                {/* Receipt date */}

                <Form.Group className="mb-3" controlId="controlInput">
                    <Form.Label>Receipt date</Form.Label>
                    <Form.Control type="date"
                                  value={deviceReceiptDate} placeholder="Receipt date"
                                  onChange={e => {
                                      setDeviceReceiptDate(e.target.value);
                                      setReceiptDateValid(e.target.value <= getCurrentDate());
                                  }}
                                  />
                    <div hidden={receiptDateValid} style={{color: "#dc3545"}}>
                        Invalid receipt date
                    </div>
                </Form.Group>

                {/* Price */}
                <Form.Group className="mb-3" controlId="controlInput">
                    <Form.Label>Price</Form.Label>
                    <br/>
                    <div style={{display: "inline-flex"}}>
                        <span style={{borderRadius: "10px 0 0 10px"}} className="input-group-text">
                            {devicePriceCurrency}
                        </span>
                        <Form.Control style={{borderRadius: "0 10px 10px 0"}}
                                      type="text"
                                      value={devicePrice} placeholder="Price"
                                      onChange={e => {
                                          const value = e.target.value;

                                          setPriceFormatIsValid(priceIsValid(value));
                                          setDevicePrice(value);
                                          if(priceFormatIsValid) {
                                              setNegativePrice(value < 0);
                                          }

                                          setDevicePriceEmpty(value.trim() === '');
                                      }}
                                      />
                    </div>
                    <div hidden={devicePriceEmpty || priceFormatIsValid} style={{color: "#dc3545"}}>
                        Price format is invalid
                    </div>
                    <div hidden={!priceIsNegative} style={{color: "#dc3545"}}>
                        Price must not negative!
                    </div>
                </Form.Group>
            </Form>

            <div style={{display: "flex", flexDirection: "row", gap: "20px", marginTop: "30px"}}>
                <Button
                    style={{backgroundColor: "#36bb27", borderColor: "#36bb27"}}
                    disabled={ isLoading
                              || duplicateInventoryNumberError || inventoryNumberEmpty
                              || devicePriceEmpty || priceIsNegative || !priceFormatIsValid
                              || !receiptDateValid || !inventoryNumberFormatIsValid
                        }
                    onClick={(e) => {
                        submit(e).then(_ => navigate('/device/' + deviceInventoryNumber));
                    }}>
                    <span hidden={isLoading}> Save </span>
                    <Spinner
                        as="span"
                        animation="border"
                        size="sm"
                        role="status"
                        hidden={!isLoading}
                    />
                </Button>
                <Button
                    style={{backgroundColor: "#b72339", borderColor: "#b72339"}}
                    onClick={() => navigate('/device/' + inventoryNumber, {replace: false})}>
                    Cancel
                </Button>
            </div>
        </CenterInnerLayout>
    );

};

export default DeviceEditPage;