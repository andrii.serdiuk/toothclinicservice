import {useNavigate, useParams} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {deleteDevice, getDevice} from "../../http/deviceApi";
import CenterInnerLayout from "../../components/layout/CenterInnerLayoutV";
import {Button, Form} from 'react-bootstrap';
import {getUserFromToken} from "../../logic/auth";
import DIRECTOR from "../../utils/enums/userRoles.enum";

const DevicePage = () => {

    const [deviceData, setDeviceData] = useState(null);
    const [showEditButton, setShowEditButton] = useState(false);
    const {inventoryNumber} = useParams();

    const [deviceCannotBeRemoved, setDeviceCannotBeRemoved] = useState(false);

    useEffect(async () => {
        setShowEditButton((await getUserFromToken()).role === DIRECTOR.DIRECTOR);
        const result = (await getDevice(inventoryNumber)).data;
        setDeviceData(result);
    }, []);

    const navigate = useNavigate();

    const removeDevice = async () => {
        try {
            await deleteDevice(inventoryNumber);
            navigate(-1);
        } catch (e) {
            setDeviceCannotBeRemoved(true);
        }
    };

    return (
        deviceData &&
        <CenterInnerLayout style={{marginTop: "120px"}}>
            <h3>Device</h3>
            <hr/>
            <Form>
                <Form.Label>Inventory number</Form.Label>
                <Form.Control readOnly={true} value={inventoryNumber}/>
                <br/>
                <Form.Label>
                    Receipt date
                </Form.Label>
                <Form.Control readOnly={true} value={deviceData.receiptDate}/>
                <br/>
                <Form.Label>
                    Price
                </Form.Label>
                <Form.Control readOnly={true} value={deviceData.price}/>
            </Form>
            {
                showEditButton && <div style={{display: "flex", gap: "30px"}}>
                    <Button style={{backgroundColor: "#FF8C00", borderColor: "#FF8C00", marginTop: "40px", height: "50px", width: "80px"}}
                            onClick={() => navigate('/device/' + inventoryNumber + '/edit', {replace: false})}>
                        Edit
                    </Button>
                    <Button style={{backgroundColor: "#b72339", borderColor: "#b72339", marginTop: "40px",  height: "50px", width: "80px"}}
                            disabled={deviceCannotBeRemoved}
                            onClick={() =>  {removeDevice();} }>
                        Remove
                    </Button>
                </div>

            }
            <div style={{color: "#dc3545", marginTop: "30px"}} hidden={!deviceCannotBeRemoved}>
                This device has been already used
            </div>

        </CenterInnerLayout>
    );

};

export default DevicePage;
