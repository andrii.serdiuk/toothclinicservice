import React, {Component, createRef} from "react";

class InfoModal extends Component{
    constructor(props) {
        super(props);

        this.state = {
            title: '',
            body: '',
            buttonText: '',
        }

        this.modal = createRef();
    }

    render (){
        return (
            <div className="modal fade" ref={this.modal} id={this.props.id} tabIndex="-1" role="dialog" aria-labelledby="infoModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="infoModalLabel">{this.state.title}</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p>{this.state.body}</p>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary">{this.state.buttonText}</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default InfoModal;