import React, {useEffect, useState} from 'react';
import {getUserFromToken} from "../logic/auth";
import DOCTOR from "../utils/enums/userRoles.enum";
import DIRECTOR from "../utils/enums/userRoles.enum";
import REGISTRANT from "../utils/enums/userRoles.enum";
import PATIENT from "../utils/enums/userRoles.enum";
import {getDoctorByAccountId} from "../http/doctorApi";
import {getPatientByAccountId} from "../http/patientApi";
import {DoctorProfileComponent} from "./doctor";
import PatientProfileComponent from "./patient/PatientProfileComponent";
import {PersonProfile} from "../components/common";
import {getAccountInformation} from "../http/userApi";
import CenterInnerLayout from "../components/layout/CenterInnerLayoutV";

const MyProfilePage = () => {

    const [profile, setProfile] = useState(null);

    const getDoctor = async () => {
        const token = await getUserFromToken();
        const accountUserId = token.accountId;
        return (await getDoctorByAccountId(accountUserId)).data;
    }

    const getPatient = async () => {
        const token = await getUserFromToken();
        const accountUserId = token.accountId;
        return (await getPatientByAccountId(accountUserId)).data;
    }

    useEffect(async () => {
        const token = await getUserFromToken();
        const role = token.role;
        const accountUserId = token.accountId;
        switch (role) {
            case DOCTOR.DOCTOR:
                const doctorId = (await getDoctorByAccountId(accountUserId)).data.doctorId;
                setProfile(
                    <DoctorProfileComponent
                        doctorId={doctorId}
                        editingAllowedNotOnlyForDirector={true}
                        getData={getDoctor}
                    />
                );
                break;
            case PATIENT.PATIENT:
                const patientId = (await getPatientByAccountId(accountUserId)).data.patientCardId;
                setProfile(
                    <PatientProfileComponent
                        patientId={patientId}
                        editingAllowedNotOnlyForDirector={true}
                        getData={getPatient}
                    />);
                break;
            default:
                const accountItem = (await getAccountInformation(accountUserId)).data;
                let imageSource = "https://cdn-icons-png.flaticon.com/512/921/921059.png";
                if(role === DIRECTOR.DIRECTOR) {
                    imageSource = "https://cdn-icons-png.flaticon.com/512/7439/7439589.png";
                } else if (role === REGISTRANT.REGISTRANT) {
                    imageSource = "https://cdn-icons-png.flaticon.com/512/1584/1584961.png";
                }

                const profileData = {
                    firstName: '',
                    secondName: '',
                    lastName: '',
                    fields: [
                        {title: "Email", content: accountItem.accountEmail},
                    ]
                };
                setProfile(
                    <div style={{position: "absolute", left: "45%", top: "30%"}}>
                        <PersonProfile
                            roleName={role}
                            personFields={profileData}
                            editingAllowed={false}
                            onProfileDelete={null}
                            onProfileEdit={null}
                            onAccountDelete={null}
                            onAccountEdit={null}
                            onAccountAdd={null}
                            imageSource={imageSource}
                        />
                    </div>
                );
                break;
        }
    }, []);

    return (
        <>
            {profile}
        </>
    );
};

export default MyProfilePage;
