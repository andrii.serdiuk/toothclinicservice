import React, { useEffect, useState } from 'react';
import { Header, NavbarWrapper } from '../components/base';
import jwt_decode from 'jwt-decode';
import { customLocalStorage } from '../utils/localStorage';
import { getUserFromToken } from '../logic/auth';
import { useLocation, useNavigate } from 'react-router-dom';
import { HOME_ROUTE } from '../utils/routesNames';
import { Container } from 'react-bootstrap';
import { InnerLogin } from '../components';
import { CenterInnerLayoutH, FullHeightLayout, HorizontalCenterLayout } from '../components/layout';
import CenterInnerLayout from "../components/layout/CenterInnerLayoutV";

const NotFoundPage = () => {

  const [ user, setUser ] = useState(null);
  const [ loading, setLoading ] = useState(true);
  const { state } = useLocation();
  const location = state?.location;

  useEffect(() => {
    getUserFromToken().then(user => {
      setUser(user);
    })
      .catch(ignored => {})
      .finally(() => setLoading(false));
  }, []);

  const navigate = useNavigate();

  const logout = () => {
    customLocalStorage.setAuthToken(null);
    setUser(null);
  }

  if (loading)
    return (
      <div style={{marginTop: '100px'}} className="d-flex justify-content-center">
        <div className="spinner-grow  text-primary" role="status"></div>
      </div>
    );

    return (
      <HorizontalCenterLayout
        style={{
          // alignItems: "normal"
        }}
      >
        <NavbarWrapper user={user}>
          <Header displayLogin={false} user={user} setUser={setUser}/>
          <div
            style={{
              display: "flex",
              flexGrow: 1,
              width: "100%"
            }}
          >
            <CenterInnerLayout
              style={{
                width: "70%",
                flexGrow: 0,
               paddingTop: "120px"
              }}
            >
              <div style={{display: "flex", flexDirection: "column", justifyContent: "center"}}>
                <h3>Page hasn't been found</h3>
                <br/>
                <img width={'210px'}
                   src="https://cdn-icons-png.flaticon.com/512/6146/6146689.png"
                   alt={'Page not found'}/>
              </div>
            </CenterInnerLayout>
            {!user && <CenterInnerLayoutH>
              <InnerLogin location={location}/>
            </CenterInnerLayoutH>}
          </div>
        </NavbarWrapper>
      </HorizontalCenterLayout>
    );
};

export default NotFoundPage;