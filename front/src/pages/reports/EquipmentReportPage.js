import React, { useEffect, useState } from 'react';
import ReactPaginate from 'react-paginate';
import { useNavigate } from 'react-router-dom';
import { BaseSpinner } from '../../components/base';
import CenterInnerLayout from '../../components/layout/CenterInnerLayoutV';
import ReportWrapper from '../../components/report/ReportWrapper';
import { equipmentReport } from '../../http/reportsApi';

const EquipmentReportPage = () => {
    //Keep track of length separately from data
    const [currentItems, setCurrentItems] = useState(null);
    const [itemsOnPage, setItemsOnPage] = useState(20);
    const [pageCount, setPageCount] = useState(0);
    const [currentPage, setCurrentPage] = useState(0);
    const [loading, setLoading] = useState(true);

    const headers = ["Equipment id", "Equipment name", "Total times used", "Total income"];
    const [allItems, setAllItems] = useState(null);

    useEffect(async () => {
      const { equipmentReport : allItems } = await equipmentReport();
      // setPageCount(Math.ceil(res.count / itemsOnPage));
      setAllItems(allItems.map(item => {
              return {
                  onClick: () => navigate('/equipment/' + item.equipmentId, {replace: false}),
                  key: item.equipmentId,
                  cells: [item.equipmentId, item.equipmentName, item.timesUsed, item.totalIncome]
              };
          }
      ));
    }, []);

    const navigate = useNavigate();
    useEffect(async () => {
        const res = await equipmentReport(currentPage, itemsOnPage);
        setLoading(false);
        setPageCount(Math.ceil(res.count / itemsOnPage));
        setCurrentItems(res.equipmentReport);
    }, [itemsOnPage, currentPage]);

    const handlePageClick = event => {
        setCurrentPage(event.selected);
    }

    const height = window.innerHeight;

    if (loading) {
      return (
        <CenterInnerLayout
          style={{
            height: height - 50
          }}
        >
          <BaseSpinner/>
        </CenterInnerLayout>
      );
    }

    return (
      <ReportWrapper title="Equipment Usage and Total Income" headers={headers} itemsToPrint={allItems}>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            // 50 - hardcoded header height
            height: height - 50,
            maxHeight: height - 50,
            justifyContent: 'space-between',
          }}
        >
          <div
            style={{
              overflowY: 'auto',
              marginBottom: '20px',
              height: '100%'
            }}
          >
              <table className="table">
                  <thead className="thead-dark">
                  <tr>
                      <th scope="col">Equipment id</th>
                      <th scope="col">Equipment name</th>
                      <th scope="col">Total times used</th>
                      <th scope="col">Total income</th>
                  </tr>
                  </thead>
                  <tbody>
                  {
                      currentItems &&
                      currentItems.map(item => (
                          <tr
                            className="cursor-pointer"
                            onClick={() => navigate('/equipment/' + item.equipmentId, {replace: false})}
                            key={item.equipmentId}
                          >
                              <th scope="row">{item.equipmentId}</th>
                              <td>{item.equipmentName}</td>
                              <td>{item.timesUsed}</td>
                              <td>{item.totalIncome}</td>
                          </tr>
                      ))}
                  </tbody>
              </table>
            </div>
            <div className='pagination-list-print-hidden'>
            <ReactPaginate
                breakLabel="..."
                nextLabel=">"
                onPageChange={handlePageClick}
                pageRangeDisplayed={5}
                pageCount={pageCount}
                previousLabel="<"
                renderOnZeroPageCount={null}
                pageClassName="page-item"
                pageLinkClassName="page-link"
                previousClassName="page-item"
                previousLinkClassName="page-link"
                nextClassName="page-item"
                nextLinkClassName="page-link"
                breakClassName="page-item"
                breakLinkClassName="page-link"
                containerClassName="pagination justify-content-center"
                activeClassName="active"
              />
              </div>
        </div>
      </ReportWrapper>
    );
};

export default EquipmentReportPage;