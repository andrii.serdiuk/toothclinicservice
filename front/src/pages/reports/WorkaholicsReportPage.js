import React, { useEffect, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import ReactPaginate from 'react-paginate';
import { useNavigate } from 'react-router-dom';
import { BaseSpinner } from '../../components/base';
import CenterInnerLayout from '../../components/layout/CenterInnerLayoutV';
import ReportWrapper from '../../components/report/ReportWrapper';
import { workaholics } from '../../http/reportsApi';

const WorkaholicsReportPage = () => {
  const navigate = useNavigate();
  const [currentItems, setCurrentItems] = useState(null);
  const [itemsOnPage, setItemsOnPage] = useState(20);
  const [pageCount, setPageCount] = useState(0);
  const [currentPage, setCurrentPage] = useState(0);
  const [loading, setLoading] = useState(true);
  const today = new Date();
  const twoWeeksAgo = new Date();
  twoWeeksAgo.setDate(twoWeeksAgo.getDate() - 14);

  const [dateFrom, setDateFrom] = useState(twoWeeksAgo.toISOString().substring(0, 10));
  const [dateTo, setDateTo] = useState(today.toISOString().substring(0, 10));

  const height = window.innerHeight;

  const click = async (e) => {
    e.preventDefault();
    setLoading(true);
    const res = await workaholics(currentPage, itemsOnPage, dateFrom, dateTo);
    setPageCount(Math.ceil(res.count / itemsOnPage));
    setCurrentItems(res.workaholics);
    setLoading(false);
  }
  const headers = ["Doctor Id", "First Name", "Last Name"];
  const [allItems, setAllItems] = useState(null);

  useEffect(async () => {
    const {workaholics: res} = await workaholics(dateFrom, dateTo);
    console.log(res);
    const allItemsToPrint = res.map(item => {
      return {
        onClick: (() => navigate('/doctor/' + item.doctorId, {replace: false})),
        key: item.doctorId,
        cells: [item.doctorId, item.firstName, item.lastName]
      };
    })
    setAllItems(allItemsToPrint);
  }, [dateFrom, dateTo]);

  useEffect(async () => {
    try {
      const res = await workaholics(dateFrom, dateTo, currentPage, itemsOnPage, );
      // console.log(res);
      setPageCount(Math.ceil(res.count / itemsOnPage));
      setCurrentItems(res.workaholics);
      setLoading(false);
    } catch (e) {
      alert('Something bad happened, could not load the page :(');
    }
  }, [itemsOnPage, currentPage, dateFrom, dateTo]);

  const handlePageClick = event => {
      setCurrentPage(event.selected);
  }

  if (loading)
    return (
      <CenterInnerLayout
        style={{
          height: height - 50
        }}
      >
        <BaseSpinner/>
      </CenterInnerLayout>
    );

    const header = (
      <div
      style={{
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center'
      }}
    >
      <Form.Group controlId="dob">
        <Form.Label>Select date from</Form.Label>
        <Form.Control
          value={(() => {console.log(dateFrom); return dateFrom})()}
          onChange={(event) => setDateFrom(event.target.value)}
          type="date"
          name="dob"
          placeholder="Date from"
        />
      </Form.Group>
      <Form.Group controlId="dob">
        <Form.Label>Select date to</Form.Label>
        <Form.Control
          value={dateTo}
          onChange={(event) => setDateTo(event.target.value)}
          type="date"
          name="dob"
          placeholder="Date to"
        />
      </Form.Group>
      <Button
        onClick={e => click(e)}
        style={{
          height: '30px',
          textAlign: 'center',
          display: 'flex',
          alignItems: 'center',
        }}
      >
        Search
      </Button>
    </div>
    );

  return (
    <ReportWrapper title="Workaholics" header={header} headersToPrint={headers} itemsToPrint={allItems}>
    <div
    style={{
      display: 'flex',
      flexDirection: 'column',
      // 50 - hardcoded header height
      height: height - 50,
      maxHeight: height - 50,
      justifyContent: 'flex-start',
    }}
  >

      <div
      style={{
        overflowY: 'auto',
        marginBottom: '20px',
        marginTop: '10px',
        height: '100%',
        borderTop: 'solid light-gray 1px'
      }}
    >
        <table className="table">
            <thead className="thead-dark">
            <tr>
                <th scope="col">Doctor Id</th>
                {/* <th scope="col">Surname</th> */}
                <th scope="col">First Name</th>
                <th scope="col">Last Name</th>
            </tr>
            </thead>
            <tbody>
            {
                currentItems &&
                currentItems.map(item => (
                    <tr
                      style={{
                        cursor: 'pointer'
                      }}
                      key={item.doctorId}
                    >
                        <th scope="row">{item.doctorId}</th>
                        {/* <td>{item.secondName}</td> */}
                        <td>{item.firstName}</td>
                        <td>{item.lastName}</td>
                    </tr>
                ))}
            </tbody>
        </table>
      </div>
      <div className='pagination-list-print-hidden'>
        <ReactPaginate
            breakLabel="..."
            nextLabel=">"
            onPageChange={handlePageClick}
            pageRangeDisplayed={5}
            pageCount={pageCount}
            previousLabel="<"
            renderOnZeroPageCount={null}
            pageClassName="page-item"
            pageLinkClassName="page-link"
            previousClassName="page-item"
            previousLinkClassName="page-link"
            nextClassName="page-item"
            nextLinkClassName="page-link"
            breakClassName="page-item"
            breakLinkClassName="page-link"
            containerClassName="pagination justify-content-center"
            activeClassName="active"
            style={{
              display: 'flex',
              justifySelf: 'flex-end'
            }}
          />
        </div>
    </div>
    </ReportWrapper>
  );
};

export default WorkaholicsReportPage;