import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import {PaginationList} from "../../components/common";
import {doctorBenefitReport, doctorUniquePatientsAmount} from "../../http/reportsApi";
import {Button, Form} from "react-bootstrap";
import ReportWrapper from '../../components/report/ReportWrapper';

const DoctorUniquePatientsReportPage = () => {

  const today = new Date();
  const twoWeeksAgo = new Date();
  twoWeeksAgo.setDate(twoWeeksAgo.getDate() - 14);

  const [dateFrom, setDateFrom] = useState(twoWeeksAgo.toISOString().substring(0, 10));
  const [dateTo, setDateTo] = useState(today.toISOString().substring(0, 10));

    const [currentItems, setCurrentItems] = useState(null);
    const [itemsOnPage, setItemsOnPage] = useState(20);
    const [pageCount, setPageCount] = useState(1);
    const [currentPage, setCurrentPage] = useState(0);

    const navigate = useNavigate();
    const choosePage = async newPage => {
        setCurrentPage(newPage);
    };

    const height = window.innerHeight;

    const getCurrentDate = () => {
        const today = new Date();
        const dd = String(today.getDate()).padStart(2, '0');
        const mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        const yyyy = today.getFullYear();
        return yyyy + '-' + mm + '-' + dd;
    }

    const headers = ["Doctor personal number", "Name", "Patients amount"]
    const [allItems, setAllItems] = useState(null);

    useEffect(async () => {
      const allItems = (await doctorUniquePatientsAmount(dateFrom, dateTo, 0, 200)).data.rows;
      // setPageCount(Math.ceil(res.count / itemsOnPage));
      setAllItems(allItems.map(item => {
              return {
                  onClick: (() => navigate('/doctors/' + item.doctorId, {replace: false})),
                  key: item.doctorId,
                  cells: [item.doctorId, item.name, item.patientsamount]
              };
          }
      ));
    }, [dateFrom, dateTo]);

    const search = async () => {
        const res = (await doctorUniquePatientsAmount(dateFrom, dateTo, currentPage, itemsOnPage)).data.rows;
        // setPageCount(Math.ceil(res.count / itemsOnPage));
        setCurrentItems(res.map(item => {
                return {
                    onClick: (() => navigate('/doctors/' + item.doctorId, {replace: false})),
                    key: item.doctorId,
                    cells: [item.doctorId, item.name, item.patientsamount]
                };
            }
        ));
    };

    useEffect(() => {
        search();
    }, [currentPage]);

    const header = (
      <div
          style={{
              display: 'flex',
              margin: "20px 0 15px 0",
              justifyContent: 'center',
              gap: "60px",
              alignItems: 'center'
          }}
      >
          <Form.Group controlId="dob">
              <Form.Label>Select date from</Form.Label>
              <Form.Control
                  value={dateFrom}
                  onChange={(event) => setDateFrom(event.target.value)}
                  type="date"
                  name="dob"
                  placeholder="Date from"
              />
          </Form.Group>
          <Form.Group controlId="dob">
              <Form.Label>Select date to</Form.Label>
              <Form.Control
                  value={dateTo}
                  onChange={(event) => setDateTo(event.target.value)}
                  type="date"
                  name="dob"
                  placeholder="Date to"
              />
          </Form.Group>
          <Button
              onClick={e => search(e)}
          >
              Search
          </Button>
      </div>
    );

    return (
        <ReportWrapper header={header} title="Doctor patients" headers={headers} itemsToPrint={allItems}>
          <PaginationList
              choosePage={choosePage}
              headers={headers}
              pageCount={pageCount}
              items={currentItems}
          />
        </ReportWrapper>
    );
};

export default DoctorUniquePatientsReportPage;