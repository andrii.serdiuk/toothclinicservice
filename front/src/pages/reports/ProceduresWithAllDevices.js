import React, {useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import {equipmentPopularityReport, proceduresWithAllDevices} from "../../http/reportsApi";
import {PaginationList} from "../../components/common";
import ReportWrapper from "../../components/report/ReportWrapper";

const ProceduresWithAllDevices = () => {
    //Keep track of length separately from data
    const [currentItems, setCurrentItems] = useState(null);
    const [itemsOnPage, setItemsOnPage] = useState(20);
    const [pageCount, setPageCount] = useState(0);
    const [currentPage, setCurrentPage] = useState(0);

    const navigate = useNavigate();
    const choosePage = async newPage => {
        setCurrentPage(newPage);
    };
    const headers = ["Procedure id", "Procedure name", "Current price", "Doctor's minimal category"];
    const [allItems, setAllItems] = useState(null);

    useEffect(async () => {
      const { proceduresWithAllDevices: allItemsRes }= await proceduresWithAllDevices();
      const allItemsToPrint = allItemsRes.map(item => {
        return {
          onClick: (() => navigate('/procedures/' + item.procedureId, {replace: false})),
          key: item.procedureId,
          cells: [item.procedureId, item.procedureName, item.currentPrice, item.minimalCategory]
        };
      })
      setAllItems(allItemsToPrint);
    }, []);

    useEffect(async () => {
        const {count, proceduresWithAllDevices: procedures} = await proceduresWithAllDevices(itemsOnPage, currentPage);
        setPageCount(Math.ceil(count / itemsOnPage));
        setCurrentItems(procedures.map(item => {
                return {
                    onClick: (() => navigate('/procedures/' + item.procedureId, {replace: false})),
                    key: item.procedureId,
                    cells: [item.procedureId, item.procedureName, item.currentPrice, item.minimalCategory]
                };
            }
        ));
    }, [currentPage]);


    return (
      <ReportWrapper title="Procedures With All Devices" headersToPrint={headers} itemsToPrint={allItems}>
        <PaginationList
            choosePage={choosePage}
            headers={headers}
            pageCount={pageCount}
            items={currentItems}
        />
      </ReportWrapper>
    );
};

export default ProceduresWithAllDevices;