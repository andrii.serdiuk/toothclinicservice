import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import {doctorBenefitReport, importantProcedure} from '../../http/reportsApi';
import {PaginationList} from "../../components/common";
import ReportWrapper from '../../components/report/ReportWrapper';

const ImportantProceduresReportPage = () => {
    const [currentItems, setCurrentItems] = useState(null);
    const [itemsOnPage, setItemsOnPage] = useState(20);
    const [pageCount, setPageCount] = useState(1);
    const [currentPage, setCurrentPage] = useState(0);

    const navigate = useNavigate();
    const choosePage = async newPage => {
        setCurrentPage(newPage);
    };

    const headers = ["Procedure name", "Current price", "Minimal category"];
    const [allItems, setAllItems] = useState(null);

    useEffect(async () => {
      const res = (await importantProcedure(0, 200)).data.rows;
      // setPageCount(Math.ceil(res.count / itemsOnPage));
      setAllItems(res.map(item => {
              return {
                onClick: (() => navigate('/procedure/' + item.procedureId, {replace: false})),
                key: item.doctorId,
                cells: [item.procedureName, item.currentPrice, item.minimalCategory]
              };
          }
      ));
    }, []);

    useEffect(async () => {
        const res = (await importantProcedure(currentPage, itemsOnPage)).data.rows;
        // setPageCount(Math.ceil(res.count / itemsOnPage));
        setCurrentItems(res.map(item => {
                return {
                    onClick: (() => navigate('/procedure/' + item.procedureId, {replace: false})),
                    key: item.doctorId,
                    cells: [item.procedureName, item.currentPrice, item.minimalCategory]
                };
            }
        ));
    }, [currentPage]);

    return (
      <ReportWrapper title="Important Procedures" headers={headers} itemsToPrint={allItems}>
        <PaginationList
            choosePage={choosePage}
            headers={headers}
            pageCount={pageCount}
            items={currentItems}
        />
      </ReportWrapper>
    );
};

export default ImportantProceduresReportPage;