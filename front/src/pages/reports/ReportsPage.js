import React from 'react';
import { Outlet } from 'react-router-dom';

const ReportsPage = () => {
  return <Outlet/>;
};

export default ReportsPage;