import React, {useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import {averageProceduresAgeReport, equipmentPopularityReport, equipmentReport} from "../../http/reportsApi";
import CenterInnerLayout from "../../components/layout/CenterInnerLayoutV";
import {BaseSpinner} from "../../components/base";
import ReactPaginate from "react-paginate";
import {PaginationList} from "../../components/common";
import ReportWrapper from "../../components/report/ReportWrapper";

const PopularityOfEquipment = () => {
    //Keep track of length separately from data
    const [currentItems, setCurrentItems] = useState(null);
    const [itemsOnPage, setItemsOnPage] = useState(20);
    const [pageCount, setPageCount] = useState(0);
    const [currentPage, setCurrentPage] = useState(0);

    const navigate = useNavigate();
    const choosePage = async newPage => {
        setCurrentPage(newPage);
    };

    const headers = ["Equipment id", "Equipment name", "Times used"];
    const [allItems, setAllItems] = useState(null);

    useEffect(async () => {
      const { equipmentPopularity : res} = (await equipmentPopularityReport());
      // setPageCount(Math.ceil(res.count / itemsOnPage));
      setAllItems(res.map(item => {
        return {
            onClick: (() => navigate(`/equipment/${item.equipmentId}/devices`, {replace: false})),
            key: item.equipmentId,
            cells: [item.equipmentId, item.equipmentName, item.timesUsed]
        };
          }
      ));
    }, []);

    useEffect(async () => {
        const {count, equipmentPopularity} = (await equipmentPopularityReport(itemsOnPage, currentPage));
        setPageCount(Math.ceil(count / itemsOnPage));
        setCurrentItems(equipmentPopularity.map(item => {
                return {
                    onClick: (() => navigate(`/equipment/${item.equipmentId}/devices`, {replace: false})),
                    key: item.equipmentId,
                    cells: [item.equipmentId, item.equipmentName, item.timesUsed]
                };
            }
        ));
    }, [currentPage]);


    return (
      <ReportWrapper title="Popularity of Equipment" headersToPrint={headers} itemsToPrint={allItems}>
        <PaginationList
            choosePage={choosePage}
            headers={headers}
            pageCount={pageCount}
            items={currentItems}
        />
      </ReportWrapper>
    );
};

export default PopularityOfEquipment;