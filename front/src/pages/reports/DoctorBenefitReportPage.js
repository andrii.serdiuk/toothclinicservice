import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import {doctorBenefitReport} from '../../http/reportsApi';
import {PaginationList} from "../../components/common";
import ReportWrapper from '../../components/report/ReportWrapper';

const DoctorBenefitReportPage = () => {

    const [currentItems, setCurrentItems] = useState(null);
    const [itemsOnPage, setItemsOnPage] = useState(20);
    const [pageCount, setPageCount] = useState(1);
    const [currentPage, setCurrentPage] = useState(0);

    const navigate = useNavigate();
    const choosePage = async newPage => {
        setCurrentPage(newPage);
    };

    const headers = ['Doctor Personal Number', 'Name', 'Income'];
    const [allItems, setAllItems] = useState(null);

    useEffect(async () => {
      const {data: {rows: allItemsRes}} = await doctorBenefitReport(0, 200);
      const allItemsToPrint = allItemsRes.map(item => {
        return {
            onClick: (() => navigate('/doctors/' + item.doctorId, {replace: false})),
            key: item.doctorId,
            cells: [item.doctorId, item.name, item.benefit]
        };
      })
      setAllItems(allItemsToPrint);
    }, []);

    useEffect(async () => {
        const res = (await doctorBenefitReport(currentPage, itemsOnPage)).data.rows;
        // setPageCount(Math.ceil(res.count / itemsOnPage));
        setCurrentItems(res.map(item => {
                return {
                    onClick: (() => navigate('/doctors/' + item.doctorId, {replace: false})),
                    key: item.doctorId,
                    cells: [item.doctorId, item.name, item.benefit]
                };
            }
        ));
    }, [currentPage]);

    return (
      <ReportWrapper title="Benefit for each doctor" headersToPrint={headers} itemsToPrint={allItems}>
        <PaginationList
            choosePage={choosePage}
            headers={headers}
            pageCount={pageCount}
            items={currentItems}
        />
      </ReportWrapper>
    );
};

export default DoctorBenefitReportPage;