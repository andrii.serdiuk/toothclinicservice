import React, {useEffect, useState} from 'react';
import {Button, Form} from 'react-bootstrap';
import ReactPaginate from 'react-paginate';
import {useNavigate} from 'react-router-dom';
import {BaseSpinner} from '../../components/base';
import CenterInnerLayout from '../../components/layout/CenterInnerLayoutV';
import {getPatientFinishedAppointments, proceduresWithAllDevices, workaholics} from '../../http/reportsApi';
import {PaginationList, DoctorDropDown} from "../../components/common";
import {getCurrentDate} from "../../utils/common";
import PatientDropDown from "../../components/common/PatientDropDown";
import ReportWrapper from '../../components/report/ReportWrapper';

const PatientFinishedAppointmentByPeriodReportPage = () => {
    const navigate = useNavigate();
    const [currentItems, setCurrentItems] = useState(null);
    const [itemsOnPage, setItemsOnPage] = useState(20);
    const [pageCount, setPageCount] = useState(0);
    const [currentPage, setCurrentPage] = useState(1);
    const [loading, setLoading] = useState(true);
    const [patientId, setPatientId] = useState(null);

    const height = window.innerHeight;

    const choosePage = async newPage => {
        setCurrentPage(newPage);
    };

    const headers = ["Appointment id", "Appointment date", "Diagnosis", "Start time", "End time", "State after", "Treatment", "Devices used"];
    const [allItems, setAllItems] = useState(null);

    useEffect(async () => {
      if (!patientId) {
        setCurrentItems(null);
        setCurrentPage(1);
        setPageCount(0);
        return;
    }
    setCurrentPage(0);
    const {
        patientFinishedAppointments: appointments
    } = await getPatientFinishedAppointments(patientId);
    setAllItems(appointments.map(item => {
            return {
                onClick: (() => navigate('/appointments/' + item.appointmentId, {replace: false})),
                key: item.appointmentId,
                cells: [item.appointmentId, item.appointmentDate, item.diagnosis,
                    item.appointmentStartTime, item.appointmentEndTime,
                    item.patientState, item.treatment, item.devices_used
                ]
            };
        }
    ));
    }, []);

    useEffect(async () => {
        if (!patientId) {
            setCurrentItems(null);
            setCurrentPage(1);
            setPageCount(0);
            return;
        }
        setCurrentPage(0);
        const {
            count,
            patientFinishedAppointments: appointments
        } = await getPatientFinishedAppointments(patientId, itemsOnPage, currentPage);
        setPageCount(Math.ceil(count / itemsOnPage));
        setCurrentItems(appointments.map(item => {
                return {
                    onClick: (() => navigate('/appointments/' + item.appointmentId, {replace: false})),
                    key: item.appointmentId,
                    cells: [item.appointmentId, item.appointmentDate, item.diagnosis,
                        item.appointmentStartTime, item.appointmentEndTime,
                        item.patientState, item.treatment, item.devices_used
                    ]
                };
            }
        ));
    }, [currentPage, patientId]);

    const header = (
        <div className="row d-flex justify-content-center w-100">
            <div className="col-7 d-flex">
                <Form.Group className="w-100">
                    <Form.Label>Select patient</Form.Label>
                    <PatientDropDown changePatient={id => setPatientId(id ? id.value : null)}/>
                </Form.Group>
            </div>
        </div>
    );

    return (
      <ReportWrapper title="Finished Appointments" header={header} headers={headers} itemsToPrint={allItems}>
        <div className="d-flex flex-column w-100 h-100">
          <div className="h-100">
                <PaginationList
                    choosePage={choosePage}
                    headers={headers}
                    pageCount={pageCount}
                    items={currentItems}
                />
            </div>
        </div>
      </ReportWrapper>
    );
};

export default PatientFinishedAppointmentByPeriodReportPage;