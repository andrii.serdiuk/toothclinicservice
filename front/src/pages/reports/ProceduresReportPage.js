import React, { useEffect, useState } from 'react';
import ReactPaginate from 'react-paginate';
import { useNavigate } from 'react-router-dom';
import { BaseSpinner } from '../../components/base';
import CenterInnerLayout from '../../components/layout/CenterInnerLayoutV';
import ReportWrapper from '../../components/report/ReportWrapper';
import { proceduresReport } from '../../http/reportsApi';

const ProceduresReportPage = () => {
    //Keep track of length separately from data
    const [currentItems, setCurrentItems] = useState(null);
    const [itemsOnPage, setItemsOnPage] = useState(20);
    const [pageCount, setPageCount] = useState(0);
    const [currentPage, setCurrentPage] = useState(0);
    const [loading, setLoading] = useState(true);

    const headers = ['Procedure Id', 'Procedure Name', 'Total Times Used', 'Total Income'];
    const [allItems, setAllItems] = useState(null);

    useEffect(async () => {
      const allItemsRes= await proceduresReport();
      const allItemsToPrint = allItemsRes.proceduresReport.map(item => {
        return {
            onClick: (() => navigate('/procedures/' + item.procedureId, {replace: false})),
            key: item.procedureId,
            cells: [item.procedureId, item.procedureName, item.timesUsed, item.totalIncome]
        };
      })
      setAllItems(allItemsToPrint);
    }, []);

    const navigate = useNavigate();
    useEffect(async () => {
        const res = await proceduresReport(currentPage, itemsOnPage);
        setLoading(false);
        setPageCount(Math.ceil(res.count / itemsOnPage));
        setCurrentItems(res.proceduresReport);
    }, [itemsOnPage, currentPage]);

    const handlePageClick = event => {
        setCurrentPage(event.selected);
    }

    const height = window.innerHeight;

    if (loading) {
      return (
        <CenterInnerLayout
          style={{
            height: height - 50
          }}
        >
          <BaseSpinner/>
        </CenterInnerLayout>
      );
    }

    return (
      <ReportWrapper title="Procedures Income and Total Usage" headersToPrint={headers} itemsToPrint={allItems}>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          // 50 - hardcoded header height
          height: height - 50,
          maxHeight: height - 50,
          justifyContent: 'space-between',
        }}
      >
        <div
          style={{
            overflowY: 'auto',
            marginBottom: '20px',
            height: '100%'
          }}
        >
            <table className="table">
                <thead className="thead-dark">
                <tr>
                    <th scope="col">Procedure id</th>
                    <th scope="col">Procedure name</th>
                    <th scope="col">Total times used</th>
                    <th scope="col">Total income</th>
                </tr>
                </thead>
                <tbody>
                {
                    currentItems &&
                    currentItems.map(item => (
                        <tr
                          className="cursor-pointer"
                          onClick={() => navigate('/procedures/' + item.procedureId, {replace: false})}
                          key={item.procedureId}
                          >
                            <th scope="row">{item.procedureId}</th>
                            <td>{item.procedureName}</td>
                            <td>{item.timesUsed}</td>
                            <td>{item.totalIncome}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
          </div>
          <div  className='pagination-list-print-hidden'>
            <ReactPaginate
                breakLabel="..."
                nextLabel=">"
                onPageChange={handlePageClick}
                pageRangeDisplayed={5}
                pageCount={pageCount}
                previousLabel="<"
                renderOnZeroPageCount={null}
                pageClassName="page-item"
                pageLinkClassName="page-link"
                previousClassName="page-item"
                previousLinkClassName="page-link"
                nextClassName="page-item"
                nextLinkClassName="page-link"
                breakClassName="page-item"
                breakLinkClassName="page-link"
                containerClassName="pagination justify-content-center"
                activeClassName="active"
              />
              </div>
        </div>
        </ReportWrapper>
    );
};

export default ProceduresReportPage;