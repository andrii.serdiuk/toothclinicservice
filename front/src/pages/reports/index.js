import ReportsPage from './ReportsPage';
import WorkaholicsReportPage from './WorkaholicsReportPage';
import ProceduresReportPage from './ProceduresReportPage';
import EquipmentReportPage from './EquipmentReportPage';
import DoctorBenefitReportPage from "./DoctorBenefitReportPage";
import DoctorUniquePatientsReportPage from "./DoctorUniquePatientsReportPage";
import ImportantProceduresReportPage from "./ImportantProceduresReportPage";
import AverageAgeForProcedure from "./AverageAgeForProcedure";
import PopularityOfEquipment from "./PopularityOfEquipment";
import ProceduresWithAllDevices from "./ProceduresWithAllDevices";
import PatientFinishedAppointmentByPeriodReportPage from "./PatientFinishedAppointmentByPeriodReportPage";
export {
    ReportsPage,
    WorkaholicsReportPage,
    ProceduresReportPage,
    EquipmentReportPage,
    DoctorBenefitReportPage,
    DoctorUniquePatientsReportPage,
    ImportantProceduresReportPage,
    AverageAgeForProcedure,
    PopularityOfEquipment,
    ProceduresWithAllDevices,
    PatientFinishedAppointmentByPeriodReportPage
};
