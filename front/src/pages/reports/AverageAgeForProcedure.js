import React, {useEffect, useRef, useState} from "react";
import {useNavigate} from "react-router-dom";
import {averageProceduresAgeReport, equipmentPopularityReport} from "../../http/reportsApi";
import CenterInnerLayout from "../../components/layout/CenterInnerLayoutV";
import {BaseSpinner} from "../../components/base";
import ReactPaginate from "react-paginate";
import {getDoctors} from "../../http/doctorApi";
import {PaginationList} from "../../components/common";
import {useReactToPrint} from "react-to-print";
import {Button} from "@mui/material";
import "./../../components/assets/css/print.css"
import "./../../components/assets/css/report.css"
import PrintButton from "../../components/report/PrintButton";
import ReportWrapper from "../../components/report/ReportWrapper";

const AverageAgeForProcedure = () => {
    //Keep track of length separately from data
    const [currentItems, setCurrentItems] = useState(null);
    const [itemsOnPage, setItemsOnPage] = useState(20);
    const [pageCount, setPageCount] = useState(0);
    const [currentPage, setCurrentPage] = useState(0);

    const navigate = useNavigate();
    const choosePage = async newPage => {
        setCurrentPage(newPage);
    };

    const headers = ['Procedure Id', 'Procedure Name', 'Average Patient Age'];
    const [allItems, setAllItems] = useState(null);

    useEffect(async () => {
      const allItemsRes = await averageProceduresAgeReport();
      const allItemsToPrint = allItemsRes.averageProceduresAge.map(item => {
        return {
            onClick: (() => navigate('/procedures/' + item.procedureId, {replace: false})),
            key: item.procedureId,
            cells: [item.procedureId, item.procedureName, item.averageYears]
        };
      })
      setAllItems(allItemsToPrint);
    }, []);

    useEffect(async () => {
        const {count, averageProceduresAge} = (await averageProceduresAgeReport(itemsOnPage, currentPage));
        setPageCount(Math.ceil(count / itemsOnPage));
        setCurrentItems(averageProceduresAge.map(item => {
                return {
                    onClick: (() => navigate('/procedures/' + item.procedureId, {replace: false})),
                    key: item.procedureId,
                    cells: [item.procedureId, item.procedureName, item.averageYears]
                };
            }
        ));
    }, [currentPage]);


    return (
        <ReportWrapper title="Average age for procedure" headersToPrint={headers} itemsToPrint={allItems}>
          <PaginationList
                    choosePage={choosePage}
                    headers={headers}
                    pageCount={pageCount}
                    items={currentItems}
                />
        </ReportWrapper>
    );
};

export default AverageAgeForProcedure;