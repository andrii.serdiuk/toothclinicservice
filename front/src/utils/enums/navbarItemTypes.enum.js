export const NavbarItemTypeEnum = Object.freeze({
  REGULAR: 'REGULAR',
  DROPDOWN: 'DROPDOWN'
});
