export default Object.freeze({
  DIRECTOR: 'DIRECTOR',
  REGISTRANT: 'REGISTRANT',
  DOCTOR: 'DOCTOR',
  PATIENT: 'PATIENT'
});
