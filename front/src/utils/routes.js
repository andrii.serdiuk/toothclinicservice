import {
    ADD_DOCTOR_ROUTE,
    ALL_DOCTORS_ROUTE,
    DOCTOR_ROUTE,
    HOME_ROUTE,
    NOT_FOUND_ROUTE,
    MY_PROFILE_ROUTE,
    LOGIN_ROUTE,
    REPORTS_ROUTE,
    WORKAHOLICS_REPORT_ROUTE,
    PROCEDURES_REPORT_ROUTE,
    EQUIPMENT_REPORT_ROUTE,
    STAFF_ROUTE,
    STAFF_DOCTORS_ROUTE,
    STAFF_RECEPTIONISTS_ROUTE,
    ALL_EQUIPMENT_ROUTE,
    MY_SCHEDULE_ROUTE,
    MY_APPOINTMENTS_ROUTE,
    MY_PATIENTS_ROUTE,
    ALL_PATIENTS_ROUTE,
    ADD_PATIENT_ROUTE,
    PATIENT_ROUTE,
    DOCTOR_BENEFIT_ROUTE,
    DOCTOR_PATIENTS_AMOUNT_ROUTE,
    IMPORTANT_PROCEDURES_ROUTE,
    EDIT_DOCTOR_ROUTE,
    PROCEDURES_WITH_ALL_DEVICES,
    EQUIPMENT_POPULARITY_ROUTE,
    AVERAGE_AGE_FOR_PROCEDURES,
    PATIENTS_FINISHED_APPOINTMENTS,
    EQUIPMENT_DEVICES,
    DEVICE_ROUTE,
    DEVICE_EDIT_ROUTE,
    APPOINTMENTS_ROUTE,
    APPOINTMENT_ROUTE,
    EDIT_PATIENT_ROUTE,
    PROCEDURES_ROUTE,
    PROCEDURE_ROUTE,
    APPOINTMENT_EDIT_ROUTE,
    ADD_PROCEDURE_ROUTE,
    STAFF_RECEPTIONIST_ROUTE
} from "./routesNames";
import {AllDoctorsPage, DoctorPage, AddDoctorPage} from '../pages/doctor';
import {
    NotFoundPage, MyProfilePage, HomePage, LoginPage
    , AllRegistrantsPage, MySchedulePage, ProceduresPage
} from "../pages";
import {UserRolesEnum} from "./enums";
import SaveLocationNotFound from '../components/SaveLocationNotFound';
import {MainLayout} from "../components/layout";
import AuthWrapper from "../components/base/AuthWrapper";
import ReportsPage from "../pages/reports/ReportsPage";
import {
    AverageAgeForProcedure,
    DoctorBenefitReportPage, DoctorUniquePatientsReportPage,
    EquipmentReportPage, ImportantProceduresReportPage,
    ProceduresReportPage,
    WorkaholicsReportPage
} from "../pages/reports";
import {Outlet} from "react-router-dom";
import {AllEquipmentPage, SpecificEquipmentPage} from "../pages/equipment";
import {MyPatientsPage} from "../pages/patient";
import PatientPage from "../pages/patient/PatientPage";
import AllPatientsPage from "../pages/patient/AllPatientsPage";
import AddPatientPage from "../pages/patient/AddPatientPage";
import EditDoctorPage from "../pages/doctor/EditDoctorPage";
import PopularityOfEquipment from "../pages/reports/PopularityOfEquipment";
import ProceduresWithAllDevices from "../pages/reports/ProceduresWithAllDevices";
import PatientFinishedAppointmentByPeriodReportPage
    from "../pages/reports/PatientFinishedAppointmentByPeriodReportPage";
import {DeviceEditPage, DevicePage} from "../pages/device";
import {AppointmentPage, EditAppointmentPage, MyAppointmentsPage} from "../pages/appointment";
import EditPatientPage from "../pages/patient/EditPatientPage";
import ProcedurePage from "../pages/procedure/ProcedurePage";
import {AddProcedurePage} from "../pages/procedure";
import {AllReceptionistsPage} from "../pages/receptionst";
import AllAppointmentsPage from "../pages/appointment/AllAppointmentsPage";

export const routes = [
    {
        path: HOME_ROUTE,
        element: <MainLayout/>,
        children: [
            {
                path: "",
                element: <HomePage/>,
            },
            {
                path: ALL_DOCTORS_ROUTE,
                element:
                    <AuthWrapper
                        roles={[UserRolesEnum.DOCTOR, UserRolesEnum.PATIENT, UserRolesEnum.DIRECTOR, UserRolesEnum.REGISTRANT]}
                    >
                        <AllDoctorsPage/>
                    </AuthWrapper>,
            },
            {
                path: DOCTOR_ROUTE,
                element:
                    <AuthWrapper
                        roles={[UserRolesEnum.DOCTOR, UserRolesEnum.PATIENT, UserRolesEnum.DIRECTOR, UserRolesEnum.REGISTRANT]}>
                        <DoctorPage/>
                    </AuthWrapper>
            },
            {
                path: ADD_DOCTOR_ROUTE,
                element:
                    <AuthWrapper
                        roles={[UserRolesEnum.DIRECTOR]}
                    >
                        <AddDoctorPage/>
                    </AuthWrapper>
            },
            {
                path: EDIT_DOCTOR_ROUTE,
                element:
                    <AuthWrapper
                        roles={[UserRolesEnum.DIRECTOR]}
                    >
                        <EditDoctorPage/>
                    </AuthWrapper>
            },
            {
                path: EDIT_PATIENT_ROUTE,
                element:
                    <AuthWrapper
                        roles={[UserRolesEnum.DIRECTOR]}
                    >
                        <EditPatientPage/>
                    </AuthWrapper>
            },
            {
                path: ALL_PATIENTS_ROUTE,
                element:
                    <AuthWrapper
                        roles={[UserRolesEnum.DOCTOR, UserRolesEnum.DIRECTOR, UserRolesEnum.REGISTRANT]}>
                        <AllPatientsPage/>
                    </AuthWrapper>,
            },
            {
                path: ADD_PATIENT_ROUTE,
                element:
                    <AuthWrapper roles={[UserRolesEnum.DIRECTOR]}>
                        <AddPatientPage/>
                    </AuthWrapper>
            },
            {
                path: PATIENT_ROUTE,
                element:
                    <AuthWrapper
                        roles={[UserRolesEnum.DOCTOR, UserRolesEnum.DIRECTOR, UserRolesEnum.REGISTRANT]}>
                        <PatientPage/>
                    </AuthWrapper>
            },
            {
                path: MY_PROFILE_ROUTE,
                element:
                    <AuthWrapper
                        roles={[UserRolesEnum.DOCTOR, UserRolesEnum.PATIENT, UserRolesEnum.DIRECTOR, UserRolesEnum.REGISTRANT]}>
                        <MyProfilePage/>
                    </AuthWrapper>
            },
            {
                path: STAFF_ROUTE,
                element:
                    <AuthWrapper
                        roles={[UserRolesEnum.DIRECTOR]}>
                        <Outlet/>
                    </AuthWrapper>,
                children: [
                    {
                        path: STAFF_DOCTORS_ROUTE,
                        element: <AllDoctorsPage/>
                    },
                    {
                        path: STAFF_RECEPTIONISTS_ROUTE,
                        element: <AllReceptionistsPage/>
                    }
                ]
            },
            {
                path: APPOINTMENTS_ROUTE,
                element:
                    <AuthWrapper
                        roles={[UserRolesEnum.DIRECTOR, UserRolesEnum.REGISTRANT]}>
                        <AllAppointmentsPage/>
                    </AuthWrapper>,
            },
            {
                path: APPOINTMENT_ROUTE,
                element:
                    <AuthWrapper
                        roles={[UserRolesEnum.DIRECTOR, UserRolesEnum.REGISTRANT, UserRolesEnum.PATIENT, UserRolesEnum.DOCTOR]}>
                        <AppointmentPage/>
                    </AuthWrapper>,
            },
            {
                path: APPOINTMENT_EDIT_ROUTE,
                element:
                    <AuthWrapper
                        roles={[UserRolesEnum.DIRECTOR, UserRolesEnum.REGISTRANT, UserRolesEnum.PATIENT, UserRolesEnum.DOCTOR]}>
                        <EditAppointmentPage/>
                    </AuthWrapper>,
            },
            {
                path: REPORTS_ROUTE,
                element:
                    <AuthWrapper
                        roles={[UserRolesEnum.DIRECTOR]}>
                        <ReportsPage/>
                    </AuthWrapper>,
                children: [
                    {
                        path: WORKAHOLICS_REPORT_ROUTE,
                        element: <WorkaholicsReportPage/>
                    },
                    {
                        path: PROCEDURES_REPORT_ROUTE,
                        element: <ProceduresReportPage/>
                    },
                    {
                        path: EQUIPMENT_REPORT_ROUTE,
                        element: <EquipmentReportPage/>
                    },
                    {
                        path: DOCTOR_BENEFIT_ROUTE,
                        element: <DoctorBenefitReportPage/>
                    },
                    {
                        path: DOCTOR_PATIENTS_AMOUNT_ROUTE,
                        element: <DoctorUniquePatientsReportPage/>
                    },
                    {
                        path: IMPORTANT_PROCEDURES_ROUTE,
                        element: <ImportantProceduresReportPage/>
                    },
                    {
                        path: AVERAGE_AGE_FOR_PROCEDURES,
                        element: <AverageAgeForProcedure/>
                    },
                    {
                        path: EQUIPMENT_POPULARITY_ROUTE,
                        element: <PopularityOfEquipment/>
                    },
                    {
                        path: PROCEDURES_WITH_ALL_DEVICES,
                        element: <ProceduresWithAllDevices/>
                    },
                    {
                        path: PATIENTS_FINISHED_APPOINTMENTS,
                        element: <PatientFinishedAppointmentByPeriodReportPage/>
                    }
                ]
            },
            {
                path: ALL_EQUIPMENT_ROUTE,
                element:
                    <AuthWrapper
                        roles={[UserRolesEnum.DIRECTOR, UserRolesEnum.DOCTOR]}>
                        <AllEquipmentPage/>
                    </AuthWrapper>
            },
            {
                path: EQUIPMENT_DEVICES,
                element:
                    <AuthWrapper
                        roles={[UserRolesEnum.DIRECTOR, UserRolesEnum.DOCTOR]}>
                        <SpecificEquipmentPage/>
                    </AuthWrapper>
            },
            {
                path: DEVICE_ROUTE,
                element:
                    <AuthWrapper
                        roles={[UserRolesEnum.DIRECTOR, UserRolesEnum.DOCTOR]}>
                        <DevicePage/>
                    </AuthWrapper>
            },
            {
                path: DEVICE_EDIT_ROUTE,
                element:
                    <AuthWrapper
                        roles={[UserRolesEnum.DIRECTOR]}>
                        <DeviceEditPage/>
                    </AuthWrapper>
            },
            {
                path: MY_SCHEDULE_ROUTE,
                element:
                    <AuthWrapper
                        roles={[UserRolesEnum.DOCTOR]}>
                        <MySchedulePage/>
                    </AuthWrapper>
            },
            {
                path: MY_APPOINTMENTS_ROUTE,
                element:
                    <AuthWrapper
                        roles={[UserRolesEnum.DOCTOR, UserRolesEnum.PATIENT]}>
                        <MyAppointmentsPage/>
                    </AuthWrapper>
            },
            {
                path: MY_PATIENTS_ROUTE,
                element:
                    <AuthWrapper
                        roles={[UserRolesEnum.DOCTOR]}>
                        <MyPatientsPage/>
                    </AuthWrapper>
            },
            {
                path: PROCEDURES_ROUTE,
                element:
                    <AuthWrapper
                        roles={[UserRolesEnum.DOCTOR, UserRolesEnum.DIRECTOR, UserRolesEnum.PATIENT, UserRolesEnum.REGISTRANT]}>
                        <ProceduresPage/>
                    </AuthWrapper>
            },
            {
                path: PROCEDURE_ROUTE,
                element:
                    <AuthWrapper
                        roles={[UserRolesEnum.DOCTOR, UserRolesEnum.DIRECTOR, UserRolesEnum.PATIENT, UserRolesEnum.REGISTRANT]}>
                        <ProcedurePage/>
                    </AuthWrapper>
            },
            {
                path: ADD_PROCEDURE_ROUTE,
                element:
                    <AuthWrapper
                        roles={[UserRolesEnum.DOCTOR, UserRolesEnum.DIRECTOR, UserRolesEnum.PATIENT, UserRolesEnum.REGISTRANT]}>
                        <AddProcedurePage/>
                    </AuthWrapper>
            }
        ]
    },
    {
        path: LOGIN_ROUTE,
        element: <LoginPage/>
    },
    {
        path: NOT_FOUND_ROUTE,
        element: <NotFoundPage/>,
    },
    {
        path: '*',
        element: <SaveLocationNotFound/>
    }
];
