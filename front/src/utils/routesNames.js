export const HOME_ROUTE = '/';
export const NOT_FOUND_ROUTE = '/oops';

//Doctor
export const ALL_DOCTORS_ROUTE ='/doctors';
export const DOCTOR_ROUTE = `${ALL_DOCTORS_ROUTE}/:doctorId`;
export const ADD_DOCTOR_ROUTE = `${ALL_DOCTORS_ROUTE}/add`;
export const EDIT_DOCTOR_ROUTE = `${ALL_DOCTORS_ROUTE}/:doctorId/edit`;

//Patient
export const ALL_PATIENTS_ROUTE ='/patients';
export const PATIENT_ROUTE = `${ALL_PATIENTS_ROUTE}/:patientId`;
export const EDIT_PATIENT_ROUTE = `${ALL_PATIENTS_ROUTE}/:patientId/edit`;
export const ADD_PATIENT_ROUTE = `${ALL_PATIENTS_ROUTE}/add`;
export const MY_PROFILE_ROUTE = '/me';
export const MY_APPOINTMENTS_ROUTE = '/my-appointments';
export const MY_PATIENTS_ROUTE = '/my-patients';

export const MY_SCHEDULE_ROUTE = '/my-schedule';
export const PROCEDURES_ROUTE = '/procedures';
export const PROCEDURE_ROUTE = `/${PROCEDURES_ROUTE}/:procedureId`;
export const ADD_PROCEDURE_ROUTE = `/${PROCEDURES_ROUTE}/add`;
export const APPOINTMENTS_ROUTE = '/appointments';
export const APPOINTMENT_ROUTE = `${APPOINTMENTS_ROUTE}/:appointmentId`;
export const APPOINTMENT_EDIT_ROUTE = `${APPOINTMENTS_ROUTE}/:appointmentId/edit`;
export const EQUIPMENT_ROUTE = '/equipment';
export const DEVICE_ROUTE = '/device/:inventoryNumber';
export const DEVICE_EDIT_ROUTE = '/device/:inventoryNumber/edit';
export const STAFF_ROUTE = '';
export const STAFF_DOCTORS_ROUTE = `${STAFF_ROUTE}/doctors`;
export const STAFF_RECEPTIONISTS_ROUTE = `${STAFF_ROUTE}/receptionists`;
export const ALL_EQUIPMENT_ROUTE = '/equipment';
export const EQUIPMENT_DEVICES = '/equipment/:equipmentId/devices';

//Reports
export const REPORTS_ROUTE = '/reports';
export const WORKAHOLICS_REPORT_ROUTE = `${REPORTS_ROUTE}/workaholics`;
export const PROCEDURES_REPORT_ROUTE = `${REPORTS_ROUTE}/procedures`;
export const EQUIPMENT_REPORT_ROUTE = `${REPORTS_ROUTE}/equipment`;
export const DOCTOR_BENEFIT_ROUTE = `${REPORTS_ROUTE}/doctorBenefit`;
export const DOCTOR_PATIENTS_AMOUNT_ROUTE = `${REPORTS_ROUTE}/doctorPatientsAmount`;
export const IMPORTANT_PROCEDURES_ROUTE = `${REPORTS_ROUTE}/importantProcedures`;
export const AVERAGE_AGE_FOR_PROCEDURES = `${REPORTS_ROUTE}/averageProceduresAge`;
export const EQUIPMENT_POPULARITY_ROUTE = `${REPORTS_ROUTE}/equipmentPopularity`;
export const PROCEDURES_WITH_ALL_DEVICES = `${REPORTS_ROUTE}/proceduresWithAllDevices`;
export const PATIENTS_FINISHED_APPOINTMENTS = `${REPORTS_ROUTE}/patientsFinishedAppointments`;

export const LOGIN_ROUTE = '/login';
