export const getCurrentDate = () => {
    const date = new Date();
    let day = date.getDate();
    day = day < 10 ? '0' + day : day;
    //Here month starts from 0
    let month = date.getMonth() + 1;
    month = month < 10 ? '0' + month : month;
    let year = date.getFullYear();
    year = year < 10 ? '0' + year : year;
    return [year, month, day].join('-');
}