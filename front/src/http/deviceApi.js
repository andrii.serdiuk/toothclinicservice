import {$authHost} from "./index";

export const getDevice = async (id) => {
    return await $authHost.get(`/api/device/${id}`);
};

export const addDevice = async (device) => {
    return await $authHost.post('/api/device', device);
};

export const updateDevice = async (device) => {
    return await $authHost.put('/api/device', device);
}

export const deleteDevice = async (deviceId) => {
    return await $authHost.delete(`/api/device/${deviceId}`);
}