import {customLocalStorage} from "../utils/localStorage";
import {$authHost, $host} from "./index";
import jwt_decode from 'jwt-decode';

export const loginPost = async (login, password) => {
    const {data: {token}} = await $host.post('/api/user/login', {
        login,
        password
    });
    customLocalStorage.setAuthToken(token);
    return jwt_decode(token);
};

export const getMyPatients = async (limit, page) => {
    const {data} = await $authHost.get(`/api/user/my-patients`, {params: {limit: limit, page: page}});
    return data;
}

export const getMyAppointments = async (limit, page) => {
    const {data} = await $authHost.get(`/api/user/my-appointments`, {params: {limit: limit, page: page}});
    return data;
}

export const getAllAppointments = async (limit, page, doctorId, patientId, dateFrom, dateTo, status) => {
    const {data} = await $authHost.get(`/api/appointment`, {
        params:
            {
                limit: limit,
                page: page,
                doctorId: doctorId,
                patientId: patientId,
                dateFrom: dateFrom,
                dateTo: dateTo,
                appointmentStatus: status
            }
    });
    return data;
}

export const getAccountInformation = async (accountId) => {
  return await $authHost.get(`/api/user/${accountId}`);
}