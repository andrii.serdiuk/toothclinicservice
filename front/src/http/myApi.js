import { $authHost } from ".";

export const createMyAppointment = async (doctorId, date, beginTime, endTime) => {
  const { data } = await $authHost.post(`/api/my-appointments`, {
    doctorId, date, beginTime: beginTime.toTimeString().substring(0, 5), endTime: endTime.toTimeString().substring(0, 5)
  });
  return data;
}
