import {$authHost} from "./index";

export const getReceptionists = async (limit, page) => {
    const {data} = await $authHost.get('/api/receptionist', {params: {limit: limit, page: page}});
    return data;
};

export const addReceptionist = async (receptionist) => {
    const {data} = await $authHost.post('/api/receptionist', receptionist);
    return data;
};

export const updateReceptionist = async (id, receptionist) => {
    const {data} = await $authHost.put('/api/receptionist/' + id, receptionist);
    return data;
};

export const deleteReceptionist = async (id, receptionist) => {
    const {data} = await $authHost.delete('/api/receptionist/' + id, receptionist);
    return data;
};