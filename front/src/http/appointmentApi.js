import {$authHost} from "./index";

export const getAppointment = async (id) => {
  return await $authHost.get(`/api/appointment/${id}`);
};

export const getPerformedProcedureData = async (performedProcedureId) => {
  return await $authHost.get(`api/appointment/performedProcedure/${performedProcedureId}`);
};

export const removePerformedProcedureById = async (performedProcedureId) => {
  return await $authHost.delete(`api/appointment/performedProcedure/delete/${performedProcedureId}`);
};

export const getAllProceduresForTheSpecifiedCategory = async (doctorCategory) => {
  return await $authHost.get(`api/appointment/procedures/${doctorCategory}`);
};

export const getProcedureData = async (procedureId) => {
  return await $authHost.get(`api/appointment/procedure/${procedureId}`);
};

export const createPerformedProcedure = async (data) => {
  return await $authHost.post('api/appointment/createPerformedProcedure', data);
};

export const createAppointment = async (doctorId, patientCardId, date, beginTime, endTime) => {
  const { data } = await $authHost.post(`/api/appointment`, {
    doctorId, patientCardId, date, beginTime: beginTime.toTimeString().substring(0, 5), endTime: endTime.toTimeString().substring(0, 5)
  });
  return data;
};

export const createFinishedAppointment = async (scheduledAppointmentId, data) => {
    return await $authHost.post('/api/appointment/createFinishedAppointment',
        {scheduledAppointmentId: scheduledAppointmentId, data: data});
};

export const getEquipmentAndAllDevicesForProcedure = async (procedureId) => {
  return await $authHost.get(`api/appointment/equipmentAndDevicesForProcedure/${procedureId}`);
};

export const updatePerformedProcedure = async (performedProcedureId, data) => {
  return await $authHost.put(`api/appointment/${performedProcedureId}`, data);
};

export const updateAppointmentInfo = async (appointmentId, data) => {
  return await $authHost.put(`api/appointment/info/${appointmentId}`, data);
}

export const cancelAppointment = async (appointmentId) => {
  return await $authHost.put(`api/appointment/cancel/${appointmentId}`);
}