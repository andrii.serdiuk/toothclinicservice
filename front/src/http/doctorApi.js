import {$authHost} from "./index";

export const add = async (doctor) => {
    return await $authHost.post('/api/doctor', doctor);
};

export const getDoctors = async (dateFrom, dateTo, timeFrom, timeTo, limit, page) => {
    return await $authHost.get('/api/doctor', {params: {dateFrom, dateTo, timeFrom, timeTo, limit: limit, page: page}});
};

export const getAllDoctors = async () => {
    const {data} = await $authHost.get('/api/doctor/all');
    return data;
};

export const getDoctor = async (id) => {
    const {data} = await $authHost.get(`/api/doctor/${id}`);
    return data;
}

export const getDoctorByAccountId = async (id) => {
    return await $authHost.get(`/api/doctor/account/${id}`);
}

export const getDoctorPatientsByPage = async (id, limit, page) =>{
    const {data} = await $authHost.get(`/api/doctor/${id}/patients`, {params: {limit: limit, page: page}});
    return data;
}

export const createWorkingPeriod = async (doctorId, date, startTime, endTime) => {
    return await $authHost.post('/api/doctor/createWorkingPeriod',
        {params: {doctorId: doctorId, date: date, startTime: startTime, endTime: endTime}});
}

export const updateWorkingPeriod = async (scheduleId, workingHoursId, startTime, endTime) => {
    return await $authHost.put('/api/doctor/workingPeriod',
        {params: {scheduleId: scheduleId, workingHoursId: workingHoursId,
                 beginTime: startTime, endTime: endTime}});
}

export const deleteWorkingPeriod = async (scheduleId, workingHoursId) => {
    return await $authHost.delete(`/api/doctor/workingPeriod/${workingHoursId}/${scheduleId}`);
}

export const editDoctor = async (id, doctor) =>{
    return (await $authHost.put(`/api/doctor/${id}`, doctor)).data;
}

export const getFullSchedule = async (doctorId, dateFrom, dateTo) => {
  const { data } = await $authHost.get(`/api/doctor/${doctorId}/schedule`, {
    params: {dateFrom, dateTo}
  });
  return data;
}

export const deleteDoctor = async (id) => {
    return await $authHost.delete(`/api/doctor/${id}`);
}

export const deleteDoctorAccount = async (id) => {
    return await $authHost.delete(`/api/doctor/${id}/account`);
}

export const updateDoctorAccount = async (doctorId, doctorAccount) => {
    return await $authHost.put(`/api/doctor/${doctorId}/account/`, doctorAccount);
}
export const addDoctorAccount = async (doctorId, doctorAccount) => {
    return await $authHost.post(`/api/doctor/${doctorId}/account/`, doctorAccount);
}