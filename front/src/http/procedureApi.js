import {$authHost} from "./index";

export const getProcedures = async (limit, page) => {
    return $authHost.get('/api/procedure', {params: {limit: limit, page: page}});
}

export const addProcedure = async (procedure) => {
    return $authHost.post('/api/procedure', procedure);
}

export const getProcedure = async (id) => {
    return $authHost.get(`/api/procedure/${id}`);
}

export const getEquipmentForProcedure = async (id) => {
    return $authHost.get(`/api/procedure/${id}/equipment`);
}

export const updateProcedure = async (id, procedure) => {
    return $authHost.put(`/api/procedure/${id}`, procedure);
}

export const deleteProcedure = async (id) => {
    return $authHost.delete(`/api/procedure/${id}`);
}