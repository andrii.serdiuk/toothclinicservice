import {$authHost} from "./index";

export const addEquipment = async (equipment) => {
    return await $authHost.post('/api/equipment', equipment);
};

export const getSpecificEquipmentDeviceList = async (id, limit, page) => {
    return await $authHost.get(`/api/equipment/${id}/devices`,
        {params: {limit: limit, page: page}});
};

export const getEquipmentPage = async (itemsOnPage, currentPage) => {
    return await $authHost.get('/api/equipment', {params: {limit: itemsOnPage, page: currentPage}});
};


export const getEquipmentDevices = async (equipmentId) => {
    return await $authHost.get(`/api/equipment/${equipmentId}/allDevices`);
};

export const getAllEquipment = async() => {
    return await $authHost.get('/api/equipment/all');
}

export const updateEquipment = async(equipmentId, equipmentName) => {
    return await $authHost.put(`/api/equipment/${equipmentId}`, {params: equipmentName});
}

export const deleteEquipment = async(equipmentId) => {
    return await $authHost.delete(`/api/equipment/${equipmentId}`);
}