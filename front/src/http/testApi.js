import {$host} from './index';

export const testApi = async () => {
    const {data} = await $host.get('api/test');
    return data;
}
