import {$authHost} from "./index";

export const addPatient = async (doctor) => {
    return await $authHost.post('/api/patient', doctor);
};

export const getPatients = async (limit, page) => {
    return await $authHost.get('/api/patient', {params: {limit: limit, page: page}});
};

export const getPatient = async (id) => {
    return $authHost.get(`/api/patient/${id}`);
}

export const getPatientByAccountId = async (id) => {
    return await $authHost.get(`/api/patient/account/${id}`);
}

export const getAllPatients = async () => {
    return await $authHost.get('/api/patient/all');
}

export const editPatient = async (id, doctor) =>{
    return (await $authHost.put(`/api/patient/${id}`, doctor)).data;
}

export const addPatientAccount = async (patientId, patientAccount) => {
    return await $authHost.post(`/api/patient/${patientId}/account/`, patientAccount);
}

export const deletePatient = async (id) => {
    return await $authHost.delete(`/api/patient/${id}`);
}

export const deletePatientAccount = async (id) => {
    return await $authHost.delete(`/api/patient/${id}/account`);
}

export const updatePatientAccount = async (patientId, patientAccount) => {
    return await $authHost.put(`/api/patient/${patientId}/account/`, patientAccount);
}

