import { $authHost } from "."

export const workaholics = async (dateFrom, dateTo, page, limit) => {
  const { data } = await $authHost.get('/api/report/workaholics', {
    params: { page, limit, dateFrom, dateTo },
  });
  return data;
};

export const proceduresReport = async (page, limit) => {
  const { data } = await $authHost.get('/api/report/procedures', {
    params: { page, limit }
  });
  return data;
};

export const equipmentReport = async (page, limit) => {
  const { data } = await $authHost.get('/api/report/equipment', {
    params: { page, limit }
  });
  return data;
};

export const doctorScheduleNextWeek = async (id, itemsOnPage, pageCount) => {
  const { data } = await $authHost.get('/api/report/doctorSchedule', {
    params: {id, itemsOnPage, pageCount}
  });
  return data;
};

export const doctorBenefitReport = async (page, limit) => {
  const { data } = await $authHost.get('/api/report/doctorBenefit', {
    params: {page, limit}
  });
  return data;
}

export const doctorUniquePatientsAmount = async (dateFrom, dateTo, page, limit) => {
  const { data } = await $authHost.get('/api/report/doctorPatientsAmount', {
    params: {dateFrom, dateTo, page, limit}
  });
  return data;
}

export const importantProcedure = async (page, limit) => {
  const { data } = await $authHost.get('/api/report/importantProcedures', {
    params: {page, limit}
  });
  return data;
}

export const equipmentPopularityReport = async (limit, page) => {
  const { data } = await $authHost.get('/api/report/equipmentPopularity', {
    params: { page, limit }
  });
  return data;
};

export const averageProceduresAgeReport = async (limit, page) => {
  const { data } = await $authHost.get('/api/report/averageProceduresAge', {
    params: { page, limit }
  });
  return data;
};

export const proceduresWithAllDevices = async (limit, page) => {
  const { data } = await $authHost.get('/api/report/proceduresWithAllDevices', {
    params: { page, limit }
  });
  return data;
};

export const getPatientFinishedAppointments = async(id, limit, page) => {
  const {data} = await $authHost.get(`/api/report/patientFinishedAppointment/${id}`, {
    params: { page, limit }
  });
  return data;
}